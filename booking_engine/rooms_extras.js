$(document).ready(function() {

    var fullAppRoot = $("div.engine_form").data("app");

    /** 
     * ROOMS DISPLAY 
     * EXTRAS DISPLAY 
     * VALIDATION
     * CALCULATION OF AVERAGE ROOMRATES AND RATES OF EXTRAS  
     **/

    /** WIDGET */

    var widgetDates = $("#available_rooms").data("widget");
    var getCheckIndate = $("#available_rooms").data("wcheckin");
    var getCheckOutDate = $("#available_rooms").data("wcheckout");
    var date = new Date();
    var year = date.getFullYear();
    var month = parseInt((date.getMonth() + 1)).toString().length < 2 ? "0" + parseInt((date.getMonth()) + 1) : (date.getMonth()) + 1;
    var day = (date.getDate()).toString().length < 2 ? "0" + date.getDate() : date.getDate();
    var today = year + "-" + month + "-" + day;

    if (widgetDates == true) {

        if (getCheckIndate !== "" && getCheckOutDate !== "") {

            $("#check_in_date").flatpickr({ // Check-in Date Default
                minDate: "today",
                defaultDate: getCheckIndate,
                disableMobile: true,
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",

                onChange: function(checkin) {

                    $("#check_out_date").flatpickr({ // Check-out Date on the onChange event of Check-in Date
                        minDate: new Date(checkin).fp_incr(1),
                        defaultDate: new Date(checkin).fp_incr(1),
                        disableMobile: true,
                        altInput: true,
                        altFormat: "F j, Y",
                        dateFormat: "Y-m-d",

                        onClose: function() {
                            $("input:focus").blur();
                        }
                    });
                },

                onClose: function() {
                    $("input:focus").blur();
                }
            });

            $("#check_out_date").flatpickr({ // Check-in Date Default
                minDate: "today",
                defaultDate: getCheckOutDate,
                disableMobile: true,
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",

                onClose: function() {
                    $("input:focus").blur();
                }
            });

            if (getCheckOutDate === today) {
                $("#error_valid_dates").show();
                return;
            } else {
                displayAvailableRooms(getCheckIndate, getCheckOutDate);

                getCheckIndate = "";
                getCheckOutDate = "";
            }
        } else {
            $("#error_valid_dates").show();
            $("#error_no_rooms").hide();
            $("#error_no_selection").hide();
            $("#error_minimum_stay").hide();
            $("#error_blackout").hide();
            $("#error_validation").hide();
        }
    }

    /** FRONTEND FORM */

    var clickCount = 0;

    $("#check_availability_button").click(function() { // Getting rooms through frontend form
        $(this).attr("disabled", true);
        $("div.alert_msg").hide();
        clickCount++;
        getCheckInDate = $("#check_in_date").val();
        getCheckOutDate = $("#check_out_date").val();

        displayAvailableRooms(getCheckInDate, getCheckOutDate);
    });

    $("#check_out_date").change(function() {
        $("div.alert_msg").hide();
        if (clickCount >= 1) {
            $("#check_availability_button").attr("disabled", true);
            getCheckIndate = $("#check_in_date").val();
            getCheckOutDate = $(this).val();

            displayAvailableRooms(getCheckIndate, getCheckOutDate);
        }
    });

    $("#check_in_date").change(function() {
        $("div.alert_msg").hide();
        if (clickCount >= 1) {
            $("#check_availability_button").attr("disabled", true);
            getCheckIndate = $(this).val();
            getCheckOutDate = $("#check_out_date").val();

            displayAvailableRooms(getCheckIndate, getCheckOutDate);
        }
    });

    /** MAIN FUNCTION */

    function displayAvailableRooms(checkInDate, checkOutDate) {

        $(".contact").removeClass("error");
        $("label.contact").removeClass("w3-text-red");

        var roomSelectedLang = "";
        $("#lang_select").length > 0 ? roomSelectedLang = $("#lang_select").val() : roomSelectedLang = '1';

        if (checkInDate === "" || checkOutDate === "") { // Validation of CheckIn & CheckOut Dates fields 
            $("#error_valid_dates").show();
            $("#error_no_rooms").hide();
            $("#error_no_selection").hide();
            $("#error_minimum_stay").hide();
            $("#error_blackout").hide();
            $("#error_validation").hide();
        } else {
            $("#error_valid_dates").hide();

            // Room Display and selection of quantities             

            $(".room_div").remove(); // Removing of selected rooms           

            $.ajax({
                url: fullAppRoot + "engine/form_ajax.php",
                type: "POST",
                data: { method: "getRooms", checkInDate: checkInDate, checkOutDate: checkOutDate, lang: roomSelectedLang },
                dataType: "html",
                cache: false,
                success: function(acc_response) {
                    $(".filler").hide();
                    $("#section_2").show();
                    $("#error_minimum_stay").hide();
                    $("#error_blackout").hide();
                    $("#check_availability_button").attr("disabled", false);
                    var hiddenRoomIds = [];
                    var hiddenRooms = sessionStorage.getItem("hidden_rooms");
                    var extras, extrasRoomKeys;

                    if (hiddenRooms != null) {
                        var hiddenRoomIds = hiddenRooms.split(",");
                    }

                    var finalResult = JSON.parse(acc_response);

                    var roomsResult = finalResult.rooms; // Getting room data
                    var keys = Object.keys(roomsResult);

                    if (typeof(finalResult.extras) != "undefined") {
                        extras = finalResult.extras; // Getting extras data
                        extrasRoomKeys = Object.keys(extras);
                    } else {
                        extrasRoomKeys = "";
                    }

                    var anyRoom = [];
                    var selectedRoom = [];
                    var minStayCheck = [];

                    finalResult.discountPanelHide.indexOf("1") > -1 ? $(".discount_panel").hide() : $(".discount_panel").show();
                    finalResult.blackout.indexOf("1") > -1 ? $("#error_blackout").show() : $("#error_blackout").hide();

                    // Add W3 references to parent
                    // $(window.parent.document.head).append('<link rel="stylesheet" type="text/css" href="' + fullAppRoot + 'lib/W3/w3.css">');
                    $(window.parent.document.head).append('<link rel="stylesheet" type="text/css" href="' + fullAppRoot + 'engine/css/modals.css">');

                    for (var i = 0; i < keys.length; i++) {

                        if (hiddenRoomIds.indexOf(roomsResult[keys[i]].id) == -1) {
                            availableRoomNumber = roomsResult[keys[i]].available_left; // NUMBER OF AVAILABLE ROOMS

                            if (availableRoomNumber > 0) {
                                anyRoom.push(roomsResult[keys[i]].id);
                            }

                            var minimumStayRule = roomsResult[keys[i]].minimum_nights;
                            minStayCheck.push(minimumStayRule);

                            for (var m = 0; m < minStayCheck.length; m++) {
                                if (minStayCheck[m] === "1") {
                                    $("#error_minimum_stay").show();
                                }
                            }

                            if (anyRoom.length > 0) { // WE HAVE AVAILABLE ROOMS
                                $("#error_no_rooms").hide();

                                if (minStayCheck.indexOf("0") > -1) {
                                    $(".filler").hide();
                                    $("#section_2").show();
                                } else {
                                    $("#section_2").hide();
                                    $(".filler").show();
                                }

                                if (roomsResult[keys[i]].active === "1") {

                                    if (availableRoomNumber > 0) {

                                        if (minimumStayRule === "0") {

                                            // Calculation of length of booking (number of days)
                                            var date1 = new Date(checkInDate);
                                            var date2 = new Date(checkOutDate);
                                            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                            var imgSrc;
                                            var imgSrcTop;

                                            // Tooltip image of rooms
                                            roomsResult[keys[i]].image.length > 0 ? imgSrc = fullAppRoot + "img/uploads/" + roomsResult[keys[i]].image : imgSrc = fullAppRoot + "img/room.png";
                                            roomsResult[keys[i]].image.length > 0 ? imgSrcTop = fullAppRoot + "img/uploads/" + roomsResult[keys[i]].image : imgSrcTop = fullAppRoot + "img/room.png";

                                            // Room description
                                            var roomNameSub = "";

                                            if (roomsResult[keys[i]].description_short.length > 40) {
                                                if (roomSelectedLang === "1") {
                                                    roomNameSub = roomsResult[keys[i]].description_short.substring(0, 40) + '...';
                                                } else {
                                                    roomNameSub = roomsResult[keys[i]].description_short_sec_lang.substring(0, 40) + '...';
                                                }
                                            } else {
                                                if (roomSelectedLang === "1") {
                                                    roomNameSub = roomsResult[keys[i]].description_short;
                                                } else {
                                                    roomNameSub = roomsResult[keys[i]].description_short_sec_lang;
                                                }
                                            }

                                            // Room params
                                            roomSelectedLang === "1" ? roomName = roomsResult[keys[i]].description_short : roomName = roomsResult[keys[i]].description_short_sec_lang;
                                            roomSelectedLang === "1" ? roomDescToolTip = roomsResult[keys[i]].description.substring(0, 100) : roomDescToolTip = roomsResult[keys[i]].description_sec_lang.substring(0, 100);
                                            roomSelectedLang === "1" ? rateToolTip = finalResult.rate_tooltip : rateToolTip = finalResult.rate_tooltip_sec_lang;
                                            roomSelectedLang === "1" ? roomDesc = roomsResult[keys[i]].description : roomDesc = roomsResult[keys[i]].description_sec_lang;
                                            roomSelectedLang === "1" ? adultsMax = finalResult.max_adults : adultsMax = finalResult.max_adults_sec_lang;
                                            roomSelectedLang === "1" ? childrenMax = finalResult.max_children : childrenMax = finalResult.max_children_sec_lang;
                                            roomSelectedLang === "1" ? occupantsMax = finalResult.max_occupants : occupantsMax = finalResult.max_occupants_sec_lang;
                                            roomSelectedLang === "1" ? quantSel = finalResult.room_quantity_selector : quantSel = finalResult.room_quantity_selector_sec_lang;
                                            roomSelectedLang === "1" ? adultsSel = finalResult.adults_selector : adultsSel = finalResult.adults_selector_sec_lang;
                                            roomSelectedLang === "1" ? childrenSel = finalResult.children_selector : childrenSel = finalResult.children_selector_sec_lang;
                                            roomSelectedLang === "1" ? extraHeading = finalResult.extra_heading : extraHeading = finalResult.extra_heading_sec_lang;
                                            roomSelectedLang === "1" ? extraIntro = finalResult.extra_intro : extraIntro = finalResult.extra_intro_sec_lang;
                                            roomSelectedLang === "1" ? avgRateTooltipText = finalResult.average_rate_tooltip : avgRateTooltipText = finalResult.average_rate_tooltip_sec_lang;

                                            if (roomsResult[keys[i]].people_max_model !== "" && roomsResult[keys[i]].people_max_model === "individual") {
                                                occupantsMaxModel = '<span data-lang="step_3_thumbnail_popup_maximum_adults" class="lang">' + adultsMax + '</span>: ' + roomsResult[keys[i]].adults_max + '<br />' +
                                                    '<span data-lang="step_3_thumbnail_popup_maximum_children" class="lang">' + childrenMax + '</span>: ' + roomsResult[keys[i]].children_max;
                                            } else if (roomsResult[keys[i]].people_max_model !== "" && roomsResult[keys[i]].people_max_model === "combined") {
                                                occupantsMaxModel = '<span data-lang="step_3_thumbnail_popup_maximum_occupants" class="lang">' + occupantsMax + '</span>: ' + roomsResult[keys[i]].people_max;
                                            } else {
                                                occupantsMaxModel = "";
                                            }

                                            $(window.parent.document.body).append('<div id="room_modal_' + roomsResult[keys[i]].id + '" class="w3-modal">' +
                                                '<div class="w3-modal-content w3-animate-zoom accommodation-modals">' +
                                                '<span onclick="document.getElementById(\'room_modal_' + roomsResult[keys[i]].id + '\').style.display=\'none\'" class="w3-button w3-display-topright">X</span>' +
                                                '<img src="' + imgSrcTop + '" alt="" class="w3-mobile mobile-image">' +
                                                '<h4 class="padding-left-right"><span data-lang="rooms_' + roomsResult[keys[i]].id + '" class="lang">' + roomName + '</span></h4>' +
                                                '<p class="padding-left-right"><span id="desc" data-lang="rooms_' + roomsResult[keys[i]].id + '" class="lang">' + roomDesc + '</span></p>' +
                                                '<p class="padding-left-right padding-bottom tooltip-footer"><span data-lang="step_3_thumbnail_popup_rate" class="lang">' + rateToolTip + '</span>' + ': ' + finalResult.currency + '<span class="running_total_sum" data-id="' + roomsResult[keys[i]].id + '">' + finalResult.rooms[i].rate + '</span><br />' +
                                                occupantsMaxModel +
                                                '</p></div></div>'
                                            );

                                            // Room & Extras Panel                                           
                                            $("#available_rooms").append('<div class="w3-row-padding w3-margin-bottom room_div">' +
                                                '<div class="w3-col l6 m6">' +
                                                '<div class="panel">' +
                                                '<a onclick="top.document.getElementById(\'room_modal_' + roomsResult[keys[i]].id + '\').style.display=\'block\'"><img align="left" class="room-thumbnail" src="' + imgSrc + '" data-id="' + roomsResult[keys[i]].id + '"></a><span id="desc_short" data-lang="rooms_' + roomsResult[keys[i]].id + '" class="lang">' + roomNameSub + '</span>' + ': <div class="tooltip" data-id="' + roomsResult[keys[i]].id + '" data-text="' + avgRateTooltipText + '" data-currency="' + finalResult.currency + '"></div><br />' +
                                                '<span id="desc_tooltip" data-lang="rooms_' + roomsResult[keys[i]].id + '" class="lang">' + roomDescToolTip + '...</span>' +
                                                '</div>' +
                                                '</div>' +
                                                '<div class="w3-col l2 m2">' +
                                                '<select class="w3-select w3-border room-quantity room-total room" name="option" data-id="' + roomsResult[keys[i]].id + '" data-quantity="' + availableRoomNumber + '" data-rate="' + parseFloat(roomsResult[keys[i]].rate) + '" data-model="' + roomsResult[keys[i]].people_max_model + '" data-adults_extra="' + roomsResult[keys[i]].adults_extra + '" data-children_extra="' + roomsResult[keys[i]].children_extra + '" data-combined_extra="' + roomsResult[keys[i]].combined_extra + '" data-combined_inc="' + roomsResult[keys[i]].people_inc_combined + '" data-people_max="' + roomsResult[keys[i]].people_max + '">' +
                                                '<option data-lang="step_3_quantity_selector" class="lang" value="rooms_quantity" selected>' + quantSel + '</option>' +
                                                '</select>' +
                                                '</div>' +
                                                '<div class="w3-col l2 m2">' +
                                                '<select class="w3-select w3-border room-occupants room-total adults" name="option" data-type="adults" data-id="' + roomsResult[keys[i]].id + '" data-occupants="' + roomsResult[keys[i]].adults_max + '" data-adults_included="' + roomsResult[keys[i]].adults_included + '" disabled>' +
                                                '<option data-lang="step_3_adults_selector" class="lang" value="rooms_adult" selected>' + adultsSel + '</option>' +
                                                '</select>' +
                                                '</div>' +
                                                '<div class="w3-col l2 m2">' +
                                                '<select class="w3-select w3-border w3-margin-bottom room-occupants room-total children" name="option" data-type="children" data-id="' + roomsResult[keys[i]].id + '" data-occupants="' + roomsResult[keys[i]].children_max + '" data-children_included="' + roomsResult[keys[i]].children_included + '" disabled>' +
                                                '<option data-lang="step_3_children_selector" class="lang" value="rooms_children" selected>' + childrenSel + '</option>' +
                                                '</select>' +
                                                '</div>' +

                                                // Extras
                                                '<div id="extras_panel">' +
                                                '<button id="extras_' + roomsResult[keys[i]].id + '"  class="w3-button w3-block w3-light-grey w3-left-align" data-roomid="' + roomsResult[keys[i]].id + '"><span data-lang="step_4_heading" class="lang">' + extraHeading + '</span><span id="extras_open_' + roomsResult[keys[i]].id + '" class="open-close"><i class="fa fa-chevron-down"></i></span></button>' +
                                                '<div id="extras_list_' + roomsResult[keys[i]].id + '" class="w3-hide w3-row-padding w3-light-grey w3-margin-bottom">' +
                                                '<p><span data-lang="step_4_intro" class="lang noExtra" data-id="' + roomsResult[keys[i]].id + '">' + extraIntro + '</span></p>');

                                            var extraKeys = "";

                                            for (var z = 0; z < extrasRoomKeys.length; z++) {
                                                if (roomsResult[keys[i]].id === extrasRoomKeys[z]) {
                                                    extraKeys = Object.keys(extras[extrasRoomKeys[z]]);
                                                }
                                            }

                                            if (extrasRoomKeys.indexOf(roomsResult[keys[i]].id) > -1) {

                                                for (var n = 0; n < extraKeys.length; n++) {

                                                    if (roomsResult[keys[i]].id === extras[roomsResult[keys[i]].id][extraKeys[n]].room_id) {

                                                        var extraRoomId = roomsResult[keys[i]].id;

                                                        var roomExtraId = extras[roomsResult[keys[i]].id][extraKeys[n]].extra_id;

                                                        var price = parseFloat(extras[roomsResult[keys[i]].id][extraKeys[n]].extra_price);
                                                        var extraImg = extras[roomsResult[keys[i]].id][extraKeys[n]].extra_image;
                                                        var extraImgSrc;
                                                        var extraImgSrcTop;
                                                        if (typeof extraImg !== 'undefined' && extraImg !== null && extraImg !== '') {
                                                            extraImgSrc = fullAppRoot + "img/uploads/" + extras[roomsResult[keys[i]].id][extraKeys[n]].extra_image;
                                                            extraImgSrcTop = fullAppRoot + "img/uploads/" + extras[roomsResult[keys[i]].id][extraKeys[n]].extra_image
                                                        } else {
                                                            extraImgSrc = fullAppRoot + "img/extra.png";
                                                            extraImgSrcTop = fullAppRoot + "img/extra.png";
                                                        }
                                                        roomSelectedLang === "1" ? extraName = extras[roomsResult[keys[i]].id][extraKeys[n]].extra_name : extraName = extras[roomsResult[keys[i]].id][extraKeys[n]].extra_name_sec_lang;
                                                        roomSelectedLang === "1" ? extraDesc = extras[roomsResult[keys[i]].id][extraKeys[n]].extra_description : extraDesc = extras[roomsResult[keys[i]].id][extraKeys[n]].extra_description_sec_lang;
                                                        roomSelectedLang === "1" ? extraRateTooltip = finalResult.extra_rate_tooltip : extraRateTooltip = finalResult.extra_rate_tooltip_sec_lang;
                                                        roomSelectedLang === "1" ? extraAvailableOnCheckOut = finalResult.extra_available_on_checkout : extraAvailableOnCheckOut = finalResult.extra_available_on_checkout_sec_lang;
                                                        roomSelectedLang === "1" ? extraExcludeDiscount = finalResult.extra_exclude_discounts_label : extraExcludeDiscount = finalResult.extra_exclude_discounts_label_sec_lang;
                                                        roomSelectedLang === "1" ? extraExcludeDiscountYes = finalResult.extra_exclude_discounts_yes : extraExcludeDiscountYes = finalResult.extra_exclude_discounts_yes_sec_lang;
                                                        roomSelectedLang === "1" ? extraExcludeDiscountNo = finalResult.extra_exclude_discounts_no : extraExcludeDiscountNo = finalResult.extra_exclude_discounts_no_sec_lang;

                                                        $(window.parent.document.body).append('<div id="extra_modal_' + extraRoomId + roomExtraId + '" class="w3-modal">' +
                                                            '<div class="w3-modal-content w3-animate-zoom accommodation-modals">' +
                                                            '<span onclick="document.getElementById(\'extra_modal_' + extraRoomId + roomExtraId + '\').style.display=\'none\'" class="w3-button w3-display-topright">X</span>' +
                                                            '<img src="' + extraImgSrcTop + '" alt="" class="w3-mobile mobile-image">' +
                                                            '<h4 class="padding-left-right"><span id="extra_name" data-lang="upsells_' + roomExtraId + '" class="lang">' + extraName + '</span></h4>' +
                                                            '<p class="padding-left-right"><span id="extra_desc" data-lang="upsells_' + roomExtraId + '" class="lang">' + extraDesc + '</span></p>' +
                                                            '<p id="extra_checkout_' + extraRoomId + '_' + roomExtraId + '" class="w3-margin-top padding-left-right padding-bottom tooltip-footer"><span data-lang="step_4_thumbnail_popup_rate" class="lang">' + extraRateTooltip + '</span>: ' + finalResult.currency + price.toFixed(2) + '<br />' +
                                                            '<span id="ext_checkout_' + extraRoomId + '_' + roomExtraId + '"><span data-lang="step_4_thumbnail_popup_available_on_checkout" class="lang">' + extraAvailableOnCheckOut + '</span>: ');

                                                        if (extras[roomsResult[keys[i]].id][extraKeys[n]].extra_available_checkout === "Yes") {

                                                            if (roomSelectedLang === '1') {
                                                                $("#ext_checkout_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_available_on_checkout_yes" class="lang">' + finalResult.extra_available_on_checkout_yes + '</span><br />');
                                                            } else {
                                                                $("#ext_checkout_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_available_on_checkout_yes" class="lang">' + finalResult.extra_available_on_checkout_yes_sec_lang + '</span><br />');
                                                            }
                                                        } else if (extras[roomsResult[keys[i]].id][extraKeys[n]].extra_available_checkout === "No") {

                                                            if (roomSelectedLang === '1') {
                                                                $("#ext_checkout_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_available_on_checkout_no" class="lang">' + finalResult.extra_available_on_checkout_no + '</span><br />');
                                                            } else {
                                                                $("#ext_checkout_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_available_on_checkout_no" class="lang">' + finalResult.extra_available_on_checkout_no_sec_lang + '</span><br />');
                                                            }
                                                        }

                                                        $("#ext_checkout_" + extraRoomId + "_" + roomExtraId, window.parent.document).append("</span>");

                                                        $("#extra_checkout_" + extraRoomId + '_' + roomExtraId, window.parent.document).append('<span id="ext_discount_' + extraRoomId + '_' + roomExtraId + '"><span data-lang="step_4_thumbnail_popup_excluded_from_discounts" class="lang">' + extraExcludeDiscount + '</span>: ');

                                                        if (extras[roomsResult[keys[i]].id][extraKeys[n]].extra_exclude_discounts === "Yes") {

                                                            if (roomSelectedLang === '1') {
                                                                $("#ext_discount_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_excluded_from_discounts_yes" class="lang">' + finalResult.extra_exclude_discounts_yes + '</span>');
                                                            } else {
                                                                $("#ext_discount_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_excluded_from_discounts_yes" class="lang">' + finalResult.extra_exclude_discounts_yes_sec_lang + '</span>');
                                                            }
                                                        } else if (extras[roomsResult[keys[i]].id][extraKeys[n]].extra_exclude_discounts === "No") {

                                                            if (roomSelectedLang === '1') {
                                                                $("#ext_discount_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_excluded_from_discounts_no" class="lang">' + finalResult.extra_exclude_discounts_no + '</span>');
                                                            } else {
                                                                $("#ext_discount_" + extraRoomId + "_" + roomExtraId, window.parent.document).append('<span data-lang="step_4_thumbnail_popup_excluded_from_discounts_no" class="lang">' + finalResult.extra_exclude_discounts_no_sec_lang + '</span>');
                                                            }
                                                        }

                                                        $("#ext_discount_" + extraRoomId + '_' + roomExtraId, window.parent.document).append("</span>");

                                                        $("#extras_list_" + roomsResult[keys[i]].id, window.parent.document).append(
                                                            '</p></div>' +
                                                            '</div>'
                                                        );

                                                        $("#extras_list_" + roomsResult[keys[i]].id).append(
                                                            '<div id="extraItem" class="w3-col l10 m10">' +
                                                            '<div class="panel">' +
                                                            '<a onclick="top.document.getElementById(\'extra_modal_' + extraRoomId + roomExtraId + '\').style.display=\'block\'"><img class="upsell-thumbnail" src="' + extraImgSrc + '"></a> <span id="extra_name" data-lang="upsells_' + roomExtraId + '" class="lang">' + extraName + '</span><span>: ' + finalResult.currency + '</span><span id="ext_avg_rate" data-id="' + extraRoomId + roomExtraId + '">' + formatRate(price.toFixed(2), finalResult.thousand, finalResult.decimal) + '</span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="w3-col l2 m2">' +
                                                            '<select class="w3-select w3-border w3-margin-bottom extras-nights room-total" data-id="' + roomsResult[keys[i]].id + '" data-checkout="' + extras[roomsResult[keys[i]].id][extraKeys[n]].extra_available_checkout + '" name="option" data-rate="' + price + '" data-extra_id="' + roomExtraId + '" disabled>' +
                                                            '<option data-lang="step_3_quantity_selector" class="lang" id="extra_quantity_' + roomsResult[keys[i]].id + '" value="extra_selector" selected>' + quantSel + '</option>' +
                                                            '</select>' +
                                                            '</div>');
                                                    }
                                                }
                                            } else {
                                                roomSelectedLang === "1" ? noExtras = finalResult.no_extras : noExtras = finalResult.no_extras_sec_lang;
                                                $("span.noExtra[data-id='" + roomsResult[keys[i]].id + "']").addClass("no-extras").html(noExtras);
                                            }
                                            $("#extras_list_" + roomsResult[keys[i]].id).append('</div>');
                                            $("#extras_panel").append('</div>');
                                            $(".room_div").append('</div>');

                                            // Displaying extras' panel (toggle)
                                            var showExtrasPanel = $("#available_rooms").data("extras");
                                            showExtrasPanel == 0 ? $("#extras_panel").remove() : '';

                                            $("#extras_" + roomsResult[keys[i]].id).click(function() { // Opening of extras' list (dropdown) 
                                                var roomId = $(this).data("roomid");
                                                var extrasListId = "extras_list_" + roomId;

                                                if ($("#extras_open_" + roomId).length > 0) {
                                                    $("#extras_open_" + roomId).replaceWith('<span id="extras_close_' + roomId + '" class="open-close"><i class="fa fa-chevron-up"></span>');
                                                } else if ($("#extras_close_" + roomId).length > 0) {
                                                    $("#extras_close_" + roomId).replaceWith('<span id="extras_open_' + roomId + '" class="open-close"><i class="fa fa-chevron-down"></span>');
                                                }

                                                myFunction(extrasListId);

                                                function myFunction(id) {

                                                    var x = document.getElementById(id);
                                                    if (x.className.indexOf("w3-show") == -1) {
                                                        x.className += " w3-show";
                                                        x.previousElementSibling.className =
                                                            x.previousElementSibling.className.replace("w3-light-grey", "w3-grey");
                                                    } else {
                                                        x.className = x.className.replace(" w3-show", "");
                                                        x.previousElementSibling.className =
                                                            x.previousElementSibling.className.replace("w3-grey", "w3-light-grey");
                                                    }
                                                }
                                            });

                                            // Enabling discount and extras selectors and adding options based on the CheckIn & CheckOut Dates                                             

                                            $(".room-quantity[data-id='" + roomsResult[keys[i]].id + "']").change(function() {
                                                var roomSelectorId = $(this).data("id");
                                                var quantSelector = $(this).val();
                                                var discPanel = [];

                                                // Discount selector
                                                $("select.room-quantity").each(function() {
                                                    var selectValue = $(this).val();
                                                    selectValue === "rooms_quantity" ? discPanel.push("0") : discPanel.push("1");
                                                });

                                                if (discPanel.indexOf("1") > -1) {
                                                    $("select#discount_select").attr("disabled", false);
                                                } else {
                                                    $("#discount_select option:selected").attr("selected", false);
                                                    $("#discount_select option[value='select']").attr("selected", true);
                                                    $("span#disc_name").text("N/A");
                                                    $("select#discount_select").attr("disabled", true);
                                                    $("input#discount_other").val("").hide();
                                                    $("#discount_apply").hide();
                                                    $("#discount_remove").hide();
                                                }

                                                // Extras selector
                                                $(".extras-nights").each(function() {
                                                    var extraId = $(this).data("id");

                                                    if (extraId === roomSelectorId) {

                                                        if (quantSelector === "rooms_quantity") {
                                                            $(".extras_days_" + roomSelectorId).remove();
                                                            $("#extra_quantity_" + roomSelectorId).attr("selected", true);
                                                            $(this).attr("disabled", true);
                                                        } else {
                                                            $(this).attr("disabled", false);
                                                            $(this).children("option.extras_days_" + roomSelectorId).remove();
                                                            var diff = new Date(Date.parse(checkOutDate) - Date.parse(checkInDate));
                                                            var nights = diff / 1000 / 60 / 60 / 24;
                                                            var upsellCheckOut = $(this).data("checkout");
                                                            var availableDays = "";

                                                            (upsellCheckOut == "No") ? availableDays = nights: availableDays = +nights + +1;
                                                            var extrasOptions = availableDays * quantSelector;

                                                            for (var d = 1; d <= extrasOptions; d++) {
                                                                $(this).append('<option value="' + [d] + '" class="extras_days_' + roomSelectorId + '">' + [d] + '</option>');
                                                            }
                                                        }
                                                    }
                                                });
                                            });

                                            /** CALCULATION OF ROOM AVERAGE RATES*/

                                            $("select.room[data-id='" + roomsResult[keys[i]].id + "']").each(function() {
                                                var roomSelectId = $(this).data("id");
                                                var avgRateToolTip = $("div.tooltip[data-id='" + roomSelectId + "']").data("text");
                                                var tooltipCurrency = $("div.tooltip[data-id='" + roomSelectId + "']").data("currency");
                                                var defaultRate = parseFloat($(this).data("rate"));
                                                var roomModel = $(this).data("model");
                                                var adultsExtraPrice = parseFloat($(this).data("adults_extra"));
                                                var childrenExtraPrice = parseFloat($(this).data("children_extra"));
                                                var combinedExtraPrice = parseFloat($(this).data("combined_extra"));
                                                var combinedIncPersons = parseFloat($(this).data("combined_inc"));
                                                var adultsIncluded = parseInt($("select.adults[data-id='" + roomSelectId + "']").data("adults_included"));
                                                var childrenIncluded = parseInt($("select.children[data-id='" + roomSelectId + "']").data("children_included"));

                                                $.ajax({
                                                    url: fullAppRoot + "engine/form_ajax.php",
                                                    type: "POST",
                                                    data: { method: "calculateRoomRate", roomId: roomSelectId, checkIn: checkInDate, checkOut: checkOutDate, defRate: defaultRate },
                                                    dataType: "html",
                                                    cache: false,
                                                    success: function(calc_response) {
                                                        var calcResult = JSON.parse(calc_response);
                                                        var calcRoomIds = Object.keys(calcResult);
                                                        var avgNightlyRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, discTotal, roomBaseTotal, adultsExtraAmount, childrenExtraAmount, finalRate, roomSummaryRate, summaryAmount, occupantsSummaryValue, summaryDiscTotal, summaryRate, decimalSeparator, thousandSeparator;
                                                        adultsExtraAmount === "" ? adultsExtraAmount = parseInt(0) : adultsExtraAmount = parseFloat(adultsExtraAmount);
                                                        childrenExtraAmount === "" ? childrenExtraAmount = parseInt(0) : childrenExtraAmount = parseFloat(childrenExtraAmount);
                                                        var discountAmounts = [];
                                                        var summaryDiscountAmounts = [];

                                                        for (var calc = 0; calc < calcRoomIds.length; calc++) { // Discounts' data
                                                            avgNightlyRate = parseFloat(calcResult[calcRoomIds[calc]].room);
                                                            roomSummaryRate = parseFloat(calcResult[calcRoomIds[calc]].roomTotalSum);
                                                            discMinNightsAmount = parseFloat(calcResult[calcRoomIds[calc]].minNightAmount);
                                                            discMinNightsPerCent = parseFloat(calcResult[calcRoomIds[calc]].minNightPerCent);
                                                            discInAdvanceAmount = parseFloat(calcResult[calcRoomIds[calc]].daysInAdvanceAmount);
                                                            discInAdvancePerCent = parseFloat(calcResult[calcRoomIds[calc]].daysInAdvancePerCent);
                                                            thousandSeparator = calcResult[calcRoomIds[calc]].thousand !== "" ? calcResult[calcRoomIds[calc]].thousand : ",";
                                                            decimalSeparator = calcResult[calcRoomIds[calc]].decimal !== "" ? calcResult[calcRoomIds[calc]].decimal : ".";
                                                        }

                                                        // Thousand and decimal separators for the rooms & extras
                                                        $("div#available_rooms").data("thousand", thousandSeparator);
                                                        $("div#available_rooms").data("decimal", decimalSeparator);
                                                        $(".extras-nights[data-id='" + roomSelectId + "']").data("thousand", thousandSeparator);
                                                        $(".extras-nights[data-id='" + roomSelectId + "']").data("decimal", decimalSeparator);

                                                        // Calculation of discounts' amounts (rooms)
                                                        discMinNightsAmount != 0 ? discountAmounts.push(discMinNightsAmount) : ""; // Average rate
                                                        discMinNightsAmount != 0 ? summaryDiscountAmounts.push((discMinNightsAmount * diffDays)) : ""; // Summary rate

                                                        if (discMinNightsPerCent != 0) {
                                                            // Average rate
                                                            var subRes = avgNightlyRate * discMinNightsPerCent;
                                                            discountAmounts.push(parseFloat(subRes));

                                                            // Summary rate
                                                            var sumSubRes = roomSummaryRate * discMinNightsPerCent;
                                                            summaryDiscountAmounts.push(parseFloat(sumSubRes));
                                                        }

                                                        discInAdvanceAmount != 0 ? discountAmounts.push(discInAdvanceAmount) : ""; // Average rate
                                                        discInAdvanceAmount != 0 ? summaryDiscountAmounts.push((discInAdvanceAmount * diffDays)) : ""; // Summary rate

                                                        if (discInAdvancePerCent != 0) {
                                                            // Average rate
                                                            var subRes = avgNightlyRate * discInAdvancePerCent;
                                                            discountAmounts.push(parseFloat(subRes));

                                                            // Summary rate
                                                            var sumSubRes = roomSummaryRate * discInAdvancePerCent;
                                                            summaryDiscountAmounts.push(parseFloat(sumSubRes));
                                                        }

                                                        // Average Room Rate (default)
                                                        discTotal = parseFloat(discountAmounts.reduce(function(a, b) { return a + b; }, 0));
                                                        discTotal != 0 ? roomBaseTotal = avgNightlyRate - discTotal : roomBaseTotal = avgNightlyRate;
                                                        formatRate(roomBaseTotal.toFixed(2), thousandSeparator, decimalSeparator);
                                                        $("div.tooltip[data-id='" + roomSelectId + "']").append('<strong>' + tooltipCurrency + rateFormat + '</strong><span class="tooltiptext lang w3-hide-small" data-lang="step_3_rate_tooltip">' + avgRateToolTip + '</span>');
                                                        $("span.running_total_sum[data-id='" + roomSelectId + "']").text(rateFormat);

                                                        // Booking Summary (default)
                                                        summaryDiscTotal = parseFloat(summaryDiscountAmounts.reduce(function(a, b) { return a + b; }, 0));
                                                        summaryDiscTotal != 0 ? summaryAmount = roomSummaryRate - parseFloat(summaryDiscTotal) : summaryAmount = roomSummaryRate;
                                                        $("select.room[data-id='" + roomSelectId + "']").data("summary", parseFloat(summaryAmount));

                                                        // Average Room Rate and Booking Summary Amount (after selecting room number)
                                                        $("select.room[data-id='" + roomSelectId + "']").change(function() {
                                                            //Average rate
                                                            var selectedRoomAmount = $(this).val();
                                                            var selectedRoomTotal;
                                                            $("div.tooltip[data-id='" + roomSelectId + "']").html("");
                                                            selectedRoomAmount !== "rooms_quantity" ? selectedRoomTotal = parseFloat(roomBaseTotal) * parseInt(selectedRoomAmount) : selectedRoomTotal = parseFloat(roomBaseTotal);
                                                            formatRate(selectedRoomTotal.toFixed(2), thousandSeparator, decimalSeparator);
                                                            $("div.tooltip[data-id='" + roomSelectId + "']").append('<strong>' + tooltipCurrency + '<span class="acc_rate" data-id="' + roomSelectId + '">' + rateFormat + '</span></strong><span class="tooltiptext lang" data-lang="step_3_rate_tooltip">' + avgRateToolTip + '</span>');

                                                            //Booking Summary
                                                            var summaryTotal;
                                                            $("select.room[data-id='" + roomSelectId + "']").data("summary", "");
                                                            selectedRoomAmount !== "rooms_quantity" ? summaryTotal = parseFloat(summaryAmount) * parseInt(selectedRoomAmount) : summaryTotal = parseFloat(summaryAmount);
                                                            $("select.room[data-id='" + roomSelectId + "']").data("summary", parseFloat(summaryTotal));
                                                        });

                                                        // Rates of Occupants
                                                        $("select.room-occupants[data-id='" + roomSelectId + "']").change(function() {
                                                            selectRoomValue = parseInt($("select.room[data-id='" + roomSelectId + "']").val());
                                                            selectAdultValue = $("select.adults[data-id='" + roomSelectId + "']").val();
                                                            selectChildrenValue = $("select.children[data-id='" + roomSelectId + "']").val();
                                                            summaryRate = parseFloat($("select.room[data-id='" + roomSelectId + "']").data("summary"));

                                                            switch (roomModel) {

                                                                case "individual": // Individual room model

                                                                    if (selectAdultValue !== "rooms_adult") { // Rates of adults
                                                                        var includedAdultsPerRoom = adultsIncluded * selectRoomValue;

                                                                        if (parseInt(includedAdultsPerRoom) < parseInt(selectAdultValue)) { // Max. included adults are less than the selected number of adults
                                                                            // Average rate                                                                           
                                                                            var extraAdultsNumber = parseInt(selectAdultValue) - parseInt(includedAdultsPerRoom);
                                                                            adultsExtraAmount = parseInt(extraAdultsNumber) * adultsExtraPrice;
                                                                            var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                            var occupantsAvgRate = parseFloat(adultsExtraAmount) + +parseFloat(childrenExtraAmount);
                                                                            finalRate = (parseFloat(roomAvgRate) + +parseFloat(occupantsAvgRate));

                                                                            // Summary rate
                                                                            occupantsSummaryValue = (parseFloat(adultsExtraAmount) * diffDays) + (parseFloat(childrenExtraAmount) * diffDays);
                                                                            summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                            extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                        } else { // Max. included adults are more than the selected number of adults
                                                                            // Average rate
                                                                            adultsExtraAmount = parseInt(0);
                                                                            var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                            var occupantsAvgRate = parseFloat(adultsExtraAmount) + +parseFloat(childrenExtraAmount);
                                                                            finalRate = (parseFloat(roomAvgRate) + +parseFloat(occupantsAvgRate));

                                                                            // Summary rate
                                                                            occupantsSummaryValue = (parseFloat(adultsExtraAmount) * diffDays) + (parseFloat(childrenExtraAmount) * diffDays);
                                                                            summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                            extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                        }
                                                                    } else { // No adults are selected
                                                                        // Average rate                                                                        
                                                                        adultsExtraAmount = parseInt(0);
                                                                        var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                        var occupantsAvgRate = parseFloat(adultsExtraAmount) + +parseFloat(childrenExtraAmount);
                                                                        finalRate = (parseFloat(roomAvgRate) + +parseFloat(occupantsAvgRate));

                                                                        // Summary rate
                                                                        occupantsSummaryValue = (parseFloat(adultsExtraAmount) * diffDays) + (parseFloat(childrenExtraAmount) * diffDays);
                                                                        summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                        extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                    }

                                                                    if (selectChildrenValue !== "rooms_children") { // Rates of children
                                                                        var includedChildrenPerRoom = childrenIncluded * selectRoomValue;

                                                                        if (parseInt(includedChildrenPerRoom) < parseInt(selectChildrenValue)) { // Max. included children are less than the selected number of children
                                                                            // Average rate
                                                                            var extraChildrenNumber = parseInt(selectChildrenValue) - parseInt(includedChildrenPerRoom);
                                                                            var childrenExtraAmount = parseInt(extraChildrenNumber) * childrenExtraPrice;
                                                                            var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                            var occupantsAvgRate = parseFloat(adultsExtraAmount) + +parseFloat(childrenExtraAmount);
                                                                            finalRate = (parseFloat(roomAvgRate) + +parseFloat(occupantsAvgRate));

                                                                            // Summary rate
                                                                            occupantsSummaryValue = (parseFloat(adultsExtraAmount) * diffDays) + (parseFloat(childrenExtraAmount) * diffDays);
                                                                            summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                            extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                        } else { // Max. included children are more than the selected number of children
                                                                            // Average rate
                                                                            childrenExtraAmount = parseInt(0);
                                                                            var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                            var occupantsAvgRate = parseFloat(adultsExtraAmount) + +parseFloat(childrenExtraAmount);
                                                                            finalRate = (parseFloat(roomAvgRate) + +parseFloat(occupantsAvgRate));

                                                                            // Summary rate
                                                                            occupantsSummaryValue = (parseFloat(adultsExtraAmount) * diffDays) + (parseFloat(childrenExtraAmount) * diffDays);
                                                                            summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                            extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                        }
                                                                    } else { // No children are selected
                                                                        // Average rate
                                                                        childrenExtraAmount = parseInt(0);
                                                                        var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                        var occupantsAvgRate = parseFloat(adultsExtraAmount) + +parseFloat(childrenExtraAmount);
                                                                        finalRate = (parseFloat(roomAvgRate) + +parseFloat(occupantsAvgRate));

                                                                        // Summary rate
                                                                        occupantsSummaryValue = (parseFloat(adultsExtraAmount) * diffDays) + (parseFloat(childrenExtraAmount) * diffDays);
                                                                        summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                        extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                    }
                                                                    break;

                                                                case "combined": // Combined room model
                                                                    isNaN(selectAdultValue) ? selectAdultValue = parseInt(0) : selectAdultValue = parseInt(selectAdultValue);
                                                                    isNaN(selectChildrenValue) ? selectChildrenValue = parseInt(0) : selectChildrenValue = parseInt(selectChildrenValue);

                                                                    var allowedCombinedPersonsSum = parseInt(combinedIncPersons) * selectRoomValue; // Total quantity of combined persons
                                                                    var selectedCombinedPersonsSum = parseInt(selectAdultValue) + +parseInt(selectChildrenValue); // Total quantity of selected persons (adults + children)                                                          

                                                                    if (parseInt(allowedCombinedPersonsSum) < parseInt(selectedCombinedPersonsSum)) { // Max. combined persons are less than the selected quantity
                                                                        // Average rate                                                                        
                                                                        var extraCombinedPersons = parseInt(selectedCombinedPersonsSum) - parseInt(allowedCombinedPersonsSum);
                                                                        var extraCombinedPersonsPrice = parseInt(extraCombinedPersons) * combinedExtraPrice;
                                                                        var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                        finalRate = (parseFloat(roomAvgRate) + +parseFloat(extraCombinedPersonsPrice));

                                                                        // Summary rate
                                                                        occupantsSummaryValue = parseFloat(extraCombinedPersonsPrice) * diffDays;
                                                                        summaryRate = parseFloat(occupantsSummaryValue) + (parseFloat(roomSummaryRate) * parseInt(selectRoomValue));

                                                                        extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                    } else { // Max. combined persons are more than the selected quantity
                                                                        // Average rate
                                                                        var roomAvgRate = avgNightlyRate * selectRoomValue;
                                                                        finalRate = parseFloat(roomAvgRate);

                                                                        // Summary rate
                                                                        summaryRate = parseFloat(roomSummaryRate) * parseInt(selectRoomValue);

                                                                        extraOccupantDiscount(roomSelectId, finalRate, discMinNightsAmount, discMinNightsPerCent, discInAdvanceAmount, discInAdvancePerCent, tooltipCurrency, avgRateToolTip, summaryRate, diffDays);
                                                                    }
                                                                    break;
                                                            }

                                                            // Calculation of occupants' rates 
                                                            function extraOccupantDiscount(extRoomId, baseRate, minNightsAmount, minNightsPerCent, inAdvanceAmount, inAdvancePerCent, currency, toolText, sumRate, days) {
                                                                var extraOccDiscAmounts = [];
                                                                var summaryTotalAmounts = [];
                                                                var extraOccDiscSum, extraFinalRate, summaryTotalSum, summaryFinal;

                                                                minNightsAmount != 0 ? extraOccDiscAmounts.push(minNightsAmount) : ""; // Average rate
                                                                minNightsAmount != 0 ? summaryTotalAmounts.push((minNightsAmount * days)) : ""; // Summary rate

                                                                if (minNightsPerCent != 0) {
                                                                    // Average rate
                                                                    var subRes = baseRate * minNightsPerCent;
                                                                    extraOccDiscAmounts.push(parseFloat(subRes));

                                                                    // Summary rate
                                                                    var sumSubRes = sumRate * minNightsPerCent;
                                                                    summaryTotalAmounts.push(parseFloat(sumSubRes));
                                                                }

                                                                inAdvanceAmount != 0 ? extraOccDiscAmounts.push(inAdvanceAmount) : ""; // Average rate
                                                                inAdvanceAmount != 0 ? summaryTotalAmounts.push((inAdvanceAmount * days)) : ""; // Summary rate

                                                                if (inAdvancePerCent != 0) {
                                                                    // Average rate
                                                                    var subRes = baseRate * inAdvancePerCent;
                                                                    extraOccDiscAmounts.push(parseFloat(subRes));

                                                                    // Summary rate
                                                                    var sumSubRes = sumRate * inAdvancePerCent;
                                                                    summaryTotalAmounts.push(parseFloat(sumSubRes));
                                                                }

                                                                // Average rate
                                                                extraOccDiscSum = parseFloat(extraOccDiscAmounts.reduce(function(a, b) { return a + b; }, 0));
                                                                extraOccDiscSum != 0 ? extraFinalRate = parseFloat(baseRate) - parseFloat(extraOccDiscSum) : extraFinalRate = parseFloat(baseRate);
                                                                $("div.tooltip[data-id='" + extRoomId + "']").html("");
                                                                formatRate(extraFinalRate.toFixed(2), thousandSeparator, decimalSeparator);
                                                                $("div.tooltip[data-id='" + extRoomId + "']").append('<strong>' + currency + '<span class="acc_rate" data-id="' + extRoomId + '">' + rateFormat + '</span></strong><span class="tooltiptext lang" data-lang="step_3_rate_tooltip">' + toolText + '</span>');

                                                                // Summary rate
                                                                summaryTotalSum = parseFloat(summaryTotalAmounts.reduce(function(a, b) { return a + b; }, 0));
                                                                summaryTotalSum != 0 ? summaryFinal = parseFloat(sumRate) - parseFloat(summaryTotalSum) : summaryFinal = parseFloat(sumRate);
                                                                $("select.room[data-id='" + roomSelectId + "']").data("summary", "");
                                                                $("select.room[data-id='" + roomSelectId + "']").data("summary", parseFloat(summaryFinal));
                                                            }
                                                        });
                                                    }
                                                });
                                            });

                                            /** CALCULATION OF EXTRA RATES */

                                            $(".extras-nights[data-id='" + roomsResult[keys[i]].id + "']").change(function() {
                                                var extraSelectorId = $(this).data("id");
                                                var extrasId = $(this).data("extra_id");
                                                var extraQuantSelector = $(this).val();
                                                var extraDefaultRate = $(this).data("rate");
                                                var thousandSeparator = $(this).data("thousand");
                                                var decimalSeparator = $(this).data("decimal");

                                                if (extraQuantSelector !== "extra_selector") { // Display in tooltip
                                                    var extraAvgTotal = (parseInt(extraQuantSelector) * parseFloat(extraDefaultRate));
                                                    formatRate(extraAvgTotal.toFixed(2), thousandSeparator, decimalSeparator);
                                                    $("#ext_avg_rate[data-id='" + extraSelectorId + extrasId + "']").text(rateFormat);
                                                } else {
                                                    $("#ext_avg_rate[data-id='" + extraSelectorId + extrasId + "']").text(extraDefaultRate.toFixed(2));
                                                }
                                            });
                                        }
                                    }
                                }
                            } else {
                                $("#section_2").hide();
                                $(".filler").show();
                                $("#error_no_rooms").show();
                                $("#error_valid_dates").hide();
                                $("#error_no_selection").hide();
                                $("#error_minimum_stay").hide();
                                $("#error_blackout").hide();
                                $("#error_validation").hide();
                            }
                        }
                    }

                    /** Number of Occupants (Calculations and creation of rooms/adults/children select lists) */

                    $(".room-quantity").each(function() { // Default room quantity select list options
                        var quantity = $(this).data("quantity");

                        for (var q = 1; q <= quantity; q++) {
                            $(this).append('<option value="' + q + '">' + q + '</option>');
                        }
                    });

                    $(".room-quantity").each(function() { // Adults-children select lists after picking room quantity
                        var occupantsModelType = $(this).data("model");
                        var roomId = $(this).data("id");

                        $(this).change(function() {
                            roomId = $(this).data("id");
                            selectedRoom.push(roomId);
                            var roomSelectedQuantity = $(this).val();
                            var occupantsCombinedIncluded = $(this).data("combined_inc");

                            $(".room-occupants").each(function() {
                                var roomOccupantsId = $(this).data("id");
                                var roomMaxOccupants = $(this).data("occupants");
                                var selectedRoomMaxOccupants = "";

                                if (roomId == roomOccupantsId) {
                                    var occupantsCombinedMaxNumber = $(".room-quantity[data-id='" + roomOccupantsId + "']").data("people_max");

                                    $("option.occupants", this).remove();
                                    roomSelectedQuantity === "rooms_quantity" ? $(this).attr("disabled", true) : $(this).attr("disabled", false);

                                    switch (occupantsModelType) {

                                        case "individual": // Individual occupant model default
                                            selectedRoomMaxOccupants = roomMaxOccupants * roomSelectedQuantity;

                                            for (var s = 1; s <= selectedRoomMaxOccupants; s++) {
                                                $(this).append('<option value="' + s + '" class="occupants">' + s + '</option>');
                                            }
                                            break;

                                        case "combined": // Combined occupant model default
                                            selectedRoomMaxOccupants = occupantsCombinedMaxNumber * roomSelectedQuantity;

                                            for (var co = 1; co <= selectedRoomMaxOccupants; co++) {
                                                $(this).append('<option value="' + co + '" class="occupants">' + co + '</option>');
                                            }
                                            break;
                                    }
                                }
                            });
                        });

                        if (occupantsModelType === "combined") { // Adults-Children select lists in combined model after selecting room quantity
                            $(".room-occupants[data-id='" + roomId + "']").each(function() {
                                $(this).change(function() {
                                    var occupantType = $(this).data("type");
                                    var occupantMaxNumber = $(".room-quantity[data-id='" + roomId + "']").data("people_max");
                                    var roomQuantity = $(".room-quantity[data-id='" + roomId + "']").val();
                                    var occupantNumberSum = parseInt(roomQuantity) * parseInt(occupantMaxNumber);
                                    var occupantNumber = $(this).val();

                                    switch (occupantType) {

                                        case "adults": // Adults select
                                            var occupantChildrenNumber = $(".children[data-id='" + roomId + "']").val();
                                            if (occupantNumber !== "rooms_adult") {
                                                $(".children[data-id='" + roomId + "']").children("option.occupants").remove();
                                                var occupantChildrenMaxNumber = parseInt(occupantNumberSum) - parseInt(occupantNumber);
                                                for (var cho = 1; cho <= occupantChildrenMaxNumber; cho++) {
                                                    $(".children[data-id='" + roomId + "']").append('<option value="' + cho + '" class="occupants">' + cho + '</option>');
                                                }
                                                occupantChildrenNumber !== "rooms_children" ? $(".children[data-id='" + roomId + "']").children("option[value='" + occupantChildrenNumber + "']").attr("selected", true) : "";
                                            } else {
                                                $(".children[data-id='" + roomId + "']").children("option.occupants").remove();
                                                for (var cho = 1; cho <= occupantNumberSum; cho++) {
                                                    $(".children[data-id='" + roomId + "']").append('<option value="' + cho + '" class="occupants">' + cho + '</option>');
                                                }
                                                occupantChildrenNumber !== "rooms_children" ? $(".children[data-id='" + roomId + "']").children("option[value='" + occupantChildrenNumber + "']").attr("selected", true) : "";
                                            }
                                            break;

                                        case "children": // Children select
                                            var occupantAdultsNumber = $(".adults[data-id='" + roomId + "']").val();
                                            if (occupantNumber !== "rooms_children") {
                                                $(".adults[data-id='" + roomId + "']").children("option.occupants").remove();
                                                var occupantAdults = parseInt(occupantNumberSum) - parseInt(occupantNumber);
                                                for (var ado = 1; ado <= occupantAdults; ado++) {
                                                    $(".adults[data-id='" + roomId + "']").append('<option value="' + ado + '" class="occupants">' + ado + '</option>');
                                                }
                                                occupantAdultsNumber !== "rooms_adult" ? $(".adults[data-id='" + roomId + "']").children("option[value='" + occupantAdultsNumber + "']").attr("selected", true) : "";
                                            } else {
                                                $(".adults[data-id='" + roomId + "']").children("option.occupants").remove();
                                                for (var ado = 1; ado <= occupantNumberSum; ado++) {
                                                    $(".adults[data-id='" + roomId + "']").append('<option value="' + ado + '" class="occupants">' + ado + '</option>');
                                                }
                                                occupantAdultsNumber !== "rooms_adult" ? $(".adults[data-id='" + roomId + "']").children("option[value='" + occupantAdultsNumber + "']").attr("selected", true) : "";
                                            }
                                            break;
                                    }
                                });
                            });
                        }
                    });

                    // Formatting the rates (thousands and decimal separators as well)
                    function formatRate(number, thousand, decimal) {
                        var y = number.split(".");
                        var th1, th2;

                        switch (true) {

                            case y[0].length == 4:
                                th1 = y[0].slice(0, 1);
                                th2 = y[0].slice(1);
                                rateFormat = th1 + thousand + th2 + decimal + y[1];
                                break;

                            case y[0].length == 5:
                                th1 = y[0].slice(0, 2);
                                th2 = y[0].slice(2);
                                rateFormat = th1 + thousand + th2 + decimal + y[1];
                                break;

                            case y[0].length == 6:
                                th1 = y[0].slice(0, 3);
                                th2 = y[0].slice(3);
                                rateFormat = th1 + thousand + th2 + decimal + y[1];
                                break;

                            case y[0].length == 7:
                                th1 = y[0].slice(0, 4);
                                th2 = y[0].slice(4);
                                rateFormat = th1 + thousand + th2 + decimal + y[1];
                                break;

                            default:
                                rateFormat = y[0] + decimal + y[1];
                                break;
                        }

                        return rateFormat;
                    }
                }
            });
        }
    }
});