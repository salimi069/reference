$(document).ready(function() {

    var fullAppRoot = $("div.engine_form").data("app");

    /** 
     * SUBMIT
     */

    $("button.submit").each(function() {

        $(this).click(function() {

            $(this).hide();
            $("button#processing").show();
            $("button#processing").attr("disabled", true);

            var lang = "";
            $("#lang_select").length > 0 ? lang = $("#lang_select").val() : lang = 1;
            var btnId = $(this).attr("id");
            var roomSelectQuantity = [];
            var validationError = false;
            $("#error_validation").hide();

            // Validation of Room Selection

            $(".room-quantity").each(function() {
                var roomOption = $(this).val();

                if (roomOption !== "rooms_quantity") {
                    var roomId = $(this).data("id");
                    roomSelectQuantity.push(roomId);
                }
            });

            if (roomSelectQuantity.length > 0) {

                for (var i = 0; i < roomSelectQuantity.length; i++) {
                    if ($(".room-occupants[data-id='" + roomSelectQuantity[i] + "']").val() === "rooms_adult") {
                        $("#error_no_selection").show();
                        var alert = document.getElementById("select_date");
                        alert.scrollIntoView(true);
                        validationError = true;
                    } else {
                        $("#error_no_selection").hide();
                    }
                }
            } else {
                $("#error_no_selection").show();
                var alert = document.getElementById("select_date");
                alert.scrollIntoView(true);
                validationError = true;
            }

            // Validation of fields (contact, payment, discount-other, checkboxes)

            $(".contact").each(function() {

                $(this).removeClass("error");

                if ($(this).is("input[type='text']")) { // Validation of text type INPUT fields.
                    var inputId = $(this).attr("id");
                    var inputVal = $(this).val();

                    if (inputVal === "") {
                        $("#error_validation").show();
                        var inputAlert = document.getElementById("select_date");
                        inputAlert.scrollIntoView(true);
                        $(this).addClass("error");
                        validationError = true;
                    } else {

                        switch (inputId) {

                            case "zip": // Postal/ZIP Code                                
                                var rez = /^[\w\d\s-]+$/;
                                var checkZip = rez.test(inputVal);
                                if (checkZip == false) {
                                    $("#error_validation").show();
                                    var inputAlert = document.getElementById("select_date");
                                    inputAlert.scrollIntoView(true);
                                    $(this).addClass("error");
                                    validationError = true;
                                } else {
                                    $(this).removeClass("error");
                                }
                                break;

                            case "phone": // Phone Number
                                var rep = /^[0-9\-]+$/;
                                var checkPhone = rep.test(inputVal);
                                if (checkPhone == false) {
                                    $("#error_validation").show();
                                    var inputAlert = document.getElementById("select_date");
                                    inputAlert.scrollIntoView(true);
                                    $(this).addClass("error");
                                    validationError = true;
                                } else {
                                    $(this).removeClass("error");
                                }
                                break;

                            case "email": // Email Address
                                var rem = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                var checkEmail = rem.test(inputVal);
                                if (checkEmail == false) {
                                    $("#error_validation").show();
                                    var inputAlert = document.getElementById("select_date");
                                    inputAlert.scrollIntoView(true);
                                    $(this).addClass("error");
                                    validationError = true;
                                } else {
                                    $(this).removeClass("error");
                                }
                                break;

                            case "cvc": // CVC
                                if (inputVal.length == 3 || inputVal.length == 4) {
                                    var rec = /^[1-9]+[0-9]*$/;
                                    var checkCVC = rec.test(inputVal);
                                    if (checkCVC == false) {
                                        $("#error_validation").show();
                                        var inputAlert = document.getElementById("select_date");
                                        inputAlert.scrollIntoView(true);
                                        $(this).addClass("error");
                                        validationError = true;
                                    } else {
                                        $(this).removeClass("error");
                                    }
                                } else {
                                    $("#error_validation").show();
                                    var inputAlert = document.getElementById("select_date");
                                    inputAlert.scrollIntoView(true);
                                    $(this).addClass("error");
                                    validationError = true;
                                }
                                break;

                            case "credit-card": // Card Number
                                var selectedCardType = $("#credit-type").children("option:selected").val();
                                var creditCardNumber = $(this).val();
                                var creditCardNumberLength = creditCardNumber.length;
                                var stripe = $(".payment-stripe").data("stripe");
                                var checkCreditCard = "";
                                var issuerId = "";
                                var issuerIdSample = "";

                                if (creditCardNumberLength >= 13 && creditCardNumberLength <= 16) {

                                    switch (selectedCardType) {

                                        case "Amex": // Amex
                                            if (creditCardNumberLength == 15) {
                                                issuerId = parseInt(creditCardNumber.substring(0, 2));

                                                if (issuerId == 34 || issuerId == 37) {
                                                    if (stripe === "nostripe") {
                                                        checkCreditCard = valid_credit_card(creditCardNumber);
                                                        if (checkCreditCard) {
                                                            $(this).removeClass("error");
                                                        } else {
                                                            $("#error_validation").show();
                                                            var inputAlert = document.getElementById("select_date");
                                                            inputAlert.scrollIntoView(true);
                                                            $(this).addClass("error");
                                                            validationError = true;
                                                        }
                                                    }
                                                } else {
                                                    $("#error_validation").show();
                                                    var inputAlert = document.getElementById("select_date");
                                                    inputAlert.scrollIntoView(true);
                                                    $(this).addClass("error");
                                                    validationError = true;
                                                }
                                            } else {
                                                $("#error_validation").show();
                                                var inputAlert = document.getElementById("select_date");
                                                inputAlert.scrollIntoView(true);
                                                $(this).addClass("error");
                                                validationError = true;
                                            }
                                            break;

                                        case "Visa": // Visa
                                            if (creditCardNumberLength == 13 || creditCardNumberLength == 16) {
                                                issuerId = parseInt(creditCardNumber.substring(0, 1));

                                                if (issuerId == 4) {
                                                    if (stripe === "nostripe") {
                                                        checkCreditCard = valid_credit_card(creditCardNumber);
                                                        if (checkCreditCard) {
                                                            $(this).removeClass("error");
                                                        } else {
                                                            $("#error_validation").show();
                                                            var inputAlert = document.getElementById("select_date");
                                                            inputAlert.scrollIntoView(true);
                                                            $(this).addClass("error");
                                                            validationError = true;
                                                        }
                                                    }
                                                } else {
                                                    $("#error_validation").show();
                                                    var inputAlert = document.getElementById("select_date");
                                                    inputAlert.scrollIntoView(true);
                                                    $(this).addClass("error");
                                                    validationError = true;
                                                }
                                            } else {
                                                $("#error_validation").show();
                                                var inputAlert = document.getElementById("select_date");
                                                inputAlert.scrollIntoView(true);
                                                $(this).addClass("error");
                                                validationError = true;
                                            }
                                            break;

                                        case "MasterCard": // MasterCard
                                            if (creditCardNumberLength == 16) {
                                                issuerIdSample = parseInt(creditCardNumber.substring(0, 1));

                                                if (issuerIdSample == 2) {
                                                    issuerId = parseInt(creditCardNumber.substring(0, 6))
                                                } else if (issuerIdSample == 5) {
                                                    issuerId = parseInt(creditCardNumber.substring(0, 2));
                                                }

                                                if ((issuerId >= 222100 && issuerId <= 272099) || (issuerId >= 51 && issuerId <= 55)) {
                                                    if (stripe === "nostripe") {
                                                        checkCreditCard = valid_credit_card(creditCardNumber);
                                                        if (checkCreditCard) {
                                                            $(this).removeClass("error");
                                                        } else {
                                                            $("#error_validation").show();
                                                            var inputAlert = document.getElementById("select_date");
                                                            inputAlert.scrollIntoView(true);
                                                            $(this).addClass("error");
                                                            validationError = true;
                                                        }
                                                    }
                                                } else {
                                                    $("#error_validation").show();
                                                    var inputAlert = document.getElementById("select_date");
                                                    inputAlert.scrollIntoView(true);
                                                    $(this).addClass("error");
                                                    validationError = true;
                                                }
                                            } else {
                                                $("#error_validation").show();
                                                var inputAlert = document.getElementById("select_date");
                                                inputAlert.scrollIntoView(true);
                                                $(this).addClass("error");
                                                validationError = true;
                                            }
                                            break;

                                        case "Discover": // Discover
                                            if (creditCardNumberLength == 16) {
                                                issuerIdSample = parseInt(creditCardNumber.substring(0, 2));

                                                switch (issuerIdSample) {

                                                    case 60:
                                                        issuerId = parseInt(creditCardNumber.substring(0, 4));
                                                        break;

                                                    case 62:
                                                        issuerId = parseInt(creditCardNumber.substring(0, 6));
                                                        break;

                                                    case 64:
                                                        issuerId = parseInt(creditCardNumber.substring(0, 3));
                                                        break;
                                                }

                                                if ((issuerId == 6011) || (issuerId >= 622126 && issuerId <= 622925) || (issuerId >= 644 && issuerId <= 649) || (issuerId == 65)) {

                                                    if (stripe === "nostripe") {
                                                        checkCreditCard = valid_credit_card(creditCardNumber);
                                                        if (checkCreditCard) {
                                                            $(this).removeClass("error");
                                                        } else {
                                                            $("#error_validation").show();
                                                            var inputAlert = document.getElementById("select_date");
                                                            inputAlert.scrollIntoView(true);
                                                            $(this).addClass("error");
                                                            validationError = true;
                                                        }
                                                    }
                                                } else {
                                                    $("#error_validation").show();
                                                    var inputAlert = document.getElementById("select_date");
                                                    inputAlert.scrollIntoView(true);
                                                    $(this).addClass("error");
                                                    validationError = true;
                                                }
                                            } else {
                                                $("#error_validation").show();
                                                var inputAlert = document.getElementById("select_date");
                                                inputAlert.scrollIntoView(true);
                                                $(this).addClass("error");
                                                validationError = true;
                                            }
                                            break;

                                        case "Diners Club": // Diners Club
                                            if (creditCardNumberLength == 14) {
                                                issuerIdSample = parseInt(creditCardNumber.substring(0, 2));

                                                if (issuerIdSample == 30) {
                                                    issuerId = parseInt(creditCardNumber.substring(0, 3));
                                                } else {
                                                    issuerId = issuerIdSample;
                                                }

                                                if ((issuerId >= 300 && issuerId <= 305) || (issuerId == 36) || (issuerId == 54)) {
                                                    if (stripe === "nostripe") {
                                                        checkCreditCard = valid_credit_card(creditCardNumber);
                                                        if (checkCreditCard) {
                                                            $(this).removeClass("error");
                                                        } else {
                                                            $("#error_validation").show();
                                                            var inputAlert = document.getElementById("select_date");
                                                            inputAlert.scrollIntoView(true);
                                                            $(this).addClass("error");
                                                            validationError = true;
                                                        }
                                                    }
                                                } else {
                                                    $("#error_validation").show();
                                                    var inputAlert = document.getElementById("select_date");
                                                    inputAlert.scrollIntoView(true);
                                                    $(this).addClass("error");
                                                    validationError = true;
                                                }
                                            } else {
                                                $("#error_validation").show();
                                                var inputAlert = document.getElementById("select_date");
                                                inputAlert.scrollIntoView(true);
                                                $(this).addClass("error");
                                                validationError = true;
                                            }
                                            break;

                                        case "JCB": // JCB
                                            if (creditCardNumberLength == 15 || creditCardNumberLength == 16) {
                                                issuerId = parseInt(creditCardNumber.substring(0, 4));

                                                if (issuerId >= 3528 && issuerId <= 3589) {

                                                    if (stripe === "nostripe") {
                                                        checkCreditCard = valid_credit_card(creditCardNumber);
                                                        if (checkCreditCard) {
                                                            $(this).removeClass("error");
                                                        } else {
                                                            $("#error_validation").show();
                                                            var inputAlert = document.getElementById("select_date");
                                                            inputAlert.scrollIntoView(true);
                                                            $(this).addClass("error");
                                                            validationError = true;
                                                        }
                                                    }
                                                } else {
                                                    $("#error_validation").show();
                                                    var inputAlert = document.getElementById("select_date");
                                                    inputAlert.scrollIntoView(true);
                                                    $(this).addClass("error");
                                                    validationError = true;
                                                }
                                            } else {
                                                $("#error_validation").show();
                                                var inputAlert = document.getElementById("select_date");
                                                inputAlert.scrollIntoView(true);
                                                $(this).addClass("error");
                                                validationError = true;
                                            }
                                            break;

                                        default:
                                            $(this).removeClass("error");
                                            $("#error_validation").hide();
                                            break;
                                    }
                                } else {
                                    $("#error_validation").show();
                                    var inputAlert = document.getElementById("select_date");
                                    inputAlert.scrollIntoView(true);
                                    $(this).addClass("error");
                                    validationError = true;
                                }
                                break;

                            default:
                                $(this).removeClass("error");
                                break;
                        }
                    }

                    function valid_credit_card(value) { // Luhn validation

                        if (/[^0-9-\s]+/.test(value)) return false;
                        var nCheck = 0,
                            nDigit = 0,
                            bEven = false;
                        value = value.replace(/\D/g, "");

                        for (var n = value.length - 1; n >= 0; n--) {
                            var cDigit = value.charAt(n),
                                nDigit = parseInt(cDigit, 10);

                            if (bEven) {
                                if ((nDigit *= 2) > 9) nDigit -= 9;
                            }

                            nCheck += nDigit;
                            bEven = !bEven;
                        }

                        return (nCheck % 10) == 0;
                    }
                } else if ($(this).is("select.contact")) { // Validation of SELECT fields.
                    var selectId = $(this).attr("id");
                    var selectVal = $(this).children("option:selected").val();
                    var todayPayment = new Date();
                    var currentYearPayment = todayPayment.getFullYear();
                    var currentMonthPayment = todayPayment.getMonth() + 1;

                    if (selectVal === "") {
                        $("#error_validation").show();
                        var inputAlert = document.getElementById("select_date");
                        inputAlert.scrollIntoView(true);
                        $(this).addClass("error");
                        validationError = true;
                    } else if (selectId === "payment-year") { // Payment-month
                        var selectedYearPayment = $(this).children("option:selected").val();
                        var selectedMonthPayment = $("#payment-month").children("option:selected").val();

                        if (currentYearPayment == selectedYearPayment && selectedMonthPayment < currentMonthPayment) {
                            $("#error_validation").show();
                            var inputAlert = document.getElementById("select_date");
                            inputAlert.scrollIntoView(true);
                            $("#payment-month").addClass("error");
                            validationError = true;
                        } else {
                            if (selectedMonthPayment !== "") {
                                $("#payment-month").removeClass("error");
                            }
                            $(this).removeClass("error");
                        }
                    }
                } else if ($(this).is("input[type='checkbox']")) { // Validation of CHECKBOXES.
                    var checkName = $(this).data("check");
                    var checkLabelName = "";

                    if ($(this).prop("checked") == true) {

                        $("label.contact").each(function() {
                            checkLabelName = $(this).data("check");

                            if (checkName === checkLabelName) {
                                $(this).removeClass("w3-text-red");
                            }
                        });
                    } else {
                        $("#error_validation").show();
                        var inputAlert = document.getElementById("select_date");
                        inputAlert.scrollIntoView(true);
                        validationError = true;

                        $("label.contact").each(function() {
                            checkLabelName = $(this).data("check");

                            if (checkName === checkLabelName) {
                                $(this).addClass("w3-text-red");
                            }
                        });
                    }
                }
            });

            /**
             * Data of Booking
             * Submission
             * Thankyou page
             */

            if (validationError == false) {

                /* Data of Booking */
                var data = {};

                // Booking period
                data.booking_period = {
                    check_in: $("input#check_in_date").val(),
                    check_out: $("input#check_out_date").val()
                }

                //Rooms & extras
                data['rooms'] = [];
                data['extras'] = [];

                $("select.room").each(function() {
                    var roomQuantity = $(this).val();
                    var submitRoomId, submitRoomQuantity, submitAdults, submitChildren, avgRoomRate, submitExtraQuantity, submitExtraId, submitExtraRate;

                    if (roomQuantity !== "rooms_quantity") { // Rooms' data                
                        submitRoomId = $(this).data("id");
                        submitRoomQuantity = parseInt(roomQuantity);
                        $("select.adults[data-id='" + submitRoomId + "']").val() !== "rooms_adult" ? submitAdults = parseInt($("select.adults[data-id='" + submitRoomId + "']").val()) : "";
                        $("select.children[data-id='" + submitRoomId + "']").val() !== "rooms_children" ? submitChildren = parseInt($("select.children[data-id='" + submitRoomId + "']").val()) : submitChildren = 0;
                        avgRoomRate = parseFloat($("span.acc_rate[data-id='" + submitRoomId + "']").text());

                        data['rooms'].push({
                            roomId: submitRoomId,
                            quantity: submitRoomQuantity,
                            adults: submitAdults,
                            children: submitChildren,
                            avgRoomRate: avgRoomRate,
                        });

                        $("select.extras-nights[data-id='" + submitRoomId + "']").each(function() { // Extras' data
                            var extraQuantity = $(this).val();

                            if (extraQuantity !== "extra_selector") {
                                submitExtraQuantity = parseInt(extraQuantity);
                                submitExtraId = $(this).data("extra_id");
                                var extraRate = $("span#ext_avg_rate[data-id='" + submitRoomId + submitExtraId + "']").text();
                                submitExtraRate = parseFloat(extraRate).toFixed(2);

                                data['extras'].push({
                                    extra_room_id: submitRoomId,
                                    extra_id: submitExtraId,
                                    extra_quantity: submitExtraQuantity,
                                    extra_rate: submitExtraRate
                                });
                            }
                        });
                    }
                });

                // Contact Information
                var comment = $("textarea#comment").val() !== "" ? $("textarea#comment").val() : "";

                data.contact = {
                    first_name: $("input#first-name").val(),
                    last_name: $("input#last-name").val(),
                    address: $("input#address").val(),
                    city: $("input#city").val(),
                    state: $("input#state").val(),
                    zip: $("input#zip").val(),
                    country: $("select#country").val(),
                    phone: $("input#phone").val(),
                    email: $("input#email").val(),
                    check_in_time: $("select#check-in").val(),
                    check_out_time: $("select#check-out").val(),
                    comment: comment
                };

                // Payment information
                data.payment = {
                    credit_type: $("select#credit-type").val(),
                    credit_number: $("input#credit-card").val(),
                    month: $("select#payment-month").val(),
                    year: $("select#payment-year").val(),
                    cvc: $("input#cvc").val(),
                };

                // Booking Summary
                var newsLetter;

                if ($("input#newsletter_subsc").length > 0) {
                    newsLetter = $("input#newsletter_subsc").is(":checked") ? '0' : '1';
                } else {
                    newsLetter = false;
                }

                var depositNew;
                var depositOrig = $("span#deposit_sum").text();

                if (depositOrig.length >= 8) {
                    var depPart = depositOrig.split(',');
                    depositNew = depPart[0] + depPart[1];
                } else {
                    depositNew = depositOrig;
                }

                data.summary = {
                    accommodation: $("span#acc_sum").text(),
                    extras: $("span#extr_sum").text(),
                    subtotal01: $("span#subtotal_01").text(),
                    discount_id: $("span#disc_name").data("discId"),
                    discount_amount: $("span#disc_sum").text(),
                    subtotal02: $("span#subtotal_02").text(),
                    total: $("span#booking_sum").text(),
                    deposit: depositNew,
                    balance: $("span#balance_sum").text(),
                    newsletter: newsLetter
                };

                // Surcharges
                data['surcharge'] = [];

                $("p.surcharge").each(function() {
                    var surchargeId = $(this).data("id");

                    data['surcharge'].push({
                        surcharge_name: $("span.surchargeName[id='" + surchargeId + "']").text(),
                        surcharge_amount: parseFloat($("span.surchargeAmount[id='" + surchargeId + "']").text()).toFixed(2)
                    });
                });

                // Taxes
                data['tax'] = [];

                $("p.taxes").each(function() {
                    var taxId = $(this).data("id");

                    data['tax'].push({
                        tax_name: $("span.taxName[id='" + taxId + "']").text(),
                        tax_amount: $("span.taxAmount[id='" + taxId + "']").text(),
                    });
                });

                // Cancellation Policy
                var cancellation = false;
                var privacy = false;
                var square = false;

                if ($("input#cancellation_policy").length > 0)
                    cancellation = $("input#cancellation_policy").is(":checked") ? '1' : '0';

                if ($("input#privacy_policy").length > 0)
                    privacy = $("input#privacy_policy").is(":checked") ? '1' : '0';

                if ($("input#square_card").length > 0)
                    square = $("input#square_card").is(":checked") ? '1' : '0';

                data.policy = {
                    cancellation: cancellation,
                    privacy: privacy,
                    square: square
                };

                if ($("span#processor").data("processor") === "square") { // Form submission if payment processor is Square
                    var nonce;
                    validationError = $("span#error_msg").text() !== '' ? true : false;

                    var getNonce = setInterval(function() {
                        nonce = $("input#card-nonce").val();

                        if (nonce !== '') {
                            clearInterval(getNonce);
                            data.payment.square_nonce = nonce;

                            validationError == false ? submitForm() : '';
                        }
                    }, 3000);
                } else { // Form submission if the payment processor is NOT Square                    
                    validationError == false ? submitForm() : '';
                }

                function submitForm() {

                    $.ajax({
                        url: fullAppRoot + "engine/form_ajax.php",
                        type: "POST",
                        data: { method: "submission", mode: btnId, lang: lang, data: data },
                        dataType: "html",
                        success: function(response) {

                            if (response === "success") {
                                $.ajax({
                                    url: fullAppRoot + "engine/form_ajax.php",
                                    type: "POST",
                                    data: { method: "thankyou", id: btnId, lang: lang },
                                    dataType: "html",
                                    cache: false,
                                    success: function(response) {
                                        var thankyouResult = JSON.parse(response);
                                        var thankyouAction = Object.keys(thankyouResult);
                                        $(".engine_form").hide();

                                        for (var i = 0; i < thankyouAction.length; i++) {

                                            if (thankyouAction[i] === "lang") {
                                                $("#thankyou_msg").html(thankyouResult[thankyouAction[i]]);
                                                $(".thankyou").show();
                                                window.parent.scroll(0, 0);
                                            } else {
                                                window.top.location.href = thankyouResult[thankyouAction[i]];
                                            }
                                        }
                                    }
                                });
                                sessionStorage.clear();
                            } else if (response !== "success") {
                                $("div#error_stripe").show();

                                //$("span#error_msg").html(' ' + response);

                                if ($("span#processor").data("processor") === "square") {
                                    var error = JSON.parse(response);
                                    $("span#error_msg").html(' ' + error.errors[0].detail);
                                } else {
                                    $("span#error_msg").html(' ' + response);
                                }

                                var inputAlert = document.getElementById("select_date");
                                inputAlert.scrollIntoView(true);
                                $("button#processing").hide();
                                $("button.submit").show().attr("disabled", false);
                            }
                        }
                    });
                }
            } else {
                $("button#processing").hide();
                $(this).show();
                $(this).attr("disabled", false);
                return false;
            }
        });
    });
});