<?php
session_start();
require_once('../base_include.php');
require_once(APP_ROOT . '/config.php');
require_once(APP_ROOT . '/classes/MyPDO.php');
require_once(APP_ROOT . '/classes/DatabasePDO.php');
require_once(APP_ROOT . '/classes/ShortCodes.php');

$method = $_REQUEST['method'];

call_user_func("form_ajax_$method");

/** LANGUAGE SELECTOR */

/** Language Selector for FlatPickr fields */

function form_ajax_language() { 
    $lang = Data::getMessages();
    echo json_encode($lang);  
}

/** ShortCodes checking for other fields than FlatPickr (language selector) */

function form_ajax_formShortCode() {  
    
    $pdo = DatabasePDO::getInstance();  
    $codes = new ShortCodes();
    $langData = Data::getMessages();
    $rawData = json_decode($_REQUEST['data']);
    $selectedLang = $_REQUEST['language'];
    $code = '';    
    $result = [];
    
    foreach($rawData as $langLabel => $langValue) {

        $tableSelect = explode('_', $langLabel);
        
        $langLabelBack = '';

        if($tableSelect[0] === 'step') { // Shortcode checking of language labels polled from messages datatable.
        
            if($selectedLang == 1) {
                $langLabelBack = $langLabel;
            }
            else {
                $langLabelBack = $langLabel . '_sec_lang';
            }
            $code = $langData[$langLabelBack];
            $readyString = $codes->replaceShortCodeLabels($code, false, false, false);        
            $result['messages'][$langLabel] = $readyString;
        }

        if($tableSelect[0] !== 'step') { // Shortcode checking of data pulled from other datatables than mesages (indicated in comment lines assigned to every case).

            $tableName = $tableSelect[0];
            $itemId = $tableSelect[1];

            switch($tableName) {

                case 'rooms': // Rooms                
                $q = $pdo->query("SELECT description, description_sec_lang, description_short, description_short_sec_lang, image, adults_max, children_max FROM $tableName WHERE id = '$itemId' LIMIT 1");
                $q->setFetchMode(PDO::FETCH_ASSOC);
                $row = $q->fetch();               
                $desc = $codes->replaceShortCodeLabels($row['description'], false, false, false);
                $descSecLang = $codes->replaceShortCodeLabels($row['description_sec_lang'], false, false, false);
                $descShort = $codes->replaceShortCodeLabels($row['description_short'], false, false, false);
                $descShortSecLang = $codes->replaceShortCodeLabels($row['description_short_sec_lang'], false, false, false);                
                if($selectedLang == 1) {
                    $result['rooms']['rooms_' . $itemId]['desc'] = $desc;
                    $result['rooms']['rooms_' . $itemId]['descShort'] = $descShort;                                                           
                }
                else {
                    $result['rooms']['rooms_' . $itemId]['desc'] = $descSecLang;
                    $result['rooms']['rooms_' . $itemId]['descShort'] = $descShortSecLang;                                                                       
                }
                $result['rooms']['rooms_' . $itemId]['image'] = $row['image'];
                $result['rooms']['rooms_' . $itemId]['adults_max'] = $codes->replaceShortCodeLabels($row['adults_max'], false, false, false);
                $result['rooms']['rooms_' . $itemId]['children_max'] = $codes->replaceShortCodeLabels($row['children_max'], false, false, false);                            
                break;           

                case 'upsells': // Upsells
                $q = $pdo->query("SELECT image, name, name_sec_lang, description, description_sec_lang  FROM $tableName WHERE id = '$itemId' LIMIT 1");
                $q->setFetchMode(PDO::FETCH_ASSOC);
                $row = $q->fetch();
                $name = $codes->replaceShortCodeLabels($row['name'], false, false, false);
                $nameSecLang = $codes->replaceShortCodeLabels($row['name_sec_lang'], false, false, false);
                $desc = $codes->replaceShortCodeLabels($row['description'], false, false, false);
                $descSecLang = $codes->replaceShortCodeLabels($row['description_sec_lang'], false, false, false);
                if($selectedLang == 1) {
                    $result['upsells']['upsells_' . $itemId]['name'] = $name;
                    $result['upsells']['upsells_' . $itemId]['desc'] = $desc;
                    
                }
                else {
                    $result['upsells']['upsells_' . $itemId]['name'] = $nameSecLang;
                    $result['upsells']['upsells_' . $itemId]['desc'] = $descSecLang;           
                }
                $result['upsells']['upsells_' . $itemId]['image'] = $row['image']; 
                break;
                
                case 'discounts': // Discounts
                $q = $pdo->query("SELECT name, name_sec_lang FROM $tableName WHERE id = '$itemId' LIMIT 1");
                $q->setFetchMode(PDO::FETCH_ASSOC);
                $row = $q->fetch();
                $name = $codes->replaceShortCodeLabels($row['name'], false, false, false);
                $nameSecLang = $codes->replaceShortCodeLabels($row['name_sec_lang'], false, false, false);                
                if($selectedLang == 1) {
                    $result['discounts']['discounts_' . $itemId]['name'] = $name;                     
                }
                else {
                    $result['discounts']['discounts_' . $itemId]['name'] = $nameSecLang;                    
                }
                break;

                case 'taxes': // Taxes and surcharges
                $q = $pdo->query("SELECT name, name_sec_lang FROM $tableName WHERE id = '$itemId' LIMIT 1");
                $q->setFetchMode(PDO::FETCH_ASSOC);
                $row = $q->fetch();
                $name = $codes->replaceShortCodeLabels($row['name'], false, false, false);
                $nameSecLang = $codes->replaceShortCodeLabels($row['name_sec_lang'], false, false, false);
                if($selectedLang == 1) {
                    $result['taxes']['taxes_' . $itemId]['name'] = $name;
                }
                else {
                    $result['taxes']['taxes_' . $itemId]['name'] = $nameSecLang;
                }
                break;
            }
        }                    
    }

    // Packing and sending results.

    $finalResult = json_encode($result);    

    echo $finalResult;    
}

/** END OF LANGUAGE SELECTOR */

/** THANK YOU PAGE */

function form_ajax_thankyou() {  
    
    $url = Data::getOptions();
    $message = Data::getMessages();
    $shortcode = new ShortCodes();

    $selectedLang = $_REQUEST['lang']; // Selected language
    $btnId = $_REQUEST['id'];

    $thankyouResult = [];
        
    if($url['integration_tracking_enabled'] == 1) { // If the client provided thankyou message URL
        $thankyouResult['url'] = $url['integration_destination_url'];    
    }
    else { // If the client had not provided thankyou message URL
        switch($selectedLang) {

            case '1':
            ($btnId === 'submit') ? $thankyouResult['lang'] = $shortcode->replaceShortCodeLabels($message['step_9_confirmation_message'], $_SESSION['resNum'], false, false) : '';
            ($btnId === 'request') ? $thankyouResult['lang'] = $shortcode->replaceShortCodeLabels($message['step_9_confirmation_message_request'], false, false, false) : '';
            break;

            case '2':
            ($btnId === 'submit') ? $thankyouResult['lang'] = $shortcode->replaceShortCodeLabels($message['step_9_confirmation_message_sec_lang'], $_SESSION['resNum'], false, false) : '';
            ($btnId === 'request') ? $thankyouResult['lang'] = $shortcode->replaceShortCodeLabels($message['step_9_confirmation_message_request_sec_lang'], false, false, false) : '';
            break;
        }
    }
    echo json_encode($thankyouResult);
    $_SESSION['frontend_submission'] = false;    
}

/** DISCOUNT */

function form_ajax_discount() { // Discount selection
    $pdo = DatabasePDO::getInstance();
    $discountCode = trim(strip_tags($_REQUEST['code']));
    $lang = $_REQUEST['lang'];
    $discountName = $pdo->query("SELECT id, name, name_sec_lang FROM discounts WHERE code = '$discountCode' LIMIT 1");
    $discountName->setFetchMode(PDO::FETCH_ASSOC);
    $discName = $discountName->fetch();
    $result = [];       
    
    if(!empty($discName)) {
        
        switch($lang) {
            
            case '1':
            $result = ['id' => $discName['id'], 'name' => $discName['name']];
            echo json_encode($result);           
            break;

            case '2':
            $result = ['id' => $discName['id'], 'name' => $discName['name_sec_lang']];
            echo json_encode($result);           
            break;
        }          
    }
    else {        
        echo json_encode($result);
    }
}

function form_ajax_discount_select() { // Language selection of discounts
    $pdo = DatabasePDO::getInstance();    
    $lang = Data::getMessages();    
    echo json_encode($lang);
}

function form_ajax_discount_list() { // List of NONE CODE BASED discounts
    $pdo = DatabasePDO::getInstance();

    // Determination of current date for discounts based on visibility
    $getTimezone = Data::getOptions();
    $timezone = empty($getTimezone) ? 'US/Pacific' : $getTimezone['timezone'];
    $displayDate = new DateTime("now", new DateTimeZone($timezone));
    $date = $displayDate->format('Y-m-d');

    // Dates for booking period based discounts
    $checkInDate = strip_tags(trim($_REQUEST['check_in']));
    $checkOutDate = strip_tags(trim($_REQUEST['check_out']));

    // Calculation of number of days (booking period)
    $checkInDT = new DateTime($checkInDate);
    $checkOutDT = new DateTime($checkOutDate);
    $dateDiff = $checkInDT->diff($checkOutDT);
    $dayDiff = $dateDiff->days;

    // Selected language
    $lang = strip_tags(trim($_REQUEST['selLang']));
    
    $discountList = [];

    if($dayDiff > 1) {
        // Booking period based discounts    
        $discountBookingQuery = $pdo->query("SELECT id, name, name_sec_lang, category FROM discounts WHERE ('$checkInDate' >= CAST(start_date AS DATE) AND '$checkOutDate' <= CAST(expiry_date AS DATE)) AND (code = 'N/A') AND (based_on = 'booking')");
        $discountBookingQuery->setFetchMode(PDO::FETCH_ASSOC);

        if($discountBookingQuery->rowCount()) {

            while($discountBookingResult = $discountBookingQuery->fetch()) {            
                $discountList[] = $discountBookingResult;
            }
        }

        // Visibility based discounts
        $discountVisibleQuery = $pdo->query("SELECT id, name, name_sec_lang, category FROM discounts WHERE ('$date' >= CAST(start_date AS DATE) AND '$date' <= CAST(expiry_date AS DATE)) AND (('$checkInDate' >= CAST(start_date AS DATE) AND '$checkOutDate' < DATE_ADD(expiry_date, INTERVAL 2 DAY)) OR ('$checkInDate' > CAST(expiry_date AS DATE))) AND (code = 'N/A') AND (based_on = 'visibility')");
        $discountVisibleQuery->setFetchMode(PDO::FETCH_ASSOC);

        if($discountVisibleQuery->rowCount()) {

            while($discountVisibleResult = $discountVisibleQuery->fetch()) {
                $discountList[] = $discountVisibleResult;
            }
        }
    }

    else if($dayDiff == 1) {
        // Booking period based discounts    
        $discountBookingQuery = $pdo->query("SELECT id, name, name_sec_lang, category FROM discounts WHERE ('$checkInDate' >= CAST(start_date AS DATE) AND '$checkInDate' <= CAST(expiry_date AS DATE)) AND (code = 'N/A') AND (based_on = 'booking')");
        $discountBookingQuery->setFetchMode(PDO::FETCH_ASSOC);

        if($discountBookingQuery->rowCount()) {

            while($discountBookingResult = $discountBookingQuery->fetch()) {            
                $discountList[] = $discountBookingResult;
            }
        }

        // Visibility based discounts
        $discountVisibleQuery = $pdo->query("SELECT id, name, name_sec_lang, category FROM discounts WHERE ('$date' >= CAST(start_date AS DATE) AND '$date' <= CAST(expiry_date AS DATE)) AND (code = 'N/A') AND (based_on = 'visibility')");
        $discountVisibleQuery->setFetchMode(PDO::FETCH_ASSOC);

        if($discountVisibleQuery->rowCount()) {

            while($discountVisibleResult = $discountVisibleQuery->fetch()) {
                $discountList[] = $discountVisibleResult;
            }
        }
    }

    echo json_encode($discountList);
}

/** ROOM AND EXTRAS AVAILABILITY */

function form_ajax_getRooms() {

    $pdo = DatabasePDO::getInstance();

    $codes = new ShortCodes();
    $currency = Data::getOptions();
    $messages = Data::getMessages();    

    // Getting and formatting dates

    $checkInDate = trim(strip_tags($_REQUEST['checkInDate']));
    $checkOutDate = trim(strip_tags($_REQUEST['checkOutDate']));
    $currentDay = date('Y-m-d');
    $currentDayDT = new DateTime($currentDay); 
    $lang = $_REQUEST['lang'];
    $checkInDT = new DateTime($checkInDate);
    $checkOutDT = new DateTime($checkOutDate);
    $dateDiff = $checkInDT->diff($checkOutDT);
    $dayDiff = $dateDiff->days;
    $dateDiffDiscount = $currentDayDT->diff($checkInDT);
    $dayDiffDiscount = $dateDiffDiscount->days;  
                
    $availability = [];
    $extras = [];    
               
    $availability['rooms'] = Data::getRooms($checkInDT, $checkOutDT);  // ROOM'S AVAILABILITY DATA BASED ON CHECK-IN & CHECK-OUT DATES
    
    usort($availability['rooms'], function($a, $b){ return strcmp($a["room"], $b["room"]); }); // Sorting array to an ascending order    
        
    $arrayIndex = (int)0;
   
    foreach($availability['rooms'] AS $checked) { // Checking of Minimum Stay Rule, Custom Rates
        $roomId = $checked['id'];                                           
        $availability['rooms'][$arrayIndex]['minimum_nights'] = '0';
        
        if($checked['available_left'] > 0) {
            $minStay = Data::getMinimumNights($checked['id']);          
                       
            if(!empty($minStay)) {  // Minimun Nights Check               
                
                foreach($minStay as $stay) {                                            
                                                           
                   if((int)$stay['room_type_id'] === (int)$checked['id']) {                                                                                 
                        $minStayStart = new DateTime($stay['start_date']);
                        $minStayEnd = new DateTime($stay['end_date']);
                    
                        if(($checkInDT >= $minStayStart) && ($checkOutDT <= $minStayEnd)) {
                            if($dayDiff < $stay['minimum_nights']) {
                                $availability['rooms'][$arrayIndex]['minimum_nights'] = '1';                                
                           }                            
                        }                                                                                     
                    }                                      
                }               
            }                    
        }
        ++$arrayIndex;                                                           
    }  
    
    // Shortcode check (Rooms)

    foreach($availability['rooms'] as $shortCodeCheck) {
        foreach($shortCodeCheck as $key => $value) {
            $key = $codes->replaceShortCodeLabels($value, false, false, false);            
        }              
    }                                
    
    foreach($availability['rooms'] as $extrasList) { // Getting extras data from database        
        $roomTypeId = $extrasList['id'];        
    
        $extrasQuery = $pdo->query("SELECT u.id, u.image, u.name, u.name_sec_lang, u.description, u.description_sec_lang, u.price, u.available_checkout, u.exclude_discounts, ru.room_type_id FROM upsells AS u INNER JOIN room_upsells AS ru ON(u.id = ru.upsell_id) WHERE (ru.room_type_id = '$roomTypeId') AND (u.active = 1)");
        $extrasQuery->setFetchMode(PDO::FETCH_ASSOC);

        if($extrasQuery->rowCount()) { 
            
            $availability['extras'][$roomTypeId] = [];
                        
           while($extrasResult = $extrasQuery->fetch()) {               

                if(!empty($extrasResult['id'])) {                               
                    $availability['extras'][$roomTypeId][$extrasResult['id']] = [];            
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['room_id'] = $roomTypeId;
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_id'] = $extrasResult['id'];
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_name'] = $extrasResult['name'];
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_name_sec_lang'] = $extrasResult['name_sec_lang'];
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_description'] = $extrasResult['description'];
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_description_sec_lang'] = $extrasResult['description_sec_lang'];
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_image'] = $extrasResult['image'];
                    $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_price'] = $extrasResult['price'];                  
                    ($extrasResult['available_checkout'] == 0) ? $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_available_checkout'] = 'No' : $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_available_checkout'] = 'Yes';
                    ($extrasResult['exclude_discounts'] == 0) ? $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_exclude_discounts'] = 'No' : $availability['extras'][$roomTypeId][$extrasResult['id']]['extra_exclude_discounts'] = 'Yes'; 
                }

                // Shortcode check (Extras)
                
                foreach($availability['extras'][$roomTypeId][$extrasResult['id']] as $key => $value) {
                    $key = $codes->replaceShortCodeLabels($value, false, false, false);                                      
                }   
            }                        
        }        
    }     
     
    // Checking for blackouts (Hiding discount panel/showing blackout error message)

    $bookedDays = [$checkInDate];        

    for($bd = 1; $bd <= $dayDiff; $bd++) {  
        $date = strtotime("+" . $bd .  "day", strtotime($bookedDays[0]));
        $days = date('Y-n-j', $date);
        array_push($bookedDays, $days);
    }

    foreach($bookedDays as $d) {
        $blackOutQuery = $pdo->query("SELECT id, blackout_place, blackout_effect FROM blackout WHERE ('$d' >= CAST(start_date AS DATE)) AND ('$d' <= CAST(end_date AS DATE))");
        $blackOutQuery->setFetchMode(PDO::FETCH_ASSOC);                

        if($blackOutQuery->rowCount()) {

            while($blackOutResult = $blackOutQuery->fetch()) {

                if(strlen($blackOutResult['blackout_effect']) > 1) {
                    $blackoutEffect = explode(',', $blackOutResult['blackout_effect']);

                    foreach($blackoutEffect as $effect) {                                            
                        $effect === '1' ? $blackOutEffectValue = '1' : '';                                        
                    }
                }
                else {
                    $blackOutEffectValue = $blackOutResult['blackout_effect'];
                }
                if(strlen($blackOutResult['blackout_place']) > 1) {
                    $blackoutPlace = explode(',', $blackOutResult['blackout_place']);

                    foreach($blackoutPlace as $place) {
                        $place === '1' ? $blackOutPlaceValue = '1' : '';                                              
                    }
                }
                else {
                    $blackOutPlaceValue = $blackOutResult['blackout_place'];
                }            

                if($blackOutEffectValue === '1' && $blackOutPlaceValue === '1') {
                    $availability['discountPanelHide'][] = '1';
                }
                else {
                    $availability['discountPanelHide'][] = '0';
                }            
            }
            $availability['blackout'][] = '1';
        }
        else {
            $availability['blackout'][] = '0';
            $availability['discountPanelHide'][] = '0';
        }
    }    

    /** Adding plus data (e.g for selectors) */
    
    // Options table

    $availability['currency'] = $codes->replaceShortCodeLabels($currency['currency_symbol'], false, false, false);
    $availability['thousand'] = $currency['thousand_separator'];
    $availability['decimal'] = $currency['decimal_separator'];

    // Messages table

    $availability['average_rate_tooltip'] = $codes->replaceShortCodeLabels($messages['step_3_rate_tooltip'], false, false, false);
    $availability['average_rate_tooltip_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_rate_tooltip_sec_lang'], false, false, false);
    $availability['rate_tooltip'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_rate'], false, false, false);
    $availability['rate_tooltip_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_rate_sec_lang'], false, false, false);
    $availability['room_quantity_selector'] = $codes->replaceShortCodeLabels($messages['step_3_quantity_selector'], false, false, false);
    $availability['room_quantity_selector_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_quantity_selector_sec_lang'], false, false, false);
    $availability['adults_selector'] = $codes->replaceShortCodeLabels($messages['step_3_adults_selector'], false, false, false);
    $availability['adults_selector_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_adults_selector_sec_lang'], false, false, false);
    $availability['children_selector'] = $codes->replaceShortCodeLabels($messages['step_3_children_selector'], false, false, false);
    $availability['children_selector_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_children_selector_sec_lang'], false, false, false);
    $availability['max_adults'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_maximum_adults'], false, false, false);
    $availability['max_adults_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_maximum_adults_sec_lang'], false, false, false);
    $availability['max_children'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_maximum_children'], false, false, false);
    $availability['max_children_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_maximum_children_sec_lang'], false, false, false);
    $availability['max_occupants'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_maximum_occupants'], false, false, false);
    $availability['max_occupants_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_3_thumbnail_popup_maximum_occupants_sec_lang'], false, false, false);

    $availability['extra_heading'] = $codes->replaceShortCodeLabels($messages['step_4_heading'], false, false, false);
    $availability['extra_heading_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_heading_sec_lang'], false, false, false);
    $availability['extra_intro'] = $codes->replaceShortCodeLabels($messages['step_4_intro'], false, false, false);
    $availability['extra_intro_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_intro_sec_lang'], false, false, false);
    $availability['extra_rate_tooltip'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_rate'], false, false, false);
    $availability['extra_rate_tooltip_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_rate_sec_lang'], false, false, false);
    $availability['extra_available_on_checkout'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_available_on_checkout'], false, false, false);
    $availability['extra_available_on_checkout_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_available_on_checkout_sec_lang'], false, false, false);
    $availability['extra_available_on_checkout_yes'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_available_on_checkout_yes'], false, false, false);
    $availability['extra_available_on_checkout_yes_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_available_on_checkout_yes_sec_lang'], false, false, false);
    $availability['extra_available_on_checkout_no'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_available_on_checkout_no'], false, false, false);
    $availability['extra_available_on_checkout_no_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_available_on_checkout_no_sec_lang'], false, false, false);
    $availability['extra_exclude_discounts_label'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_excluded_from_discounts'], false, false, false);
    $availability['extra_exclude_discounts_label_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_excluded_from_discounts_sec_lang'], false, false, false);
    $availability['extra_exclude_discounts_yes'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_excluded_from_discounts_yes'], false, false, false);
    $availability['extra_exclude_discounts_yes_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_excluded_from_discounts_yes_sec_lang'], false, false, false);
    $availability['extra_exclude_discounts_no'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_excluded_from_discounts_no'], false, false, false);
    $availability['extra_exclude_discounts_no_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_thumbnail_popup_excluded_from_discounts_no_sec_lang'], false, false, false);
    $availability['no_extras'] = $codes->replaceShortCodeLabels($messages['step_4_no_extras'], false, false, false);
    $availability['no_extras_sec_lang'] = $codes->replaceShortCodeLabels($messages['step_4_no_extras_sec_lang'], false, false, false);                       

    // Packing and sending   
    
   echo json_encode($availability);    
}

/** RATE CALCULATIONS LOGIC
 * Please note that form_ajax.php makes very limited calculations (conversion of percent type db values to numbers) and it rather pulls the necessary values from the db and does the main checkings (e.g blackout periods). 
 * Every significant calculations are done in the JS files. Booking.php is not used at all, all rates are calculated by the new standalone frontend code as follows:
 * 
 * FRONTEND ROOMS PANEL:
 *   
 * Average rate
 * - room_extras.js (line 496-777)
 * - form_ajax.php (line 538-719)
 * 
 * FRONTEND EXTRAS PANEL:
 * - rooms_extras.js (line 779-811)
 * 
 * BOOKING SUMMARY PANEL:
 * 
 * Accommodations:
 * - part of rooms_extras.js (line 496-777; indicated by comment line 'Summary Rate') AND summaryCalculation.js (line 68-83)
 * Extras:
 * - summaryCalculation.js (line 85-110)
 * Subtotal:
 * - summaryCalculation.js (line 115-119)
 * Discount:
 * - summaryCalculation.js (line 121-142: fetching the id/code of discount; line 157-184: determination of extras excluded from discounts; line 186-207: calculation of global discount amount)
 * - form_ajax.php (line 749-796: blackout check; line 800-929: fetching data of global discount; line 931-944: fetching extras excluded from discounts)
 * Surcharge:
 * - summaryCalculation.js (line 208-226)
 * - form_ajax.php (line 946-959)
 * Subtotal:
 * - summaryCalculation.js (line 228-235)
 * Tax:
 * - summaryCalculation.js (line 237-256)
 * - form_ajax.php (line 961-974)
 * Total:
 * - summaryCalculation.js (line 258-269)
 * Deposit:
 * - summaryCalculation.js (line 270-304)
 * - form_ajax.php (line 976-992)
 * Balance:
 * - summaryCalculation.js (line 306-309)  
 */

/** ROOM AVERAGE RATES CALCULATION */

function form_ajax_calculateRoomRate() { 
    $pdo = DatabasePDO::getInstance();
    $separators = Data::getOptions();
    
    $calculationResult = [];
    $roomId = strip_tags(trim($_POST['roomId']));
    $checkInDate = strip_tags(trim($_POST['checkIn']));
    $checkOutDate = strip_tags(trim($_POST['checkOut']));
    $defaultRate = strip_tags(trim($_POST['defRate']));
    $currentDate = date('Y-m-d');
    $calculationResult[$roomId]['thousand'] = $separators['thousand_separator'];
    $calculationResult[$roomId]['decimal'] = $separators['decimal_separator'];    
    
    // Determination of the booked days within the booking period
    
    $checkInDT = new DateTime($checkInDate);
    $checkOutDT = new DateTime($checkOutDate);
    $dateDiff = $checkInDT->diff($checkOutDT);
    $dayDiff = $dateDiff->days;
    $bookedDays = [$checkInDate];
    $discountEndDay = $checkOutDT->modify('-1 day');
    $discountEndDate = $discountEndDay->format('Y-m-d');
    $bookedRateTotal = [];
    $discountRates = [];
    $finalRate = '';
    $blackOutEffectValue = '';   

    for($bd = 1; $bd <= $dayDiff; $bd++) {  
        $date = strtotime("+" . $bd .  "day", strtotime($bookedDays[0]));
        $days = date('Y-n-j', $date);
        array_push($bookedDays, $days);
    }

    // CUSTOM RATES

    foreach($bookedDays as $d) {
        $customRateDaysQuery = $pdo->query("SELECT amount FROM room_rates WHERE room_id = '$roomId' AND ('$d' >= CAST(start_date AS DATE) AND '$d' <= CAST(end_date AS DATE))");
        $customRateDaysQuery->setFetchMode(PDO::FETCH_ASSOC);
        if($customRateDaysQuery->rowCount()) {           
            while($customRateDaysResult = $customRateDaysQuery->fetch()) {
                array_push($bookedRateTotal, $customRateDaysResult['amount']);
            }
        }
        else {
            array_push($bookedRateTotal, $defaultRate);
        }        

        $lastNight = end($bookedRateTotal);
        $bookingAmountSum = array_sum($bookedRateTotal);
        $averageNightPrice = (number_format((float)$bookingAmountSum, 2, '.', '') - (int)$lastNight) / (int)$dayDiff;
        $calculationResult[$roomId]['room'] = $averageNightPrice;
        $calculationResult[$roomId]['roomTotalSum'] = (float)$bookingAmountSum - (int)$lastNight;      
            
        // DISCOUNTS

        // Blackout Check

        $blackOutQuery = $pdo->query("SELECT id, blackout_place, blackout_effect FROM blackout WHERE ('$d' >= CAST(start_date AS DATE)) AND ('$d' <= CAST(end_date AS DATE))");
        $blackOutQuery->setFetchMode(PDO::FETCH_ASSOC);
        $blackOutResult = $blackOutQuery->fetch();

        if(!empty($blackOutResult)) {

            if(strlen($blackOutResult['blackout_place']) > 1) {
                $blackoutPlace = explode(',', $blackOutResult['blackout_place']);

                foreach($blackoutPlace as $place) {

                    if($place !== '2') {
                        if(strlen($blackOutResult['blackout_effect']) > 1) {
                            $blackoutEffect = explode(',', $blackOutResult['blackout_effect']);
            
                            foreach($blackoutEffect as $effect) {
            
                                if($effect === '2') {
                                    $blackOutEffectValue = '2';
                                }                    
                            }
                        }
                        else {
                            $blackOutEffectValue = $blackOutResult['blackout_effect'];
                        }                        
                    }                    
                }
            }
            else {
                if($blackOutResult['blackout_place'] !== '2') {
                    if(strlen($blackOutResult['blackout_effect']) > 1) {
                        $blackoutEffect = explode(',', $blackOutResult['blackout_effect']);
        
                        foreach($blackoutEffect as $effect) {
        
                            if($effect === '2') {
                                $blackOutEffectValue = '2';
                            }                    
                        }
                    }
                    else {
                        $blackOutEffectValue = $blackOutResult['blackout_effect'];
                    }       
                }
            }
        }                                     
    }    
    
        if($blackOutEffectValue !== '2') {

            $minNightAmount = '';
            $minNightPerCent = '';
            $daysInAdvanceAmount = '';
            $daysInAdvancePerCent = '';

            // Minimum Nights Discount

            $discountMinNightsQuery = $pdo->query("SELECT minimum_nights, MAX(amount) AS maxAmount, type FROM room_discounts WHERE (room_id = '$roomId') AND (is_special = '1') AND ('$checkInDate' >= CAST(start_date AS DATE) AND '$discountEndDate' <= CAST(end_date AS DATE)) LIMIT 1");
            $discountMinNightsQuery->setFetchMode(PDO::FETCH_ASSOC);

            if($discountMinNightsQuery->rowCount()) {
                while($discountMinNightsResult = $discountMinNightsQuery->fetch()) {                  
                    
                    $numberOfNights = count($bookedDays) - 1;

                    if($numberOfNights >= $discountMinNightsResult['minimum_nights']) {                        
                                    
                        switch($discountMinNightsResult['type']) {

                            case 'amount':
                            $minNightAmount = $discountMinNightsResult['maxAmount'];                                                                                                              
                            break;

                            case 'percent':                            
                            $minNightPerCent = number_format($discountMinNightsResult['maxAmount'] / 100, 2, '.', '');                            
                            break;                 
                        }
                    }                    
                }              
            }                    

            // Days In Advance Discount

            $discountInAdvanceQuery = $pdo->query("SELECT room_id, minimum_nights, MAX(amount) AS maxAdvanceAmount, type FROM room_discounts WHERE (room_id = '$roomId') AND (is_special = '0') AND ('$checkInDate' >= CAST(start_date AS DATE) AND '$discountEndDate' <= CAST(end_date AS DATE)) LIMIT 1");
            $discountInAdvanceQuery->setFetchMode(PDO::FETCH_ASSOC);

            if($discountInAdvanceQuery->rowCount()) {
                
                while($discountInAdvanceResult = $discountInAdvanceQuery->fetch()) {                    
                    $bookingDay = date('Y-m-d');
                    $bookingDayDT = new DateTime($bookingDay);
                    $diff = $bookingDayDT->diff($checkInDT);
                    $diffNumber = $diff->days;

                    if($diffNumber >= $discountInAdvanceResult['minimum_nights']) {
                    
                        switch($discountInAdvanceResult['type']) {

                            case 'amount':
                            $daysInAdvanceAmount = $discountInAdvanceResult['maxAdvanceAmount'];                            
                            break;

                            case 'percent':
                            $daysInAdvancePerCent = number_format($discountInAdvanceResult['maxAdvanceAmount'] / 100, 2, '.', '');                            
                            break;        
                        }
                    }
                }                
            }            
            (!empty($minNightAmount)) ? $calculationResult[$roomId]['minNightAmount'] = $minNightAmount : $calculationResult[$roomId]['minNightAmount'] = 0;
            (!empty($minNightPerCent)) ? $calculationResult[$roomId]['minNightPerCent'] = $minNightPerCent : $calculationResult[$roomId]['minNightPerCent'] = 0;
            (!empty($daysInAdvanceAmount)) ? $calculationResult[$roomId]['daysInAdvanceAmount'] = $daysInAdvanceAmount : $calculationResult[$roomId]['daysInAdvanceAmount'] = 0;
            (!empty($daysInAdvancePerCent)) ? $calculationResult[$roomId]['daysInAdvancePerCent'] = $daysInAdvancePerCent : $calculationResult[$roomId]['daysInAdvancePerCent'] = 0;                  
        }
        else {
            $calculationResult[$roomId]['minNightAmount'] = 0;
            $calculationResult[$roomId]['minNightPerCent'] = 0;
            $calculationResult[$roomId]['daysInAdvanceAmount'] = 0;
            $calculationResult[$roomId]['daysInAdvancePerCent'] = 0;                        
        }      
                
    echo json_encode($calculationResult);
}

/** BOOKING SUMMARY */

function form_ajax_calculateBookingSummary() {

    $pdo = DatabasePDO::getInstance();
    $options = Data::getOptions();

    $checkInDate = $_POST['check_in'];
    $checkOutDate = $_POST['check_out'];
    $discountId = isset($_POST['discount_id']) ? $_POST['discount_id'] : '';
    $discountCode = isset($_POST['discount_code']) ? $_POST['discount_code'] : '';

    $checkInDT = new DateTime($checkInDate);
    $checkOutDT = new DateTime($checkOutDate);
    $dateDiff = $checkInDT->diff($checkOutDT);
    $dayDiff = $dateDiff->days;
    $bookedDays = [$checkInDate];
    $discountEndDay = $checkOutDT->modify('-1 day');
    $discountEndDate = $discountEndDay->format('Y-m-d');     
    $rates = [];    
    $blackOutEffectValue = '';      

    for($bd = 1; $bd <= $dayDiff; $bd++) {  
        $date = strtotime("+" . $bd .  "day", strtotime($bookedDays[0]));
        $days = date('Y-n-j', $date);
        array_push($bookedDays, $days);
    }

    foreach($bookedDays as $d) { // Blackout check
        $blackOutQuery = $pdo->query("SELECT id, blackout_place, blackout_effect FROM blackout WHERE ('$d' >= CAST(start_date AS DATE)) AND ('$d' <= CAST(end_date AS DATE))");
        $blackOutQuery->setFetchMode(PDO::FETCH_ASSOC);
        $blackOutResult = $blackOutQuery->fetch();

        if(!empty($blackOutResult)) {

            if(strlen($blackOutResult['blackout_place']) > 1) {
                $blackoutPlace = explode(',', $blackOutResult['blackout_place']);

                foreach($blackoutPlace as $place) {

                    if($place !== '2') {
                        if(strlen($blackOutResult['blackout_effect']) > 1) {
                            $blackoutEffect = explode(',', $blackOutResult['blackout_effect']);
            
                            foreach($blackoutEffect as $effect) {
            
                                if($effect === '1') {
                                    $blackOutEffectValue = '1';
                                }                    
                            }
                        }
                        else {
                            $blackOutEffectValue = $blackOutResult['blackout_effect'];
                        }                        
                    }                    
                }
            }
            else {
                if($blackOutResult['blackout_place'] !== '2') {
                    if(strlen($blackOutResult['blackout_effect']) > 1) {
                        $blackoutEffect = explode(',', $blackOutResult['blackout_effect']);
        
                        foreach($blackoutEffect as $effect) {
        
                            if($effect === '1') {
                                $blackOutEffectValue = '1';
                            }                    
                        }
                    }
                    else {
                        $blackOutEffectValue = $blackOutResult['blackout_effect'];
                    }       
                }
            }
        }        
    }
    
    !empty($blackOutEffectValue) ? $rates['blackout_in_effect'] = '1' : $rates['blackout_in_effect'] = '0'; 

    if($blackOutEffectValue !== '1') { // Global discounts 
        $rates['discounts'] = [];              
        
        if($discountId !== '0') { 
            $discountIdQuery = $pdo->query("SELECT id, rate, type, category FROM discounts WHERE id = '$discountId'");
            $discountIdQuery->setFetchMode(PDO::FETCH_ASSOC);

            if($discountIdQuery->rowCount()) {                               

                while($discountResult = $discountIdQuery->fetch()) {

                    if($discountResult['type'] === 'percentage') {
                        $rates['discounts']['type'] = $discountResult['type'];
                        $rates['discounts']['rate'] = round($discountResult['rate'] / 100, 2);
                        $rates['discounts']['category'] = $discountResult['category'];
                    }
                    else {
                        $rates['discounts']['type'] = $discountResult['type'];
                        $rates['discounts']['rate'] = $discountResult['rate'];
                        $rates['discounts']['category'] = $discountResult['category'];
                    }
                }
            }
            else {
                $rates['discounts'] = '';
            }
        }
        
        if($discountCode !== '0') {
            // Determination of current date for discounts based on visibility
            $getTimezone = Data::getOptions();
            $timezone = empty($getTimezone) ? 'US/Pacific' : $getTimezone['timezone'];
            $displayDate = new DateTime("now", new DateTimeZone($timezone));
            $date = $displayDate->format('Y-m-d');
            
            $codeBasedDiscount = $pdo->query("SELECT * FROM discounts WHERE code = '$discountCode' LIMIT 1");
            $codeBasedDiscount->setFetchMode(PDO::FETCH_ASSOC);

            if($codeBasedDiscount->rowCount()) {
                $rates['discounts'] = [];

                while($codeBasedDiscountResult = $codeBasedDiscount->fetch()) {

                    if($dayDiff > 1) {
                        
                        if($codeBasedDiscountResult['based_on'] === 'booking') {

                            if($checkInDate >= $codeBasedDiscountResult['start_date'] && $discountEndDate <= $codeBasedDiscountResult['expiry_date']) {

                                if($codeBasedDiscountResult['type'] === 'percentage') {
                                    $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                    $rates['discounts']['rate'] = round($codeBasedDiscountResult['rate'] / 100, 2);
                                    $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                }
                                else {
                                    $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                    $rates['discounts']['rate'] = $codeBasedDiscountResult['rate'];
                                    $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                }    
                            }
                        }
                        else if($codeBasedDiscountResult['based_on'] === 'visibility') {

                            if($date >= $codeBasedDiscountResult['start_date'] && $date <= $codeBasedDiscountResult['expiry_date']) {

                                if(($checkInDate >= $codeBasedDiscountResult['start_date'] && $discountEndDate < date('Y-m-d', strtotime("+2 day", $codeBasedDiscountResult['expiry_date']))) || ($checkInDate > $codeBasedDiscountResult['expiry_date'])) {

                                    if($codeBasedDiscountResult['type'] === 'percentage') {
                                        $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                        $rates['discounts']['rate'] = round($codeBasedDiscountResult['rate'] / 100, 2);
                                        $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                    }
                                    else {
                                        $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                        $rates['discounts']['rate'] = $codeBasedDiscountResult['rate'];
                                        $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                    }  
                                }
                            }
                        }
                    }
                    
                    else if($dayDiff == 1) {

                        if($codeBasedDiscountResult['based_on'] === 'booking') {

                            if($checkInDate >= $codeBasedDiscountResult['start_date'] && $discountEndDate <= $codeBasedDiscountResult['expiry_date']) {

                                if($codeBasedDiscountResult['type'] === 'percentage') {
                                    $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                    $rates['discounts']['rate'] = round($codeBasedDiscountResult['rate'] / 100, 2);
                                    $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                }
                                else {
                                    $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                    $rates['discounts']['rate'] = $codeBasedDiscountResult['rate'];
                                    $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                }    
                            }
                        }
                        else if($codeBasedDiscountResult['based_on'] === 'visibility') {

                            if($date >= $codeBasedDiscountResult['start_date'] && $date <= $codeBasedDiscountResult['expiry_date']) {

                                if($checkInDate >= $codeBasedDiscountResult['start_date'] && $discountEndDate <= $codeBasedDiscountResult['expiry_date']) {

                                    if($codeBasedDiscountResult['type'] === 'percentage') {
                                        $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                        $rates['discounts']['rate'] = round($codeBasedDiscountResult['rate'] / 100, 2);
                                        $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                    }
                                    else {
                                        $rates['discounts']['type'] = $codeBasedDiscountResult['type'];
                                        $rates['discounts']['rate'] = $codeBasedDiscountResult['rate'];
                                        $rates['discounts']['category'] = $codeBasedDiscountResult['category'];
                                    }  
                                }
                            }
                        }
                    }
                }
            }
            else {
                $rates['discounts'] = '';
            }                                     
        }        
    }
    else {
        $rates['discounts'] = '';
    }

    // Extras excluded from discounts
    $extrasQuery = $pdo->query("SELECT id FROM upsells WHERE exclude_discounts = '1' AND active = '1'");
    $extrasQuery->setFetchMode(PDO::FETCH_ASSOC);

    if($extrasQuery->rowCount()) {
        $rates['excludedExtras'] = [];

        while($extrasResult = $extrasQuery->fetch()) {
            array_push($rates['excludedExtras'], $extrasResult['id']);
        }
    }
    else {
        $rates['excludedExtras'] = '';
    }    

    // Surcharges
    $surchargeQuery = $pdo->query("SELECT id, name, name_sec_lang, rate, type, is_surcharge FROM taxes WHERE is_surcharge = '1' ORDER BY name ASC");
    $surchargeQuery->setFetchMode(PDO::FETCH_ASSOC);

    if($surchargeQuery->rowCount()) {        
        $rates['surcharges'] = [];        

        while($surchargeResult = $surchargeQuery->fetch()) {
            $rates['surcharges'][] = $surchargeResult;            
        }
    }
    else {
        $rates['surcharges'] = '';
    }

    // Taxes
    $taxQuery = $pdo->query("SELECT id, name, name_sec_lang, rate, type, is_surcharge FROM taxes WHERE is_surcharge = '0' ORDER BY name ASC");
    $taxQuery->setFetchMode(PDO::FETCH_ASSOC);

    if($taxQuery->rowCount()) {        
        $rates['tax'] = [];              

        while($taxResult = $taxQuery->fetch()) {              
            $rates['tax'][] = $taxResult;            
        }
    }
    else {
        $rates['tax'] = '';
    }

    //Deposit
    $depositType = '';
    $depositValue = '';

    if(!empty($options['payment_deposit_type']) && !empty($options['payment_deposit_type_value'])) {
        $depositType = $options['payment_deposit_type'];
        $depositValue = $options['payment_deposit_type_value'];
    }
    else if(!empty($options['payment_stripe_type']) && !empty($options['payment_stripe_type_value'])) {
        $depositType = $options['payment_stripe_type'];
        $depositValue = $options['payment_stripe_type_value'];
    }
    else if(!empty($options['payment_square_type']) && !empty($options['payment_square_type_value'])) {
        $depositType = $options['payment_square_type'];
        $depositValue = $options['payment_square_type_value'];
    }
    
    (!empty($depositType) && !empty($depositValue)) ? $rates['deposit'] = ['type' => $depositType, 'value' => round($depositValue, 2)] : $rates['deposit'] = '';    

    echo json_encode($rates);
}

function form_ajax_submission() {    
    $data = $_REQUEST['data'];           
    $language = $_REQUEST['lang'] === '1' ? 'default' : 'second';
    $hotel = Data::getProperty();
    $options = Data::getOptions();
    $pdo = DatabasePDO::getInstance();
    $mode = '';
    $_SESSION['frontend_submission'] = true;

    if($options['payment_booking_mode'] === '1' && $options['frontend_is_demo'] === '0') {
        $mode = 'submit';
    }
    else if($options['payment_booking_mode'] === '0' && $options['frontend_is_demo'] === '0') {
        $mode = 'request';
    }
    else if($options['payment_booking_mode'] === '2' && $options['frontend_is_demo'] === '1') {
        $mode = 'demo';
    }          
    
    $timeZone = empty($options['timezone']) ? 'US/Eastern' : $options['timezone'];
    $checkIn = '';
    $checkInTime = '';
    $checkOut = '';
    $checkOutTime = '';

    if(!empty($data['contact']['check_in_time'])) {
        $checkInDT = new DateTime($data['booking_period']['check_in'] . ' ' . $data['contact']['check_in_time'], new DateTimeZone($timeZone));
        $checkIn = $checkInDT->format('Y-m-d H:i:s');
        $checkInTime = $checkInDT->format('h:i a');
    }
    else {
        $checkInDT = new DateTime($data['booking_period']['check_in'] . ' 3:00 pm' , new DateTimeZone($timeZone));
        $checkIn = $checkInDT->format('Y-m-d H:i:s');
        $checkInTime = $checkInDT->format('h:i a');
    }    

    if(!empty($data['contact']['check_out_time'])) {
        $checkOutDT = new DateTime($data['booking_period']['check_out'] . ' ' . $data['contact']['check_out_time'], new DateTimeZone($timeZone));
        $checkOut = $checkOutDT->format('Y-m-d H:i:s');
        $checkOutTime = $checkOutDT->format('h:i a');
    }
    else {
        $checkOutDT = new DateTime($data['booking_period']['check_out'] . ' 11:00 am', new DateTimeZone($timeZone));
        $checkOut = $checkOutDT->format('Y-m-d H:i:s');
        $checkOutTime = $checkOutDT->format('h:i a');
    }  

    $guest = [];
    $guestId = '';
    $booking = [];
    $bookingRooms = [];      
            
    // Guest data
    $guestQuery = $pdo->prepare("SELECT id FROM guests WHERE last_name = ? AND email = ? LIMIT 1");
    $guestQuery->execute([$data['contact']['last_name'], $data['contact']['email']]);
    $guestQuery->setFetchMode(PDO::FETCH_ASSOC);

    if($guestQuery->rowCount()) {
        $guestResult = $guestQuery->fetch();
        $guestId = $guestResult['id'];        
    }
    
    $guest['id'] = '';
    $guest['first_name'] = $data['contact']['first_name'];
    $guest['last_name'] = $data['contact']['last_name'];
    $guest['address'] = $data['contact']['address'];
    $guest['city'] = $data['contact']['city'];
    $guest['province'] = $data['contact']['state'];
    $guest['zip_code'] = $data['contact']['zip'];
    $guest['country'] = $data['contact']['country'];
    $guest['telephone'] = $data['contact']['phone'];
    $guest['mobile'] = '';
    $guest['fax'] = '';
    $guest['email'] = $data['contact']['email'];
    $guest['comments'] = $data['contact']['comment'];
    $_SESSION['guest_email_frontend'] = $guest['email'];                 

    // Booking Data
    $booking['reservation_number'] = '';
    $booking['guest_id'] = empty($guestId) ? '' : $guestId;
    $booking['booking_type'] = 'online';
    $booking['check_in'] = $checkIn;
    $booking['check_out'] = $checkOut;
    $booking['discount_id'] = !empty($data['summary']['discount_id']) ? $data['summary']['discount_id'] : '';
    $booking['comments'] = !empty($data['contact']['comment']) ? $data['contact']['comment'] : '';
    $booking['card_type'] = !empty($data['payment']['credit_type']) ? $data['payment']['credit_type'] : '';
    $booking['card_number'] = !empty($data['payment']['credit_number']) ? $data['payment']['credit_number'] : '';

    if(!empty($data['payment']['month'])) {
        $booking['card_expiry_month'] = strlen($data['payment']['month']) < 2 ? '0' . $data['payment']['month'] : $data['payment']['month'];
    }
    else {
        $booking['card_expiry_month'] = '';
    }
    
    $booking['card_expiry_year'] = !empty($data['payment']['year']) ? $data['payment']['year'] : '';
    $booking['card_cvc'] = !empty($data['payment']['cvc']) ? $data['payment']['cvc'] : '';
    $booking['billing_zip'] = !empty($data['contact']['zip']) ? $data['contact']['zip'] : '';
    $booking['name_on_card'] = '';
    $booking['manual_discount'] = 0;
    $booking['payment_method'] = isset($data['payment']['credit_type']) ? $data['payment']['credit_type'] : '';
    $booking['last_updated'] = date('Y-m-d H:i:s');
    $booking['exemptions'] = [];    

    if(isset($data['summary']['deposit'])) {
        $deposit = $data['summary']['deposit'];
        $comma = strpos($deposit, ',');

        if($comma !== false) {
            $num = explode(',', $deposit);
            $booking['payments'] = implode('', $num);
        }
        else {
            $booking['payments'] = $deposit;
        }
    }
    else {
        $booking['payments'] = '';
    }
                
    $booking['created'] = date('Y-m-d H:i:s');

    $cardNumber = !empty($data['payment']['credit_number']) ? '************' . substr($data['payment']['credit_number'], -4) : '';  

    // Other
    $newsletter = $data['summary']['newsletter'] !== false ? $data['summary']['newsletter'] : 1;
    $squareNonce = !empty($data['payment']['square_nonce']) ? $data['payment']['square_nonce'] : ''; // Square card nonce
    
    // Cancellation policies
    $cancellation = $data['policy']['cancellation'] !== false ? $data['policy']['cancellation'] : 0;
    $privacy = $data['policy']['privacy'] !== false ? $data['policy']['privacy'] : 0;
    $square_card = $data['policy']['square'] !== false ? $data['policy']['square'] : 0; 
    
    // Rooms & Extras     
    foreach($data['rooms'] as $room) {
        $roomId = $room['roomId'];
        $roomQuantity = $room['quantity'];
        $adults = $room['adults'];
        $children = $room['children'];                        
        $upsells = [];            

        if(isset($data['extras'])) {
            for($x = 1; $x <= $roomQuantity; $x++) {
                foreach($data['extras'] as $extras) {
                    if($roomId === $extras['extra_room_id']) {
                        $upsells[$x][] = ['upsell_id' => $extras['extra_id'], 'quantity' => $extras['extra_quantity']];
                    }
                }
            }                                               
        }            
            
        $bookingRooms[] = [
           'room_type_id' => $roomId,
           'quantity' => $roomQuantity,
           'upsells' => $upsells,                
           'adults' => $adults,
           'children' => $children
        ];
    }
    
    // Checking the discount type
    $discountType = '';

    if(!empty($booking['discount_id'])) {
        $discountId = $booking['discount_id'];  
        $discountTypeQuery = $pdo->prepare("SELECT category FROM discounts WHERE id = ? LIMIT 1");
        $discountTypeQuery->execute([$discountId]);
        $discountTypeQuery->setFetchMode(PDO::FETCH_ASSOC);
        $discountTypeResult = $discountTypeQuery->fetch();
        $discountType = $discountTypeResult['category'];
    }
                
    if(!empty($mode)) {
          
        switch($mode) {
            
            case 'request': // Request Mode                             
            $invoiceHtml = Invoice::createHTML('REQUEST', $hotel, $guest, $booking, $bookingRooms, $data['booking_period']['check_in'], $data['booking_period']['check_out'], $checkInTime, $checkOutTime, true, $language, false, false, false, false);
            $result = Invoice::emailInvoice('REQUEST', $guest, $hotel, $invoiceHtml, false, true, $language);
            echo($result) ? 'success' : '';                       
            break;

            case 'demo': // Demo Mode                
            $invoiceHtml = Invoice::createHTML('DEMO', $hotel, $guest, $booking, $bookingRooms, $data['booking_period']['check_in'], $data['booking_period']['check_out'], $checkInTime, $checkOutTime, true, $language, false, false, false, false);
            $result = Invoice::emailInvoice('DEMO', $guest, $hotel, $invoiceHtml, false, false, $language);
            echo($result) ? 'success' : '';                        
            break;

            case 'submit': // Standard Mode
            $resNum = Data::generateReservationNumber();
            $_SESSION['resNum'] = $resNum;
            $stripeError = false;
            $squareError = false;

            if(!empty($options['payment_processor'])) {

                // Stripe          
                if($options['payment_processor'] === 'stripe') {
                    require_once(APP_ROOT . '/lib/Stripe/init.php');
                    $currency = strtolower(isset($options['currency']) ? $options['currency'] : 'CAD');

                    try {

                        switch($options['payment_enable_processor']) {

                            case '':
                            \Stripe\Stripe::setApiKey($options['payment_stripe_private_key_test']);
                            break;

                            case '1':
                            \Stripe\Stripe::setApiKey($options['payment_stripe_private_key']);
                            break;
                        }
                    
                        $stripeType = $options["payment_stripe_type"];
                        $stripeValue = $options["payment_stripe_type_value"];
                        $amountRaw = $data['summary']['total'];
                        $thousandSep = $options['thousand_separator'];
                        $amount = '';

                        if(strpos($amountRaw, $thousandSep) !== false) {
                            $amount01 = explode($thousandSep, $amountRaw);
                            $amount = implode('', $amount01);
                        }
                        else {
                            $amount = $amountRaw;
                        }

                        $stripeGuest = $guest['first_name'] . ' ' . $guest['last_name'];
                        $stripeEmail = $guest['email'];                     

                        if($stripeType === 'percentage') {
                            $amount = ($amount / 100) * $stripeValue;                    
                        }
                        else {
                            if($stripeValue < $amount) {
                                $amount = $stripeValue;
                            }
                        }

                        $token = \Stripe\Token::create([
                            'card' => [
                                'number' => $booking['card_number'],
                                'exp_month' => $booking['card_expiry_month'],
                                'exp_year' => $booking['card_expiry_year'],
                                'cvc' => $booking['card_cvc'] 
                            ]
                        ]);

                        $customer = \Stripe\Customer::create([
                            "description" => $stripeGuest,
                            "email" => $stripeEmail,
                            "source" => $token 
                        ]);

                        $charge = \Stripe\Charge::create(array(
                            "amount" => round($amount * 100, 2), // submit in cents                            
                            "currency" => $currency,
                            "customer" => $customer->id, 
                            "description" => "Reservation: $resNum" 
                        ));

                        // create log entry
                        $charge_id = $charge->id;
                        $charge_amount = round($charge->amount / 100, 2);
                        $charge_currency = $charge->currency;
                        $charge_created = date("Y-m-d H:i:s", $charge->created);
                        $charge_customer_id = $charge->customer;
                        $charge_failure_code = $charge->failure_code;
                        $charge_failure_message = $charge->failure_message;
                        $charge_livemode = !$charge->livemode ? "false" : "true";
                        $charge_outcome_network_status = $charge->outcome->network_status;
                        $charge_outcome_type = $charge->outcome->type;                       
            
                        $sql = "INSERT INTO stripe_log 
                            (stl_reservation, stl_guest, stl_date, stl_charge_id, stl_charge_amount, stl_charge_currency,
                            stl_charge_customer_id, stl_charge_failure_code, stl_charge_failure_message, stl_charge_livemode,
                            stl_charge_outcome_network_status, stl_charge_outcome_type)
                            VALUES(:stl_reservation, :stl_guest, :stl_date, :stl_charge_id, :stl_charge_amount, :stl_charge_currency,
                            :stl_charge_customer_id, :stl_charge_failure_code, :stl_charge_failure_message, :stl_charge_livemode,
                            :stl_charge_outcome_network_status, :stl_charge_outcome_type)";            
            
                        $stmt = $pdo->prepare($sql);
            
                        $stmt->bindValue(":stl_reservation", $resNum);
                        $stmt->bindValue(":stl_guest", $stripeGuest);
                        $stmt->bindValue(":stl_date", $charge_created);
                        $stmt->bindValue(":stl_charge_id", $charge_id);
                        $stmt->bindValue(":stl_charge_amount", $charge_amount);
                        $stmt->bindValue(":stl_charge_currency", $charge_currency);
                        $stmt->bindValue(":stl_charge_customer_id", $charge_customer_id);
                        $stmt->bindValue(":stl_charge_failure_code", $charge_failure_code);
                        $stmt->bindValue(":stl_charge_failure_message", $charge_failure_message);
                        $stmt->bindValue(":stl_charge_livemode", $charge_livemode);
                        $stmt->bindValue(":stl_charge_outcome_network_status", $charge_outcome_network_status);
                        $stmt->bindValue(":stl_charge_outcome_type", $charge_outcome_type);
            
                        $stmt->execute();                   
                    }
                    catch(Exception $e) {
                        $sql = "INSERT INTO stripe_log 
                            (stl_reservation, stl_guest, stl_date, stl_charge_failure_code, stl_charge_failure_message)
                            VALUES(:stl_reservation, :stl_guest, :stl_date, :stl_charge_failure_code, :stl_charge_failure_message)";            
            
                        $stmt = $pdo->prepare($sql);
            
                        $stmt->bindValue(":stl_reservation", $resNum);
                        $stmt->bindValue(":stl_guest", $stripeGuest);
                        $stmt->bindValue(":stl_date", date("Y-m-d H:i:s"));
                        $stmt->bindValue(":stl_charge_failure_code", $e->getCode());
                        $stmt->bindValue(":stl_charge_failure_message", $e->getMessage());
            
                        $stmt->execute();
                    
                        echo $e->getMessage();
                        $stripeError = true;
                    }                            
                }
                // End of Stripe

                // Square
                else if($options['payment_processor'] === 'square') {                    
                    require_once(APP_ROOT . '/lib/Square/autoload.php');
                    $location_id = empty($options['payment_square_location_id']) && empty($options['payment_square_location_id_sandbox']) ? '' : empty($options['payment_square_location_id_sandbox']) ? $options['payment_square_location_id'] : $options['payment_square_location_id_sandbox'];
                    $access_token = empty($options['payment_square_access_token']) && empty($options['payment_square_access_token_sandbox']) ? '' : empty($options['payment_square_access_token_sandbox']) ? $options['payment_square_access_token'] : $options['payment_square_access_token_sandbox'];
                    \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($access_token);
                    
                    if($square_card === '1') { // If the customer approved the save of credit card data

                        /* Important: One card nonce can be used only once during a transaction! If it is used it can be substituted with the customer_id and customer_card_id
                        used in the transaction part. */

                        $defaultApiConfig = new \SquareConnect\Configuration();
                        $defaultApiConfig->setAccessToken($access_token);
                        $defaultApiClient = new \SquareConnect\ApiClient($defaultApiConfig);
                        $customersApi = new SquareConnect\Api\CustomersApi($defaultApiClient);
                        
                        // Save of Customer Data
                        $customer = new \SquareConnect\Model\CreateCustomerRequest();
                        $customer->setGivenName($guest['first_name'] . ' ' . $guest['last_name']);
                        $customer->setEmailAddress($guest['email']);

                        $customer->setAddress(
                            array(
                                'address_line_1'=>$guest['address'],
                                'address_line_2'=>'',
                                'city'=>$guest['city'],
                                'zip'=>$guest['zip_code']
                            )
                        );

                        try {
                            $result = $customersApi->createCustomer($customer);
                            $resCus = json_decode($result); // Customer data
                            $customerId = $resCus->customer->id; // Customer id
                            //print_r($resCus);
                        } catch (Exception $e) {                            
                            $jsonStartPos = strpos($e->getMessage(), '{');
                            echo substr($e->getMessage(), $jsonStartPos);
                            $squareError = true;
                        }                             
                        
                        // Save of Card Data
                        $body = new \SquareConnect\Model\CreateCustomerCardRequest();
                        $body->setCardNonce($squareNonce);

                        try {
                            $resultCard = $customersApi->createCustomerCard($customerId, $body);
                            $resCard = json_decode($resultCard); // Card Data
                            $cardId = !empty($resCard->card->id) ? $resCard->card->id : '';
                            $booking['card_type'] = $resCard->card->card_brand;
                            $booking['payment_method'] = $resCard->card->card_brand;                            
                            $booking['card_expiry_month'] = strlen($resCard->card->exp_month) < 2 ? '0' . $resCard->card->exp_month : $resCard->card->exp_month;
                            $booking['card_expiry_year'] = $resCard->card->exp_year;
                            $cardNumber = '************' . $resCard->card->last_4;                              
                            //print_r($resCard);
                        }
                        catch (Exception $e) {                                                        
                            $jsonStartPos = strpos($e->getMessage(), '{');
                            echo substr($e->getMessage(), $jsonStartPos);
                            //print_r($e->getMessage());                                                                                                                                                       
                            $squareError = true;
                        }

                        // Transaction

                        if($squareError == false) {

                            $transactions_api = new \SquareConnect\Api\TransactionsApi();

                            $squareType = $options["payment_square_type"];
                            $squareValue = $options["payment_square_type_value"];
                            $squareAmountRaw = $data['summary']['total'];
                            $squareThousandSep = $options['thousand_separator'];
                            $squareAmount = '';                    

                            // Calculation of the amount                            
                            if(strpos($squareAmountRaw, $squareThousandSep) !== false) {
                                $squareAmount01 = explode($squareThousandSep, $squareAmountRaw);
                                $squareAmount = implode('', $squareAmount01);
                            }
                            else {
                                $squareAmount = $squareAmountRaw;
                            }                                          

                            if(!empty($discountType)) {
                                
                                if($discountType !== 'guarantee') {
                                    
                                    if($squareType === 'percentage') {
                                        $squareAmount = ($squareAmount / 100) * $squareValue;                    
                                    }
                                    else {
                                        if($squareValue < $squareAmount) {
                                            $squareAmount = $squareValue;
                                        }
                                    }                                                         
                                }
                                else {
                                    $squareAmount = $squareAmount;                                    
                                }
                            }
                            else {
                                if($squareType === 'percentage') {
                                    $squareAmount = ($squareAmount / 100) * $squareValue;                    
                                }
                                else {
                                    if($squareValue < $squareAmount) {
                                        $squareAmount = $squareValue;
                                    }
                                }          
                            }                            
                            // End of calculation
                                        
                            $request_body = array (
                                'customer_id' => $customerId,
                                'customer_card_id' => $cardId,                                                                               
                                "amount_money" => array (
                                    "amount" => $squareAmount * 100,
                                    "currency" => $options['currency']
                                ),                        
                                "idempotency_key" => uniqid()
                            );

                        try {
                            $result = $transactions_api->charge($location_id, $request_body);                        
                            $res = json_decode($result); // Transaction data
                            //print_r($result);
                            // Save of Transaction Data
                            $guestFullName = $guest['first_name'] . ' ' . $guest['last_name'];
                            $chargeAmount = $res->transaction->tenders[0]->amount_money->amount / 100;
                            $q = $pdo->prepare("INSERT INTO square_log(sql_reservation, sql_guest, sql_date, sql_charge_id, sql_charge_amount, sql_charge_currency, sql_charge_customer_id, sql_charge_customer_card_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                            $q->execute([$resNum, $guestFullName, $res->transaction->created_at, $res->transaction->id, $chargeAmount, $res->transaction->tenders[0]->amount_money->currency, $res->transaction->tenders[0]->customer_id, $res->transaction->tenders[0]->card_details->card->id]);                                                                                                             
                        } 
                        catch (\SquareConnect\ApiException $e) {                        
                            echo json_encode($e->getResponseBody());
                        
                            // Save of transaction data and error message
                            $error = $e->getResponseBody();
                            $errorMsg = $error->errors[0]->detail;
                            $guestFullName = $guest['first_name'] . ' ' . $guest['last_name'];
                            $date = date('Y-m-d H:i:s');                        
                            $q = $pdo->prepare("INSERT INTO square_log(sql_reservation, sql_guest, sql_date, sql_charge_failure_message) VALUES(?, ?, ?, ?)");
                            $q->execute([$resNum, $guestFullName, $date, $errorMsg]);

                            $squareError = true;
                        }
                    }                  
                }
                else {
                    // Transaction without saving card and customer data (the client does not approved it or it is not required)

                    $transactions_api = new \SquareConnect\Api\TransactionsApi();

                    $squareType = $options["payment_square_type"];
                    $squareValue = $options["payment_square_type_value"] * 100;
                    $squareAmountRaw = $data['summary']['total'];
                    $squareThousandSep = $options['thousand_separator'];
                    $squareAmount = '';                    

                    if(strpos($squareAmountRaw, $squareThousandSep) !== false) {
                        $squareAmount01 = explode($squareThousandSep, $squareAmountRaw);
                        $squareAmount = implode('', $squareAmount01);
                    }
                    else {
                        $squareAmount = $squareAmountRaw;
                    }                                          

                    if(!empty($discountType)) {
                                
                        if($discountType !== 'guarantee') {
                            
                            if($squareType === 'percentage') {
                                $squareAmount = ($squareAmount / 100) * $squareValue;                    
                            }
                            else {
                                if($squareValue < $squareAmount) {
                                    $squareAmount = $squareValue;
                                }
                            }                                                         
                        }
                        else {
                            $squareAmount = $squareAmount;                                    
                        }
                    }
                    else {
                        if($squareType === 'percentage') {
                            $squareAmount = ($squareAmount / 100) * $squareValue;                    
                        }
                        else {
                            if($squareValue < $squareAmount) {
                                $squareAmount = $squareValue;
                            }
                        }          
                    }                                                                                                  
                                    
                    $request_body = array (
                        "card_nonce" => $squareNonce,                                               
                        "amount_money" => array (
                            "amount" => $squareAmount * 100,
                            "currency" => $options['currency']
                        ),                        
                        "idempotency_key" => uniqid()
                    );

                    try {
                        $result = $transactions_api->charge($location_id, $request_body);                        
                        $res = json_decode($result); // Transaction data                        
                        $booking['payment_method'] = $res->transaction->tenders[0]->card_details->card->card_brand;
                        //print_r($res);
                        
                        // Save of transaction data
                        $guestFullName = $guest['first_name'] . ' ' . $guest['last_name'];
                        $chargeAmount = $res->transaction->tenders[0]->amount_money->amount / 100;
                        $q = $pdo->prepare("INSERT INTO square_log(sql_reservation, sql_guest, sql_date, sql_charge_id, sql_charge_amount, sql_charge_currency) VALUES(?, ?, ?, ?, ?, ?)");
                        $q->execute([$resNum, $guestFullName, $res->transaction->created_at, $res->transaction->id, $chargeAmount, $res->transaction->tenders[0]->amount_money->currency]);
                    } 
                    catch (\SquareConnect\ApiException $e) {
                        echo json_encode($e->getResponseBody());                      
                        //print_r($e->getResponseBody());
                        
                        // Save of transaction data and error message
                        $error = $e->getResponseBody();                        
                        $errorMsg = (isset($error->errors[0]->field) && !empty($error->errors[0]->field)) ? "{$error->errors[0]->detail} ({$error->errors[0]->field})" : "{$error->errors[0]->detail}";
                        $guestFullName = $guest['first_name'] . ' ' . $guest['last_name'];
                        $date = date('Y-m-d H:i:s');                        
                        $q = $pdo->prepare("INSERT INTO square_log(sql_reservation, sql_guest, sql_date, sql_charge_failure_message) VALUES(?, ?, ?, ?)");
                        $q->execute([$resNum, $guestFullName, $date, $errorMsg]);
                        $squareError = true;
                    }                      
                }
            }          
            // End of Square
            }            
 
            if(!$stripeError && !$squareError) {

                $existingGuest = Data::getGuestByEmailLastName($guest['email'], $guest['last_name']);
                $guestID = 0;
                $guestCountry = $guest['country'];            

                $countryQuery = $pdo->query("SELECT country FROM countries WHERE ccode = '$guestCountry' LIMIT 1");
                $countryQuery->setFetchMode(PDO::FETCH_ASSOC);
                $country = $countryQuery->fetch();
            
                if($existingGuest == false) { // Adding new guest
                    $guestID = Data::addGuest($guest['first_name'], $guest['last_name'], $guest['address'], $guest['city'], $guest['province'], $guest['zip_code'], $country['country'], $guest['telephone'], false, false, $guest['email'], false);
                    ActionLog::insertFromTable('guests', ['id' => $guestID], 'insert');                
                }
                else { // Updating existing guest
                    $guestID = $existingGuest['id'];
                    Data::updateGuest($guestID, $guest['first_name'], $guest['last_name'], $guest['address'], $guest['city'], $guest['province'], $guest['zip_code'], $country['country'], $guest['telephone'], false, false, $guest['email'], false);
                    ActionLog::insertFromTable('guests', ['id' => $guestID], 'update');                
                }
                
                $booking['guest_id'] = $guestID; // Guest ID for the e-mails sent to new guests

                // Adding booking                
                Data::addBooking($resNum, $guestID, 'online', $checkIn, $checkOut, $booking['discount_id'], $guest['comments'], $booking['card_type'], $cardNumber, $booking['card_expiry_month'], $booking['card_expiry_year'], false, $booking['billing_zip'], $booking['name_on_card'], false, $booking['payment_method'], $cancellation, $privacy, $newsletter);                       
                        
                if($options['capture_guest_ip'] != 0) { // Capturing guest's IP (if it is allowed) 
                    if(isset($_COOKIE['guestIp'])) {               
                        $gID = $pdo->query("SELECT guest_id FROM bookings WHERE reservation_number = '$resNum' LIMIT 1");
                        $gID->setFetchMode(PDO::FETCH_ASSOC);
                        $gIdResult = $gID->fetch();
                        $date = date('Y-m-d H:i:s');

                        $guestIP = $pdo->prepare("INSERT INTO guest_ip_addresses(guest_id, ipaddress, datetime_added) VALUES(?, ?, ?)");
                        $guestIP->execute([$gIdResult['guest_id'], $_COOKIE['guestIp'], $date]);              
                    }
                }            
            
                foreach($data['rooms'] as $room) {
                    $bookingRoomId = false;

                    if($room['quantity'] > 0) {
                        // Adding booked rooms
                        $bookingRoomId = Data::addBookingRoom($resNum, $room['roomId'], $room['quantity'], $room['adults'], $room['children']);

                        if($bookingRoomId != false) {                       

                            if(isset($data['extras'])) { // Adding extras
                                foreach($data['extras'] as $extras) {                                   

                                    if($room['roomId'] === $extras['extra_room_id']) {
                                        $pdo->exec("SET FOREIGN_KEY_CHECKS = 0;");
                                        $roomIdNo = $room['roomId']; 
                                        $q = $pdo->query("SELECT booking_room_id FROM booking_rooms WHERE reservation_number = '$resNum' AND room_type_id = '$roomIdNo' LIMIT 1");
                                        $q->setFetchMode(PDO::FETCH_ASSOC);
                                        $bookedRoomId = $q->fetch();

                                        $q2 = $pdo->prepare("INSERT INTO booking_room_upsells (booking_room_id, room_index, upsell_id, quantity) VALUES(?, ?, ?, ?)");
                                        $q2->execute([$bookedRoomId['booking_room_id'], '1', $extras['extra_id'], $extras['extra_quantity']]);
                                    }                                
                                }
                            }                                               
                        }
                    }
                }

                // Adding newsletter subscription to Campaign Monitor
                if(($newsletter == 0) || ($options['cam_subscription'] == 1)) {
                    $espQuery = $pdo->query("SELECT newsletter FROM bookings WHERE reservation_number = '$resNum' LIMIT 1");
                    $espQuery->setFetchMode(PDO::FETCH_ASSOC);
                    $espResult = $espQuery->fetch();
            
                    if(($options['cam_subscription'] == 1) || ($espResult['newsletter'] == 0 && $options['email_service_provider'] === 'Campaign Monitor')) {
                        Mail::run_esp_rules($guest['email'], $guest['first_name'] . ' ' . $guest['last_name'], $resNum, 'books');
                    }
                }

                $deposit = isset($data['summary']['deposit']) ? $data['summary']['deposit'] : 0; 
                
                // Payment Calculations
                Data::updateBookingTotal($resNum);
                Data::addBookingPayment($resNum, $deposit, $booking['card_type']);            

                // Sending confirmation e-mails and fax
                $fax = $options['fax_enabled'] == true ? true : false;            
                $invoiceHtml = Invoice::createHTML($resNum, $hotel, $guest, $booking, $bookingRooms, $data['booking_period']['check_in'], $data['booking_period']['check_out'], $checkInTime, $checkOutTime, true, $language, false, false, false, false);
                $result = Invoice::emailInvoice($resNum, $guest, $hotel, $invoiceHtml, false, true, $language);
            
                if($fax) { // Fax
                    $invoiceHtmlFax = Invoice::createHTML($resNum, $hotel, $guest, $booking, $bookingRooms, $data['booking_period']['check_in'], $data['booking_period']['check_out'], $checkInTime, $checkOutTime, true, $language, false, true, false, false);
                    Invoice::faxInvoice($invoiceHtmlFax, $hotel['fax']);
                }
            
                echo($result) ? 'success' : '';
            }                                 
            break;

            default:
            break;
        }
    }             
}