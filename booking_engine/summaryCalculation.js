$(document).ready(function() {

    var fullAppRoot = $("div.engine_form").data("app");
    var decSeparator = $("section#section_1").data("decimal") !== "" ? $("section#section_1").data("decimal") : ".";

    $("#check_availability_button").click(function() {
        $("span.sum").text("0" + decSeparator + "00");

        $("p.surcharge").remove();
        $("p.surcharge-default").show();

        $("p.taxes").remove();
        $("p.taxes-default").show();

        if ($(".panel").length > 0) {
            calculateSummary();
        } else {
            setTimeout(function() {
                calculateSummary();
            }, 3000);
        }
    });

    $(".check").change(function() {
        $("span.sum").text("0" + decSeparator + "00");

        $("p.surcharge").remove();
        $("p.surcharge-default").show();

        $("p.taxes").remove();
        $("p.taxes-default").show();

        if ($(".panel").length > 0) {
            calculateSummary();
        } else {
            setTimeout(function() {
                calculateSummary();
            }, 3000);
        }
    });

    $("#discount_remove").click(function() {
        $("span#disc_sum").text("0" + decSeparator + "00");
    });

    function calculateSummary() {

        $(".room-total").on('change click', function() {
            $("button.submit").length > 0 ? $("button.submit").attr("disabled", true) : "";

            setTimeout(function() {

                var checkInDate = $("#check_in_date").val();
                var checkOutDate = $("#check_out_date").val();
                var lang = $("select#lang_select").length > 0 ? $("select#lang_select").val() : "1";
                var thousandSeparator = $("div#available_rooms").data("thousand");
                var decimalSeparator = $("div#available_rooms").data("decimal");
                var accommodationRates = [];
                var extrasRates = [];
                var displayDefault = [];
                var accommodationRatesTotal, extrasRatesTotal, discountId, discountCode, discAmount, subtotal01, newSubtotal01;

                // Calculation of length of booking (number of days)
                var date1 = new Date(checkInDate);
                var date2 = new Date(checkOutDate);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                // Accommodations
                $("select.room-quantity").each(function() {
                    var roomSelectValue = $(this).val();
                    var roomId = $(this).data("id");
                    var roomSummaryAmount = $(this).data("summary");

                    if (roomSelectValue !== "rooms_quantity") {
                        accommodationRates.push(parseFloat(roomSummaryAmount));
                    } else {
                        accommodationRates.push(parseInt(0));
                    }
                    accommodationRatesTotal = parseFloat(accommodationRates.reduce(function(a, b) { return a + b; }, 0));
                    formatRate(accommodationRatesTotal.toFixed(2), thousandSeparator, decimalSeparator);
                    $("span#acc_sum").text(rateFormat);
                    roomSelectValue === "rooms_quantity" ? displayDefault.push("0") : displayDefault.push("1");
                });

                // Extras
                if ($("div#extraItem").length > 0) {

                    $("select.extras-nights").each(function() {
                        var extraSelectValue = $(this).val();
                        var extraRoomId = $(this).data("id");
                        var extraId = $(this).data("extra_id");

                        if (extraSelectValue !== "extra_selector") {
                            var extraAmount = $("span#ext_avg_rate[data-id='" + extraRoomId + extraId + "']").text();

                            if (extraAmount.indexOf(",") > -1) {
                                var amount = extraAmount.split(",");
                                var correctAmount = amount[0] + amount[1];
                                extrasRates.push(parseFloat(correctAmount));
                            } else {
                                extrasRates.push(parseFloat(extraAmount));
                            }
                        } else {
                            extrasRates.push(parseInt(0));
                        }
                        extrasRatesTotal = parseFloat(extrasRates.reduce(function(a, b) { return a + b; }, 0));
                    });
                } else {
                    extrasRatesTotal = parseInt(0);
                }

                formatRate(extrasRatesTotal.toFixed(2), thousandSeparator, decimalSeparator);
                $("span#extr_sum").text(rateFormat);

                // Subtotal
                subtotal01 = parseFloat(accommodationRatesTotal) + +parseFloat(extrasRatesTotal);
                formatRate(subtotal01.toFixed(2), thousandSeparator, decimalSeparator);
                $("span#subtotal_01").data("sub1", parseFloat(subtotal01.toFixed(2)));
                $("span#subtotal_01").text(rateFormat);

                // Discounts and Surcharges/Taxes
                if ($("select#discount_select").val() !== "select") {
                    var discountVal = $("select#discount_select").val();

                    if (discountVal !== "other") {
                        discountId = $("select#discount_select").val();
                        discountCode = parseInt(0);
                    } else {
                        $("span#disc_sum").text("0" + decimalSeparator + "00");
                        if ($("input#discount_other").length > 0 && $("input#discount_other").val() !== "") {
                            discountCode = $("input#discount_other").val();
                            discountId = parseInt(0);
                        } else {
                            discountId = parseInt(0);
                            discountCode = parseInt(0);
                        }
                    }
                } else {
                    $("span#disc_sum").text("0" + decimalSeparator + "00");
                    discountId = parseInt(0);
                    discountCode = parseInt(0);
                }

                $.ajax({
                    url: fullAppRoot + "engine/form_ajax.php",
                    type: "POST",
                    data: { method: "calculateBookingSummary", check_in: checkInDate, check_out: checkOutDate, discount_id: discountId, discount_code: discountCode },
                    dataType: "html",
                    cache: false,
                    success: function(response) {
                        var summaryResult = JSON.parse(response);
                        var subTotal = $("span#subtotal_01").data("sub1");
                        var excludedExtrasSum;

                        $("select#discount_select").data("blackout_in_effect", summaryResult.blackout_in_effect);

                        if (summaryResult.excludedExtras.length > 0) { // Extras excluded from discounts
                            var extrasExcludedDiscounts = [];

                            $("select.extras-nights").each(function() {
                                var extrValue = $(this).val();
                                var extrRoomId = $(this).data("id");
                                var extrId = $(this).data("extra_id");

                                if (extrValue !== "extra_selector") {
                                    var extrAmount = $("span#ext_avg_rate[data-id='" + extrRoomId + extrId + "']").text();

                                    if (summaryResult.excludedExtras.indexOf(String(extrId)) > -1) {

                                        if (extrAmount.indexOf(",") > -1) {
                                            var amount = extrAmount.split(",");
                                            var correctExtrAmount = amount[0] + amount[1];
                                            extrasExcludedDiscounts.push(parseFloat(correctExtrAmount));
                                        } else {
                                            extrasExcludedDiscounts.push(parseFloat(extrAmount));
                                        }
                                    }
                                }

                            });
                            excludedExtrasSum = parseFloat(extrasExcludedDiscounts.reduce(function(a, b) { return a + b; }, 0));
                        } else {
                            excludedExtrasSum = parseInt(0);
                        }

                        if ($("select#discount_select").val() !== "select") { // Global Discounts

                            if ((typeof summaryResult.discounts === "object") && (summaryResult.discounts != false)) {

                                if (summaryResult.discounts.type === "percentage") {
                                    discAmount = (parseFloat(subtotal01) - parseFloat(excludedExtrasSum)) * parseFloat(summaryResult.discounts.rate);
                                } else {
                                    baseDiscAmount = parseFloat(subtotal01) - parseFloat(excludedExtrasSum);
                                    discAmount = Math.min(parseFloat(baseDiscAmount), parseFloat(summaryResult.discounts.rate));
                                }
                                formatRate(parseFloat(discAmount).toFixed(2), thousandSeparator, decimalSeparator);
                                $("span#disc_sum").text(rateFormat);
                            } else {
                                discAmount = parseInt(0);
                                $("span#disc_sum").text("0" + decimalSeparator + "00");
                            }
                        } else {
                            discAmount = parseInt(0);
                            $("span#disc_name").text("N/A");
                            $("span#disc_sum").text("0" + decimalSeparator + "00");
                        }

                        if (summaryResult.surcharges !== "") { // Surcharges
                            var surchargeAmount = [];
                            var skeys = Object.keys(summaryResult.surcharges);

                            for (var sk = 0; sk < skeys.length; sk++) {
                                if (summaryResult.surcharges[skeys[sk]].type === "amount") {
                                    surchargeAmount.push(parseFloat(summaryResult.surcharges[skeys[sk]].rate));
                                    displaySurchargesTaxes(summaryResult.surcharges[skeys[sk]].id, summaryResult.surcharges[skeys[sk]].name, summaryResult.surcharges[skeys[sk]].rate, false, summaryResult.surcharges[skeys[sk]].is_surcharge, thousandSeparator, decimalSeparator);
                                } else {
                                    var surchargePercent = parseFloat(summaryResult.surcharges[skeys[sk]].rate) / 100;
                                    var surchargePercentSum = parseFloat(subTotal) * parseFloat(surchargePercent);
                                    surchargeAmount.push(parseFloat(surchargePercentSum));
                                    displaySurchargesTaxes(summaryResult.surcharges[skeys[sk]].id, summaryResult.surcharges[skeys[sk]].name, false, surchargePercentSum, summaryResult.surcharges[skeys[sk]].is_surcharge, thousandSeparator, decimalSeparator);
                                }
                            }
                            var surchargeSum = parseFloat(surchargeAmount.reduce(function(a, b) { return a + b; }, 0));
                        } else {
                            surchargeSum = parseInt(0);
                        }

                        // Subtotal 2                    
                        if (displayDefault.indexOf("1") > -1) {
                            var subTotal02 = (parseFloat(subTotal) - parseFloat(discAmount)) + +parseFloat(surchargeSum);
                            subTotal02 > 0 ? formatRate(subTotal02.toFixed(2), thousandSeparator, decimalSeparator) : formatRate("0.00", thousandSeparator, decimalSeparator);
                            $("span#subtotal_02").text(rateFormat);
                        } else {
                            $("span#subtotal_02").text(parseInt(0).toFixed(2));
                        }

                        if (summaryResult.tax !== "") { // Taxes
                            var taxAmount = [];

                            var tkeys = Object.keys(summaryResult.tax);

                            for (var tk = 0; tk < tkeys.length; tk++) {
                                if (summaryResult.tax[tkeys[tk]].type === "amount") {
                                    taxAmount.push(parseFloat(summaryResult.tax[tkeys[tk]].rate));
                                    displaySurchargesTaxes(summaryResult.tax[tkeys[tk]].id, summaryResult.tax[tkeys[tk]].name, summaryResult.tax[tkeys[tk]].rate, false, summaryResult.tax[tkeys[tk]].is_surcharge, thousandSeparator, decimalSeparator);
                                } else {
                                    var taxPercent = parseFloat(summaryResult.tax[tkeys[tk]].rate) / 100;
                                    var taxPercentSum = parseFloat(subTotal02) > 0 ? parseFloat(subTotal02) * parseFloat(taxPercent) : 0.00;
                                    taxAmount.push(parseFloat(taxPercentSum));
                                    displaySurchargesTaxes(summaryResult.tax[tkeys[tk]].id, summaryResult.tax[tkeys[tk]].name, false, taxPercentSum, summaryResult.tax[tkeys[tk]].is_surcharge, thousandSeparator, decimalSeparator);
                                }
                            }
                            var taxSum = parseFloat(taxAmount.reduce(function(a, b) { return a + b; }, 0)).toFixed(2);
                        } else {
                            taxSum = parseInt(0);
                        }

                        // Booking Total
                        var bookingTotal;

                        if (displayDefault.indexOf("1") > -1) {
                            bookingTotal = parseFloat(subTotal02) + +parseFloat(taxSum);
                            bookingTotal > 0 ? formatRate(bookingTotal.toFixed(2), thousandSeparator, decimalSeparator) : formatRate("0.00", thousandSeparator, decimalSeparator);
                            $("span#booking_sum").text(rateFormat);
                        } else {
                            bookingTotal = parseInt(0).toFixed(2);
                            $("span#booking_sum").text(parseInt(0).toFixed(2));
                        }

                        // Deposit
                        var depositAmount, policyText, policyTextSecLang;

                        if (summaryResult.discounts.category !== "guarantee") {

                            if (summaryResult.deposit !== "") {

                                if (summaryResult.deposit.type === "amount") {
                                    depositAmount = parseFloat(summaryResult.deposit.value);
                                } else {
                                    var depositPerCent = parseFloat(summaryResult.deposit.value) / 100;
                                    depositAmount = parseFloat(bookingTotal) * parseFloat(depositPerCent);
                                }
                            } else {
                                depositAmount = parseInt(0);
                            }

                            if ($("span.disc_fixed").length == 0) { // Swapping the text of cancellation policy
                                policyText = $("p.subheader").data("text_fixed");
                                policyTextSecLang = $("p.subheader").data("textseclang_fixed");
                                $("p.subheader").empty();
                                lang === "1" ? $("p.subheader").append('<span data-lang="step_8_policy" class="lang disc_fixed">' + policyText + '</span>') : $("p.subheader").append('<span data-lang="step_8_policy" class="lang disc_fixed">' + policyTextSecLang + '</span>');
                            }
                        } else {
                            depositAmount = parseFloat(bookingTotal).toFixed(2);

                            if ($("span.disc_guaranteed").length == 0) { // Swapping the text of the cancellation policy
                                policyText = $("p.subheader").data("text_guaranteed");
                                policyTextSecLang = $("p.subheader").data("textseclang_guaranteed");
                                $("p.subheader").empty();
                                lang === "1" ? $("p.subheader").append('<span data-lang="step_8_policy_guaranteed" class="lang disc_guaranteed">' + policyText + '</span>') : $("p.subheader").append('<span data-lang="step_8_policy_guaranteed" class="lang disc_guaranteed">' + policyTextSecLang + '</span>');
                            }
                        }
                        bookingTotal > 0 ? formatRate(parseFloat(depositAmount).toFixed(2), thousandSeparator, decimalSeparator) : formatRate("0.00", thousandSeparator, decimalSeparator);
                        $("span#deposit_sum").text(rateFormat);

                        // Balance
                        var balance = parseFloat(bookingTotal).toFixed(2) - parseFloat(depositAmount).toFixed(2);
                        balance > 0 ? formatRate(balance.toFixed(2), thousandSeparator, decimalSeparator) : formatRate("0.00", thousandSeparator, decimalSeparator);
                        $("span#balance_sum").text(rateFormat);

                        /** Display of Surcharges and Taxes */

                        function displaySurchargesTaxes(rateId, rateName, rateAmount, ratePerCent, rateType, thousand, decimal) {
                            var currency = $("span#surcharge").data("currency");
                            var surchargeTextlabel = $("span#surcharge").data("textlabel");
                            var taxesTextlabel = $("span#taxes").data("textlabel");
                            var type = rateType === "0" ? type = "tax" : type = "surcharge";
                            var displayDefaultValues = [];

                            $("select.room-quantity").each(function() {
                                $(this).val() === "rooms_quantity" ? displayDefaultValues.push("0") : displayDefaultValues.push("1");
                            });

                            switch (rateType) {

                                case "1": // Surcharges

                                    if (displayDefaultValues.indexOf("1") > -1) {
                                        $("p.surcharge-default").length > 0 ? $("p.surcharge-default").hide() : "";

                                        if (rateAmount != false && rateAmount !== "0.00") {
                                            $("p.surcharge[data-id='" + rateId + "']").remove();
                                            formatRate(parseFloat(rateAmount).toFixed(2), thousand, decimal);
                                            $("span#surcharge").append('<p class="booking-summary surcharge" data-id="' + rateId + '"><span data-lang="step_7_surcharge_text_label" class="lang" data-type="' + type + '">' + surchargeTextlabel + '</span> (<span id="' + rateId + '" data-lang="taxes_' + rateId + '" class="lang surchargeName">' + rateName + '</span>): ' + currency + '<span id="' + rateId + '" data-type="surcharge_' + type + '" class="surchargeAmount">' + rateFormat + '</span></p>');
                                        } else if (ratePerCent != false && ratePerCent !== "0.00") {
                                            $("p.surcharge[data-id='" + rateId + "']").remove();

                                            if ($("p.surcharge[data-id='" + rateId + "']").length == 0) {
                                                if (ratePerCent !== "0.00") {
                                                    formatRate(ratePerCent.toFixed(2), thousand, decimal);
                                                    $("span#surcharge").append('<p class="booking-summary surcharge" data-id="' + rateId + '"><span id="' + rateId + '" data-type="' + type + '"><span data-lang="step_7_surcharge_text_label" class="lang">' + surchargeTextlabel + '</span> (<span id="' + rateId + '" data-lang="taxes_' + rateId + '" class="lang surchargeName">' + rateName + '</span>): ' + currency + '<span id="' + rateId + '" data-type="surcharge_' + type + '" class="surchargeAmount">' + rateFormat + '</span></span></p>');
                                                }
                                            }
                                        }
                                    } else {
                                        $("p.surcharge").remove();
                                        $("p.surcharge-default").show();
                                    }
                                    break;

                                case "0": // Taxes

                                    if (displayDefaultValues.indexOf("1") > -1) {
                                        $("p.taxes-default").length > 0 ? $("p.taxes-default").hide() : "";

                                        if (rateAmount != false && rateAmount !== "0.00") {
                                            $("p.taxes[data-id='" + rateId + "']").remove();
                                            formatRate(parseFloat(rateAmount).toFixed(2), thousand, decimal);
                                            $("span#taxes").append('<p class="booking-summary taxes" data-id="' + rateId + '"><span data-lang="step_7_taxes_text_label" class="lang" data-type="' + type + '">' + taxesTextlabel + '</span> (<span id="' + rateId + '" data-lang="taxes_' + rateId + '" class="lang taxName">' + rateName + '</span>): ' + currency + '<span id="' + rateId + '" data-type="' + type + '" class="taxAmount">' + rateFormat + '</span></p>');
                                        } else if (ratePerCent != false && ratePerCent !== "0.00") {
                                            $("p.taxes[data-id='" + rateId + "']").remove();

                                            if ($("p.taxes[data-id='" + rateId + "']").length == 0) {
                                                if (ratePerCent !== "0.00") {
                                                    formatRate(ratePerCent.toFixed(2), thousand, decimal);
                                                    $("span#taxes").append('<p class="booking-summary taxes" data-id="' + rateId + '"><span id="' + rateId + '" data-type="' + type + '"><span data-lang="step_7_taxes_text_label" class="lang">' + taxesTextlabel + '</span> (<span id="' + rateId + '" data-lang="taxes_' + rateId + '" class="lang taxName">' + rateName + '</span>): ' + currency + '<span id="' + rateId + '" data-type="taxes_' + type + '" class="taxAmount">' + rateFormat + '</span></span></p>');
                                                }
                                            }
                                        } else {
                                            $("p.taxes").remove();
                                            $("p.taxes-default").show();
                                        }
                                    } else {
                                        $("p.taxes").remove();
                                        $("p.taxes-default").show();
                                    }
                                    break;
                            }
                        }
                        $("button.submit").length > 0 ? $("button.submit").attr("disabled", false) : "";
                    }
                });
            }, 1000);
        });

        // Formatting the rates (thousands and decimal separators as well)
        function formatRate(number, thousand, decimal) {
            var y = number.split(".");
            var th1, th2;

            switch (true) {

                case y[0].length == 4:
                    th1 = y[0].slice(0, 1);
                    th2 = y[0].slice(1);
                    rateFormat = th1 + thousand + th2 + decimal + y[1];
                    break;

                case y[0].length == 5:
                    th1 = y[0].slice(0, 2);
                    th2 = y[0].slice(2);
                    rateFormat = th1 + thousand + th2 + decimal + y[1];
                    break;

                case y[0].length == 6:
                    th1 = y[0].slice(0, 3);
                    th2 = y[0].slice(3);
                    rateFormat = th1 + thousand + th2 + decimal + y[1];
                    break;

                case y[0].length == 7:
                    th1 = y[0].slice(0, 4);
                    th2 = y[0].slice(4);
                    rateFormat = th1 + thousand + th2 + decimal + y[1];
                    break;

                default:
                    rateFormat = y[0] + decimal + y[1];
                    break;
            }
            return parseFloat(rateFormat).toFixed(2);
        }
    }
});