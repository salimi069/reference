<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public $mainNav = null;
    public $slider = null;
    public $albumRoot = null;
    public $catContent = null;
    public $artContent = null;
    public $artMetaContent = false;
    private $imgFileType = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data');
        $this->imgFileType = ['jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF'];
    }
    
    /* Főoldal és főmenü */
    public function index()
    {
        $this->load->view('templates/site/head');

        $homePage = $this->Data->getData('article', 'artTitleAlias', ['homePage' => 1], false, 1);
        $albums = scandir('assets/images/articles');

        // Slider
        if (!empty($homePage)) {
            if (in_array($homePage[0]->artTitleAlias, $albums)) {
                $this->albumRoot = $homePage[0]->artTitleAlias;
                $albumFiles = scandir('assets/images/articles/' . $homePage[0]->artTitleAlias);

                if (in_array('imgData.json', $albumFiles)) {
                    $sliderImages = file_get_contents('assets/images/articles/' . $homePage[0]->artTitleAlias . '/imgData.json');
                    $this->slider = json_decode($sliderImages);
                }
            }
        }

        $this->load->view('pages/site/home');
        
        // Főmenü
        $this->mainNav = $this->Data->getData('menu', 'menuId, menuTitle, menuType', ['menuStat' => 1], 'menuPos ASC');

        $this->load->view('templates/site/nav');
        $this->load->view('templates/site/footer');
    }

    /** Tartalom */
    public function getContent($id, $linkType)
    {
        if ($linkType == 1) {
            $content = $this->Data->getData('article', 'artTitle, artTitleShow, artText, artCat, artThumb, artMetaKey, artMetaDesc', ['artParentMenu' => $id], 1);
        } elseif ($linkType == 2) {
            $content = $this->Data->getDataJoin('catTitle, catId, menuTitle', 'menu', 'category', 'menu.menuId = category.catMenu', ['category.catMenu' => $id, 'category.catStat' => 1], 'category.catPos ASC');
        }
        echo json_encode($content);
    }

    /** Kategória aloldal megjelenítése */
    public function cat($catId, $menuId)
    {
        $this->load->view('templates/site/head');

        // Kategória és menü adatok (kategória és menü név az oldalmenüben)
        $this->catContent = $this->Data->getDataJoin('catId, catTitle, menuId, menuTitle', 'menu', 'category', 'menu.menuId = category.catMenu', ['category.catMenu' => $menuId, 'category.catStat' => 1], 'category.catPos ASC');
        
        // Cikk adatok (cím és bélyegkép)
        $this->artContent = $this->Data->getData('article', 'artId, artTitle, artTitleAlias, artThumb', ['artCat' => $catId, 'artStat' => 1], 'artMenuPos ASC');

        $this->load->view('templates/site/catNav');
        $this->load->view('pages/site/cat');
        $this->load->view('templates/site/footer');
    }

    /** Cikk aloldal megjelenítése */
    public function art($artId, $menuId)
    {
        $this->artMetaContent = true;
        // Cikk tartalom
        $this->artContent = $this->Data->getData('article', 'artId, artTitleAlias, artText, artMetaKey, artMetaDesc', ['artId' => $artId, 'artStat' => 1], false, 1);
        $this->load->view('templates/site/head');

        // Kategória és menü adatok (kategória és menü név az oldalmenüben)
        $this->catContent = $this->Data->getDataJoin('catId, catTitle, menuId, menuTitle', 'menu', 'category', 'menu.menuId = category.catMenu', ['category.catMenu' => $menuId, 'category.catStat' => 1], 'category.catPos ASC');

        // Cikk képek
        if (in_array($this->artContent[0]->artTitleAlias, scandir('assets/images/articles'))) {
            $this->albumRoot = $this->artContent[0]->artTitleAlias;
            
            if (file_exists('assets/images/articles/' . $this->albumRoot . '/imgData.json')) {
                $sliderImages = file_get_contents('assets/images/articles/' . $this->albumRoot . '/imgData.json');
                $this->slider = json_decode($sliderImages);
            } else {
                $this->slider = [];
                $files = scandir('assets/images/articles/' . $this->albumRoot);

                foreach ($files as $file) {
                    if (in_array(pathinfo('assets/images/articles/' . $this->albumRoot . '/' . $file, PATHINFO_EXTENSION), $this->imgFileType)) {
                        array_push($this->slider, $file);
                    }
                }
            }
        }

        $this->load->view('templates/site/catNav');
        $this->load->view('pages/site/art');
        $this->load->view('templates/site/footer');
    }

    public function getArtContent($artId)
    {
        $content = $this->Data->getData('article', 'artTitle, artText, artMetaKey, artMetaDesc', ['artId' => $artId], false, 1);
        echo json_encode($content);
    }
}
