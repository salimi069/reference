<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public $validation = null;
    public $menuValidation = null;    
    public $menuList = null;
    public $menuCount = null;
    public $menu = null;
    public $userAccess = null;
    public $homePage = null;    

    public function __construct() {
        parent::__construct();
        require_once('assets/php/baseFunctions.php');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Data');
        $this->userAccess = $this->session->userdata('userAccess');
    }

    /** Főoldal */
    public function index() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/menuNav');
            $this->load->view('pages/admin/menu');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Menüpont készítése */
    public function createMenu() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/menuNav');

            $this->form_validation->set_rules('menuTitle', 'a menüpont nevét', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg %s!']);
            $this->form_validation->set_rules('menuType', 'a menüpont típusát', 'strip_tags|trim|required', ['required' => 'Kérem, válassza ki %s!']);
            $this->form_validation->set_rules('menuPos', 'a menüpont pozícióját', 'strip_tags|trim|required', ['required' => 'Kérem, válassza ki %s!']);
            $this->menuList = $this->Data->getData('menu', false, false, 'menuPos ASC');        
            $this->menuCount = count($this->menuList);       
                
            if(!$this->form_validation->run()) {
                $this->validation = false;

                if(!empty($this->input->post('menuTitle'))) {                
                    $alias = BaseFunctions::createAlias($this->input->post('menuTitle'));
                    $titleCheck = $this->Data->getData('menu', false, ['menuAlias' => $alias], false, 1);                
                    $this->menuValidation = count($titleCheck) > 0 ? false : true;
                }
                else {
                    $this->menuValidation = true;
                } 
            }
            else {            
                $alias = BaseFunctions::createAlias($this->input->post('menuTitle'));
                $titleCheck = $this->Data->getData('menu', false, ['menuAlias' => $alias], false, 1);                        
                $this->menuValidation = count($titleCheck) > 0 ? false : true;            

                if($this->menuValidation) {

                    if($this->menuCount > 0) {
                        foreach($this->menuList as $menu) {
                    
                            if($menu->menuPos == $this->input->post('menuPos')) {
                                $this->Data->updateData('menu', ['menuPos' => ++$this->menuCount], ['menuPos' => $this->input->post('menuPos')], 1);                
                            }
                        }
                    }

                    $formData = [
                        'menuTitle' => $this->input->post('menuTitle'),
                        'menuAlias' => $alias,
                        'menuType' => $this->input->post('menuType'),
                        'menuPos' => !empty($this->input->post('menuPos')) ? $this->input->post('menuPos') : '',
                        'menuStat' => !empty($this->input->post('menuStat')) ? $this->input->post('menuStat') : '',
                    ];

                    $this->Data->saveData('menu', $formData);
                    $this->validation = !empty($this->Data->dbResponse) && $this->Data->dbResponse === 'success' ? true : false;            
                }                          
            }
            $this->load->view('pages/admin/createmenu');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');        
        }
    }

    /** Menük kezelése */
    public function manageMenu() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/menuNav');
        
            $this->menuList = $this->Data->getData('menu', false, false, 'menuPos ASC');

            if(count($this->menuList) > 0) {
                $counter = 1;
                $this->load->library('table');                

                $template = [
                    'table_open' => '<table class="w3-table-all">',
                    'thead_open' => '<thead>',
                    'thead_close' => '</thead>',
                    'tbody_open' => '<tbody id="table">',
                    'tbody_close' => '</tbody>'
                ];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Név', 'Típus', 'Pozíció', 'Státusz', 'Kezelés');

                foreach($this->menuList as $menu) {
                    $type = $menu->menuType == 1 ? 'Cikk' : 'Kategória';
                    $stat = $menu->menuStat == 1 ? 'Aktív' : 'Inaktív';

                    $this->table->add_row($counter++, $menu->menuTitle, $type, $menu->menuPos, $stat, '<a href="' . base_url() . 'index.php/admin/menu/updatemenu/' . $menu->menuId . '" class="w3-btn w3-orange">Kezelés</a>');
                }
            }

            $this->load->view('pages/admin/managemenu');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Menük frissítése */
    public function updateMenu($menuId) {
        
        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/menuNav');

            $this->menu = $this->Data->getData('menu', false, ['menuId' => $menuId], false, 1);
            $this->menuList = $this->Data->getData('menu', false, false, 'menuPos ASC');
            $this->menuCount = count($this->menuList);
            $this->homePage = $this->Data->getData('article', 'homePage', ['artParentMenu' => $menuId], false, 1);            

            $this->form_validation->set_rules('menuTitle', 'a menüpont nevét', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg %s!']);
            $this->form_validation->set_rules('menuType', 'a menüpont típusát', 'strip_tags|trim|required', ['required' => 'Kérem, válassza ki %s!']);
            $this->form_validation->set_rules('menuPos', 'a menüpont pozícióját', 'strip_tags|trim|required', ['required' => 'Kérem, válassza ki %s!']);

            if(!$this->form_validation->run()) {
                $this->validation = false;

                if(!empty($this->input->post('menuTitle'))) {
                    $alias = BaseFunctions::createAlias($this->input->post('menuTitle'));
                    $titleCheck = $this->Data->getData('menu', false, ['menuAlias' => $alias], false, 1);                
                    $this->menuValidation = count($titleCheck) > 0 ? false : true;
                }
                else {
                    $this->menuValidation = true;
                } 
            }
            else {
                $alias = BaseFunctions::createAlias($this->input->post('menuTitle'));
                $titleCheck = $this->Data->getData('menu', false, "menuAlias = '$alias' AND menuId != '$menuId'", false, 1);            
                $this->menuValidation = count($titleCheck) > 0 ? false : true;            

                if($this->menuValidation) {

                    if($this->menuCount > 0) {
                        foreach($this->menuList as $menu) {
                    
                            if($menu->menuPos == $this->input->post('menuPos')) {
                                $this->Data->updateData('menu', ['menuPos' => $this->menu[0]->menuPos], ['menuPos' => $this->input->post('menuPos')], 1);                
                            }
                        }
                    }

                    if(!empty($this->homePage[0]->homePage) && $this->homePage[0]->homePage == 1) { // Menü státusza (főoldal mindig aktív)
                        $menuStat = 1;
                    }
                    else if(empty($this->homePage[0]->homePage) || $this->homePage[0]->homePage == 0) {
                        $menuStat = !empty($this->input->post('menuStat')) ? $this->input->post('menuStat') : '';
                    }


                    $formData = [
                        'menuTitle' => $this->input->post('menuTitle'),
                        'menuAlias' => $alias,
                        'menuType' => $this->input->post('menuType'),
                        'menuPos' => !empty($this->input->post('menuPos')) ? $this->input->post('menuPos') : '',
                        'menuStat' => $menuStat
                    ];

                    $this->Data->updateData('menu', $formData, ['menuId' => $menuId], 1);
                    $this->validation = !empty($this->Data->dbResponse) && $this->Data->dbResponse === 'success' ? true : false;            
                }          
            }

            $this->load->view('pages/admin/updatemenu');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Menü törlése */
    public function deleteMenu($menuId) {
        $menu = $this->Data->getData('menu', false, ['menuId' => $menuId], false, 1);
        $menuPos = $menu[0]->menuPos;
        $menuList = $this->Data->getData('menu', false, "$menuPos < menuPos");
        $catMenu = $this->Data->getData('category', false, ['catMenu' => $menuId], false, 1);
        $artMenu = $this->Data->getData('article', false, ['artParentMenu' => $menuId], false, 1);        

        if(count($catMenu) == 0 && count($artMenu) == 0) {
            if(count($menuList) > 0) {

                foreach($menuList as $menuItem) {
                    $newMenuPos = --$menuItem->menuPos;
                    $this->Data->updateData('menu', ['menuPos' => $newMenuPos], ['menuId' => $menuItem->menuId]);
                }
            }                 

            $this->Data->deleteData('menu', ['menuId' => $menuId], 1);
            echo(empty($this->Data->dbResponse)) ? 'success' : 'fail';
        }
        else {

            switch(true) {

                case count($catMenu) > 0:            
                echo 'A menüpont kategóriát tartalmaz. Kérem, először törölje a kategóriát!';
                break;

                case count($artMenu) > 0:
                echo 'A menüpont cikket tartalmaz. Kérem, először törölje a cikket!';
                break;
            }
        }
    }     
}