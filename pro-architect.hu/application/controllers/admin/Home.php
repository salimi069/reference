<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public $validation = null;
    public $alert = null;
    public $adminCheckRes = null;
    public $users = null;    

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Data');
    }  
        
    /** Admin főoldal */
    public function index() {
        
        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/sideNav');          
            $this->load->view('pages/admin/home');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Bejelentkezés */
    public function login() {        
        $this->load->view('templates/admin/head');
        $this->load->view('templates/admin/mainNav');
        $this->load->view('templates/admin/sideNav');
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('loginName', 'felhasználó nevét', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a %s!']);
        $this->form_validation->set_rules('loginPass', 'jelszavát', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a %s!']);

        if(!$this->form_validation->run()) {
            $this->validation = false;            
        }
        else {
            $this->validation = true;
            $this->load->model('Data');
            $user = $this->Data->getData('users', 'userId, userAccess, userPass, userStat', ['userEmail' => $this->input->post('loginName')], false, 1);            

            if(count($user) > 0) {

                if(password_verify($this->input->post('loginPass'), $user[0]->userPass)) {

                    if($user[0]->userStat == 1) {
                        $this->load->library('session');
                        $sessionData = [
                            'userId' => $user[0]->userId,
                            'userAccess' => $user[0]->userAccess                            
                        ];
                        $this->session->set_userdata($sessionData);
                        header('Location: ' . base_url() . 'index.php/admin/home');
                    }
                    else {
                        $this->validation = false;
                        $this->alert = 'Önnek nincs jogosultsága belépni az oldalra. Kérem, lépjen kapcsolatba az adminisztrátorral!';
                    }
                }
                else {
                    $this->validation = false;
                    $this->alert = 'Helytelen jelszó. Kérem, javítsa!';
                }
            }
            else {
                $this->validation = false;
                $this->alert = 'Sajnos nincs ilyen felhasználó név. Kérem, javítsa!';
            }
        }

        $this->load->view('pages/admin/login');
        $this->load->view('templates/admin/footer');
    }

    /** Kijelentkezés */
    public function logout() {
        $sessionData = ['userId', 'userAccess'];
        $this->load->library('session');
        $adminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);
        $homePageCheck = $this->Data->getData('article', false, ['homePage' => 1], false, 1);
        $this->alert = [];

        if(count($homePageCheck) > 0) {
            $homePageCheck[0]->artCat == 0 && $homePageCheck[0]->artParentMenu == 0 ? $this->alert[] = 'homePageLinkFail' : '';
        }
        else {
            $this->alert[] = 'homePageFail';
        }        
        count($adminCheck) == 0 ? $this->alert[] = 'adminFail' : '';
        
        if(count($this->alert) == 0) {
            $this->adminCheckRes = true;
            $this->session->unset_userdata($sessionData);
            $this->session->sess_destroy();
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
        else {
            if($this->session->has_userdata('userId')) {
                $this->load->view('templates/admin/head');
                $this->load->view('templates/admin/mainNav');
                $this->load->view('templates/admin/sideNav');                
                $this->load->view('pages/admin/home');
                $this->load->view('templates/admin/footer');
            }
            else {
                header('Location: ' . base_url() . 'index.php/admin/home/login');
            }
        }                    
    }
}