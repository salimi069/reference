<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

    public $validation = null;
    public $menuList = null;
    public $menuCount = null;    
    public $catList = null;
    public $catTitleValid = null;
    public $cat = null;
    public $catMenu = null;
    public $catName = null;
    public $artList = null;
    public $art = null;
    public $artStat = null;
    public $imgRootFolder = null;
    public $images = [];
    public $userAccess = null;
    public $homePageCheck = null;
    private $_imgFileType = [];    

    public function __construct() {
        parent::__construct();
        require_once('assets/php/baseFunctions.php');        
        $this->load->model('Data');
        $this->load->library('session');        
        $this->_imgFileType = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'];
        $this->imgRootFolder = 'assets/images/articles/';
        $this->userAccess = $this->session->userdata('userAccess'); 
    }

    /** Főoldal */
    public function index() {
        
        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');                  
            $this->load->view('pages/admin/content');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** 
     * KATEGÓRIÁK
    */

    /** Kategória készítése */
    public function createCategory() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');

            // Menük lekérdezése
            $this->menuList = $this->Data->getData('menu', 'menuId, menuTitle', ['menuType' => 2], 'menuTitle ASC');                                    

            $this->load->view('pages/admin/createcategory');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Kategória mentése */
    public function saveCategory() {

        if(isset($_POST)) {
            $data = json_decode($_POST['form']);

            $alias = BaseFunctions::createAlias(BaseFunctions::protectInput($data->catTitle));
            $catTitleCheck = $this->Data->getData('category', false, ['catAlias' => $alias], false, 1);
            $this->catTitleValid = count($catTitleCheck) > 0 ? false : true;

            if($this->catTitleValid) {

                $catList = $this->Data->getData('category', 'catId, catTitle, catPos', ['catMenu' => $data->catMenu], 'catPos ASC'); 

                if(count($catList) > 0) {
                    $catCount = count($catList);

                    foreach($catList as $cat) {

                        if($cat->catPos === $data->catPos) {
                            $this->Data->updateData('category', ['catPos' => ++$catCount], ['catPos' => $data->catPos], 1);
                        }
                    }
                }                           

                $formData = [
                    'catTitle' => BaseFunctions::protectInput($data->catTitle),
                    'catAlias' => $alias,
                    'catMenu' => BaseFunctions::protectInput($data->catMenu),
                    'catPos' =>  BaseFunctions::protectInput($data->catPos),
                    'catStat' => !empty($data->catStat) ? BaseFunctions::protectInput($data->catStat) : '' 
                ];                
                
                $this->Data->saveData('category', $formData);
                echo(!empty($this->Data->dbResponse)) ? $this->Data->dbResponse : 'Sikertelen mentés!';                
            }
            else {
                echo 'Ezen a néven már van kategória elmentve. Kérem, válasszon egy másik nevet!';
            }            
        }
    }

    /** Kategória kezelése */
    public function manageCategory() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');

            $this->catList = $this->Data->getData('category', false, false, 'catMenu ASC');

            if(count($this->catList) > 0) {
                $this->load->library('table');
                $counter = 1;
                                
                $template = [
                    'table_open' => '<table class="w3-table-all">',
                    'thead_open' => '<thead>',
                    'thead_close' => '</thead>',
                    'tbody_open' => '<tbody id="table">',
                    'tbody_close' => '</tbody>'
                ];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Cím', 'Menüpont', 'Pozíció', 'Státusz', 'Kezelés');

                foreach($this->catList as $cat) {
                    $menu = $this->Data->getData('menu', 'menuTitle', ['menuId' => $cat->catMenu], false, 1);               
                    $stat = $cat->catStat == 1 ? 'Aktív' : 'Inaktív';
                    $this->table->add_row($counter++, $cat->catTitle, $menu[0]->menuTitle, $cat->catPos, $stat, '<a href="' . base_url() . 'index.php/admin/content/updatecategory/' . $cat->catId . '" class="w3-btn w3-orange">Kezelés</a>');
                }
            }        
        
            $this->load->view('pages/admin/managecategory');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Kategória frissítése */
    public function updateCategory($catId) {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');                         

            //Menüpontok lekérdezése
            $this->menuList = $this->Data->getData('menu', 'menuId, menuTitle', ['menuType' => 2], 'menuTitle ASC');

            // Főoldal ellenőrzése
            $this->homePageCheck = $this->Data->getData('article', false, ['artCat' => $catId, 'homePage' => 1], false, 1);

            // Kategória lekérdezése
            $this->cat = $this->Data->getData('category', false, ['catId' => $catId], false, 1);        
        
            $this->load->view('pages/admin/updatecategory');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Frissítés mentése */
    public function saveUpdate($catId) {

        if(isset($_POST)) {
            $data = json_decode($_POST['form']);
            $origData = $this->Data->getData('category', false, ['catId' => $catId], false, 1);            
            $catList = $this->Data->getData('category', false, ['catMenu' => $origData[0]->catMenu]);
            $alias = BaseFunctions::createAlias($data->catTitle);
            $catTitleCheck = $this->Data->getData('category', false, "catAlias = '$alias' AND catId != $catId", false, 1);
            $this->homePageCheck = $this->Data->getData('article', false, ['artCat' => $catId, 'homePage' => 1], false, 1);            

            if(count($catTitleCheck) == 0) {
                
                if($data->catPos !== $origData[0]->catPos && $data->catMenu === $origData[0]->catMenu) {
                    $this->Data->updateData('category', ['catPos' => $origData[0]->catPos], "catPos = $data->catPos AND catMenu = $data->catMenu", 1);
                }
                else if($data->catMenu !== $origData[0]->catMenu) {
                    $newCat = $this->Data->getData('category', false, ['catMenu' => $data->catMenu]);

                    foreach($newCat as $newCatItem) {

                        if($newCatItem->catPos >= $data->catPos) {
                            $this->Data->updateData('category', ['catPos' => ++$newCatItem->catPos], ['catId' => $newCatItem->catId], 1);
                        }
                    }

                    foreach($catList as $origCatItem) {

                        if($origCatItem->catPos > 1 && $origCatItem->catPos >= $origData[0]->catPos) {
                            $this->Data->updateData('category', ['catPos' => --$origCatItem->catPos], ['catId' => $origCatItem->catId], 1);
                        }
                    }
                }
                
                if(!empty($this->homePageCheck[0]->homePage) && $this->homePageCheck[0]->homePage == 1) {
                    $catStat = 1;
                }
                else if(empty($this->homePageCheck[0]->homePage) || $this->homePageCheck[0]->homePage == 0) {
                    $catStat = BaseFunctions::protectInput($data->catStat);
                }
                
                $formData = [
                    'catTitle' => BaseFunctions::protectInput($data->catTitle),
                    'catAlias' => $alias,
                    'catMenu' => BaseFunctions::protectInput($data->catMenu),
                    'catPos' => BaseFunctions::protectInput($data->catPos),
                    'catStat' => $catStat
                ];

                $this->Data->updateData('category', $formData, ['catId' => $catId], 1);
                echo(!empty($this->Data->dbResponse)) ? $this->Data->dbResponse : 'fail';
            }
            else {
                echo 'Ezen a néven már van kategória elmentve. Kérem, válasszon egy másik nevet!';
            }                       
        }
    }

    /** Kategória törlése */
    public function deleteCategory($catId) {
        $cat = $this->Data->getData('category', false, ['catId' => $catId], false, 1);
        $artInCat = $this->Data->getData('article', false, ['artCat' => $catId], false, 1);
        $catPos = $cat[0]->catPos;
        $catMenu = $cat[0]->catMenu;
        $catList = $this->Data->getData('category', false, "$catPos < catPos AND catMenu = $catMenu");

        if(count($artInCat) == 0) {

            if(count($catList) > 0) {            

                foreach($catList as $catItem) {
                    $newCatPos = --$catItem->catPos;
                    $this->Data->updateData('category', ['catPos' => $newCatPos], ['catId' => $catItem->catId]);
                }
            }        
        
        $this->Data->deleteData('category', ['catId' => $catId], 1);
        echo(count($this->Data->dbResponse) > 0) ? 'fail' : 'success';
        }
        else {
            echo 'A kategória cikk(ek)et tartalmaz. Kérem, először azokat törölje!';
        }        
    }

    /** Azonos menüben lévő kategóriák lekérdezése */
    public function getCatListInMenu($menu) {
        $catList = $this->Data->getData('category', 'catId, catTitle, catPos', ['catMenu' => $menu], 'catPos ASC');        
        echo(count($catList) > 0) ? json_encode($catList) : 'fail';        
    }

    /** 
     * CIKKEK
    */

    /**Cikk készítése */
    public function createArticle() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');

            // Kategóriák lekérdezése
            $this->catList = $this->Data->getData('category', 'catId, catTitle', false, 'catTitle ASC');

            // Menük lekérdezése
            $assignedMenus = $this->Data->getData('article', 'artParentMenu', 'artParentMenu != 0');
            $artMenus = $this->Data->getData('menu', 'menuId', ['menuType' => 1]);
            $artMenuId = [];
            $menuIds = [];
            $menuId = [];
            $menuTitle = [];
        
            foreach($assignedMenus as $menuItem) {
                array_push($artMenuId, $menuItem->artParentMenu);            
            }

            foreach($artMenus as $menu) {
                array_push($menuIds, $menu->menuId);
            }

            $freeMenus = array_diff($menuIds, $artMenuId);

            foreach($freeMenus as $menu) {
                $menuAttrs = $this->Data->getData('menu', 'menuTitle', ['menuId' => $menu]);            
                array_push($menuId, $menu);

                foreach($menuAttrs as $attr) {                              
                    array_push($menuTitle, $attr->menuTitle);
                }       
            }     

            $this->menuList = array_combine($menuId, $menuTitle);
            
            // Főoldal ellenőrzés
            $homePageQuery = $this->Data->getData('article', false, ['homePage' => 1], false, 1);
            $this->homePageCheck = count($homePageQuery) > 0 ? false : true;            
              
            $this->load->view('pages/admin/createarticle');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Menüpozíciók kezelése */
    public function getMenuPosList($catId) {
        $menuPosList = $this->Data->getData('article', 'artTitle, artMenuTitle, artMenuPos', "artMenuPos != 0 AND artCat = $catId", 'artMenuPos ASC');
        echo(!empty($menuPosList)) ? json_encode($menuPosList) : '';
    }
    
    /** Cikk mentése  */
    public function saveArticle() {

        if(isset($_POST)) {
            $data = $_POST;
            $img = !empty($_FILES['artImg']) ? $_FILES['artImg'] : '';
            $artTitleAlias = BaseFunctions::createAlias($data['artTitle']);
            $artMenuAlias = !empty($data['artMenuTitle']) ? BaseFunctions::createAlias($data['artMenuTitle']) : '';
            $artTitleAliasValid = true;
            $artMenuAliasValid = true;
            $maxFileUpload = true;          
                        
            // Cím ellenőrzés
            $artTitleAliasCheck = $this->Data->getData('article', false, ['artTitleAlias' => $artTitleAlias], false, 1);
            $artTitleAliasValid = count($artTitleAliasCheck) == 0 ? true : false;

            // Menünév ellenőrzés
            if(!empty($artMenuAlias)) {
                $artMenuAliasCheck = $this->Data->getData('article', false, ['artMenuAlias' => $artMenuAlias], false, 1);
                $artMenuAliasValid = count($artMenuAliasCheck) > 0 ? false : true;
            }

            // Menüpozíció ellenőrzés és szükség esetén frissítés
            if(!empty($data['artMenuPos'])) {
                $menuPos = $data['artMenuPos'];
                $cat = $data['artCat'];
                $menuPosList = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= $menuPos AND artCat = $cat");

                if(count($menuPosList) > 0) {

                    foreach($menuPosList as $menuItem) {
                        $this->Data->updateData('article', ['artMenuPos' => ++$menuItem->artMenuPos], ['artId' => $menuItem->artId]);
                    }
                }
            }
            
            // Feltöltendő képfájlok számának ellenőrzése (max. 10 db)
            if(!empty($img)) {
                $maxFileUpload = count($img['name']) <= 10 ? true : false;
            }

            // Státusz beállítása a főoldalnak történő kiválasztás függvényében
            $artStat = (!empty($data['homePage']) && $data['homePage'] == 1) || !empty($data['artParentMenu']) ? 1 : trim($this->input->post('artStat'));

            if($artTitleAliasValid && $artMenuAliasValid && $maxFileUpload) {
            
                $formData = [
                    'artTitle' => trim($this->input->post('artTitle')),
                    'artTitleAlias' => trim($artTitleAlias),
                    'artTitleShow' => !empty($data['artTitleShow']) ? trim($this->input->post('artTitleShow')) : '',
                    'artText' => !empty($data['artText']) ? trim($this->input->post('artText')) : '',
                    'artCat' => !empty($data['artCat']) ? trim($this->input->post('artCat')) : '',
                    'artMenuTitle' => !empty($data['artMenuTitle']) ? trim($this->input->post('artMenuTitle')) : NULL,
                    'artMenuAlias' => !empty($artMenuAlias) ? trim($artMenuAlias) : NULL,
                    'artMenuPos' => !empty($data['artMenuPos']) ? trim($this->input->post('artMenuPos')) : '',
                    'artParentMenu' => !empty($data['artParentMenu']) ? trim($this->input->post('artParentMenu')) : '',
                    'homePage' => !empty($data['homePage']) ? trim($this->input->post('homePage')) : '',
                    'artMetaKey' => !empty($data['artMetaKey']) ? trim($this->input->post('artMetaKey')) : NULL,
                    'artMetaDesc' => !empty($data['artMetaDesc']) ? trim($this->input->post('artMetaDesc')) : NULL,
                    'artStat' => $artStat 
                ];

                if(!empty($_FILES)) {
                    $img = $_FILES['artImg'];
                
                    if(!empty($img['name'][0])) {
                        mkdir($this->imgRootFolder . $artTitleAlias);
                        copy($this->imgRootFolder . 'index.html', $this->imgRootFolder . $artTitleAlias . '/index.html');
                        $imgNum = count($img['name']);

                        for($ci = 0; $ci < $imgNum; $ci++) {

                            if(in_array(pathinfo($img['name'][$ci], PATHINFO_EXTENSION), $this->_imgFileType)) {                            
                                move_uploaded_file($img['tmp_name'][$ci], $this->imgRootFolder . $artTitleAlias . '/' . $img['name'][$ci]);
                            }
                            else {
                                echo 'Az egyik fájl formátuma nem engedélyezett! Csak .jpg, .jpeg, .gif, .png kiterjesztésű fájlok tölthetők fel!';
                            }
                        }                             
                    }
                }
                $this->Data->saveData('article', $formData);
                $newArticle = $this->Data->getData('article', 'artId, artTitleAlias', ['artTitleAlias' => $artTitleAlias], false, 1);

                if(!empty($data['homePage']) && $data['homePage'] == 1) { // Főoldal menüstátuszának aktívra állítása (ha van menüpont elmentve)
                    
                    if(!empty($data['artParentMenu'])) {

                        $homePageMenu = $this->Data->getData('menu', false, ['menuId' => $data['artParentMenu']], false, 1);

                        if(!empty($homePageMenu) && $homePageMenu[0]->menuStat == 0) {
                            $this->Data->updateData('menu', ['menuStat' => 1], ['menuId' =>  $data['artParentMenu']], 1);
                        }
                    }
                    else if(!empty($data['artCat'])) {

                        $homePageCat = $this->Data->getData('category', false, ['catId' => $data['artCat']], false, 1);

                        if(!empty($homePageCat) && $homePageCat[0]->catStat == 0) {
                            $this->Data->updateData('category', ['catStat' => 1], ['catId' => $data['artCat']], 1);
                        }
                    }
                }
                
                if(in_array($newArticle[0]->artTitleAlias, scandir($this->imgRootFolder))) {
                    echo(!empty($this->Data->dbResponse)) ? $this->Data->dbResponse . '_' . $newArticle[0]->artId : 'Sikertelen mentés!';
                }
                else {
                    echo(!empty($this->Data->dbResponse)) ? $this->Data->dbResponse : 'Sikertelen mentés!';
                }
            }
            else {

                switch(true) {

                    case !$artTitleAliasValid:
                    echo 'Ezen a címen már szerepel cikk az adatbázisban. Kérem, válasszon másik címet!';
                    break;

                    case !$artMenuAliasValid:
                    echo 'Ezen a néven már szerepel menüpont az adatbázisban. Kérem, válasszon másik menüpont nevet!';
                    break;

                    case !$maxFileUpload:
                    echo 'Az egyszerre feltölthető képek maximális száma: 10';
                    break;
                }
            }            
        }
    }

    /** Cikkek kezelése */
    public function manageArticle($catName = false, $artStat = false) {
        $this->session->unset_userdata('catName');
        $this->session->unset_userdata('artStat');

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');

            $this->catList = $this->Data->getData('category', false, ['catStat' => 1], 'catTitle ASC');

            $getCatName = '';
            $getArtStat = '';

            if($catName != false || $artStat != false) { // Megjelenítés szűréssel
                $getCatName = $catName;
                $getArtStat = $artStat;
            }
            else if(isset($_POST['filter_send'])) {
                $this->session->unset_userdata('catName');
                $this->session->unset_userdata('artStat');
                $getCatName = $this->input->post('catName');
                $getArtStat = $this->input->post('artStat');
            }

            if(!empty($getCatName) || !empty($getArtStat)) {
                $sessionData = [
                    'catName' => $getCatName,
                    'artStat' => $getArtStat
                ];
                $this->session->set_userdata($sessionData);

                switch(true) {

                    case $this->session->catName !== 'select'  && $this->session->artStat !== 'select':
                    $this->artList = $this->Data->getData('article', false, ['artCat' => $this->session->catName, 'artStat' => $this->session->artStat], 'artMenuPos ASC');
                    break;

                    case $this->session->catName !== 'select'  && $this->session->artStat === 'select':
                    $this->artList = $this->Data->getData('article', false, ['artCat' => $this->session->catName], 'artMenuPos ASC');
                    break;

                    case $this->session->catName === 'select'  && $this->session->artStat !== 'select':
                    $this->artList = $this->Data->getData('article', false, ['artStat' => $this->session->artStat], 'artMenuPos ASC');
                    break;

                    default:
                    $this->artList = $this->Data->getData('article', false, false, 'artTitle ASC');
                    break;                                                        
                }
            }            
            else { // Megjelenítés szűrés nélkül
                $this->artList = $this->Data->getData('article', false, false, 'artTitle ASC');
            }            

            if(count($this->artList) > 0) {
                $this->load->library('table');
                $counter = 1;            
                $template = [
                    'table_open' => '<table class="w3-table-all">',
                    'thead_open' => '<thead>',
                    'thead_close' => '</thead>',
                    'tbody_open' => '<tbody id="table">',
                    'tbody_close' => '</tbody>'
                ];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Cím', 'Kategória', 'Menüpont', 'Szülő menü', 'Státusz', 'Kezelés');               
                
                foreach($this->artList as $art) {                

                    if($art->artCat != 0) {
                        $cat = $this->Data->getData('category', 'catTitle', ['catId' => $art->artCat], false, 1);
                        $artCat = $cat[0]->catTitle;
                        $artMenu = $art->artMenuTitle;
                    }
                    else {
                        $artCat = 'N/A';
                        $artMenu = 'N/A';
                    }                

                    if($art->artParentMenu != 0) {
                        $menu = $this->Data->getData('menu', 'menuTitle', ['menuId' => $art->artParentMenu], false, 1);
                        $menuParent = $menu[0]->menuTitle;                    
                    }
                    else {
                        $menuParent = 'N/A';
                    }                
                    $stat = $art->artStat == 1 ? 'Aktív' : 'Inaktív';                    

                    $this->table->add_row($counter++, $art->artTitle, $artCat, $artMenu, $menuParent, $stat, '<a href="' . base_url() . 'index.php/admin/content/updatearticle/' . $art->artId . '" class="w3-btn w3-orange">Kezelés</a>');
                }
            }          

            $this->load->view('pages/admin/managearticle');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Cikkek frissítése */
    public function updateArticle($artId) {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/articleNav');            
        
            $this->art = $this->Data->getData('article', false, ['artId' => $artId], false, 1);
            $this->catName = $this->session->has_userdata('catName') ? $this->session->catName : '';
            $this->artStat = $this->session->has_userdata('artStat') ? $this->session->artStat : '';

            // Kategóriák
            $this->catList = $this->Data->getData('category', 'catId, catTitle', false, 'catTitle ASC');
        
            // Szülő menüpontok megjelenítése
            $this->menuList = [];           
            $assignedArtMenus = $this->Data->getData('menu', 'menuId, menuTitle', ['menuType' => 1]);

            if($this->art[0]->artParentMenu != 0) {
                $menu = $this->Data->getData('menu', 'menuId, menuTitle', ['menuId' => $this->art[0]->artParentMenu]);
                $this->menuList[$menu[0]->menuId] = $menu[0]->menuTitle;
            }
        
            if(count($assignedArtMenus) > 0) {

                foreach($assignedArtMenus as $menuItem) {
                    $menuAttr = $this->Data->getData('article', false, ['artParentMenu' => $menuItem->menuId], false, 1);

                    if(count($menuAttr) == 0) {
                        $this->menuList[$menuItem->menuId] = $menuItem->menuTitle;
                    }
                }
            }                     
            
            // Főoldal beállítás ellenőrzése
            $homePageQuery = $this->Data->getData('article', false, ['homePage' => 1], false, 1);
            $this->homePageCheck = !empty($homePageQuery) ? true : false; 
        
            // Képek
            if(in_array($this->art[0]->artTitleAlias, scandir($this->imgRootFolder))) {
                $images = scandir($this->imgRootFolder . $this->art[0]->artTitleAlias);
                $sliderImages = '';

                if(file_exists($this->imgRootFolder . $this->art[0]->artTitleAlias . '/imgData.json')) {
                    $sliderImages = file_get_contents($this->imgRootFolder . $this->art[0]->artTitleAlias . '/imgData.json');
                    $sliderImgFiles = json_decode($sliderImages);
                }
            
                foreach($images as $img) {

                    $sliderImg = !empty($sliderImages) ? in_array($img, $sliderImgFiles) ? 1 : 0 : 0;
                
                    if(in_array(pathinfo($img, PATHINFO_EXTENSION), $this->_imgFileType)) {                        
                        $this->images[$img] = $sliderImg;
                    }
                }
            }            

            $this->load->view('pages/admin/updatearticle');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }  
    
    /** Cikk frissítés mentése */
    public function saveArticleUpdate($artId) {

        if(isset($_POST)) {
            $data = $_POST;
            $img = !empty($_FILES) ? $_FILES : '';
            $art = $this->Data->getData('article', false, ['artId' => $artId], false, 1);
            $this->imgRootFolder = 'assets/images/articles/';
            $errors = [];
            $formData = [];          
                        
            // Cím ellenőrzése
            $artTitleAlias = BaseFunctions::createAlias(BaseFunctions::protectInput($data['artTitle']));
            $artTitleAliasCheck = $this->Data->getData('article', false, "artTitleAlias = '$artTitleAlias' AND artId != $artId", false, 1);            
            count($artTitleAliasCheck) > 0 ? array_push($errors, 'artTitle') : '';
            
            // Menünév ellenőrzése
            $artMenuAlias = !empty($data['artMenuTitle']) ? BaseFunctions::createAlias(BaseFunctions::protectInput($data['artMenuTitle'])) : '';
            $artMenuAliasCheck = $this->Data->getData('article', false, "artMenuAlias = '$artMenuAlias' AND artId != $artId", false, 1);            
            count($artMenuAliasCheck) > 0 ? $artMenuAliasCheck !== NULL ? array_push($errors, 'menuTitle') : '' : '';

            // Menüpozíció és/vagy kategória váltás ellenőrzése, illetve szükség szerint frissítése
            $oldMenuPos = $art[0]->artMenuPos;
            $oldParentMenu = $art[0]->artParentMenu;
            $oldCat = $art[0]->artCat;
            $newMenuPos = !empty($data['artMenuPos']) ? $data['artMenuPos'] : '';
            $newCat = !empty($data['artCat']) ? $data['artCat'] : '';            

            if(!empty($data['artCat'])) { // Kategórián belül, illetve kategóriák között                
                $artMenuPosValid = true;                

                if($oldMenuPos != $newMenuPos || ($oldMenuPos == $newMenuPos && $oldCat !== $newCat)) { // Menü pozíció váltás azonos kategórián belül

                    if($oldCat === $newCat) {
                        $menuPosList = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos = '$newMenuPos' AND artCat = '$newCat'", false, 1);

                        if(count($menuPosList) > 0) {

                            foreach($menuPosList as $menuPos) {
                            $this->Data->updateData('article', ['artMenuPos' => $art[0]->artMenuPos], ['artId' => $menuPos->artId], 1);
                            }
                        }
                    }
                    else if($oldCat !== $newCat) { // Menü pozíció és kategória váltás
                        // Új kategória
                        $newCatMenuPosList = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= $newMenuPos AND artCat = $newCat");                    

                        if(count($newCatMenuPosList) > 0) {
                        
                            foreach($newCatMenuPosList as $menuPos) {
                                $this->Data->updateData('article', ['artMenuPos' => ++$menuPos->artMenuPos], ['artId' => $menuPos->artId], 1);
                                $this->Data->dbResponse === 'success' ? '' : array_push($errors, 'menuPos');
                            }
                        }

                        // Régi kategória
                        $oldCatMenuPosList = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > $oldMenuPos AND artCat = $oldCat");

                        if(count($oldCatMenuPosList) > 0) {

                            foreach($oldCatMenuPosList as $menuPos) {
                                $this->Data->updateData('article', ['artMenuPos' => --$menuPos->artMenuPos], ['artId' => $menuPos->artId], 1);
                                $this->Data->dbResponse === 'success' ? '' : array_push($errors, 'menuPos');
                            }
                        }
                        
                        // Régi önálló menüpont törlése a menük sorszámának átszámozása
                        $menuList = $this->Data->getData('menu', 'menuId, menuPos', "menupos > $oldParentMenu");
                        
                        if(count($menuList) > 0) {

                            foreach($menuList as $menuItem) {
                                $this->Data->updateData('menu', ['menuPos' => --$menuItem->menuPos], ['menuId' => $menuItem->menuId], 1);
                            }
                        }
                        $this->Data->deleteData('menu', ['menuId' => $oldParentMenu], 1);
                    }                                              
                }
            }
            else { // Kategóriából kilépve, új önálló menüpont hozzáadása esetén
                $oldCatItems = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > $oldMenuPos AND artCat = $oldCat");

                if(count($oldCatItems) > 0) {

                    foreach($oldCatItems as $catItems) {
                        $this->Data->updateData('article', ['artMenuPos' => --$catItems->artMenuPos], ['artId' => $catItems->artId], 1);
                        $this->Data->dbResponse === 'success' ? '' : array_push($errors, 'menuPos');
                    }
                }
            }
            
            // Státusz beállítása a főoldalnak történő kiválasztás függvényében
            $artStat = !empty($data['homePage']) && $data['homePage'] == 1 ? 1 : trim($this->input->post('artStat'));

            // Főoldal linkjének aktiválása (szükség esetén)
            if(!empty($data['homePage']) && $data['homePage'] == 1) {

                if(!empty($data['artCat'])) { // Amennyiben kategóriához tartozik
                    $artCat = $this->Data->getData('category', 'catMenu, catStat', ['catId' => $data['artCat']], false, 1);
                    
                    if($artCat[0]->catStat == 0) {
                        $this->Data->updateData('category', ['catStat' => 1], ['catId' => $data['artCat']], 1);
                    }
                    $this->Data->updateData('menu', ['menuStat' => 1], ['menuId' => $artCat[0]->catMenu], 1);
                }
                else { // Cikk esetében
                    $this->Data->updateData('menu', ['menuStat' => 1], ['menuId' => $data['artParentMenu']], 1);
                }
            }

            // Feltöltendő képek számának ellenőrzés (max. 10 db)
            /*if(!empty($img)) {
                count($img['name']) > 10 ? array_push($errors, 'maxUploadImage') : '';
            }*/
            
            // Adatok rendezése                        
            $formData = [
                'artTitle' => trim($this->input->post('artTitle')),
                'artTitleAlias' => trim($artTitleAlias),
                'artTitleShow' => !empty($data['artTitleShow']) ? trim($this->input->post('artTitleShow')) : '',
                'artText' => !empty($data['artText']) ? trim($this->input->post('artText')) : '',
                'artCat' => !empty($data['artCat']) ? trim($this->input->post('artCat')) : '',
                'artMenuTitle' => !empty($data['artMenuTitle']) ? trim($this->input->post('artMenuTitle')) : NULL,
                'artMenuAlias' => !empty($artMenuAlias) ? trim($artMenuAlias) : NULL,
                'artMenuPos' => !empty($data['artMenuPos']) ? trim($this->input->post('artMenuPos')) : '',
                'artParentMenu' => !empty($data['artParentMenu']) ? trim($this->input->post('artParentMenu')) : '',
                'homePage' => !empty($data['homePage']) ? trim($this->input->post('homePage')) : '', 
                'artMetaKey' => !empty($data['artMetaKey']) ? trim($this->input->post('artMetaKey')) : NULL,
                'artMetaDesc' => !empty($data['artMetaDesc']) ? trim($this->input->post('artMetaDesc')) : NULL,
                'artStat' => $artStat                 
            ];
            
            // Képek mentése
            $folder = scandir($this->imgRootFolder);

            if (!empty($img['artImg']['name'][0])) {
                $newImgNum = count($img['artImg']['name']);
                
                if ($newImgNum <= 10) { // Feltöltendő képek számának ellenőrzés (max. 10 db)                 
                
                    if (in_array($artTitleAlias, $folder)) { // Új képek mentése meglévő albumba
                        $images = scandir($this->imgRootFolder . $artTitleAlias);

                        for ($ci = 0; $ci < $newImgNum; $ci++) {
                            if (in_array(pathinfo($img['artImg']['name'][$ci], PATHINFO_EXTENSION), $this->_imgFileType)) {
                                if (!in_array($img['artImg']['name'][$ci], $images)) {
                                    move_uploaded_file($img['artImg']['tmp_name'][$ci], $this->imgRootFolder . $artTitleAlias . '/' . $img['artImg']['name'][$ci]);
                                } else {
                                    array_push($errors, 'imgName');
                                }
                            } else {
                                array_push($errors, 'imgFile');
                            }
                        }
                    } else { // Régi album átnevezése vagy új album nyitása
                    
                        if (in_array($art[0]->artTitleAlias, $folder)) { // Régi album mappa átnevezése

                            if ($art[0]->artTitleAlias !== $artTitleAlias) {
                                rename($this->imgRootFolder . $art[0]->artTitleAlias, $this->imgRootFolder . $artTitleAlias);
                            }
                        } else { // Új album mappa nyitása
                            mkdir($this->imgRootFolder . $artTitleAlias);
                            copy($this->imgRootFolder . 'index.html', $this->imgRootFolder . $artTitleAlias . '/index.html');

                            for ($ci = 0; $ci < $newImgNum; $ci++) {
                                if (in_array(pathinfo($img['artImg']['name'][$ci], PATHINFO_EXTENSION), $this->_imgFileType)) {
                                    move_uploaded_file($img['artImg']['tmp_name'][$ci], $this->imgRootFolder . $artTitleAlias . '/' . $img['artImg']['name'][$ci]);
                                } else {
                                    array_push($errors, 'imgFile');
                                }
                            }
                        }
                    }
                }
                else {
                    array_push($errors, 'maxUploadImage');
                }
            }
            else { // Mappa átnevezése, ha változik a cikk címe, de nincsenek új képek
                if($art[0]->artTitleAlias !== $artTitleAlias) {
                    
                    if(in_array($art[0]->artTitleAlias, $folder)) {
                        rename($this->imgRootFolder . $art[0]->artTitleAlias, $this->imgRootFolder . $artTitleAlias);
                    }
                }
            }
                
            // Frissítés
            if(count($errors) == 0) {
                $this->Data->updateData('article', $formData, ['artId' => $artId], 1);
                echo(!empty($this->Data->dbResponse)) ? 'success' : 'Sikertelen frissítés!';            
            }                
            else {

                switch(true) {

                    case in_array('artTitle', $errors):
                    echo 'Ezen a címen már van cikk elmentve. Kérem, válasszon másikat!';
                    break;

                    case in_array('menuTitle', $errors):
                    echo 'Ezen a néven már van menüpont elmentve. Kérem, válasszon egy másik nevet!';
                    break;

                    case in_array('menuPos', $errors):
                    echo 'A menü pozíciójának frissítése nem sikerült. Kérem, próbálja meg újra!';
                    break;

                    case in_array('imgName', $errors):
                    echo 'Az egyik kép fájlneve már szerepel az albumban. Kérem, változtassa meg a kép fájlnevét.';
                    break;
                    
                    case in_array('imgFile', $errors):
                    echo 'Az egyik feltöltendő kép fájltípusa nem engedélyezett. Csak .jpeg, .jpg, .png, .gif formátumú fájlok tölthetők fel.';
                    break;

                    case in_array('maxUploadImage', $errors):
                    echo 'Az egyszerre feltölthető képek maximális száma: 10';
                    break; 
                }
            }                      
        }
    }

    /** Cikk bélyegkép és háttérképek (slider) kezelése */
    public function manageArticleThumbnail($artId, $action, $imgFile = false) {
        $alert = '';
        $dbAlert = '';
        $artTitle = $this->Data->getData('article', 'artTitleAlias', ['artId' => $artId], false, 1);
        $folder = 'assets/images/articles/' . $artTitle[0]->artTitleAlias . '/';
        $files = scandir($folder);

        switch($action) {

            case "update":
            $sliderError01 = false;
            $sliderError02 = false;
            $sliderDelError01 = true;
            $sliderDelError02 = true;
            
            $this->Data->updateData('article', ['artThumb' => $_POST['thumb']], ['artId' => $artId], 1);
            $thumbError = !empty($this->Data->dbResponse) ? $this->Data->dbResponse === 'success' ? false : true : false;          
                        
            if($_POST['slider'] !== 'false') {

                if(!in_array('imgData.json', $files)) {
                    $img = [$_POST['slider']];
                    $imgJSON = json_encode($img, JSON_PRETTY_PRINT);
                    $json = fopen($folder . 'imgData.json', 'w');
                    fwrite($json, $imgJSON);
                    fclose($json);
                    $sliderError01 = file_exists($folder . 'imgData.json') ? false : true;                    
                }
                else {
                    $sliderImages = file_get_contents($folder . 'imgData.json');
                    $sliderArray = json_decode($sliderImages);
                    
                    if (!in_array($_POST['slider'], $sliderArray)) {
                        array_push($sliderArray, $_POST['slider']);
                        $sliderJSON = json_encode($sliderArray, JSON_PRETTY_PRINT);
                        $json = fopen($folder . 'imgData.json', 'w');
                        fwrite($json, $sliderJSON);
                        fclose($json);
                    }                                                                                                                        
                }                
            }
            else {                
                if(in_array('imgData.json', $files)) {
                    $sliderImages = file_get_contents($folder . 'imgData.json');
                    $sliderArray = json_decode($sliderImages);

                    if(in_array($_POST['img'], $sliderArray)) {
                        $key = array_search($_POST['img'], $sliderArray);
                        unset($sliderArray[$key]);

                        if(count($sliderArray) > 0) {
                            $sliderJSON = json_encode(array_values($sliderArray), JSON_PRETTY_PRINT);
                            $json = fopen($folder . 'imgData.json', 'w');
                            fwrite($json, $sliderJSON);
                            $sliderDelError01 = fclose($json);
                        }
                        else {
                            $sliderDelError02 = unlink($folder . 'imgData.json');
                        }
                    }                    
                }           
            }
            
            switch(true) {

                case $thumbError:
                echo 'A bélyegkép mentése nem sikerült. Kérem, próbálja meg újra!';
                break;

                case $sliderError01:
                echo 'A háttérkép mentése nem sikerült. Kérem, próbálja meg újra!';
                break;                

                case !$sliderDelError01:
                case !$sliderDelError02:
                echo 'A háttérkép eltávolítása nem sikerült. Kérem, próbálja meg újra!';
                break;

                default:
                echo 'success';
                break;
            }            
            break;        

            case "delete":
            $thumb = $this->Data->getData('article', 'artTitleAlias, artThumb', ['artId' => $artId], false, 1);

            if(!empty($thumb[0]->artThumb) && $thumb[0]->artThumb === $_POST['img']) {
                $this->Data->updateData('article', ['artThumb' => ''], ['artId' => $artId], 1);
                $dbAlert = $this->Data->dbResponse;
            }
            else {
                $dbAlert = 'success';
            }
            
            try {
                if(file_exists('assets/images/articles/' . $thumb[0]->artTitleAlias . '/imgData.json')) {
                    $sliderImages = file_get_contents('assets/images/articles/' . $thumb[0]->artTitleAlias . '/imgData.json');
                    $sliderArray = json_decode($sliderImages);

                    if(in_array($imgFile, $sliderArray)) {
                        $key = array_search($imgFile, $sliderArray);
                        unset($sliderArray[$key]);

                        if(count($sliderArray) > 0) {
                            $sliderJSON = json_encode(array_values($sliderArray), JSON_PRETTY_PRINT);
                            $file = fopen('assets/images/articles/' . $thumb[0]->artTitleAlias . '/imgData.json', 'w');
                            fwrite($file, $sliderJSON);
                            fclose($file);
                        }
                        else {
                            unlink('assets/images/articles/' . $thumb[0]->artTitleAlias . '/imgData.json');
                        }
                    }
                }
                unlink('assets/images/articles/' . $thumb[0]->artTitleAlias . '/' . $imgFile);
            }
            catch(Exception $e) {
                $alert = 'Hiba a képfájl törlésében: ' . $e->getMessage();                
            }
            
            if($dbAlert === 'success' && empty($alert)) {
                echo 'success';
            }
            else if($dbAlert !== 'success') {
                echo 'Sikertelen törlés az adatbázisból. Kérem, póbálja meg újra!';
            }
            else if(!empty($alert)) {
                echo $alert;
            }
            else {
                echo 'Sikertelen törlés. Kérem, próbálja meg újra!';
            }
            break;        
        }
    }

    /** Az összes kép törlése */
    public function deleteAllImage($artId) {
        $albName = $this->Data->getData('article', 'artTitleAlias', ['artId' => $artId], false, 1);
        $album = scandir($this->imgRootFolder);
        $errors = [];

        if(in_array($albName[0]->artTitleAlias, $album)) {
            $images = scandir($this->imgRootFolder . $albName[0]->artTitleAlias);

            // Bélyegkép eltávolítása
            if(count($images) > 0) {
                $this->Data->updateData('article', ['artThumb' => ''], ['artId' => $artId], 1);                               
                
                // Képek eltávolítása a cikk szövegéből
                $artText = $this->Data->getData('article', 'artText', ['artId' => $artId], false, 1);

                if(!empty($artText)) {                  
                    $newArtText = preg_replace('/<img[^>]+\>/i', '', $artText[0]->artText);                    
                    $this->Data->updateData('article', ['artText' => $newArtText], ['artId' => $artId], 1); 
                }                                                                 
                                
                // Képek törlése
                foreach($images as $img) { 

                    if(in_array(pathinfo($img, PATHINFO_EXTENSION), $this->_imgFileType)) {
                        $del = unlink($this->imgRootFolder . $albName[0]->artTitleAlias . '/' . $img);
                        $del ? '' : array_push($errors, $img);
                    }
                }

                // JSON törlése
                if(in_array('imgData.json', $images)) {
                    unlink($this->imgRootFolder . $albName[0]->artTitleAlias . '/imgData.json');
                }
                echo(count($errors) == 0) ? 'success' : "A képek törlése nem sikerült.";
            }
            else {
                echo 'success';
            }
        }
    }

    /** Cikk törlése */
    public function deleteArticle($artId) {

        if(isset($_POST)) {
            $art = $this->Data->getData('article', 'artCat, artTitleAlias, artMenuPos', ['artId' => $artId], false, 1);
            $error = true;
            
            // Képek törlése
            $album = scandir($this->imgRootFolder);

            if(in_array($art[0]->artTitleAlias, $album)) {
                array_map('unlink', glob($this->imgRootFolder . $art[0]->artTitleAlias . '/*.*'));
                rmdir($this->imgRootFolder . $art[0]->artTitleAlias);
            }
            
            // Kategória sorrend megváltoztatása
            if(!empty($art[0]->artCat)) {
                $menuPos = $art[0]->artMenuPos;
                $artCat = $art[0]->artCat;
                $catItems = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > $menuPos AND artCat = $artCat");

                if(count($catItems) > 0) {

                    foreach($catItems as $item) {
                        $this->Data->updateData('article', ['artMenuPos' => --$item->artMenuPos], ['artId' => $item->artId], 1);
                        $error = $this->Data->dbResponse === 'success' ? false : true;
                    }
                }
                else {
                    $error = false;
                }                
            }
            else {
                $error = false;
            }

            if(!$error) {
                $this->Data->deleteData('article', ['artId' => $artId], 1);
                echo(count($this->Data->dbResponse) == 0) ? 'success' : 'Sikertelen törlés. Kérem, próbálja meg újra!';
            }
        }
    }
}