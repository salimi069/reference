<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public $validationError = null;
    public $superAdminCheck = null;
    public $adminError = null;    
    public $emailError = null;
    public $passwordError = null;
    public $dataSave = null;
    public $users = null;
    public $userId = null;
    public $userAccess = null;
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Data');
        $this->userId = $this->session->userdata('userId');
        $this->userAccess = $this->session->userdata('userAccess');
    }

    /** Főoldal */
    public function index() {
        
        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/userNav');      
            $this->load->view('pages/admin/users');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Felhasználó mentése */
    public function createUser() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/userNav');

            $this->form_validation->set_rules('userName', 'nevét', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a felhasználó %s!']);
            $this->form_validation->set_rules('userAccess', 'jogosultságát', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a felhasználó %s!']);
            $this->form_validation->set_rules('userEmail', 'e-mail címét', 'strip_tags|trim|valid_email|required', ['required' => 'Kérem, adja meg a felhasználó %s!', 'valid_email' => 'Kérem, adjon meg egy érvényes e-mail címet']);
            $this->form_validation->set_rules('userPass', 'jelszavát', 'strip_tags|trim|required|min_length[6]', ['required' => 'Kérem, adja meg a felhasználó %s!', 'min_length' => 'A jelszónak minimum 6 karakterből kell állnia!']);
            $this->form_validation->set_rules('userPassConf', 'erősítse meg a jelszót', 'strip_tags|trim|required|matches[userPass]', ['required' => 'Kérem, %s!', 'matches' => 'A két megadott jelszó nem egyezik meg!']);        
        
            $this->superAdminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1); 
        
            if($this->form_validation->run() == FALSE) {
                $this->validationError = true;
            }
            else {
                $this->validationError = false;
                $adminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);            
                $emailCheck = $this->Data->getData('users', false, ['userEmail' => $_POST['userEmail']], false, 1);            

                if(((count($adminCheck) == 0 && $_POST['userAccess'] == 1) || (count($adminCheck) > 0 && $_POST['userAccess'] != 1)) && count($emailCheck) == 0) {
                    $this->adminError = false;
                    $this->emailError = false;
                    $formData = [
                        'userName' => $this->input->post('userName'),
                        'userAccess' => $this->input->post('userAccess'),
                        'userEmail' => $this->input->post('userEmail'),
                        'userPass' => password_hash($this->input->post('userPass'), PASSWORD_DEFAULT),
                        'userStat' => !empty($this->input->post('userStat')) ? $this->input->post('userStat') : '' 
                    ];
                    $this->Data->saveData('users', $formData);
                    $this->dataSave = $this->Data->dbResponse == 'success' ? true : false;
                }
                else {                
                    $this->adminError = $_POST['userAccess'] == 1 ? true : false;
                    $this->emailError = count($emailCheck) > 0 ? true : false;
                }
            }
        
            $this->load->view('pages/admin/createuser');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Felhasználók kezelése */
    public function manageUsers() {

        if($this->session->has_userdata('userId')) {
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/userNav');
        
            $this->users = $this->Data->getData('users', 'userId, userName, userEmail, userAccess, userStat', false, 'userAccess ASC');

            if(count($this->users) > 0) {
                $this->load->library('table');
                $counter = 1;                

                $template = [
                    'table_open' => '<table class="w3-table-all">',
                    'thead_open' => '<thead>',
                    'thead_close' => '</thead>',
                    'tbody_open' => '<tbody id="table">',
                    'tbody_close' => '</tbody>'
                ];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Név', 'E-mail', 'Jogosultság', 'Státusz', 'Kezelés');

                foreach($this->users as $user) {

                    switch($user->userAccess) {

                        case 1:
                        $access = 'Fő Adminisztrátor';
                        break;

                        case 2:
                        $access = 'Szerkesztő';
                        break;
                        
                        default:
                        $access = 'N/A';
                        break;
                    }

                    $stat = $user->userStat == 1 ? 'Aktív' : 'Inaktív';

                    $this->table->add_row($counter++, $user->userName, $user->userEmail, $access, $stat, '<a href="' . base_url() . 'index.php/admin/users/updateuser/' . $user->userId . '" class="w3-btn w3-orange">Kezelés</a>');
                }
            }
        
            $this->load->view('pages/admin/manageusers');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }
    
    /** Felhasználói adatok módosítása */
    public function updateUser($userId) {

        if($this->session->has_userdata('userId')) {            
            $this->load->view('templates/admin/head');
            $this->load->view('templates/admin/mainNav');
            $this->load->view('templates/admin/userNav');

            $this->users = $this->Data->getData('users', false, ['userId' => $userId], false, 1);
            $this->superAdminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);

            $this->form_validation->set_rules('userName', 'nevét', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a felhasználó %s!']);
            $this->form_validation->set_rules('userAccess', 'jogosultságát', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a felhasználó %s!']);
            $this->form_validation->set_rules('userEmail', 'e-mail címét', 'strip_tags|trim|valid_email|required', ['required' => 'Kérem, adja meg a felhasználó %s!', 'valid_email' => 'Kérem, adjon meg egy érvényes e-mail címet']);
            $this->form_validation->set_rules('userOldPass', 'jelenlegi jelszavát', 'strip_tags|trim|required', ['required' => 'Kérem, adja meg a felhasználó %s!']);
        
            if(!empty($this->input->post('userPass'))) {
                $this->form_validation->set_rules('userPass', 'jelszavát', 'strip_tags|trim|required|min_length[6]', ['required' => 'Kérem, adja meg a felhasználó %s!', 'min_length' => 'A jelszónak minimum 6 karakterből kell állnia!']);
                $this->form_validation->set_rules('userPassConf', 'erősítse meg a jelszót', 'strip_tags|trim|required|matches[userPass]', ['required' => 'Kérem, %s!', 'matches' => 'A két megadott jelszó nem egyezik meg!']);
            }

            if($this->form_validation->run() == FALSE) {
                $this->validationError = true;
            }
            else {
                $this->validationError = false;
                $adminCheck = '';

                if($_POST['userAccess'] == 1) {
                    $adminCheck = $this->Data->getData('users', false, "userAccess = 1 AND userId != $userId", false, 1);
                }

                $email = $_POST['userEmail'];
                $emailCheck = $this->Data->getData('users', false, "userEmail = '$email' AND userId != $userId", false, 1);                               
                
                if($this->userAccess == 1) {
                    $adminPass = $this->Data->getData('users', 'userPass', ['userAccess' => 1], false, 1);                    

                    if(count($adminPass) > 0) {                        
                        $pass = $adminPass[0]->userPass;
                    }
                    else {
                        $adminPass = $this->Data->getData('users', 'userPass', ['userId' => $userId], false, 1);
                        $pass = $adminPass[0]->userPass;
                    }  
                }
                else {
                    $pass = $this->users[0]->userPass;
                }

                if(password_verify($_POST['userOldPass'], $pass)) {
                    $this->passwordError = false;
               
                    if(empty($adminCheck) && count($emailCheck) == 0) {
                        $passWord = !empty($_POST['userPass']) ? password_hash($this->input->post('userPass'), PASSWORD_DEFAULT) : $this->users[0]->userPass;
                        
                        if($_POST['userAccess'] == 1) {
                            $userStat = 1;
                        }
                        else {
                            $userStat = $this->input->post('userStat');
                        }                       
                                                
                        $formData = [
                            'userName' => $this->input->post('userName'),
                            'userAccess' => $this->input->post('userAccess'),
                            'userEmail' => $this->input->post('userEmail'),
                            'userPass' => $passWord,
                            'userStat' => $userStat
                        ];
                        $this->Data->updateData('users', $formData, ['userId' => $userId], 1);
                        $this->dataSave = $this->Data->dbResponse == 'success' ? true : false;
                    }
                    else {
                        $this->adminError = !empty($adminCheck) ? true : false;
                        $this->emailError = count($emailCheck) > 0 ? true : false;
                    }
                }
                else {
                    $this->passwordError = true;
                }
            }

            $this->load->view('pages/admin/updateuser');
            $this->load->view('templates/admin/footer');
        }
        else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Felhasználó törlése */
    public function deleteUser($userId) {
        $users = $this->Data->getData('users');

        if(count($users) > 1) { 
            $this->Data->deleteData('users', ['userId' => $userId], 1);
            echo(empty($this->Data->dbResponse)) ? 'success' : 'Sikertelen törlés. Kérem, próbálja meg újra!';
        }
        else {
            echo 'Legalább egy felhasználónak szerepelnie kell az adatbázisban!';
        }
    }
}