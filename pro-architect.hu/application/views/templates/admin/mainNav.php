<?php 
$uri = $_SERVER['REQUEST_URI'];
$homeUrl = base_url();
$adminHomeUrl = 'index.php/admin/home';
?>

<nav class="w3-rest w3-mobile">
    <div class="w3-container w3-bar w3-theme-d4 w3-xlarge">
        <a href="<?php echo(stripos($uri, 'login') > 0) ? $homeUrl : $homeUrl . $adminHomeUrl; ?>" class="w3-bar-item w3-button">Főoldal</a>
        <?php
        if(stripos($uri, 'login') == 0) { ?> 
        <a href="<?php echo base_url(); ?>index.php/admin/menu" class="w3-bar-item w3-button w3-hide-small">Menük</a>
        <a href="<?php echo base_url(); ?>index.php/admin/content" class="w3-bar-item w3-button w3-hide-small">Tartalom</a>       
        <a href="<?php echo base_url(); ?>index.php/admin/users" class="w3-bar-item w3-button w3-hide-small">Felhasználók</a>
        <a href="<?php echo base_url(); ?>index.php/admin/home/logout" class="w3-bar-item w3-button w3-hide-small">Kilépés</a>
        <a href="#" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium hamb-icon">&#9776;</a>               
        <?php } ?>
    </div>

    <div id="menu-collapse" class="w3-bar-block w3-theme-l4 w3-hide w3-hide-large w3-hide-medium">
        <a href="<?php echo base_url(); ?>index.php/admin/home" class="w3-bar-item w3-button">Menük</a>
        <a href="<?php echo base_url(); ?>index.php/admin/content" class="w3-bar-item w3-button">Tartalom</a>
        <a href="<?php echo base_url(); ?>index.php/admin/users" class="w3-bar-item w3-button">Felhasználók</a>
        <a href="<?php echo base_url(); ?>index.php/admin/home/logout" class="w3-bar-item w3-button">Kilépés</a>
    </div>
</nav>
</header>