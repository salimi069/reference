<section id="content-placeholder" class="w3-row w3-mobile">
    <aside class="w3-sidebar w3-bar-block w3-card w3-animate-right" id="leftMenu">
        <button class="w3-bar-item w3-button w3-large side-close">Bezárás &times;</button>
        <button id="blog" class="w3-button w3-block w3-left-align acc">Kategóriák <i class="fa fa-caret-down"></i></button>
        <div id="blog" class="w3-hide w3-white w3-card">
            <a href="<?php echo base_url(); ?>index.php/admin/content/createcategory" class="w3-bar-item w3-button">Kategória készítése</a>
            <a href="<?php echo base_url(); ?>index.php/admin/content/managecategory" class="w3-bar-item w3-button">Kategóriák kezelése</a>
        </div>
        <button id="cikk" class="w3-button w3-block w3-left-align acc">Cikkek <i class="fa fa-caret-down"></i></button>
        <div id="cikk" class="w3-hide w3-white w3-card">
            <a href="<?php echo base_url(); ?>index.php/admin/content/createarticle" class="w3-bar-item w3-button">Cikk készítése</a>
            <a href="<?php echo base_url(); ?>index.php/admin/content/managearticle" class="w3-bar-item w3-button">Cikk kezelése</a>
        </div>
    </aside>
    <button class="w3-button w3-theme-l2 w3-xlarge w3-right side-open">&#9776;</button>    
    <main class="w3-rest w3-mobile">
        <script>
        $("button.acc").each(function() {

            $(this).click(function() {
                var id = $(this).attr("id");

                if($("div[id='" + id + "']").hasClass("w3-show")) {
                    $("div[id='" + id + "']").removeClass("w3-show");
                    $("div[id='" + id + "']").prev().removeClass("w3-green");                                   
                }
                else {
                    $("div[id='" + id + "']").addClass("w3-show");
                    $("div[id='" + id + "']").prev().addClass("w3-green");                    
                }
            });
        });        
        </script>