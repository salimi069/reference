<section id="content-placeholder" class="w3-row w3-mobile">
    <aside class="w3-sidebar w3-bar-block w3-card w3-animate-right" id="leftMenu">
        <button class="w3-bar-item w3-button w3-large side-close">Bezárás &times;</button>
        <a href="<?php echo base_url(); ?>index.php/admin/menu/createmenu" class="w3-bar-item w3-button">Menüpont készítése</a>
        <a href="<?php echo base_url(); ?>index.php/admin/menu/managemenu" class="w3-bar-item w3-button">Menüpont kezelése</a>
       
    </aside>
    <button class="w3-button w3-theme-l2 w3-xlarge w3-right side-open">&#9776;</button>
    <main class="w3-rest w3-mobile">