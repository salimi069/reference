<!DOCTYPE html>
<html lang="hu">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <title>PRo-Architect Admin</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/w3.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/w3-theme-dark-grey.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminStyle.css" />
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
</head>

<body>
    <section id="wrapper" class="w3-row w3-mobile">
        <header class="w3-row w3-mobile w3-theme-d4">
            <figure class="w3-container w3-quarter w3-mobile"><img class="w3-image" src="<?php echo base_url(); ?>assets/images/sys/admin_logo.png" alt="Pro-Architect Logo" /></figure>