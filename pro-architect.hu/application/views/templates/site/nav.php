<?php defined('BASEPATH') OR exit('No direct script access allowed');
$menuItemNumber = count($this->mainNav) > 0 ? count($this->mainNav) : 0;
?>
<div class="w3-sidebar w3-bar-block w3-black w3-animate-left catNav mp-sidebar" style="display:none" id="mySidebar">
    <button id="menuClose" class="w3-bar-item w3-button w3-large">Bezárás &times;</button>
    <?php 
    if($menuItemNumber > 0) {        
        echo '<ul id="mp-side-menu">';

        for($i = 0; $i < $menuItemNumber; $i++) {
            echo "<li><a href=\"#\" class=\"w3-bar-item w3-button w3-mobile mp-link mp-list\" data-id=\"{$this->mainNav[$i]->menuId}\" data-type=\"{$this->mainNav[$i]->menuType}\">{$this->mainNav[$i]->menuTitle}</a></li>";
        }
        echo '</ul>';
    }
    ?>    
</div>
<button id="menuOpen" class="w3-button w3-white w3-xlarge w3-left w3-hide-middle w3-hide-large menuOpen">Menü</button>

<nav id="mp-menu-large" class="w3-hide-small">
    <div id="mp-nav" class="w3-bar w3-black">
        <?php if($menuItemNumber > 0) {            

            for($i = 0; $i < $menuItemNumber; $i++) {
                echo "<a href=\"#\" class=\"w3-bar-item w3-button w3-mobile mp-link\" data-id=\"{$this->mainNav[$i]->menuId}\" data-type=\"{$this->mainNav[$i]->menuType}\">{$this->mainNav[$i]->menuTitle}</a>";
            }                      
        }
        ?>
        <span id="siteDev" class="w3-right"><small>Honlap: <a href="https://www.swh-it.hu" target="_blank">swh-it.hu</a></small></span>        
    </div>    
</nav>