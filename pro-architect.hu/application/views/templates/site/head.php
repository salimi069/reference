<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="hu">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8" />
        <title>Pro-Architect Építész Stúdió Kft.</title>
        <meta name="author" content="Pro-Architect Épitész Stúdió">
        <meta name="generator" content="CodeIgniter 3.1.11">
        <meta name="keywords" content="<?php echo($this->artMetaContent) ? $this->artContent[0]->artMetaKey : ''; ?>">
        <meta name="description=" content="<?php echo($this->artMetaContent) ? $this->artContent[0]->artMetaDesc : ''; ?>">
        <meta name="robots" content="index, follow">
        <meta property="og:url" content="http://www.pro-architect.hu/" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Pro-Architect Épitész Stúdió" />
        <meta property="og:description" content="Épületek professzionális tervezése." />
        <meta property="og:image" content="<?php echo base_url(); ?>assets/images/sys/logo_white.png" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/w3.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/siteStyle.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/js/lightslider/dist/css/lightslider.css" />                
        <link href='https://fonts.googleapis.com/css?family=Big Shoulders Text' rel='stylesheet'>
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>        
    </head>

    <body>
        <div id="wrapper" class="w3-row">        