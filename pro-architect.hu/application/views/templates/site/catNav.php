<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$baseUrl = base_url();
?>
<div class="w3-sidebar w3-bar-block w3-black w3-animate-left catNav" style="display:none" id="mySidebar">
    <button id="menuClose" class="w3-bar-item w3-button w3-large">Bezárás &times;</button>
    <a href="<?php echo base_url(); ?>" class="w3-bar-item w3-button w3-xlarge">Főoldal</a>    
    <button class="w3-button w3-xlarge"><?php echo $this->catContent[0]->menuTitle; ?><i class="fa fa-caret-down"></i></button>        
    <?php
    foreach($this->catContent as $catItem) {                
        echo "<span class=\"w3-bar-item w3-button catNavItem\">&#187; <a href=\"{$baseUrl}index.php/home/cat/{$catItem->catId}/{$catItem->menuId}\">{$catItem->catTitle}</a></span>";
    }
    ?>                        
</div>
<button id="menuOpen" class="w3-button w3-white w3-xlarge w3-left">Menü</button>