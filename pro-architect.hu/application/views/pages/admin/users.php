<div class="w3-row">
    <article class="w3-half w3-panel w3-theme-l1 w3-xxlarge w3-card-4 w3-mobile title">Üdvözlöm a felhasználó kezelő
        főoldalán</article>
</div>
<div class="w3-row">
    <article class="w3-half doc">
        <p>A felhasználókezelő felületén belül a honlap adminisztrációs felületére belépési jogosultsággal rendelkező
            <strong>adminisztrátor
                és szerkesztők</strong> adatai kezelhetők.</p>
        <p><strong>Funkcióit</strong> tekintve a modul a következőkre képes:
            <ul>
                <li>Új felhasználó felvétele</li>
                <li>Meglévő felhasználók adatinak módosítása</li>
                <li>Felhasználó törlése</li>
            </ul>
        </p>
        <p><strong>Hozzáférési jogosultságok</strong> tekintetében a felhasználók két típusát különböztetjük meg,
            amelyek a
            <ul>
                <li>Adminisztrátor</li>
                <li>Szerkesztő</li>
            </ul>
        </p>
        <p>Az oldalon megjelenő <strong>tartalmi anyagok</strong> (cikkek, képek stb.) hozzáférési és szerkesztési
            jogosultságának tekintetében a két kategória
            <strong>nem különbözik</strong> egymástól, vagyis az adminisztrátor és szerkesztő ugyanolyan módon képes
            szerkeszteni az oldalon megjelenő szöveges és képi tartalmakat.<br />
            A két kategória közötti különbség az oldalra belépési joggal rendelkező személyek <strong>adatainak a
                kezelésében</strong> jelentkezik meg. Egyszerűbben leírva: az odalra új adminisztrátort kijelölni,
            vagy új szerkesztőt felvenni csak az <strong>Adminisztrátornak van jogosultsága</strong> és a tagok bizonyos
            jogosultságait is csak ő szerkesztheti. Ezzel szemben a Szerkesztő csak a <strong>saját adataihoz</strong>
            fér hozzá és csak azokat szerkesztheti, a státusza (aktív, inaktív) kivételével. Az egyes tagok
            <strong>státuszának</strong> meghatározására szintén csak az Adminisztrátornak van joga.</p>
        <p><strong>FONTOS:</strong> A rendszerben csak egy ADMINISZTRÁTOR jogosultságú személy lehet, viszont legalább
            egy főnek szerepelnie kell az adatbázisban, mint belépésre jogoult személy és annak ADMINISZTRÁTORI joggal
            kell rendelkezni. Az <strong>adminisztrátori felületről történő kijelentkezés előtt</strong> a rendszer
            automatikusan ellenőrzi, hogy az adatbázisban szerepel-e <strong>legalább egy aktív státuszú
                adminisztrátor</strong> és amennyiben
            nem talál ilyen személyt <strong>nem engedélyezi a kilépést</strong>, hanem egy figyelmezető üzenet
            megjelenítése mellett visszairányítja a kilépni szándékozó felhasználót az adminisztratív felület
            főoldalára. Ezt követően a bejelentkezett személynek lehetősége van a státuszának megváltoztatására (aktív
            státuszú adminisztrátor).<br />
            Az alapértelmezett beállítások szerint az adminisztrátor státusza mindig <strong>aktív</strong>, azt nem
            lehet inaktívvá tenni. Amennyiben valamilyen oknál fogva az adatbázisban egyáltalán nem szerepelne aktív
            felhasználó
            (szerkesztő vagy adminisztrátor) úgy a rendszer adminisztratív felülete <strong>elérhetetlenné
                válik</strong>, ezért a ellenőrzés lefuttatásához fontos, hogy a munka végeztével, mindig a
            <strong>KILÉPÉS</strong> gombot használva hagyjuk el
            a adminisztratív felületet.</p>
        <p>A felhasználók <strong>jogosultságuk</strong> alapján az alábbi műveleteket végezhetik el:<br />
            <ol>
                <li><strong>Adminisztrátor</strong>
                    <ul>
                        <li><strong>Új felhasználó felvétele:</strong> a "Felhasználó mentése" menüpont alatt található
                            űrlap értelemszerű kitöltésével és mentésével.<br />
                            Amennyiben a rendszerben van aktív adminisztrátor csak szerkesztők felvételére van
                            lehetőség. A felvehető szerkesztők száma nem korlátozott.</li>
                        <li><strong>Felhasználói adatok módosítása:</strong> "Felhasználók kezelése" aloldal megnyitása,
                            illetve az adott felhasználó sorában található Kezelés gomb megnyomása után megnyíló
                            aloldalon
                            található űrlap értelemszerű kitöltésével és mentésével.<br />A megadott felhasználói adatok
                            közül (természetesen a saját adatok módosításának kivételével)
                            kizárólag a felhasználó <strong>nevének, jogosultságának és státuszának</strong>
                            megváltoztatására van lehetőség. A megadott bejelentkezési e-mail cím és jelszó csak a
                            <strong>saját adatok módosítása</strong> során lehetséges,
                            ezekhez az adatokhoz az adminisztrátor sem férhet hozzá. Az adminisztrátor más felhasználó
                            adatait csak a <strong>saját adminisztrátori jelszavának</strong> megadása után
                            módosíthatja.</li>
                        <li><strong>Felhasználók státuszának megváltoztatása:</strong> A felhasználó adatlapjának
                            "Aktív" jelzésű jelölőnégyzetének kijelölésével, vagy a kijelölés eltávolításával.<br />
                            Az inaktív tag adatai továbbra is szerepelnek az adatbázisban, de a tag nem tud
                            bejelentkezni az adminisztratív felületre a státusz Aktívra állításáig.</li>
                        <li><strong>A felhasználó törlése:</strong> Az adott felhasználó adatlapján található "Törlés"
                            gomb segítségével.<br />
                            A törölt felhasználó adatai <strong>nem állíthatók vissza</strong>, azok véglegesen
                            eltávolításra kerülnek. A rendszerben mentett egyetlen regisztrált felhasználó esetén a
                            törlés <strong>nem lehetséges.</strong></li>
                    </ul>
                </li>
                <li><strong>Szerkesztő</strong>
                    <ul>
                        <li>Saját felhasználói adatok szerkesztése<br />
                            A Szerkesztő jogosultságú személyek az fent leírt adminisztrátori funkciókhoz <strong>nem
                                férnek hozzá</strong>, magukat a rendszerből nem törölhetik és a státuszukat sem
                            változtathatják meg.
                        </li>
                    </ul>
                </li>
            </ol>
        </p>
        <p>Amennyiben az adminisztrátor szeretne <strong>lemondani</strong> adminisztrátori jogáról és azt más
            felhasználóra szeretné átruházni, akkor a saját adatainál módosíthatja a jogosultságát szerkesztőre,
            majd ezt követően kijelölheti az új adminisztrátort.</p>
        <p><strong>FONTOS:</strong> mivel a regisztrált tagok bejelentkezése e-mail cím/jelszó páros megadása után
            lehetséges, ezért ezen adatok pontos megadása feltétlenül szükséges.</p>
        <p><strong><u>Fejlesztői információ:</u></strong></p>
        <p>A felhasználókezelő funkcióhoz tartozó fájlok az alábbiak:</p>
        <ol>
            <li>PHP:
                <ul>
                    <li>Controller: /application/controllers/admin/Users.php</li>
                    <li>Model: /application/models/Data.php</li>
                    <li>Views:
                        <ul>
                            <li>/application/views/pages/admin/user.php</li>
                            <li>/application/views/pages/admin/createuser.php</li>
                            <li>/application/views/pages/admin/manageusers.php</li>
                            <li>/application/views/pages/admin/updateuser.php</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Javascript: /assets/js/admin/user.js</li>
            <li>CSS: /assets/css/adminStyle.css</li>
        </ol>
    </article>
</div>