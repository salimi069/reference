<div class="w3-row">
    <article class="w3-half w3-panel w3-theme-l1 w3-xxlarge w3-card-4 w3-mobile title">Üdvözlöm a tartalomkezelő
        főoldalán</article>
</div>
<div class="w3-row">
    <article class="w3-half w3-mobile doc">
        <p>A tartalomkezelő fő feladata a honlap tartalmi elemeinek létrehozásához és kezeléséhez szükséges felületek és
            funkciók biztosítása. Az említett felületek
            és funkciók az oldal jobb oldalán kattintásra kinyíló menüoszlop hivatkozásaira kattintva érhetők el.</p>
        <p>A honlap tartalmi elemei és megjelenítésük módjai a következők:</p>
        <ol>
            <li><strong>Kategória:</strong></li>
            <p>A kategória az azonos tematikához tartozó cikkek csoportosítását és megjelenítését teszi lehetővé. Maga a
                kategória a menükezelőben létrehozott és a kategóriához rendelt
                "Kategória" típusú menüponton keresztül érhető el, ami a honlap alján elhelyezett főmenüsávban jelenik
                meg. Az említett menüpontra kattintva az adott menüponthoz rendelt kategóriák hivatkozásai
                a honlap főoldalának jobb oldalán megnyíló tartalomdobozban jelennek meg. A kategóriák hivatkozásai az
                adott kategória aloldalára mutatnak, amelyek a kategóriához sorolt cikkek megjelenítésére
                szolgáló linkeket tartalmazza.</p>
            <li><strong>Cikk:</strong></li>
            <p>A cikkeknek két fajtáját különböztethetjük meg:</p>
            <ul>
                <li>önálló, kategóriához nem tartozó cikk</li>
                <li>Kategóriához tartozó cikk</p>
            </ul>
            <p>A két cikk típust alapvetően a megjelenítésük módja különbözteti meg egymástól.<br />
                Az önálló cikkek a menükezelőben létrehozott és hozzájuk rendelt (főmenüsávban) megjelenő hivatkozásra
                történő kattintás után a főoldal jobb oldalán megnyíló tartalomdobozban olvashatók.<br />
                A kategóriába osztott cikkek az adott kategória aloldalán szereplő hivatkozásukon keresztül külön
                aloldalon jelennek meg.</p>
        </ol>
        <p>Az egyszerűség kedvéért a továbbiakban a két típushoz tartozó egy-egy cikk létrehozásának és kezelésének
            módját mutatom be a szükséges űrlapok célszerű használatának leírásán keresztül.</p>
        <ol>
            <li><strong>Önálló cikk készítése és a galéria beállítása a cikk mentése után:</strong></li>
            <p>Önálló cikk létrehozásához kattintsuk a jobb oldali menüoszlop "Cikkek" menüpontjának "Cikk készítése"
                almenüpontjára. A megnyíló űrlapon a cikk létrehozásához szükséges elemeket láthatjuk, amelyeket
                különböző panelek csoportosítanak.</p>
            <ul>
                <li>Alapadatok</li>
                <p>Első lépésként az "Alapadatok" panelen belül a "Cikk címe" mező használatával adjuk meg a cikk címét.
                    Figyeljünk arra, hogy a cikk címének egyedinek kell lennie, nem szerepelhet két ugyanolyan cím az
                    adatbázisban.
                    A cikk címének megadása kötelező.<br />
                    Ezt követően a "Cikk címének megjelenítése" jelölődoboz használatával eldönthetjük, hogy a cikk címe
                    megjelenjen-e a cikket megjelenítő tartalomdoboz tetején. Amennyiben a jelölődobozt kiválasztjuk a
                    cím látható lesz
                    ellenkező esetben a cím rejtve marad.</p>
                <p>A "Cikk szövege" szövegdobozban elkészíthetjük a cikk szövegét. A szövegdoboz kitöltése nem kötelező,
                    használata esetén a szöveg a megadott szövegszerkesztő elemekkel formázható.
                    Az adatbázisban cikkenként tárolható szöveg mérete maximum 65 000 karakter. Ennél hosszabb szöveget
                    az adatbázis nem tárol a maximum érték feletti szövegrész mentéskor törlésre kerül.
                    A főoldalon található jobb oldali tartalomdoboz csak abban az esetben jelenik meg, ha a cikk
                    rendelkezik szöveges tartalommal.</p>
                <li>Menü beállítás</li>
                <p>Önálló cikk esetében a cikk tartalma a menükezelőben létrehozott és a főmenüsávban megjelenő "Cikk"
                    típusú menüponton keresztül érhető el, amelyet a "Menü beállítás" panel "Szülő menü" legördülő
                    választólistájának használatával lehet az adott cikkhez rendelni. Abban az esetben, ha a
                    tartalomkezelőben nem került kategória létrehozásra a panelen belül csak a "Szülő menü" legördülő
                    lista jelenik meg,
                    amely kizárólag a menükezelőben előzetesen létrehozott <strong>cikk típusú és hozzárendelt cikkel
                        nem rendelkező (szabad) menüpontokat</strong> jeleníti meg. Tehát amennyiben már a menükezelőben
                    létrehoztuk az adott cikknek szánt menüpontot, akkor azt válasszuk ki a listából. Menüpont megadása
                    nem kötelező a cikk létrehozatalakor, ezt akár később is megtehetjük és egy cikk módosítás
                    keretében hozzárendelhetjük a cikkhez. Figyeljünk arra, hogy a weboldalon csak aktív menüponttal
                    rendelkező cikkek jelennek meg.</p>
                <p>Amennyiben a tartalomkezelő már tartalmaz elmentett kategóriát a panelen belül megjelennek a
                    kategóriához rendeléshez szükséges kezelőelemek, de ezeket önálló cikk esetén figyelmen kívül
                    kell hagyni és egyből a "Szülő menü" listát kell használni. Szülő menü választása esetén a
                    kategóriához rendelés kezelőelemei inaktívvá válnak és csak a Szülő menü lista alaphelyzetbe
                    állítása
                    esetén ("Kérem, válasszon") aktiválódnak újra.</p>
                <p>A "Menü beállítás" panelen belül van lehetőség a honlap kezdőoldalának beállítására. A kezdőoldalnak
                    különös jelentősége van a honlap szempontjából, mivel annak hiányában a honlap elérhetetlenné válik.
                    A kezdőoldal kiválasztásához jelöljük ki a panelen belül található "Főoldal" jelölőnégyzetet. Mivel
                    a honlap csak egy kezdőoldallal rendelkezhet ezért alaphelyzetben, az aktuális kezdőoldal
                    kivételével,
                    a jelölőnégyzet inaktív, ami meggátolja több kezdőoldal kiválasztását. Tehát a kezdőoldal
                    megváltoztatásához először cikk módosítás keretében szüntessük meg az eredeti kezdőodal kijelölését
                    és
                    belépve az új kezdőoldal módosításának űrlapjára jelöljük ki az új kezdőoldalt. Amennyiben a
                    weboldal bejelentkezett kezelője főoldal, illetve a főoldalhoz tartozó aktív hivatkozás kijelölése
                    nélkül kisérli meg az admin felületről történő kijelentkezést, a rendszer visszairányítja az admin
                    felület főoldalára és hibaüzenettel figyelmezteti a kezdőoldal, valamint a rámutató aktív link
                    hiányára.</p>
                <li>Galéria</li>
                <p>A cikkhez történő képek feltöltésére a "Galéria" panelen belül van lehetőség. Az egyszerre
                    maximálisan feltölthető képek száma: 10 db. A feltölhető képek fájltípusai: jpg, jpeg, gif,
                    png.<br />
                    FIGYELEM: cikk létrehozatalakor a képek szerepe nem szerkeszthető és szövegbe illesztésükre is csak
                    a cikk teljes mentése után, cikk módosítás keretében van lehetőség. A képekkel kapcsolatban a cikk
                    módosításának
                    leírásánál szerepel bővebb tájékoztatás.</p>
                <li>Meta adatok</li>
                <p>A meta adatoknál szereplő kulcsszavak és leírás a keresőrobotok számára fontos információk az oldalak
                    indexelése és kereshetőségének javítása érdekében.<br />
                    A "Kulcsszavak" mezőben maximum 20 db, egymástól vesszővel elválasztott a cikk tartalmát leginkább
                    jellemző kulcsszó megadása célszerű, míg a "Cikk rövid leírása" mezőben a cikk tartalmának rövid,
                    lehetőleg
                    egy mondat terjedelmet nem meghaladó leírást célszerű megadni.<br />
                    A mezők kitöltése nem kötelező.</p>
                <li>Státusz</li>
                <p>Önálló cikkek esetében a státusz (aktív, inaktív) beállítására a menükezelőben létrehozott és a
                    cikkhez rendelt menüpontnál van lehetőség. Amennyiben a menüpont aktív a cikk megjelenik az oldalon,
                    ha a menüpont
                    inaktív a cikk sem jelenik meg.</p>
                <li>A cikk mentése</li>
                <p>A cikk mentése a "Mentés" gomb megnyomásával lehetséges. Ezt követően az oldal marad az adott cikk
                    aloldalán és amennyiben szükséges elvégezhetők a szükséges módosítások (pl. képek szövegbe
                    illesztése, bélyegkép kijelölése stb.).
                    Amennyiben a cikk adatai között a mentés szempontjából nem megfelelő, vagy hiányos adat szerepel azt
                    a rendszer hibaüzenettel jelzi. Mentést követően az oldal az adott cikk aloldalán marad és szükség
                    esetén elvégezhetők a képeket
                    érintő módosítások (pl. képek szövegbe illesztése, bélyegkép, háttérkép kijelölése stb.)</p>
                <li>Képek beállításainak módosítása a cikk mentését követően</li>
                <p>A cikk mentését követően megnyíló aloldalon a "Galéria" panelen belül kerülnek felsorolásra az adott
                    cikkhez előzetesen feltöltött képek.</p>
                <p>A képeket
                    <ul>
                        <li>a "Cikk szövege" mező szövegszerkesztőjének segítségével be lehet illeszteni egy cikk
                            szövegébe,</li>
                        <li>kategóriához tartozó cikk esetében ki lehet jelölni az adott cikk bélyegképeként, ami az
                            adott kategória alodalán a cikkre mutató hivatkozásként jelenik meg,</li>
                        <li>főoldal esetében be lehet állítani háttérképként (kategóriába osztott cikkek esetén a
                            háttérképként történő kijelölés automatikus)</li>
                    </ul>
                </p>
                <p>A kép szerepének módosításához először is kattintsunk az adott képre, amelynek következtében a
                    kiválasztott kép és a módosításhoz szükséges kezelőszervek megjelennek egy felugró ablakban.</p>
                <p><i>Kép beillesztése cikk szövegébe</i></p>
                <p>Amennyiben a képet a cikk szövegébe szeretnénk beilleszteni
                    <ul>
                        <li>A "Hivatkozás másolása" című gombra kattintva másoljuk ki a kép hivatkozását.</li>
                        <li>Zárjuk be a felugró ablakot és görgessünk a "Cikk szövege" mezőhöz. Keressük meg a szövegben
                            azt a helyet, ahová a képet be szeretnénk illeszteni és kattintsunk az egérrel a helyen lévő
                            sor ELEJÉRE.
                            Ezt követően a szövegszerkesztő panelen keressük meg a kép beszúrását lehetővé tevő gombot
                            (4. sor balról az első) és kattintsunk rá. A megnyíló ablak "Hivatkozás" című mezőjébe
                            CTRL+V billentyű kombináció használatával szúrjuk be a kép linkjét.
                            A kép elsődleges paramétereit a "Szélesség" és "Magasság" mezőkben tudjuk beállítani.
                            Amennyiben a mezők melletti lakat alakú ikon zárva van a kép a szélesség állításakor
                            aránytartóan méreteződik újra. A kép szövegtől való távolságtartását
                            a "Vizsz.táv" és "Függ.táv" mezőkbe írt értékekkel (egész számok) tudjuk szabályozni. A
                            méretadatok pixelben kerülnek átszámításra.<br />
                            Az "Igazítás" mezőben a kép elhelyezkedését tudjuk beállítani. Amennyiben azt szeretnénk,
                            hogy a kép az oldal bal oldalán helyezkedjen el válasszuk ki a "Bal" értéket (ebben az
                            esetben a szöveg a képtől jobbra fog elhelyezkedni), ha jobb oldalra szeretnénk
                            pozícionálni, akkor válasszuk a "Jobb" opciót. Utóbbi esetben a szöveg a képtől balra fog
                            elhelyezkedni.<br />
                            Az "Alternatív szöveg" mezőben érdemes a kép nagyon rövid (egy-két szavas) leírását megadni,
                            mivel betöltési hiba esetén ez jelenik meg a böngészőben leírva a kép tartalmát, és a
                            látássérülteket segítő szoftver is ezt olvassa be azoknak a látogatóknak, akik
                            fogyatékosságuk
                            miatt nem láthatják a képet.</li>
                        <li>A fent leírt beállításokat követően a "Rendben" gombbal zárjuk be az ablakot és ha mindent
                            jól csináltunk a képnek a megfelelő helyen meg kell jelennie a szövegszerkesztőben.</li>
                        <li>Utolsó lépésként az aloldal alján található "Módosítás" gombbal mentsük el a cikket. Ha ez a
                            lépés kimarad a kép beillesztése nem kerül mentésre, ezért a kép nem fog megjelenni az
                            oldalon.</li>
                    </ul>
                </p>
                <p><i>Bélyegkép beállítása</i></p>
                <p>Bélyegképként történő kijelölés esetén a képre történő kattintás után jelöljük ki a "Bélyegkép"
                    jelölönégyzetet, majd kattintsunk az aktiválódó "Módosítás" gombra és végül a felugró ablakban
                    erősítsük meg a módosítási szándékunkat.
                    Figyeljünk arra, hogy bélyegképnek csak a kategóriába osztott cikkeknél van szerepe, mivel ezek
                    jelennek meg az adott kategória aloldalán és ezekre kattintva nyílik meg az adott cikk aloldala.
                    Önálló cikkek esetében a bélyegképeknek nincs külön szerepe, azok a tartalomban nem jelennek meg
                    sehol.
                    Cikkenként egy bélyegkép megadására van lehetőség, amelyet a kiválasztás után a cikkmódosító
                    aloldalon vastag piros keret jelöl. Amennyiben egy cikknek nincs bélyegképe a kategória aloldalon
                    egy helykitöltő kép jelenik meg.</p>
                <p><i>Háttérkép beállítása</i></p>
                <p>A háttérképek az oldalakra beállító slider (beúszó) képek, amelyeknek a külön beállítására kizárólag
                    a főoldal esetén van lehetőség. Főoldalnak nem kiválasztott önálló cikkek esetében nem állíthatók be
                    háttérképek, míg a kategóriákba osztott cikkekhez feltöltött képek automatikusan háttérképnek
                    kerülnek kijelölésre.<br />
                    Háttérképek beállításához nyissuk meg a főoldalnak kiválasztott cikk módosító aloldalát (Cikkek ->
                    Cikk kezelése -> "Kezelés" gomb az adott cikk sorában) és a Galéria paneken belül kattintsunk a
                    beállítandó képre
                    Ezt követően jelöljük ki a "Háttérkép" jelölőnégyzetet és a "Módosítás" gombbal mentsük el a
                    változtatást. Háttérkép sliderből történő eltávolításához szüntessük meg a jelölőnégyzet kijelölését
                    és mentsük a módosítást.
                    Amennyiben a főoldalhoz nem kerül egyetlen háttérkép sem kiválasztásra a főoldalon egy sötét hátterű
                    helykitöltő kép jelenik meg a cég nevével. Ha csak egy háttérkép kerül kijelölésre, akkor a slider
                    funkció nem fog működni, de az egyetlen kiválasztott kép megjelenik.
                    A főoldal háttérképeinek kiválasztásánál vegyük figyelembe, hogy a képek nem méreteződnek
                    dinamikusan, vagyis az eredeti méretüknek megfelelően töltik ki a képernyőt. Ezért érdemes lehetőség
                    szerint nagyobb méretű (kb. 1500-1600 px széles), ugyanolyan méretadatokkal rendelkező háttérképeket
                    kiválasztani.</p>
                <p><i>Kép(ek) törlése</i></p>
                <p>A szükségtelen képeket szintén a cikkmódosító aloldal "Galéria" panelén keresztül lehet eltávolítani.
                    A törléshez kattintsunk az adott képre, majd a megnyiló felugró ablakban válasszuk a "Törlés"
                    gombot. A kép eltávolítása a törlési szándék megerősítése (felugró ablak) után következik be.
                    A képek törlése végleges azok visszaállítására nincs lehetőség. A galéria képei egyben is törölhetők
                    a képek alatt található "Az összes kép törlése" gomb segítségével. A törlési szándékot ebben az
                    eetben is meg kell erősíteni a képek végleges eltávolítása előtt.</p>
            </ul>
            <li><strong>Önálló cikk kezelése (módosítás, törlés)</strong></li>
            <p>Önálló cikk kezeléséhez kattintsunk a "Cikkek" almenü "Cikk kezelése" pontjára, majd az adott cikk
                sorában található "Kezelés" gombra kattintva nyissuk meg a cikkmódosító aloldalt.</p>
            <p><i>Kereső funkciók</i></p>
            <p>A megnyíló aloldalon az adatbázisban mentett cikkek áttekintő táblázata látható, amelyben a cikkek ABC
                sorrendben kerülnek felsorolásra.
                Az aloldal tetején láthatók a cikkek szűrését, illetve a kulcsszavas keresést lehetővé tevő
                kezelőeszközök. A szűrő segítségével kategória és státusz szerint szűrhetjük a cikkeket, míg a
                gyorskereső kulcsszavas keresést tesz lehetővé.
                Kategória és/vagy státusz szerinti szűrés esetén, a kiválasztott cikk módosítását követően, a legutolsó
                szűrés eredményeit tartalmazó táblázathoz léphetünk vissza, ami megkönnyíti több azonos tematikájú cikk
                egymást követő módosítását. A keresési eredményektől történő visszalépés ("Vissza a listához") az összes
                cikket
                tartalmazó táblázathoz irányít minket vissza. A gyorskereső funkció nem őrzi meg a keresés
                eredménylistáját.</p>
            <p><i>Cikk módosítása</i></p>
            <p>Egy adott cikk módosításához válasszuk ki a cikket a "Cikkek kezelése" című aloldalon található
                táblázatból és kattintsunk a cikk sorában található "Kezelés" gombra. A megnyíló és a cikk adataival
                kitöltött űrlap megegyezik a cikk létrehozásakor használt űrlappal és használatának szabályai is
                ugyanazok (lsd. fent).
                A cikk módosítását az oldal alján található "Módosítás" gombbal véglegesíthetjük. Fontos részlet, hogy a
                galéria képeinek módosításakor, A KÉP A CIKK SZÖVEGÉBE ILLESZTÉSÉNEK ESETÉT KIVÉVE (lsd. fent), elegendő
                a kép módosítást lehetővé tevő felugró ablakban elmenteni a módosítást, a teljes cikk módosításának (lap
                alján található "Módosítás" gomb)
                mentésére nincs szükség.</p>
            <p><i>Cikk törlése</i></p>
            <p>Cikk törlésére szintén a cikkmódosító aloldalon van lehetőség az aloldal alján található "Törlés" gomb
                segítségével. A cikk törlésével a cikk valamennyi adata és galériájának képei is véglegesen és
                visszaállíthatatlanul törlődnek. A cikk törléséhez a törlési szándékot egy felugró ablakon keresztül meg
                kell erősíteni.</p>
            <li><strong>Kategóriába illesztett cikk létrehozása és a kategóriák kezelése</strong></li>
            <p>Kategóriába beosztott cikkek létrehozása és kezelése összefügg a kategóriákkal végzendő műveletekkel.</p>
            <p>A kategóriák alapvetően az ugyanolyan tematikájú cikkeket csoportosítják (pl. Családi házak).
                Megjelenésüket az oldalon egy "kategória" típusú menüpont biztosítja, amelyet a "Menükezelő" részben
                tudunk létrehozni az ott leírtak szerint.</p>
            <p><i>Kategória létrehozásának a lépései a következők:</i></p>
            <ul>
                <li>Amennyiben még nem tettük meg a menükezelőben hozzunk létre egy "kategória" típusú menüpontot (Menük
                    -> Menüpont készítése), amelyhez majd hozzárendeljük a kategóriát.</li>
                <li>A tartalomkezelőben nyissuk meg a kategóriák létrehozására szolgáló aloldalt (Tartalom -> Kategóriák
                    -> Kategória készítése) és a mezők kitöltésével hozzuk létre a kategóriát.
                    <ul>
                        <li>Kategória címe: a kategória címe, ami megjelenik a kategóriára mutató hivatkozásként
                            (kötelező mező).</li>
                        <li>Kategória szülőmenüje: a kategória számára létrehozott menüpont. A listában csak azok a
                            kategória típusú menüpontok jelennek meg, amelyekhez még nem került hozzárendelésre
                            kategória (kötelező mező).</li>
                        <li>Kategória pozíciója: a kategória linkjének a szülőmenün belül elfoglalt helye (kötelező
                            mező).</li>
                        <li>Kategória státusza: Az Aktív státusz kiválasztása esetén a kategória és a kategóriába
                            beosztott cikkek megjelennek az odalon, ellenkező esetben sem a kategória linkje sem a
                            kategória cikkei nem lesznek láthatók.</li>
                    </ul>
                </li>
            </ul>
            <p>A létrehozott kategóriák listáját a tartalomkezelő megfelelő aloldalán (Tartalom -> Kategóriák ->
                Kategóriák kezelése) tekinthetjük, illetve az adott kategória sorában található "Kezelés" gomb
                megnyomása után megjelenő űrlap értelemszerű kitöltésével módosíthatjuk.
                Fontos, hogy egy kategória csak abban az esetben törölhető, ha nem tartalmaz beosztott cikkeket.</p>
            <p>A megfelelő kategória létrehozása után nyissuk meg a cikkek elkészítését szolgáló aloldalt (Tartalom ->
                Cikkek -> Cikk készítése).</p>
            <p>A kategóriába osztott cikkek létrehozása javarészt megegyezik az önálló cikkekével, azzal a különbséggel,
                hogy a "Menü beállítása" panelen belül a cikket
                <ul>
                    <li>hozzá kell rendelnünk a kategóriájához (Kategória),</li>
                    <li>meg kell adnunk a menüpont nevét, amely hivatkozásként a kategória aloldalon fog megjelenni
                        (Menüpont neve). Figyelem: a menüpont nevének egyedinek kell lennie. Két ugyanolyan menüpontnév
                        nem szerepelhet az adatbázisban. A menüpont neve megadható a cikk címének átmásolásával is ("Cím
                        másolása" jelölőnégyzet).</li>
                    <li>ki kell választanunk a cikk pozícióját, amelyben a cikk a kategória aloldalon meg fog jelenni
                        (Menüpont pozíciója).</li>
                </ul>
                <p>Minden más tekintetben a kategóriába osztott cikkek kezelése megegyezik az önálló cikkeknél
                    ismertetett folyamattal.</p>
        </ol>
        <p><strong><u>Fejlesztői információ:</u></strong></p>
        <p>A tartalomkezelő funkcióhoz tartozó fájlok az alábbiak:</p>
        <ol>
            <li>PHP:
                <ul>
                    <li>Controller: /application/controllers/admin/Content.php</li>
                    <li>Model: /application/models/Data.php</li>
                    <li>View:
                        <ul>
                            <li>/application/views/admin/createcategory.php</li>
                            <li>/application/views/admin/managecategory.php</li>
                            <li>/application/views/admin/updatecategory.php</li>
                            <li>/application/views/admin/createarticle.php</li>
                            <li>/application/views/admin/managearticle.php</li>
                            <li>/application/views/admin/updatearticle.php</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>JavaScript:
                <ul>
                    <li>/assets/js/admin/category.js</li>
                    <li>/assets/js/admin/article.js</li>
                    <li>/assets/js/admin/filter.js</li>
                    <li>/assets/js/admin/validation.js</li>
                </ul>
            </li>
            <li>CSS: /assets/css/adminStyle.css
        </ol>
    </article>
</div>