<div class="w3-row">
    <article class="w3-half w3-panel w3-theme-l1 w3-xxlarge w3-card-4 w3-mobile title">Üdvözlöm az admin felület
        kezdőoldalán!</article>
</div>
<?php 
if(!empty($this->alert)) {
    if(in_array('adminFail', $this->alert)) { ?>
<div class="w3-row">
    <div class="w3-container w3-half w3-padding w3-panel w3-red">Jelenleg nincs fő adminisztrátor kiválasztva. A
        felhasználók jogosultságát csak a fő adminisztrátor szerkesztheti ezért kérem, hogy kilépés előtt <a
            href="<?php echo base_url(); ?>index.php/admin/users/manageusers">kattinston ide</a> és jelöljön ki egy fő
        adminisztrátort!</div>
</div>
<?php } 
    if(in_array('homePageFail', $this->alert)) { ?>
<div class="w3-row">
    <div class="w3-container w3-half w3-padding w3-panel w3-red">Jelenleg nincs főoldal kiválasztva, amelynek hiányában
        az odal elérhetetlenné válik. A főoldal kiválasztásához kérem, <a
            href="<?php echo base_url(); ?>index.php/admin/content/managearticle">kattintson ide</a> és válasszon a
        cikkek közül!</div>
</div>
<?php } 
    if(in_array('homePageLinkFail', $this->alert)) { ?>
<div class="w3-row">
    <div class="w3-container w3-half w3-padding w3-panel w3-red">A főldalhoz jelenleg nincs aktív hivatkozás rendelve,
        amelynek hiányában az oldal elérhetetlen lesz. A hivatkozás kiválasztásához kérem, <a
            href="<?php echo base_url(); ?>index.php/admin/content/managearticle">kattintson ide</a>.</div>
</div>
<?php } } ?>
<div class="w3-row"><div class="w3-content w3-half">
    <article class="doc">
        <p>A honlap adminisztratív felületén belül a honlap <strong>különböző funkciói</strong> kezelhetők, amelyek az alábbiak:</p>
        <ul>
            <li>Menük létrehozása, kezelése.</li>
            <li>Tartalmi elemek (kategóriák, cikkek) létrehozása és kezelése.</li>
            <li>A honlapon különböző jogosultságokkal rendelkező felhasználók hozzáadása és kezelése.</li>
        </ul>
        <p>Az egyes funkciók alfunkciói az oldalak <strong>jobb oldalán elhelyezett kattintásra kinyíló menüsorban</strong> lévő pontokon keresztül érhetők el.</p>
        <p>Az egyes funkciók kezeléséhez tartozó tudnivalók <strong>a funkciók főoldalain</strong> kerülnek ismertetetésre, amelyekhez a fenti <strong>vízszintes főmenüsor</strong> elemein keresztül lehet eljutni.</p>
        <p>A honlap felhasználói személyes adatot vagy egyéb látogatói statisztikát  semmilyen módon sem rögzít és nem kezel!</p>
        <p>A honlap <strong>technikai jellemzőire</strong> vonatkozó főbb információk az alábbiak:</p>
        <ul>
        <li>Használt programozási nyelvek: PHP (v7.0+), JavaScript</li>
        <li>Adatbázis típusa: MySQL</li>
        <li>PHP keretrendszer típusa: <a href="https://codeigniter.com/" target="_blank">CodeIgniter 3.1.11</a></li>
        <li>Felhasznált <strong>JavaScript</strong> könyvtárak (/assets/js/):</li>
        <ul>
        <li><a href="https://jquery.com/" target="_blank">jQuery 3.4.1</a></li>
        <li><a href="https://ckeditor.com/" target="_blank">CKeditor4</a></li>
        <li><a href="http://sachinchoolur.github.io/lightslider/examples.html" target="_blank">LightSlider</a></li>
        </ul>
        <li>Felhasznált <strong>CSS</strong> könyvtár (/assets/css/): <a href="https://www.w3schools.com/w3css/" target="_blank">W3.CSS</a></li>
        </ul>
        <p><strong>A különböző funckiókra tartozó információkért kérem, kattintson a felső menüsáv megfelelő pontjára.</strong></p>       
    </article>
</div></div>
<script>
$("button.side-open").css("display", "none");
</script>