<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Cikkek kezelése</article>
    </div>
    <div class="w3-row w3-margin-bottom">
    <h2>Szürő</h2>
    <div class="w3-row">
        <form method="POST"> 
        <div class="w3-third w3-left w3-margin-right w3-mobile">
            <select class="w3-input w3-border" name="catName">
                <option value="select" selected>Kategória</option>
                <?php
                foreach($this->catList as $cat) {
                    echo "<option value=\"{$cat->catId}\">{$cat->catTitle}</option>";
                }
                ?>
            </select> 
        </div>
        <div class="w3-third w3-left  w3-mobile">
            <select class="w3-input w3-border" name="artStat">
                <option value="select" selected>Státusz</option>
                <option value="1">Aktív</option>
                <option value="0">Inaktív</option>
            </select>
        </div>
        <div class="w3-third w3-left w3-margin-top w3-mobile">
            <input type="submit" class="w3-btn w3-red" name="filter_send" value="Keresés" />&nbsp;<a href="<?php echo base_url(); ?>index.php/admin/content/managearticle" class="w3-btn w3-blue">Vissza a listához</a>
        </div>
        </form> 
    </div>
    <div class="w3-row w3-margin-top">
        <h3>Gyorsszűrő</h3>
        <input type="text" class="filter" placeholder="Bármelyik adat" size="50%" />&nbsp;
    </div>        
    </div>
    <div class="w3-row w3-mobile">
        <?php 
        if(count($this->artList) > 0) {
            echo $this->table->generate();
        }
        else { ?>
            <div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Jelenleg nincsenek mentett cikkek!</div>
        <?php } ?>
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/js/admin/filter.js"></script>
