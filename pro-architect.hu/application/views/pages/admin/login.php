<script>
$("button.side-open").css("display", "none");
</script>
<div class="w3-row">
    <section class="w3-container w3-mobile loginForm">
        <div class="w3-container">
        <?php
        if(isset($_POST['loginSend'])) {
            if(!empty($this->alert)) {                
                echo "<div class=\"w3-panel w3-orange w3-large w3-padding\">{$this->alert}</div>";            
            }
        }
        ?>
        <article class="w3-twothird w3-padding w3-panel w3-theme-l1 w3-xlarge w3-card-4 w3-mobile title">Kérem, adja meg a belépési adatait!</article>
        </div>
        <article>
            <form method="POST" action="">
                <div class="w3-container form-group">
                    <input type="text" class="w3-input w3-border" name="loginName" placeholder="Felhasználó név" value="<?php echo set_value('loginName'); ?>" />
                    <?php echo form_error('loginName', '<span class="alert">', '</span>'); ?>
                </div>
                <div class="w3-container form-group">
                    <input type="password" class="w3-input w3-border" name="loginPass" placeholder="Jelszó" value="" autocomplete="off" />
                    <?php echo form_error('loginPass', '<span class="alert">', '</span>'); ?>
                </div>
                <div class="w3-container form-group">
                    <input type="submit" class="w3-btn w3-red" name="loginSend" value="Belépés" />
                </div>
            </form>
        </article>
    </section>
</div>