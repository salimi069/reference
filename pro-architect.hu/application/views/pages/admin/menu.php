<div class="w3-row">
    <article class="w3-half w3-panel w3-theme-l1 w3-xxlarge w3-card-4 w3-mobile title">Üdvözlöm a menükezelő főoldalán
    </article>
</div>
<div class="row">
    <article class="w3-half doc">
        <p>A menükezelőben hozhatók létre azok a menüpontok, amelyeken keresztül az odal tartalmi elemei (kategóriák,
            cikkek) elérhetők.</p>
        <p>A létrehozott menüpontok jelenleg a <strong>honlap főoldalának alsó részén látható vízszintes
                menüsávban</strong> helyezkednek
            el.
            Új menüpontok létrehozásánál figyeljünk arra, hogy bár az említett menüsáv korlátlan számú menüpont
            megjelenítésére képes, de a menüpontok rendezett megjelenítését
            befolyásolja az adott <strong>megjelenítő képernyő mérete</strong>, illetve hatással van rá az egyes
            menüpontok <strong>nevének
                hossza</strong>. Ezen okokból javasolt <strong>rövid, lényegretörő</strong> menüpont neveket
            adni, illetve ügyelni a <strong>korlátozott számú menüpontok</strong> létrehozására. Amennyiben mégis
            szükség lenne sok új
            menüpontra, úgy érdemes az adott tartalmakat <strong>kategóriákba</strong>
            rendezni és az adott kategóriát egy menüponthoz rendelni. A kategóriák létrehozásáról és kezeléséről a
            "Tartalom" funkció főoldalán lehet többet megtudni. A főmenüsáv <strong>950px
                képernyőszélesség</strong> alatt vált át a mobilnézetben megnyíló bal oldali menüsávra, amelyben a
            menüpontok egymás
            alatt helyezkednek el.</p>
        A menükezelőnek <strong>két alfunkciója</strong> van (jobb oldali menüsáv):
        <ul>
            <li>Menüpont készítése</li>
            <li>Menüpont kezelése</li>
        </ul>
        <ol>
            <li><strong>Menüpont készítése:</strong></li>
            <p>Menüpontok létrehozásához a <strong>"Menüpont készítése"</strong> almenüpont alatti aloldalon található
                űrlapot kell kitölteni, amely
                az alábbi mezőkkel rendelkezik:</p>
            <ul>
                <li><u>Menü neve:</u></li>
                <p>A menüpont neve, amely a honlap főoldalának alsó részén elhelyezett vízszintes főmenüsávban jelenik
                    meg. Lehetőség szerint törekedjünk a menüpont tartalmát legjobban meghatározó
                    <strong>rövid és lényegretörő</strong> név megadására.<br /><strong>FIGYELEM:</strong> a menüpontok
                    nevének
                    különbözniük kell egymástól, két teljesen egyforma menüpont név a tartalommegjelenítés ütközéseinek
                    elkerülése érdekében <strong>nem engedélyezett</strong>!</p>
                <p><strong>A menünév megadása kötelező!</strong></p>
                <li><u>Menüpont típusa:</u></li>
                <p>A menüponthoz az alábbi típusok egyike rendelhető:</p>
                <ul>
                    <li>Cikk:</li>
                    <p>A cikk típusú menüpontokon keresztül a tartalomkezelőben létrehozott <strong>cikk típusú tartalmi
                            elem</strong>
                        jeleníthető meg. Egy menüponthoz egy cikk rendelhető hozzá a "Tartalom" funkciónál
                        található leírás szerint.</p>
                    <li>Kategória:</li>
                    <p>A kategória típusú menüpontokon keresztül a tartalomkezelőben létrehozott <strong>kategória
                            típusú
                            tartalmi elem(ek)</strong>
                        jeleníthető(k) meg. Egy
                        menüponthoz tetszőleges számú kategória rendelhető hozzá.
                        A menüponthoz rendelt kategóriák hivatkozásai a menüpontra (vízszintes főmenüsáv)
                        történő kattintás után kinyíló <strong>jobb oldali tartalomdobozban</strong> jelennek meg a honlap
                        főoldalán. A
                        létrehozott menüponthoz a "Tartalom" funkciónál rendelhető hozzá a megfelelő kategória a szintén
                        a "Tartalom" funkciónál leírtak
                        szerint.</p>
                </ul>
                <p><strong>A menüpont típusának megadása kötelező!</strong></p>
                <li><u>Menüpont pozíciója:</u></li>
                <p>A menüpont pozíciója a menüpont <strong>főmenüsávon belül elfoglalt helyét</strong> határozza meg. A
                    pozíciók számozása
                    0-tól, a menüpontok elhelyezése pedig a menüsáv bal oldalától
                    kezdődik, növekvő számsorrend szerint. Új menüpont létrehozásakor a legördülő listában a következő
                    szabad pozíció automatikusan a lista végén feltüntetésre kerül.
                    Amennyiben az új menüpont <strong>egy már foglalt pozícióba</strong> kerülne beosztásra, a
                    leggördülő listában
                    egyszerűen válasszuk ki a megfelelő pozíciót és mentsük a menüpontot. Ebben az esetben az új
                    menüpont a kijelölt pozícióba kerül és a pozíción eredetileg elhelyezkedő régi menüpont, illetve az
                    utána
                    következő összes többi automatikusan <strong>eggyel hátrébb kerül</strong> a sorrendben.</p>
                <p><strong>A menüpont pozíciójának megadása kötelező!</strong></p>
                <li><u>Menüpont státusza:</u></li>
                <p>A menüpont státusza szerint lehet <strong>aktív, illetve inaktív</strong>, amely a jelölő doboz
                    kijelölésével (Aktív),
                    valamint a kijelölés megszüntetésével (Inaktív) határozható meg.
                    Aktív státusz esetén a menüpont <strong>megjelenik</strong> a honlap főoldalán található vízszintes
                    főmenüsávban és a
                    menüponthoz rendelt tartalom is <strong>elérhető</strong>. Inaktív státusz esetén a menüpont
                    <strong>nem
                        jelenik meg</strong> a főmenüsávban és a hozzárendelt tartalom <strong>sem érhető el</strong> a
                    honlapon. Alaphelyzetben a
                    státusz <strong>inaktív</strong>, ezért a menüpont létrehozásakor, amennyiben azt már a
                    létrehozáskor aktiválni
                    akarjuk, a jelölő négyzetet mindenképpen ki kell választani. A státusz és vele együtt a menüpont,
                    illetve a
                    hozzárendelt tartalmak megjelenése <strong>bármikor megváltozhatatható</strong> a "Menüpont
                    kezelése" című alfunkció
                    alatt.</p>
            </ul>
            <p>A menüpont készítésének adatai a <strong>"Mentés"</strong> gombra kattintva véglegesíthetők az
                adatbázisban. A mentéssel
                kapcsolatban felmerülő esetleges problémákat <strong>hibaüzenetek</strong> jelzik.</p>
            <li><strong>Menüpont kezelése</strong></li>
            <p>A alfunkció nyitóoldalán a létező menüpontokat tartalmazó <strong>táblázat</strong> látható, amelyben
                szerepelnek a
                menüpontok:</p>
            <ul>
                <li>nevei</li>
                <li>típusai</li>
                <li>pozíciói</li>
                <li>státuszai</li>
                <li>a kezelő aloldalra vezető gomb</li>
            </ul>
            <p>A menüpont kezelését végző aloldalra a <strong>"Kezelés"</strong> című gombra történő kattintás után
                juthatunk el. Az
                aloldal a menüpont létrehozásakor használt űrlapot tartalmazza, amely
                <strong>előre kitöltésre</strong> kerül a módosításra kiválasztott menüpont adataival. Az űrlap
                kezelésére vonatkozó
                tudnivalók <strong>megegyeznek az 1. pontban leírtakkal</strong>, azzal a különbséggel, hogy
                módosításkor a menüpont <strong>nem osztható be teljesen új pozícióba</strong>, kizárólag a már meglévő
                pozíciók közül
                történő választás lehetséges. A módosítás adatai a <strong>"Módosítás"</strong> gombra történő
                kattintással véglegesíthetők az adatbázisban. A módosítással kapcsolatban felmerülő esetleges
                problémákat <strong>hibaüzenetek</strong> jelzik.</p>
            <p>Ugyancsak az aloldalon van lehetőség a feleslegessé vált menüpont <strong>törlésére</strong>, amely a
                <strong>"Törlés"</strong> gombra
                történő kattintással végezhető el. A menüpont törlése nem történik meg azonnal a gombra
                történő kattintás után, azt a vonatkozó felugró ablakban <strong>jóvá kell hagyni</strong>. A törlés
                <strong>végleges</strong>, azt
                követően az érintett menüpont <strong>nem állítható vissza</strong>.<br />
                <strong>FIGYELEM:</strong> csak olyan menüpont törölhető véglegesen, amely <strong>nem
                    rendelkezik</strong> hozzárendelt
                tartalmi elemmel (cikk, kategória). Ennek megfelelően egy menüpont törléséhez először a
                hozzá tartozó kategóriát, vagy cikket kell törölni.</p>
        </ol>
        <p><strong><u>Fejlesztői információ:</u></strong></p>
        <p>A menükezelő funkcióhoz tartozó fájlok az alábbiak:</p>
        <ol>
            <li>PHP:
                <ul>
                    <li>Controller: /application/controllers/admin/Menu.php</li>
                    <li>Model: /application/models/Data.php</li>
                    <li>Views:<br />
                        <ul>
                            <li>/application/views/pages/admin/menu.php</li>
                            <li>/application/views/pages/admin/createmenu.php</li>
                            <li>/application/views/pages/admin/managemenu.php</li>
                            <li>/application/views/pages/admin/updatemenu.php</li>
                            <li>/application/views/templates/admin/menuNav.php</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>JavaScript:
                <ul>
                    <li>/assets/js/admin/menu.js</li>
                    <li>/assets/js/admin/filter.js</li>                    
                </ul>
            </li>
            <li>CSS:
                <ul>
                    <li>/assets/css/w3.css</li>
                    <li>/assets/css/adminStyle.css</li>
                </ul>
            </li>
        </ol>
    </article>
</div>