<script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<span class="baseUrl"><?php echo base_url(); ?></span>
<span class="catName"><?php echo $this->catName; ?></span>
<span class="artStat"><?php echo $this->artStat; ?></span>
<span id="validationStatus"></span>
<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Cikk frissítése</article>
    </div>

    <div class="w3-row w3-mobile">
        <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező</p>        
    </div>
    <div class="w3-row w3-mobile">
        <form class="w3-half w3-mobile" method="POST" enctype="form/multipart">
        <div class="form-part"><div class="w3-container"><h2>Alapadatok:</h2></div>
            <div class="w3-container form-group">
                <label for="artTitle">Cikk címe:<span class="asterisk">*</span></label>
                <input type="text" class="w3-input w3-border valid" id="artTitle" name="artTitle" value="<?php echo $this->art[0]->artTitle; ?>" />
                <span id="artTitle_error" class="alert"></span>                
            </div>
            <div class="w3-container form-group">
                <input type="checkbox" class="w3-check" name="artTitleShow" id="artTitleShow" value="1" <?php echo($this->art[0]->artTitleShow == 1) ? 'checked' : ''; ?> />
                &nbsp;A cím megjelenítése                
                &nbsp;<span class="w3-tooltip"><img class="hintImg" src="<?php echo base_url(); ?>assets/images/sys/hint.png" alt="Tipp" /><span class="w3-text w3-tag tooltipText">Az oldalsó tartalomsáv tetején.</span></span>
            </div>            
            <div class="w3-container form-group">
                <label for="artText">Cikk szövege:</label>
                <textarea class="w3-input w3-border ckeditor" name="artText" id="artText"><?php echo(!empty($this->art[0]->artText)) ? $this->art[0]->artText : ''; ?></textarea>
                <span class="alert"></span>
            </div>
            </div>
            <div class="form-part"><div class="w3-container"><h2>Menü beállítás:</h2></div>
            <?php if(count($this->catList) > 0) { ?>                      
            <div class="w3-container form-group">
                <label id="artCatLabel" for="artCat">Kategória:</label>
                <select class="w3-select w3-border pos" name="artCat" id="artCat">
                    <option value="" selected>Kérem, válasszon</option>
                    <?php foreach($this->catList as $cat) { ?>
                        <option value="<?php echo $cat->catId; ?>" <?php echo($cat->catId == $this->art[0]->artCat) ? 'selected' : ''; ?>><?php echo $cat->catTitle; ?></option>";
                    <?php } ?>                                                       
                </select>
                <span class="alert"></span>                
            </div>            
            <div class="w3-container form-group">
                <label id="menuTitleLabel" for="artMenuTitle">Menüpont neve:</label>
                <p>Cím másolása: <input type="checkbox" class="w3-check" id="titleCopy" /></p>
                <input type="text" class="w3-input w3-border" id="artMenuTitle" name="artMenuTitle" value="<?php echo(!empty($this->art[0]->artMenuTitle)) ? $this->art[0]->artMenuTitle : ''; ?>" />
                <span id="artMenuTitle_error" class="alert"></span>                                
            </div>
            <div class="w3-container form-group">
                <label id="menuPosLabel" for="artMenuPos">Menüpont pozíciója:</label>
                <select class="w3-select w3-border pos" name="artMenuPos" id="artMenuPos">
                    <option value="" selected>Kérem, válasszon</option>                                        
                </select>
                <span class="alert"></span>                                
            </div>                                                                            
            <?php } ?>           
            <div class="w3-container form-group">
                <label for="artParentMenu">Szülő menü:&nbsp;<span class="w3-tooltip"><img class="hintImg" src="<?php echo base_url(); ?>assets/images/sys/hint.png" alt="Tipp" /><span class="w3-text w3-tag tooltipText">Nem kategóriához tartozó cikk esetén.</span></span></label>
                <select class="w3-select w3-border pos" name="artParentMenu" id="artParentMenu">
                    <option value="0" selected>Kérem, válasszon</option>
                    <?php foreach($this->menuList as $menuId => $menuTitle) { ?>
                        <option value="<?php echo $menuId; ?>" <?php echo($menuId == $this->art[0]->artParentMenu) ? 'selected' : ''; ?>><?php echo $menuTitle; ?></option>";
                    <?php } ?>                                                                                 
                </select>                
            </div>
            <div class="w3-container form-group">
                <input type="checkbox" class="w3-check" name="homePage" id="homePage" value="1" <?php if($this->homePageCheck) echo($this->art[0]->homePage == 1) ? 'checked' : 'disabled'; ?>/>&nbsp;Főoldal
            </div>
            </div>
            <div class="form-part"><div class="w3-container w3-margin-bottom"><h2>Galéria:</h2>
            <p class="alert">FIGYELEM! Egyszerre csak 10 db kép tölthető fel!<br />
            Feltölthető fájltípusok: jpg, jpeg, gif, png.</p>
            <small>A kiválasztott bélyegképet vastag <span style="color: red; font-weight: bold">piros</span> keret jelöli.</small>
            </div>                                                                                                   
            <div class="w3-container form-group">
                <label for="artImg">Képek feltöltése:</label>
                <input type="file" class="w3-input w3-border artImg" id="artImg[]" name="artImg[]" value="" multiple/>                
            </div>            
            <div class="w3-row">
                <div class="galery w3-container">
                <?php
                if(!empty($this->images)) {

                    foreach($this->images as $img => $slider) { ?>
                        <img src="<?php echo base_url() . $this->imgRootFolder . $this->art[0]->artTitleAlias . '/' . $img; ?>" class="w3-image imgUpdate <?php echo($this->art[0]->artThumb === $img) ? 'thumbMark' : ''; ?>" alt="<?php echo $img; ?>" data-thumb="<?php echo(!empty($this->art[0]->artThumb)) ? $this->art[0]->artThumb : ''; ?>" data-art_id="<?php echo $this->art[0]->artId; ?>" data-slider="<?php echo $slider; ?>" />
                    <?php }
                }
                ?>                
                </div>
                <?php if(!empty($this->images)) { ?>
                <div class="w3-container">
                    <button type="button" id="delAllImg" class="w3-btn w3-red" data-art_id="<?php echo $this->art[0]->artId; ?>">Az összes kép törlése</button>
                </div>    
                <?php } ?>
            </div>
            </div>
            <div class="form-part"><div class="w3-container"><h2>Meta adatok:</h2></div>            
            <div class="w3-container form-group">
                <label for="artMetaKey">Kulcsszavak:</label>
                <input type="text" class="w3-input w3-border" id="artMetaKey" name="artMetaKey" value="<?php echo(!empty($this->art[0]->artMetaKey)) ? $this->art[0]->artMetaKey : ''; ?>" placeholder="pl. tervező, építész, épület, tervrajz (max. 20 db vesszővel elválasztott kulcsszó)" />                
            </div>
            <div class="w3-container form-group">
                <label for="artMetaDesc">Cikk rövid leírása:</label>
                <input type="text" class="w3-input w3-border" id="artMetaDesc" name="artMetaDesc" value="<?php echo(!empty($this->art[0]->artMetaDesc)) ? $this->art[0]->artMetaDesc : ''; ?>" placeholder="A cikk tartalmának rövid, egymondatos összefoglalása." />                
            </div>
            </div>
            <div class="form-part"><div class="w3-container"><h2>Státusz:</h2></div>                        
            <div class="w3-container form-group">
            <p class="alert">A főoldal automatikusan Aktív státusszal kerül mentésre és később sem lehet deaktiválni!<br />
                Önálló cikkeket csak a menükezelőben szereplő menüpontjuk deaktiválásával lehet elérhetetlenné tenni!</p>
                <input type="checkbox" class="w3-check" name="artStat" id="artStat" value="1" <?php echo($this->art[0]->artStat == 1 || $this->art[0]->homePage == 1) ? 'checked' : ''; ?> <?php echo($this->art[0]->artParentMenu != 0 || $this->art[0]->homePage == 1) ? 'disabled' : ''; ?> />&nbsp;Aktív
            </div>
            </div>
            <div class="w3-container form-group">
                <input type="submit" class="w3-btn w3-green send" data-art_id="<?php echo $this->art[0]->artId; ?>" data-action="update" name="artUpdate" id="artUpdate" value="Módosítás" />&nbsp;
                <input type="submit" class="w3-btn w3-red send" data-art_id="<?php echo $this->art[0]->artId; ?>" data-action="delete" name="artDelete" id="artDelete" value="Törlés" />&nbsp;
                <a href="<?php echo base_url(); ?>index.php/admin/content/managearticle/<?php echo $this->catName . '/' . $this->artStat; ?>" class="w3-btn w3-blue">Vissza a cikkek listájához</a>
            </div>
        </form>
    </div>
    </div>
</section>
<!-- The Modal -->
<div id="imgModal" class="w3-modal">
  <div class="w3-modal-content">
    <div class="w3-container">
      <span class="w3-button w3-display-topright closeModal">&times;</span>
      <div class="w3-container w3-half albImg"></div>
      <div class="w3-container imgData"></div>
    </div>
  </div>
</div>
<script>
CKEDITOR.replace('artText');
</script>
<script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/admin/article.js"></script>