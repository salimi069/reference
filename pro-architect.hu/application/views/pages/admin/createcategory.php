<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span class="baseUrl"><?php echo base_url(); ?></span>
<span id="validationStatus" class="baseUrl"></span>
<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Kategória készítése</article>
    </div>
    <div class="w3-row w3-mobile">
        <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező</p>                           
    </div>
    <div class="w3-row w3-mobile">
        <form class="w3-half w3-mobile" method="POST">
            <div class="w3-container form-group">
                <label for="catTitle">Kategória címe:<span class="asterisk">*</span></label>
                <input type="text" class="w3-input w3-border valid" id="catTitle" name="catTitle" value="" />                
                <span class="alert"></span>               
            </div>            
            <div class="w3-container form-group">
                <label for="catMenu">Kategória szülő menüje:<span class="asterisk">*</span></label>
                <select class="w3-select w3-border valid" name="catMenu" id="catMenu">
                    <option value="0" disabled selected>Kérem, válasszon</option>
                    <?php foreach($this->menuList as $menu) { ?>
                    <option value="<?php echo  $menu->menuId; ?>"><?php echo $menu->menuTitle; ?></option> 
                    <?php } ?>                                                                                                       
                </select>                
                <span class="alert"></span>
            </div>                                                                                                           
            <div class="w3-container form-group">
                <label for="catPos">Kategória pozíciója:<span class="asterisk">*</span></label>
                <select class="w3-select w3-border valid" name="catPos" id="catPos" data-pos="">
                    <option value="0" disabled selected>Kérem, válasszon</option>                                                                                                  
                </select>                
                <span class="alert"></span>
            </div>                                  
            <div class="w3-container form-group">
                <input type="checkbox" class="w3-check" name="catStat" id="catStat" value="1" />&nbsp;Aktív
            </div>
            <div class="w3-container form-group">
                <input type="submit" class="w3-btn w3-green send" data-action="save" name="catSave" id="catSave" value="Mentés" />&nbsp;
                <a href="<?php echo base_url(); ?>index.php/admin/content/managecategory" class="w3-btn w3-blue">A kategóriák listájához</a>
            </div>
        </form>
    </div>
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/admin/category.js"></script>