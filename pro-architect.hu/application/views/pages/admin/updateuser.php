<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span class="baseUrl"><?php echo base_url(); ?></span> 
<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Felhasználó módosítása</article>
    </div>
    <div class="w3-row w3-mobile">
        <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező</p>
    </div>
    <?php
        if(isset($_POST['userUpdate'])) {

            if(!$this->validationError) {

                switch(true) {

                    case $this->dataSave && !$this->adminError && !$this->emailError && !$this->passwordError:
                    echo '<div class="w3-half w3-container w3-panel w3-green w3-large w3-padding-16">Sikeres módosítás!</div>';
                    break;

                    case $this->adminError:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Csak egy Fő Adminisztrátor mentésére van lehetőség!</div>';
                    break;

                    case $this->emailError:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Ez az e-mail cím már szerepel az adatbázisban!</div>';
                    break;
                    
                    case $this->passwordError:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Hibás jelszó. Kérem, javítsa!</div>';
                    break;

                    case !$this->dataSave:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Sikertelen módosítás. Kérem, próbálja meg újra!</div>';
                    break;
                }
            }
        }
    ?>
    <div class="w3-row w3-mobile">
        <form class="w3-half w3-mobile" method="POST">
            <div class="w3-container form-group">
                <label for="userName">Felhasználó neve:<span class="asterisk">*</span></label>
                <input type="text" class="w3-input w3-border" id="userName" name="userName" value="<?php echo(isset($_POST['userUpdate'])) ? set_value('userName') : $this->users[0]->userName; ?>" <?php echo($this->userAccess == 1 && $this->userId != $this->users[0]->userId) ? 'readonly' : ''; ?> />
                <?php echo form_error('userName', '<span class="alert">', '</span>'); ?>                
            </div>
            <div class="w3-container form-group">
                <label for="userAccess">Felhasználó jogosultsága:<span class="asterisk">*</span></label>
                <select class="w3-select w3-border" name="userAccess" id="userAccess">
                    <option value="0" disabled selected>Kérem, válasszon</option>
                    <?php if(count($this->superAdminCheck) == 0 || $this->users[0]->userAccess == 1) { ?>
                    <option value="1" <?php echo(isset($_POST['userUpdate'])) ? set_select('userAccess', 1, false) : $this->users[0]->userAccess == 1 ? 'selected' : ''; ?>>Fő Adminisztrátor</option>
                    <?php } ?>                    
                    <option value="2" <?php echo(isset($_POST['userUpdate'])) ? set_select('userAccess', 2, false) : $this->users[0]->userAccess == 2 ? 'selected' : ''; ?>>Szerkesztő</option>
                </select>
                <?php echo form_error('userAccess', '<span class="alert">', '</span>'); ?>                
            </div>
            <div class="w3-container form-group">
                <label for="userEmail">Felhasználó e-mail címe:<span class="asterisk">*</span></label>
                <input type="email" class="w3-input w3-border" id="userEmail" name="userEmail" value="<?php echo(isset($_POST['userUpdate'])) ? set_value('userEmail') : $this->users[0]->userEmail; ?>" <?php echo($this->userAccess == 1 && $this->userId != $this->users[0]->userId ? 'readonly' : ''); ?> />
                <?php echo form_error('userEmail', '<span class="alert">', '</span>'); ?>                
            </div>
            <div class="w3-container form-group">
                <label for="userOldPass">Felhasználó jelenlegi jelszava:<span class="asterisk">*</span></label>
                <input type="password" class="w3-input w3-border valid" id="userOldPass" name="userOldPass" value="<?php echo set_value('userOldPass') ?>" />
                <?php echo form_error('userOldPass', '<span class="alert">', '</span>'); ?>                
            </div>
            <?php if($this->users[0]->userId == $this->userId) { ?> 
            <div class="w3-container form-group pass">
                <label id="passLabel" for="userPass">Felhasználó új jelszava:</label>
                <input type="password" class="w3-input w3-border pass" id="userPass" name="userPass" value="<?php echo set_value('userPass') ?>" />
                <?php echo form_error('userPass', '<span class="alert">', '</span>'); ?>
            </div>
            <div class="w3-container form-group pass">
                <label id="passLabel" for="userPassConf">Új jelszó megerősítése:</label>
                <input type="password" class="w3-input w3-border pass" id="userPassConf" name="userPassConf" value="<?php echo set_value('userPassConf'); ?>" />
                <?php echo form_error('userPassConf', '<span class="alert">', '</span>'); ?>
            </div>
            <?php }
            if($this->userAccess == 1) { ?>
            <div class="w3-container form-group">            
                <input type="checkbox" class="w3-check" name="userStat" id="userStat" value="1" 
                <?php
                if(isset($_POST['userUpdate'])) {
                    echo set_checkbox('userStat', 1, false);
                }
                else if($this->users[0]->userStat == 1 && $this->users[0]->userAccess == 1) {
                    echo 'checked disabled';
                }
                else if($this->users[0]->userStat == 1 && $this->users[0]->userAccess != 1) {
                    echo 'checked';
                }
                else {
                    echo '';
                }
                ?>                
                />&nbsp;Aktív
            </div>
            <?php } ?>
            <div class="w3-container form-group">
                <input type="submit" class="w3-btn w3-green send" name="userUpdate" id="userUpdate" value="Módosítás" />&nbsp;
                <?php if($this->userAccess == 1) { ?>
                <input type="submit" class="w3-btn w3-red" data-id="<?php echo $this->users[0]->userId; ?>" name="userDelete" id="userDelete" value="Törlés" />&nbsp;
                <a href="<?php echo base_url(); ?>index.php/admin/users/manageusers" class="w3-btn w3-blue">A felhasználók listájához</a>
                <?php } ?>
            </div>
        </form>   
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/js/admin/users.js"></script>