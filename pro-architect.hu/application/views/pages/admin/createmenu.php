<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Menü készítése</article>
    </div>
    <div class="w3-row w3-mobile">
        <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező</p>
        <?php if(isset($_POST['menuSave'])) { if($this->validation && $this->menuValidation) { ?>
           <div class="w3-half w3-container w3-panel w3-green w3-large w3-padding-16">Sikeres mentés!</div>
        <?php } else if(!$this->menuValidation) { ?>
            <div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Ez a menünév már szerepel az adatbázisban. Kérem, válasszon egy másikat!</div>
        <?php } } ?>                   
    </div>
    <div class="w3-row w3-mobile">
        <form class="w3-half w3-mobile" method="POST">
            <div class="w3-container form-group">
                <label for="menuTitle">Menü neve:<span class="asterisk">*</span></label>
                <input type="text" class="w3-input w3-border" id="menuTitle" name="menuTitle" value="<?php echo set_value('menuTitle'); ?>" />
                <?php echo form_error('menuTitle', '<span class="alert">', '</span>'); ?>                
            </div>
            <div class="w3-container form-group">
                <label for="menuType">Menüpont típusa:<span class="asterisk">*</span></label>
                <select class="w3-select w3-border" name="menuType" id="menuType">
                    <option value="0" disabled selected>Kérem, válasszon</option>
                    <option value="1" <?php echo set_select('menuType', 1, false); ?>>Cikk</option>
                    <option value="2" <?php echo set_select('menuType', 2, false); ?>>Kategória</option>                    
                </select>
                <?php echo form_error('menuType', '<span class="alert">', '</span>'); ?>
            </div>                                                                                  
            <div class="w3-container form-group">
                <label for="menuPos">Menüpont pozíciója:<span class="asterisk">*</span></label>
                <select class="w3-select w3-border" name="menuPos" id="menuPos">
                    <option value="0" disabled selected>Kérem, válasszon</option>
                    <?php
                    if($this->menuCount == 0) { ?>
                        <option value="1" <?php echo set_select('menuType', 1, false); ?> selected>1</option>
                    <?php } else {                        
                        $maxMenuNum = $this->menuCount == 1 ? 2 : ++$this->menuCount;
                
                        foreach($this->menuList as $menu) { ?>
                            <option value="<?php echo $menu->menuPos; ?>" <?php echo set_select('menuPos', $menu->menuPos, false); ?>><?php echo $menu->menuPos . ' - ' . $menu->menuTitle; ?></option>
                        <?php } ?>
                        <option value="<?php echo $maxMenuNum; ?>" <?php echo set_select('menuPos', $maxMenuNum, false); ?>><?php echo $maxMenuNum; ?></option>                      
                    <?php } ?>                                                                                    
                </select>
                <?php echo form_error('menuPos', '<span class="alert">', '</span>'); ?>
            </div>                                  
            <div class="w3-container form-group">
                <input type="checkbox" class="w3-check" name="menuStat" id="menuStat" value="1" <?php echo set_checkbox('menuStat', 1, false); ?> />&nbsp;Aktív
            </div>
            <div class="w3-container form-group">
                <input type="submit" class="w3-btn w3-green send" data-action="save" name="menuSave" id="menuSave" value="Mentés" />&nbsp;
                <a href="<?php echo base_url(); ?>index.php/admin/menu/managemenu" class="w3-btn w3-blue">A menük listájához</a>
            </div>
        </form>
    </div>
    </div>
</section>
