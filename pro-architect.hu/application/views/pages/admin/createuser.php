<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Felhasználó felvétele</article>
    </div>
    <div class="w3-row w3-mobile">
        <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező</p>                           
    </div>
    <?php
        if(isset($_POST['userSave'])) {

            if(!$this->validationError) {

                switch(true) {

                    case $this->dataSave && !$this->adminError && !$this->emailError:
                    echo '<div class="w3-half w3-container w3-panel w3-green w3-large w3-padding-16">Sikeres mentés!</div>';
                    break;

                    case $this->adminError:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Csak egy Fő Adminisztrátor mentésére van lehetőség!</div>';
                    break;

                    case $this->emailError:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Ez az e-mail cím már szerepel az adatbázisban!</div>';
                    break;                

                    case !$this->dataSave:
                    echo '<div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Sikertelen mentés. Kérem, próbálja meg újra!</div>';
                    break;
                }
            }
        }
    ?>
    <div class="w3-row w3-mobile">
        <form class="w3-half w3-mobile" method="POST">
            <div class="w3-container form-group">
                <label for="userName">Felhasználó neve:<span class="asterisk">*</span></label>
                <input type="text" class="w3-input w3-border" id="userName" name="userName" value="<?php echo set_value('userName'); ?>" />
                <?php echo form_error('userName', '<span class="alert">', '</span>'); ?>                                               
            </div>            
            <div class="w3-container form-group">
                <label for="userAccess">Felhasználó jogosultsága:<span class="asterisk">*</span></label>
                <select class="w3-select w3-border" name="userAccess" id="userAccess">
                    <option value="0" disabled selected>Kérem, válasszon</option>
                    <?php if(count($this->superAdminCheck) == 0) { ?>
                        <option value="1" <?php echo set_select('userAccess', 1, false); ?>>Fő Adminisztrátor</option>
                    <?php } ?>                    
                    <option value="2" <?php echo set_select('userAccess', 2, false); ?>>Szerkesztő</option>                                                                     
                </select>
                <?php echo form_error('userAccess', '<span class="alert">', '</span>'); ?>                                
            </div>
            <div class="w3-container form-group">
                <label for="userEmail">Felhasználó e-mail címe:<span class="asterisk">*</span></label>
                <input type="text" class="w3-input w3-border" id="userEmail" name="userEmail" value="<?php echo set_value('userEmail'); ?>" />
                <?php echo form_error('userEmail', '<span class="alert">', '</span>'); ?>                                               
            </div>
            <div class="w3-container form-group">
                <label for="userPass">Felhasználó jelszava:<span class="asterisk">*</span></label>
                <input type="password" class="w3-input w3-border" id="userPass" name="userPass" value="<?php echo set_value('userPass') ?>" />
                <?php echo form_error('userPass', '<span class="alert">', '</span>'); ?>                                               
            </div>
            <div class="w3-container form-group">
                <label for="userPassConf">Jelszó megerősítése:<span class="asterisk">*</span></label>
                <input type="password" class="w3-input w3-border" id="userPassConf" name="userPassConf" value="<?php echo set_value('userPassConf'); ?>" />
                <?php echo form_error('userPassConf', '<span class="alert">', '</span>'); ?>                                               
            </div>                                                                                                                                             
            <div class="w3-container form-group">
                <input type="checkbox" class="w3-check" name="userStat" id="suerStat" value="1" <?php echo set_checkbox('userStat', 1, false); ?> />&nbsp;Aktív
            </div>
            <div class="w3-container form-group">
                <input type="submit" class="w3-btn w3-green" name="userSave" id="userSave" value="Mentés" />&nbsp;
                <a href="<?php echo base_url(); ?>index.php/admin/users/manageusers" class="w3-btn w3-blue">A felhasználók listájához</a>
            </div>
        </form>
    </div>
    </div>
</section>