<section class="w3-row w3-mobile">
    <div class="w3-row w3-mobile">
        <article class="w3-container w3-third w3-panel w3-xxlarge w3-theme-l1 w3-card-4 w3-mobile title">Menük kezelése</article>
    </div>
    <div class="w3-row w3-margin-bottom">
    <h2>Szürő</h2>
        <input type="text" class="filter" placeholder="Cím, típus vagy státusz" size="50%" />&nbsp;        
    </div>
    <div class="w3-row w3-mobile">
        <?php 
        if(count($this->menuList) > 0) {
            echo $this->table->generate();
        }
        else { ?>
            <div class="w3-half w3-container w3-panel w3-red w3-large w3-padding-16">Jelenleg nincsenek mentett menük!</div>
        <?php } ?>
    </div>
</section>
<script src="<?php echo base_url(); ?>assets/js/admin/filter.js"></script>