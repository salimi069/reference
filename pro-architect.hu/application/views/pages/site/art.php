<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span class="baseUrl" data-page="article"><?php echo base_url(); ?></span>
<?php if(count($this->artContent) > 0) {    
    $baseUrl = base_url(); 
    echo '<section class="slider w3-center">';          
    if(!empty($this->slider)) { ?>
<div id="slider_placeholder" class="w3-center">
    <div id="slider_padd"></div>
    <ul id="slider">
        <?php foreach($this->slider as $sliderImg) { ?>
        <li class="slider_item"><img
                src="<?php echo base_url() . 'assets/images/articles/' . $this->albumRoot . '/' . $sliderImg ?>"
                alt="<?php echo $sliderImg; ?>" /></li>
        <?php } ?>
    </ul>
</div>
<?php } 
    else {        
        echo "<img class=\"blank\" src=\"{$baseUrl}/assets/images/sys/blank.png\" alt=\"Pro-architect\" />";    
    }    
    echo '</section>'; 
} ?>
<button id="contentOpen" class="w3-button w3-white w3-xlarge w3-right">Részletek</button>
<span id="content-placeholder"></span>
<?php if(!empty($this->artContent[0]->artText)) { ?>
<span id="artData" data-id="<?php echo $this->artContent[0]->artId; ?>" data-type="1"></span>
<?php } ?>