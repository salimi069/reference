<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span class="baseUrl" data-page="category"><?php echo base_url(); ?></span>
<span id="articlesData" data-menu="<?php echo $this->catContent[0]->menuId; ?>"><?php echo json_encode($this->artContent); ?></span>
<div id="loader-placeholder">
    <div id="loader-img" class="w3-display-middle">
        <img class="w3-image" src="<?php echo base_url(); ?>assets/images/sys/ajax-loader.gif" alt="Loader" />
    </div>
</div>
<section id="thumbPlaceholder" class="w3-row"></section>
<span id="content-placeholder"></span>