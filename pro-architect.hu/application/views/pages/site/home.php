<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<span class="baseUrl" data-page="home"><?php echo base_url(); ?></span>
<?php
if (!empty($this->slider)) {
    if (count($this->slider) > 0) { ?>
<section id="mp-logo">PRo-Architect</section>
<section class="slider w3-center">
    <div id="slider_placeholder" class="w3-center">
        <div id="slider_padd"></div>
        <ul id="slider">
            <?php foreach ($this->slider as $sliderImg) { ?>
            <li class="slider_item"><img 
                    src="<?php echo base_url() . 'assets/images/articles/' . $this->albumRoot . '/' . $sliderImg ?>"
                    alt="<?php echo $sliderImg; ?>" /></li>
            <?php } ?>
        </ul>
    </div>
</section>
<?php }
} else { ?>
<div id="blankImg" class="w3-row"><img src="<?php echo base_url(); ?>assets/images/sys/blank01.png"
        alt="PRo-Architect Építész Stúdió" width="100%" /></div>
<?php } ?>
<span id="content-placeholder"></span>