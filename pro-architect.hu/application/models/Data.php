<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Model {
    
    public $dbResponse = null;
        
    public function saveData($table, $data) { // Adatok mentése
        $q = $this->db->insert($table, $data);
        $this->dbResponse = $q ? 'success' : 'error'; 
    }

    public function getData($table, $values = false, $where = false, $orderBy = false, $limit = false) { // Adatok lekérdezése
        
        $dataSet = '';

        switch(true) {            
            
            case $values != false && !empty($where) && !empty($orderBy):
            $dataSet = $limit != false ? $this->db->select($values)->order_by($orderBy)->where($where)->limit($limit)->get($table)->result() : $this->db->select($values)->order_by($orderBy)->where($where)->get($table)->result();
            break;
            
            case $values != false && !empty($where):
            $dataSet = $limit != false ? $this->db->select($values)->where($where)->limit($limit)->get($table)->result() : $this->db->select($values)->where($where)->get($table)->result();            
            break;            

            case $values != false && !empty($orderBy):
            $dataSet = $limit != false ? $this->db->select($values)->order_by($orderBy)->limit($limit)->get($table)->result() : $this->db->select($values)->order_by($orderBy)->get($table)->result();
            break;
            
            case !empty($where) && !empty($orderBy):
            $dataSet = $limit != false ? $this->db->order_by($orderBy)->get_where($table, $where, $limit)->result() : $this->db->order_by($orderBy)->get_where($table, $where)->result();             
            break;
            
            case !empty($where):
            $dataSet = $limit != false ? $this->db->get_where($table, $where, $limit)->result() : $this->db->get_where($table, $where)->result();             
            break;

            case !empty($orderBy):
            $dataSet = $limit != false ? $this->db->order_by($orderBy)->get($table, $limit)->result() : $this->db->order_by($orderBy)->get($table)->result();             
            break;

            case $values != false:
            $dataSet = $limit != false ? $this->db->select($values)->limit($limit)->get($table)->result() : $this->db->select($values)->get($table)->result();
            break;                                                                  
            
            default:
            $dataSet = $this->db->get($table)->result();
            break;
        }
        return $dataSet;
    }

    public function getDataJoin($values, $tableFrom, $tableJoin, $on, $where = false, $orderBy = false, $limit = false) { // Adatok lekérdezése (INNER JOIN 2 tábla)
        $dataSet = '';

        switch(true) {

            case $where && $orderBy:
            $dataSet = $limit ? $this->db->select($values)->from($tableFrom)->join($tableJoin, $on)->where($where)->order_by($orderBy)->limit($limit)->get()->result() : $this->db->select($values)->from($tableFrom)->join($tableJoin, $on)->where($where)->order_by($orderBy)->get()->result();
            break;
            
            case $where && !$orderBy:
            $dataSet = $limit ? $this->db->select($values)->from($tableFrom)->join($tableJoin, $on)->where($where)->limit($limit)->get()->result() : $this->db->select($values)->from($tableFrom)->join($tableJoin, $on)->where($where)->get()->result();
            break;
            
            case !$where && !$orderBy:
            $dataSet = $limit ? $this->db->select($values)->from($tableFrom)->join($tableJoin, $on)->limit($limit)->get()->result() : $this->db->select($values)->from($tableFrom)->join($tableJoin, $on)->get()->result();
            break;                      
        }
        return $dataSet;
    }

    public function updateData($table, $set = [], $clause = [], $limit = false) { // Adatok frissítése
        $q = null;

        switch(true) {
            
            case $clause != false:
            $q = $limit != false ? $this->db->where($clause)->limit($limit)->update($table, $set) : $this->db->where($clause)->update($table, $set);
            break;

            default:
            $q = $this->db->update($table, $set);
            break;            
        }
        $this->dbResponse = $q ? 'success' : 'error';
    }

    public function deleteData($table, $clause = [], $limit = false) { // Adatok törlése

        switch(true) {

            case !empty($clause):
            $limit != false ? $this->db->limit($limit)->delete($table, $clause) : $this->db->delete($table, $clause);            
            $this->dbResponse = self::getData($table, false, $clause);            
            break;

            default:
            $this->db->delete($table);
            $this->dbResponse = self::getData($table, false, $clause);
            break;
        }
    }    
}