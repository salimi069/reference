<?php

final class BaseFunctions {

    /** Alias készítése */
    public static function createAlias($title) {
        $source = ['Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ö', 'ö', 'Ő', 'ő', 'Ú', 'ú', 'Ü', 'ü', 'Ű', 'ű'];
	    $target = ['a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u'];

        if(strpos($title, ' ') != false) {
            $titleParts = explode(' ', trim($title));
            $titleNoSpace = implode('_', $titleParts);
            $title = $titleNoSpace;         
        }
        $replace = str_replace($source, $target, $title);
        $alias = strtolower($replace);
        return $alias; 
    }

    /** Felhasználó adatok vizsgálata */
    public static function protectInput($input) {
        $filteredInput = strip_tags(trim($input));
        return $filteredInput;
    }
}