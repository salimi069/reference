$(document).ready(function() {

    var clickCounter = 0;
    var baseUrl = $("span.baseUrl").text();
    var page = $("span.baseUrl").data("page");
    var height, resizedFinished;

    /** Slider */

    // Slider képek dinamikus méretezése a referencia cikkek aloldalain
    imgResize(parseFloat($("div#slider_placeholder").height()));

    setInterval(function() { // Képek váltásakor        
        imgResize(parseFloat($("div#slider_placeholder").height()));
    }, 100);

    $(window).resize(function() { // Képernyő méretezés esetén        
        clearTimeout(window.resizedFinished);

        window.resizedFinished = setTimeout(function() {
            imgResize(parseFloat($("div#slider_placeholder").height()));
        }, 250);
    });

    function imgResize(size) {

        if ($("div#slider_padd").length > 0) {
            var paddHeight = $("div#slider_padd").css("padding-top");
            var winHeight = $(window).height();
            var diff = (parseInt(winHeight) - parseFloat(size)) / 2;
            var paddingSize = diff > 0 ? diff : 0;
            $("div#slider_padd").css({
                "padding-top": diff + "px",
                "padding-bottom": diff + "px",
                "transition": "padding-top 1s, padding-bottom 1s"
            });

            if (parseFloat(paddHeight) !== parseFloat(paddingSize)) {
                $("div#slider_padd").css({
                    "padding-top": parseFloat(paddingSize) + "px",
                    "padding-bottom": parseFloat(paddingSize) + "px"
                });
            }
        }
    }

    // Slider plugin konfiguráció
    $('ul#slider').lightSlider({
        item: 1,
        auto: true,
        loop: true,
        pauseOnHover: false,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 1200,
        slideMargin: 0,
        speed: 2000,
        pause: 6000,
        pager: false,
        adaptiveHeight: true,
    });

    // Slider doboz képek magasságának beállítása
    height = $(window).height();
    $("section.slider").css({ "height": height + "px", "padding-top": "10px" });

    /**Menü és tartalomdoboz kezelése */
    $("article#mp-content").hide();

    $("a.mp-link").each(function() {

        $(this).click(function() {

            var contentId = $(this).data("id");
            var linkType = $(this).data("type");

            $.ajax({
                url: baseUrl + 'index.php/home/getcontent/' + contentId + '/' + linkType,
                type: "POST",
                success: function(response) {
                    var content = JSON.parse(response);
                    var artIndex = Object.keys(content);
                    var artContentStat = false;
                    var artContent;

                    if (artIndex.length == 1) {
                        var artTitle = content[0].artTitleShow == 1 ? '<p><div class="logo-placeholder">' + content[0].artTitle + '</div></p>' : '';
                        artContent = '<div class="w3-row"><div class="w3-container w3-mobile">' + artTitle + '</div><div class="w3-container w3-mobile">' + content[0].artText + '</div></div>';
                        artContentStat = content[0].artText !== '' ? true : false;
                    } else if (artIndex.length > 1) {
                        var $menu = $('<ul id="catMenuItems"></ul>');

                        for (var i = 0; i < artIndex.length; i++) {
                            $menu.append('<li class="catLink"><a href="' + baseUrl + 'index.php/home/cat/' + content[artIndex[i]].catId + '/' + contentId + '">' + content[artIndex[i]].catTitle + '</a></li>');
                        }
                        artContent = '<div class="w3-row"><p><div class="logo-placeholder">' + content[0].menuTitle + '</div></p><div class="w3-container">' + $menu.get(0).outerHTML + '</div></div>';
                        artContentStat = true;
                    }

                    if ($("article#mp-content").length > 0) {
                        $("article#mp-content").fadeOut(500);

                        setTimeout(function() {
                            $("article#mp-content").html(artContent);

                            if (artContentStat) {
                                $("article#mp-content").fadeIn(500);
                            }

                            $(window).resize(function() {
                                $("article#mp-content").css({ "height": "100vh", "overflow": "auto" });
                            });
                        }, 1000);
                    } else {

                        if (artContentStat) {
                            $("span#content-placeholder").append('<article id="mp-content"></article>').css("height", height + "px");
                            $("article#mp-content").css("height", height + "px");
                            $("article#mp-content").html(artContent);
                            $("article#mp-content").show();

                            $(window).resize(function() {
                                $("article#mp-content").css({ "height": "100vh", "overflow": "auto" });
                            });
                        }
                    }
                }
            });

            $("span#ref").css({ "cursor": "pointer", "text-decoration": "underline" });
        });
    });

    /** Oldalmenü kezelése */
    $("button#menuOpen").click(function() {
        $("#mySidebar").css("display", "block");
    });

    $("button#menuClose").click(function() {
        $("#mySidebar").css("display", "none");
    });

    // Oldalmenü bezárása
    $(".mp-list").click(function() {
        $(".mp-sidebar").fadeOut(500);
    });

    // Tartalom doboz becsukása
    if (page === 'home') {
        $(window).click(function(e) {

            if (clickCounter > 0) {
                var target = e.target;

                if (target.getAttribute("id") !== 'mp-content') {
                    $("article#mp-content").fadeOut(500);
                }
            }
            clickCounter++;
        });
    }
});