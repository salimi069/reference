$(document).ready(function() {

    var baseUrl = $("span.baseUrl").text();
    var artId = $("span#artData").length > 0 ? $("span#artData").data("id") : '';
    var artData, artContent;

    if (artId !== '') {

        $.ajax({
            url: baseUrl + 'index.php/home/getartcontent/' + artId,
            type: "POST",
            success: function(response) {
                artData = JSON.parse(response);
                artContent = '<div class="w3-row"><div class="w3-container w3-mobile logo-placeholder">' + artData[0].artTitle + '</div><div class="w3-container w3-mobile">' + artData[0].artText + '</div></div><div class="w3-row w3-right"><button id="menuClose" class="w3-bar-item w3-button w3-large">Bezárás &times;</button></div>';
                $("span#content-placeholder").append('<article id="mp-content"></article>');
                $("article#mp-content").html(artContent);

                if ($(window).width() > 1080) {
                    $("article#mp-content").fadeIn(500);
                } else {
                    $("article#mp-content").hide();
                    $("#contentOpen").css("display", "block");
                }

                if ($("article#mp-content").length > 0) {
                    $("button#menuClose").click(function() {
                        $("article#mp-content").fadeOut(500);
                        $("#contentOpen").css("display", "block");
                    });
                }
                $("#contentOpen").click(function() {
                    $("article#mp-content").fadeIn(500);
                    $("#mySidebar").fadeOut(500);
                    $("#contentOpen").css("display", "none");
                    return false;
                });

                $("#menuOpen").click(function() {
                    $("article#mp-content").fadeOut(500);
                    $("#contentOpen").css("display", "block");
                });
            }
        });
    }
});