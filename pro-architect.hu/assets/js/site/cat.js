$(document).ready(function() {

    var baseUrl = $("span.baseUrl").text();
    $("div#loader-placeholder").show();
    var articlesData = $("span#articlesData").text() !== '' ? JSON.parse($("span#articlesData").text()) : '';
    var menu = $("span#articlesData").data("menu");
    var artNum = 0;
    var article;

    if (articlesData.length > 0) {

        for (article in articlesData) { // Képdobozok elkészítése
            var articleThumb = baseUrl + 'assets/images/articles/' + articlesData[article].artTitleAlias + '/' + articlesData[article].artThumb;
            var articleDiv = '<div class="w3-third w3-mobile thumb artThumb" data-title="' + articlesData[article].artTitle + '"><a href="' + baseUrl + 'index.php/home/art/' + articlesData[article].artId + '/' + menu + '"><img class="buildImg w3-image" src="' + articleThumb + '" alt="' + articlesData[article].artThumb + '" data-imgTitle="' + articlesData[article].artTitle + '"/></a></div>';
            $("section#thumbPlaceholder").append(articleDiv);

            // Képek magasságának beállítása
            thumbDivHeight();

            if ($("div.thumb").length > 0) {

                $("div.thumb").each(function() {
                    var title = $(this).data("title");
                    $(this).append('<div class="text">' + title + '</div>');

                    $(this).mouseenter(function() {
                        $("img.buildImg[data-imgTitle='" + title + "']").css("opacity", "0.5");
                        $(this).children("div.text").slideDown();
                    });

                    $(this).mouseleave(function() {
                        $("img.buildImg[data-imgTitle='" + title + "']").css("opacity", "1");
                        $(this).children("div.text").slideUp();
                    });
                });
            } else { // Blank háttérkép, amennyiben nincsenek bélyegképek.                    
                var uri = window.location.href;

                if (uri.indexOf('cat') > -1) {
                    $("body").css({ "background-image": "url(" + baseUrl + "assets/images/sys/blank.png)", "background-repeat": "no-repeat", "background-size": "cover" });
                }
            }

            var height = $(window).height();
            $("img.blank").css("height", height);

            $(window).resize(function() {

                if ($("span.baseUrl").data("page") === "category") {
                    thumbDivHeight();
                }
            });
            $("div.thumb").hide();
            artNum++;
        }

        // Oldal felépítése és megjelenítése (loader elrejtése)
        var pageBuild = setInterval(function() {

            if (artNum >= articlesData.length) {
                $("div#loader-placeholder").hide();
                $("div.thumb").show();
                clearInterval(pageBuild);
            } else {
                $("div#loader-placeholder").show();
                $("div.thumb").hide();
            }
        }, 2000);
    }

    function thumbDivHeight() {
        var cellHeight = [];

        setTimeout(function() {
            cellHeight.push($("div.thumb").height());
            var min = Math.min(...cellHeight);
            $("div.thumb").css("height", min + "px");
            $("img.buildImg").css("height", "100%");
            $("section.slider").css("overflow", "auto");
        }, 1500);
    }
});