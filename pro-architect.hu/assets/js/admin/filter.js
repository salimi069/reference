$(document).ready(function() {

    $("input.filter").keyup(function() {
        var str = $(this).val().toLowerCase();

        $("#table tr").filter(function() {

            if ($(this).children("td").text().toLowerCase().indexOf(str) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });
});