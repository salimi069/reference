$(document).ready(function() {

    /** Elem magasságok beállítása */
    var height = $(window).height();
    $("section#wrapper").css("height", height + "px"); // wrapper

    var mainHeight = $("main").height();
    var headerHeight = $("header").height();
    var footerHeight = $("footer").height();
    mainHeight + +headerHeight + +footerHeight < height ? $("footer").css({ "position": "fixed", "bottom": 0 }) : ''; // footer pozíció    

    /** Vízszintes reszponzív menü kezelése */
    $("a.hamb-icon").click(function() {

        if ($("div#menu-collapse").hasClass("w3-show")) {
            $("div#menu-collapse").removeClass("w3-show");
        } else {
            $("div#menu-collapse").addClass("w3-show");
        }
    });

    /** Oldalmenü kezelése */
    $("button.side-open").click(function() {
        $("aside#leftMenu").css("display", "block");
    });

    $("button.side-close").click(function() {
        $("aside#leftMenu").css("display", "none");
    });
});