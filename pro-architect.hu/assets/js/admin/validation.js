$(document).ready(function() {

    $(".send").click(function(e) {
        e.preventDefault();
        var validStatus = [];

        $(".valid").each(function() {

            switch (true) {

                case $(this).is("input[type='text']"): // Input (text)

                    if ($(this).val() === '') {
                        $(this).siblings("span.alert").text("Kérem, töltse ki a mezőt!");
                        validStatus.push($(this).attr("id"));
                    } else {
                        $(this).siblings("span.alert").text("");
                        removeItem($(this).attr("id"), validStatus);
                    }
                    break;

                case $(this).is("input[type='email']"): // Input (e-mail)

                    if ($(this).val() !== '') {
                        $(this).siblings("span.alert").text("");
                        removeItem($(this).attr("id"), validStatus);
                        var rem = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        var checkEmail = rem.test($(this).val());

                        if (checkEmail == false) {
                            $(this).siblings("span.alert").text("Kérem, adjon meg egy érvényes e-mail címet!");
                            validStatus.push($(this).attr("id"));
                        } else {
                            $(this).siblings("span.alert").text("");
                            removeItem($(this).attr("id"), validStatus);
                        }
                    } else {
                        $(this).siblings("span.alert").text("Kérem, adjon meg egy érvényes e-mail címet!");
                        validStatus.push($(this).attr("id"));
                    }
                    break;

                case $(this).is("input[type='password']"): // Input (password)

                    if ($(this).val() === '') {
                        $(this).siblings("span.alert").text("Kérem, töltse ki a mezőt!");
                        validStatus.push($(this).attr("id"));
                    } else {

                        if ($(this).val().length >= 6) { // Jelszó minimális hossza: 6 karakter
                            var passwords = [];
                            $(this).siblings("span.alert").text("");
                            removeItem($(this).attr("id"), validStatus);

                            $("div.pass").each(function() { // Jelszavak azonosságának ellenőrzése
                                var pass = $(this).find("input[type='password']").val();
                                passwords.push(pass);
                            });

                            if (passwords[0] !== passwords[1]) {
                                $(this).siblings("span.pass-check").text("A két megadott jelszó nem egyezik meg egymással. Kérem, javítsa!");
                                validStatus.push($(this).attr("id"));
                                passwords = [];
                                return;
                            } else {
                                $(this).siblings("span.pass-check").text("");
                                removeItem($(this).attr("id"), validStatus);
                            }
                        } else {
                            $(this).siblings("span.pass-check").text("");
                            $(this).siblings("span.alert").text("Kérem, adjon meg egy minimum 6 karakterből álló jelszót!");
                            validStatus.push($(this).attr("id"));
                        }
                    }
                    break;

                case $(this).is("select"): // Input (select)

                    if (($(this).val() === '') || ($(this).children(':selected').attr("disabled"))) {

                        if (($(this).attr("id") === 'artPos') && ($("select#artParent").val() === '')) {
                            $(this).siblings("span.alert").text("Kérem, válassza ki a menü pozicíóját vagy szülőmenüjét!");
                            validStatus.push($(this).attr("id"));
                        } else if (($(this).attr("id") === 'artParent') && ($("select#artPos").val() === '')) {
                            $(this).siblings("span.alert").text("Kérem, válassza ki a menü pozicíóját vagy szülőmenüjét!");
                            validStatus.push($(this).attr("id"));
                        } else {
                            $(this).siblings("span.alert").text("Kérem, válasszon egy lehetőséget!");
                            validStatus.push($(this).attr("id"));
                        }
                    } else {
                        $(this).siblings("span.alert").text("");
                        removeItem($(this).attr("id"), validStatus);
                    }
                    break;

                case $(this).is("textarea.ckeditor"): // CKEditor textarea

                    if (!CKEDITOR.instances['artText'].getData().length) {
                        $(this).siblings("span.alert").text("Kérem, töltse ki a szövegmezőt!");
                        validStatus.push($(this).attr("id"));
                    } else {
                        $(this).siblings("span.alert").text("");
                        removeItem($(this).attr("id"), validStatus);
                    }
                    break;

                case $(this).is("textarea"): // Textarea

                    if ($(this).val() === '') {
                        $(this).siblings("span.alert").text("Kérem, töltse ki a szövegmezőt!");
                        validStatus.push($(this).attr("id"));
                    } else {
                        $(this).siblings("span.alert").text("");
                        removeItem($(this).attr("id"), validStatus);
                    }
                    break;

                default:
                    validStatus = [];
                    break;
            }
        });

        validStatus.length > 0 ? $("span#validationStatus").text("Error") : $("span#validationStatus").text("");

        function removeItem(id, array) {
            var index = array.indexOf(id);
            index > -1 ? array.splice(index, 1) : '';
            return array;
        }
    });
});