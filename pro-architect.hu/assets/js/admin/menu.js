$(document).ready(function() {

    var baseUrl = $("span.baseUrl").text();

    /** Menü törlése */
    $("input#menuDelete").click(function(e) {
        e.preventDefault();

        if (confirm("Biztosan törölni akarja a menüt?")) {
            var id = $("input#menuDelete").data("id");

            $.ajax({
                url: baseUrl + 'index.php/admin/menu/deleteMenu/' + id,
                type: "POST",
                success: function(response) {

                    if (response === 'success') {
                        alert('Sikeres törlés!');
                        window.location.href = baseUrl + 'index.php/admin/menu/managemenu';
                    } else {
                        alert(response);
                        return false;
                    }
                }
            });
        }
    });
});