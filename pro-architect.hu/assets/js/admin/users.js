$(document).ready(function() {

    var baseUrl = $("span.baseUrl").text();

    $("input.pass").each(function() {

        $(this).blur(function() {

            if ($(this).val() !== '') {
                $("label#passLabel").children('span.asterisk').length == 0 ? $("label#passLabel").append('<span class="asterisk">*</span>') : ''
            } else {
                $("label#passLabel").children('span.asterisk').remove();
            }
        });
    });

    $("select#userAccess").change(function() {
        var accessLevel = $(this).val();

        if (accessLevel == 1) {
            $("input#userStat").attr("disabled", true).prop("checked", true);
        } else {
            $("input#userStat").attr("disabled", false);
        }
    });

    /** Felhasználó törlése */
    $("input#userDelete").click(function(e) {
        e.preventDefault();

        if (confirm("Biztosan törölni akarja a felhasználót?")) {
            var userId = $(this).data("id");

            $.ajax({
                url: baseUrl + 'index.php/admin/users/deleteuser/' + userId,
                type: "POST",
                success: function(response) {

                    if (response === 'success') {
                        alert('Sikeres törlés!');
                        window.location.href = baseUrl + 'index.php/admin/users/manageusers';
                    } else {
                        alert(response);
                        return false;
                    }
                }
            });
        }
    });
});