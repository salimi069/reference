$(document).ready(function() {

    var baseUrl = $("span.baseUrl").text();
    var menu;

    /** Az azonos menühöz tartozó kategóriák lekérdezése */
    if ($("select#catPos").data("pos") === "") {

        $("select#catMenu").change(function() {
            $("option.pos").remove();
            menu = $(this).val();
            selectPosition(menu);
        });
    } else {
        $("option.pos").remove();
        menu = $("select#catMenu").val();
        var pos = $("select#catPos").data("pos");
        selectPosition(menu, pos);

        $("select#catMenu").change(function() {
            menu = $("select#catMenu").val();

            if (parseInt(menu) !== $("select#catPos").data("menu")) {
                $("option.pos").remove();
                selectPosition(menu, pos, 1);
            } else {
                $("option.pos").remove();
                selectPosition(menu, pos);
            }
        });
    }

    // Adatok kezelése
    $(".send").click(function() {
        var validStatus = $("span#validationStatus").text();
        var action = $(this).data("action");
        var targetUrl, id, confMsg, failMsg;

        switch (action) {

            case "save":
                targetUrl = 'savecategory';
                confMsg = 'Sikeres mentés!';
                break;

            case "update":
                id = $(this).data("id");
                targetUrl = "saveupdate/" + id;
                confMsg = "A módosítás sikerült!";
                break;

            case "delete":
                if (confirm('Biztosan törölni akarja a kategóriát?')) {
                    id = $(this).data("id");
                    targetUrl = "deletecategory/" + id;
                    confMsg = "A törlése sikerült!";
                }
                break;
        }

        if (validStatus === '') {

            var formData = {
                catTitle: $("input#catTitle").val(),
                catMenu: $("select#catMenu").val(),
                catPos: $("select#catPos").val(),
                catStat: $("input#catStat").prop("checked") ? 1 : ''
            }

            var data = JSON.stringify(formData);

            $.ajax({
                url: baseUrl + 'index.php/admin/content/' + targetUrl,
                type: "POST",
                data: { form: data },
                dataType: "html",
                success: function(response) {

                    if (response === 'success') {
                        alert(confMsg);
                        window.location.href = baseUrl + 'index.php/admin/content/managecategory';
                    } else {
                        alert(response);
                        return false;
                    }
                }
            });
        }
    });

    /** Pozíció kiválasztása */
    function selectPosition(menu, pos = false, newPos = false) {

        $.ajax({
            url: baseUrl + 'index.php/admin/content/getcatlistinmenu/' + menu,
            type: "POST",
            success: function(response) {

                if (response !== 'fail') {
                    var cats = JSON.parse(response);
                    var catKeys = Object.keys(cats);
                    var catNum = catKeys.length + +1;
                    var selected;

                    for (var i = 0; i < catKeys.length; i++) {
                        selected = parseInt(cats[catKeys[i]].catPos) === pos ? 'selected' : '';
                        $("select#catPos").append('<option class="pos" value="' + cats[catKeys[i]].catPos + '" ' + selected + '>' + cats[catKeys[i]].catPos + ' - ' + cats[catKeys[i]].catTitle + '</option>');
                    }
                    if (pos == false || newPos != false) {
                        $("select#catPos").append('<option class="pos" value="' + catNum + '">' + catNum + '</option>');
                    }
                } else {
                    $("select#catPos").append('<option class="pos" value="1" selected>1</option>');
                }
            }
        });
    }
});