$(document).ready(function() {

    var baseUrl = $("span.baseUrl").text();
    var targetUrl, confMsg;

    /**
     * MENTÉS, FRISSÍTÉS, TÖRLÉS
     */

    // Cím másolása
    $("input#titleCopy").change(function() {

        if ($(this).prop("checked")) {

            if ($("input#artTitle").val() !== '') {
                $("input#artMenuTitle").val($("input#artTitle").val());
            } else {
                alert("Előbb kérem, adjon a cikknek egy címet!");
                $(this).prop("checked", false);
                return false;
            }
        } else {
            $("input#artMenuTitle").val("");
        }
    });

    $(".send").click(function() {
        var validStatus = $("span#validationStatus").text();
        var catName = $("span.catName").text();
        var artStat = $("span.artStat").text();
        var redirect;

        if (validStatus === "") {
            var action = $(this).data("action");
            var artId = $(this).data("art_id");

            switch (action) {

                case "save":
                    targetUrl = 'savearticle';
                    confMsg = 'Sikeres mentés!';
                    break;

                case "update":
                    targetUrl = 'savearticleupdate/' + artId;
                    confMsg = 'Sikeres módosítás!';
                    break;

                case "delete":
                    if (confirm("Biztosan törölni akarja a cikket?")) {
                        targetUrl = 'deletearticle/' + artId;
                        confMsg = 'Sikeres törlés!';
                    }
                    break;
            }

            var data = new FormData($("form")[0]);
            var text = CKEDITOR.instances.artText.getData();
            text !== '' ? data.append('artText', text) : data.append('artText', '');

            $.ajax({
                url: baseUrl + 'index.php/admin/content/' + targetUrl,
                type: "POST",
                enctype: "multipart/formdata",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(response) {

                    if (response.indexOf('_') > -1) {
                        var resp = response.split('_');

                        if (resp[0] === 'success') {
                            alert(confMsg);
                            window.location.href = baseUrl + 'index.php/admin/content/updatearticle/' + resp[1];
                        } else {
                            alert(response);
                            return false;
                        }
                    } else {
                        if (response === 'success') {
                            alert(confMsg);

                            switch (true) {

                                case catName !== '' && artStat !== '':
                                    redirect = 'managearticle/' + catName + '/' + artStat;
                                    break;

                                case catName !== '' && artStat === '':
                                    redirect = 'managearticle/' + catName + '/select';
                                    break;

                                case catName === '' && artStat !== '':
                                    redirect = 'managearticle/select/' + artStat;
                                    break;

                                default:
                                    redirect = 'managearticle';
                                    break;
                            }

                            window.location.href = baseUrl + 'index.php/admin/content/' + redirect;
                        } else {
                            alert(response);
                            return false;
                        }
                    }
                }
            });
        } else {
            var alertMsg;
            $("span.alert").each(function() {

                if ($(this).text() !== '') {
                    inputId = $(this).attr("id");

                    switch (inputId) {

                        case "artTitle_error":
                            alertMsg = 'A cikk címe nem került megadásra.';
                            break;

                        case "artMenuTitle_error":
                            alertMsg = 'A menüpont neve nem került megadásra.'
                            break;
                    }
                }
            });
            alert(alertMsg + " Kérem, ellenőrizze!");
            return false;
        }
    });

    /** 
     * KATEGÓRIA ÉS MENÜLISTÁK 
     * */

    // Menüpozíció megjelenítése frissítéskor
    var artCategory = $("select#artCat").val() !== '' ? $("select#artCat").val() : '';
    selectMenuPosition(artCategory, update = true);

    $("select#artCat").length == 0 ? $("select#artParentMenu").attr("disabled", false) : '';

    $("select#artCat").change(function() { // Kategória lista és menüpozíció megjelenítése     
        var selectedCat = $(this).val();
        selectMenuPosition(selectedCat);
    });

    $("input#artMenuTitle").blur(function() { // Menüpont neve 

        if ($(this).val() !== "") {
            $("select#artParentMenu").attr("disabled", true);
            $("select#artParentMenu").find("option:selected").prop("selected", false);
            $("select#artCat, select#artMenuPos").addClass("valid");
            $("label#artCatLabel, label#menuPosLabel").children("span.asterisk").length == 0 ? $("label#artCatLabel, label#menuPosLabel").append('<span class="asterisk">*</span>') : '';
        } else {
            $("select#artParentMenu").attr("disabled", false);
            $("label#menuTitleLabel, label#artCatLabel, label#menuPosLabel").children("span.asterisk").remove();
            $("select#artCat, select#artMenuPos").removeClass("valid");
            $("select#artCat, select#artMenuPos").siblings('span.alert').html("");
            $("select#artMenuPos, select#artCat").find("option:selected").prop("selected", false);
        }
    });

    $("select#artMenuPos").change(function() { // Menüpozíció

        if ($(this).val() !== '') {
            $("select#artParentMenu").attr("disabled", true);
            $("select#artParentMenu").find("option:selected").prop("selected", false);
            $("select#artCat, input#artMenuTitle").addClass("valid");
            $("label#artCatLabel, label#menuPosLabel, label#menuTitleLabel").children("span.asterisk").length == 0 ? $("label#artCatLabel, label#menuPosLabel, label#menuTitleLabel").append('<span class="asterisk">*</span>') : '';
        } else {
            $("select#artParentMenu").attr("disabled", false);
            $(this).removeClass("valid").siblings("span.alert").html("");
            $("label#artCatLabel, label#menuTitleLabel, label#menuPosLabel").children("span.asterisk").remove();
            $("input#artMenuTitle").removeClass("valid").val("").siblings('span.alert').html("");
            $("select#artCat").removeClass("valid").siblings('span.alert').html("");
            $("select#artCat").find("option:selected").prop("selected", false);
        }
    });

    $("select#artParentMenu").change(function() { // Szülőmenü

        if ($(this).val() != 0) {
            $("select#artCat, input#artMenuTitle, select#artMenuPos, input#artStat").attr("disabled", true);
            $("input#artStat").prop("checked", true);
            $("select#artCat option, select#artMenuPos").find("option:selected").prop("selected", false);
            $("input#artMenuTitle, select#artMenuPos").removeClass("valid");
            $("input#artMenuTitle").val("");
            $("label#menuTitleLabel, label#menuPosLabel").children('span.asterisk').remove();
            $("select#artMenuPos, select#artCat, input#artMenuTitle").siblings('span.alert').html("");
        } else {
            $("select#artCat, input#artMenuTitle, select#artMenuPos, input#artStat").attr("disabled", false);
        }
    });

    function selectMenuPosition(catId, update = false) { // Menüpozíciók megjelenítése        

        if (catId !== "" && catId !== undefined) {
            $("select#artMenuPos option").remove();
            $("select#artParentMenu").attr("disabled", true);
            $("select#artParentMenu").find("option:selected").prop("selected", false);
            $("input#artMenuTitle, select#artMenuPos").addClass("valid");
            $("label#menuTitleLabel, label#menuPosLabel").children("span.asterisk").length == 0 ? $("label#menuTitleLabel, label#menuPosLabel").append('<span class="asterisk">*</span>') : '';
            var artTitle = $("input#artTitle").val();

            // Menüpozíciók megjelenítése és kezelése
            $.ajax({
                url: baseUrl + 'index.php/admin/content/getmenuposlist/' + catId,
                type: "POST",
                success: function(response) {

                    if (response !== '') {
                        var menuPosList = JSON.parse(response);
                        var menuItems = Object.keys(menuPosList);
                        var menuNum = menuItems.length;
                        var nextMenuItem = ++menuNum;

                        for (var mi = 0; mi < menuItems.length; mi++) {
                            $("select#artMenuPos").append('<option class="menuPos" value="' + menuPosList[menuItems[mi]].artMenuPos + '">' + menuPosList[menuItems[mi]].artMenuPos + ' - ' + menuPosList[menuItems[mi]].artMenuTitle + '</option>');

                            if (update && menuPosList[menuItems[mi]].artTitle === artTitle) {
                                $("select#artMenuPos option[value='" + menuPosList[menuItems[mi]].artMenuPos + "']").attr("selected", true);
                            }
                        }!update ? $("select#artMenuPos").append('<option value="' + nextMenuItem + '">' + nextMenuItem + ' - </option>') : '';
                    } else {
                        $("select#artMenuPos").append('<option value="1" selected>1 - </option>');
                    }
                }
            });
        } else if (catId === '' || catId === undefined) {
            $("select#artMenuPos option").remove();
            $("select#artMenuPos").append('<option value="" selected>Kérem, válasszon</option>');
            $("select#artParentMenu").attr("disabled", false);
            $(this).removeClass("valid").siblings("span.alert").html("");
            $("label#artCatLabel, label#menuTitleLabel, label#menuPosLabel").children("span.asterisk").remove();
            $("input#artMenuTitle").removeClass("valid").val("").siblings('span.alert').html("");
            $("select#artMenuPos").removeClass("valid").siblings('span.alert').html("");
            $("select#artMenuPos").find("option:selected").prop("selected", false);
            update == false ? $("select#artCat, input#artMenuTitle, select#artMenuPos").attr("disabled", true) : '';
        }
    }

    /** 
     * FELTÖLTENDŐ KÉPEK MEGJELENÍTÉSE
     */
    $(function() {
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).addClass('w3-image').appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('input.artImg').on('change', function() {
            imagesPreview(this, 'div.galery');
        });
    });

    // Főoldal státusz választásának aktiválása/deaktiválása
    $("input#homePage").change(function() {
        !$(this).prop("checked") ? $("input#artStat").attr("disabled", false) : $("input#artStat").attr("disabled", true);
    });

    /** 
     * KÉPEK MEGJELENÍTÉSE ÉS KEZELÉSE MODALBAN (FRISSÍTÉS)
     */
    $("img.imgUpdate").each(function() {

        $(this).click(function() {
            var imgPath = $(this).attr("src");
            var imgFile = $(this).attr("alt");
            var thumb = $(this).data("thumb");
            var slider = parseInt($(this).data("slider"));
            var artId = $(this).data("art_id");

            $("div.albImg").html('<p><strong>' + imgFile + '</strong></p><img src="' + imgPath + '" class="w3-image" alt="' + imgFile + '" />');
            $("div.imgData").html('<div class="w3-row"><input type="text" class="imgUrl modal-group" name="imgUrl" value="' + imgPath + '" size="80%" readonly /></div>' +
                '<div class="w3-row"><button class="artImg w3-btn w3-orange modal-group">Hivatkozás másolása</button><br /></div>' +
                '<div class="w3-row"><input type="checkbox" class="w3-check modal-group check" name="thumb" />&nbsp;Bélyegkép<br />' +
                '<input type="checkbox" class="w3-check modal-group check" name="slider" data-file="' + imgFile + '" />&nbsp;<span id="sliderImgLabel">Háttérkép</span></div>' +
                '<div class="w3-row"><button data-img_btn="' + imgFile + '" class="updateImg modalBtn w3-btn w3-green" data-action="update">Módosítás</button>&nbsp;' +
                '<button data-img_btn="' + imgFile + '" class="delImage modalBtn w3-btn w3-red" data-action="delete">Törlés</button></div>');

            $("button.updateImg").attr("disabled", true);

            // Kép hivatkozásának másolása
            $("button.artImg").click(function() {
                $("input.imgUrl").select();
                document.execCommand("copy");
            });

            // Bélyegkép
            thumb !== "" && thumb === imgFile ? $("input[name='thumb']").prop("checked", true) : $("input[name='thumb']").prop("checked", false);

            // Slider
            sliderImgSelect();

            $("input#homePage").change(function() {
                sliderImgSelect();
            });

            function sliderImgSelect() { // Csak a főoldal esetében lehessen háttérképet (slider) választani.
                if ($("input#homePage").prop("checked")) {
                    $("input[name='slider'], span#sliderImgLabel").show();
                    slider !== "" && slider == 1 ? $("input[name='slider']").prop("checked", true) : $("input[name='slider']").prop("checked", false);
                } else {
                    $("input[name='slider'], span#sliderImgLabel").hide();
                }
            }

            $("div#imgModal").css("display", "block");

            $("input.check").change(function() {
                $("button.updateImg").attr("disabled", false);

                if ($(this).attr("name") == "thumb") {
                    $(this).attr("name", "thumb").data("change", 1);
                }
            });

            // Bélyegkép megadása, módosítása és törlése
            $("button.modalBtn").click(function() {
                var action = $(this).data("action");
                var img = $(this).data("img_btn");
                var thumb = [];
                var thumbImg;

                if ($("input[name='thumb']").prop("checked")) { // Bélyegkép meghatározása
                    thumbImg = img;
                } else {

                    $("img.imgUpdate").each(function() {
                        $(this).data("thumb") !== '' ? thumb.push($(this).attr("alt")) : '';
                    });

                    if (thumb.length > 0) {
                        thumbImg = thumb[0] === img ? '' : thumb[0];
                    } else {
                        thumbImg = '';
                    }
                }

                var sliderImg = $("input[name='slider']").prop("checked") ? img : false;

                switch (action) {

                    case "update":

                        if (confirm("Biztosan módosítani akarja a képet?")) {
                            targetUrl = "managearticlethumbnail/" + artId + "/" + action;
                            confMsg = "Sikeres módosítás!";
                        }
                        break;

                    case "delete":
                        if (confirm("Biztosan törölni akarja a képet?")) {
                            targetUrl = "managearticlethumbnail/" + artId + "/" + action + "/" + imgFile;
                            confMsg = "Sikeres törlés!";
                        }
                        break;
                }

                $.ajax({
                    url: baseUrl + 'index.php/admin/content/' + targetUrl,
                    type: "POST",
                    data: { img: img, thumb: thumbImg, slider: sliderImg },
                    dataType: "html",
                    success: function(response) {

                        if (response === 'success') {
                            alert(confMsg);
                            $("div#imgModal").css("display", "none");

                            if ($("input[name='thumb']").data("change") == 1) {

                                $("img.imgUpdate").each(function() {

                                    if ($(this).hasClass("thumbMark") && $(this).attr("alt") !== img) {
                                        $(this).removeClass("thumbMark");
                                    } else if (!$(this).hasClass("thumbMark") && $(this).attr("alt") === img) {
                                        $(this).addClass("thumbMark");
                                    } else if (!$("input[name='thumb']").prop("checked")) {
                                        $(this).removeClass("thumbMark");
                                    }
                                });
                            }
                        } else {
                            alert(response);
                            return false;
                        }

                        if (action === "update") {

                            $("img.imgUpdate").each(function() {
                                $(this).attr("alt") === thumbImg ? $(this).data("thumb", thumbImg) : $(this).data("thumb", "");

                                if (sliderImg !== false) {
                                    $(this).attr("alt") === sliderImg ? $(this).data("slider", 1) : '';
                                } else {
                                    $(this).attr("alt") === img ? $(this).data("slider", 0) : '';
                                }
                            });
                        } else if (action === "delete") {
                            $("img.imgUpdate").each(function() {
                                $("div#imgModal").css("display", "none");
                                $(this).attr("alt") === imgFile ? $(this).remove() : '';
                            });
                        }
                    }
                });
            });
        });
    });
    $("span.closeModal").click(function() {
        $("div#imgModal").css("display", "none");
    });

    /**
     * ÖSSZES KÉP TÖRLÉSE
     */
    $("button#delAllImg").click(function(e) {
        e.preventDefault();
        var artId = $(this).data("art_id");

        if (confirm('Biztosan törölni akarja a képeket?')) {

            $.ajax({
                url: baseUrl + 'index.php/admin/content/deleteallimage/' + artId,
                type: "POST",
                success: function(response) {

                    if (response === 'success') {
                        alert('A képek törlése sikerült!');
                        $("img.imgUpdate").remove();
                        location.reload();
                    } else {
                        alert(response);
                    }
                }
            });
        }
    });
});