$(document).ready(function() {

    /** Kinyíló tartalmak kezelése */

    setTimeout(function() {

        $("img.toggle").click(function() {
            var content = $(this).data("toggle");
            $("article#" + content + "-content").slideToggle("slow");
            $(this).attr("src") === 'images/open.png' ? $(this).attr({ "src": 'images/close.png', "alt": "Zár" }) : $(this).attr({ "src": 'images/open.png', "alt": "Nyit" });
        });
    }, 1000);
});