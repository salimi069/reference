$(document).ready(function() {

    /** Napi tipp és gyorskereső funkció */

    // Napi adatok (dátum, hőmérséklet)
    $.ajax({
        url: 'php/dailyData.php',
        success: function(response) {

            if (response !== 'error') {
                var dailyData = JSON.parse(response);
                var dateSplit = dailyData.date.split('-');
                var month = dateSplit[1];
                var tempNumber = parseInt(dailyData.temp.replace('°C', ''));
                var tyreType;

                switch (true) {

                    case month === '01':
                    case month === '02':
                    case month === '03' && tempNumber < 7:
                    case month === '04' && tempNumber < 7:
                    case month === '05' && tempNumber < 7:
                    case month === '10' && tempNumber < 7:
                    case month === '11':
                    case month === '12':
                        tyreType = "TÉLI";
                        break;

                    case month === '03' && tempNumber >= 7:
                    case month === '04' && tempNumber >= 7:
                    case month === '05' && tempNumber >= 7:
                    case month === '06':
                    case month === '07':
                    case month === '08':
                    case month === '09':
                    case month === '10' && tempNumber >= 7:
                        tyreType = "NYÁRI";
                        break;
                }

                if (dailyData.temp !== '') {
                    $("span#dailyData").html('<section id="dailyHint" class="w3-row-padding"><header class="w3-panel w3-grey w3-padding w3-xxlarge w3-card-4 w3-round title">Napi Tipp&nbsp;<img id="hint-toggle" src="images/open.png" class="w3-right toggle" data-toggle="hint" alt="Nyit" /></header>' +
                        '<article id="hint-content"><strong>Mai dátum:</strong> ' + dailyData.date + '<br />' +
                        '<strong>Jelenlegi hőmérséklet: </strong>' + dailyData.temp + ' (Budapest)*<br />' +
                        '<p id="tyre-rec" class="w3-panel w3-grey w3-padding w3-xlarge w3-round w3-card-4">Javasolt gumiabroncs fajta: ' + tyreType + '</p>' +
                        '<p id="tyre-enq"><a href="#price-link" class="w3-btn w3-red w3-round">Abroncsot szeretnék cserélni<br />Megkeresem az árat.</a></p>' +
                        '<p id="idokep-link"><small>* A hőmérsékleti adatot köszönjük az <a href="https://www.idokep.hu/idojaras/BudapestIdőkép.hu" target="_blank">Időképnek.</a></small></p></article>' +
                        '</section><hr>');
                } else {
                    $("section#filter-panel").css("margin-top", "20px");
                }
            }
        }
    });

    // Gyorskereső
    var details, service;

    $("select#filter").change(function() {
        service = $(this).val();
        $("span.alert").text("");
        $("input#send-result").val("");
        $("input#email-consent").prop("checked", false);

        if (service === "tyre") { // Felni fajták és méretek megjelenítése
            $("section#filter-result").css("display", "none");
            $("section#tyre-filters").css("display", "block");
        } else {

            if ($("select#tyre-type-select").val() !== "select") {
                $("select#tyre-type-select").val("select");
                $("select#tyre-size-select").val("select").attr("disabled", true);
            }
            $("section#tyre-filters").css("display", "none");
        }
        $("option.tyre-size").remove(); // Felesleges méret opciók eltávolítása

        $("select#tyre-type-select").change(function() { // Méret lista aktiválása
            $(this).val() !== "select" ? $("select#tyre-size-select").attr("disabled", false) : $("select#tyre-size-select").attr("disabled", true);
        });

        $.getJSON('js/servicePrices.json', function(data) { // Árak lekérdezése

            var price;

            for (var s = 0; s < data.size.length; s++) { // Méretek megadása a legördülő listában
                $("select#tyre-size-select").append('<option class="tyre-size" value="' + data.size[s] + '">' + data.size[s] + '"' + '</option>');
            }

            switch (service) { // Keresési eredmények megjelenítése

                case "tyre": // Gumijavítás és szerelés 

                    $("select#tyre-size-select").change(function() {
                        var type = $("select#tyre-type-select").val();
                        var size = $(this).val();
                        price = data.tyre[type].repair[size];
                        var season = data.tyre[type].season[size];
                        var centr = data.tyre[type].balance[size];
                        type = data.tyre[type]["name"] + "s";
                        typeName = type.toLowerCase();
                        details = '<p>GUMIJAVÍTÁS ÉS SZERELÉS:</p>' +
                            '<p>' + size + '" méretű ' + typeName + ' keréken: <p>' +
                            '<ul>' +
                            '<li>' + data.services.repair + ': <strong>' + price + ' Ft</strong></li>' +
                            '<li>' + data.services.season + ': <strong>' + season + ' Ft</strong></li>' +
                            '<li>' + data.services.balance + ': <strong>' + centr + ' Ft</strong></li>' +
                            '</ul>';
                        displayResult(details);
                    });
                    break;

                case "oil": // Olajcsere
                    price = data.oil;
                    details = '<p>OLAJCSERE:</p>Munkadíj, olaj- és levegőszűrők, motorolaj: <strong>' + price + ' Ft-tól.</strong>';
                    displayResult(details);
                    break;

                case "headlight": // Műszeres fényszóró beállítás
                    price = data.headlight;
                    details = 'Műszeres fényszóró beállítás: <strong>' + price + ' Ft.</strong>';
                    displayResult(details);
                    break;

                case "wheel": // Műszeres futómű beállítás
                    price = data.wheel;
                    details = 'Műszeres futómű beállítás: <strong>' + price + ' Ft.</strong>';
                    displayResult(details);
                    break;
            }

            function displayResult(result) {
                $("article#result").html(result);
                $("section#filter-result").css("display", "block");
            }
        });

        $.ajax({
            url: "../php/log.php",
            type: "POST",
            data: { action: "keresés", service: service },
            dataType: "html"
        });
    });

    $("button#send-result-btn").click(function() { // Ajánlat küldése                

        if ($("input#email-consent").prop("checked")) { // Tájékoztató elfogadásának ellenőrzése

            if ($("input#send-result").val() !== "") {
                // Felhasználó e-mail címének validálása 
                var email = $("input#send-result").val();
                var rem = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var checkEmail = rem.test(email);

                if (checkEmail == false) {
                    $("input#send-result").siblings("span.alert").text("Kérem, adjon meg egy érvényes e-mail címet!");
                } else {
                    $("input#send-result").siblings("span.alert").text("");

                    $.ajax({ // Küldés
                        url: "../php/mail.php",
                        type: "POST",
                        data: { email: email, msg: details, service: service },
                        dataType: "html",
                        success: function(response) {
                            alert(response);
                            deleteSearch();
                        }
                    });
                }
            } else {
                $("input#send-result").siblings("span.alert").text("Kérem, adjon meg egy érvényes e-mail címet!");
            }
        } else {
            $("input#send-result").siblings("span.alert").text("Kérem, fogadja el a *-al jelölt tájékoztatást!");
        }
    });

    $("button#new-search").click(function() { // Új keresés indítása

        if (confirm("Biztosan törli a jelenlegi keresési eredményt?")) {
            deleteSearch();
        }
    });

    function deleteSearch() {
        $("select#filter, select#tyre-type-select, select#tyre-size-select").val("select");
        $("input#send-result").val("");
        $("input#email-consent").prop("checked", false);
        $("section#filter-result, section#tyre-filters").css("display", "none");
    }
});