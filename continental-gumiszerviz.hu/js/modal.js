$(document).ready(function() {

    /** Modal kezelés */

    setTimeout(function() {

        $(".category").click(function() { // Szolgáltatások megjelenítése
            var text;
            var category = $(this).is("img") ? $(this).attr("alt") : $(this).data("category");

            $.getJSON('js/servicePrices.json', function(data) {

                switch (category) {

                    case "Gumiszerelés":
                        text = '<div class="w3-row"><img src="images/gumi_modal.png" class="w3-image" alt="Gumiszerelés és javítás" /></div><div class="w3-row"><p>A <strong>jármű abroncsainak</strong> gyakran kevesebb jelentősséget tulajdonítunk, mint az első pillantásra lényegesebbnek tűnő motorteljesítménynek, biztonsági berendezéseknek és külsőségeknek.</p>' +
                            '<p>Az viszont tagadhatatlan tény, hogy az ember és úttest közötti tényleges kapcsolatot az abroncsok biztosítják, vagyis sokszor azon a <strong>négy tenyérnyi felületen múlik az életünk</strong>. A jármű és úttest között működő erők is elsősorban az abroncsokat terhelik, vagyis a <strong>gyorsítás és a fékezés, vagy a kanyarvétel</strong> mind a gumiabroncsokat veszik leginkább igénybe.' +
                            ' S, minél nagyobb a jármű sebessége, annál nagyobb a gumiabroncsokra háruló terhelés... </p>' +
                            '<strong>Tudta Ön, hogy</strong>' +
                            '<ul>' +
                            '<li>az <strong>alacsony légnyomás</strong> az abroncsban a gépjármű feletti kontroll elvesztését okozhatja,</li>' +
                            '<li><strong>nedves időben a kopott gumiabroncsok</strong> nem képesek elvezetni az út és az abroncs között felgyülemlő vízréteget, így <strong>megszakad a közvetlen érintkezés az úttal</strong>, az autó pedig irányíthatatlanná válik,</li>' +
                            '<li>már az ideálistól való <strong>fél báros levegőnyomás eltérés is akár 5%-os fogyasztásnövekedést okozhat</strong>, emellett <strong>csökkenti a gumiabroncsok élettartamát és a fékezés biztonságát</strong>,</li>' +
                            '<li>a <strong>nitrogénnel</strong> feltöltött abroncs <strong>kevésbé melegszik fel</strong>, az abroncsban uralkodó nyomás pedig állandó ezáltal <strong>nagyobb biztonságot</strong> nyújt Önnek és gépjárművének,</li>' +
                            '<li>valamennyi típus <strong>maximális élettartama 6-8 év</strong>, még akkor is, ha jó állapotúnak tűnnek vagy nem voltak sokat használva.</li>' +
                            '</ul>' +
                            '<p>Talán fölösleges is több szót vesztegetnünk a gumiabroncsok karbantartásának fontosságáról.</p>' +
                            ' <strong>Gumijavítási szolgáltatásunk</strong> keretén belül vállaljuk:' +
                            '<ul>' +
                            '<li>gumiabroncsok <strong>futófelületen</strong> történő javítását,</li>' +
                            '<li>gumiabroncsok oldalfalának <strong>külső és belső vulkanizálását</strong>,</li>' +
                            '<li>kerekek <strong>nyomásellenőrzését</strong> és igény szerint <strong>gázzal történő feltöltését</strong>.</li>' +
                            '</ul>' +
                            '<strong>Gumiszerelési szolgáltatásunk</strong> keretében vállaljuk:' +
                            '<ul>' +
                            '<li>komplett kerekek <strong>szét-és összeszerelését</strong>,</li>' +
                            '<li><strong>szelepházak</strong> cseréjét,</li>' +
                            '<li><strong>acélllemez felnik</strong> javítását és felújítását,</li>' +
                            '<li>kerekek műszeres <strong>centrírozását</strong>.</li>' +
                            '</ul>' +
                            '<p id="prices-title"><strong>Szolgáltatási áraink</strong> az alábbiak:</p>' +
                            '<div class="tyrePrices"></div>' +
                            '</div>';
                        break;

                    case "Olajcsere":
                        text = '<div class="w3-row"><img src="images/olaj_modal.png" class="w3-image" alt="Olajcsere" /></div><div class="w3-row"><p>Egy évtizeddel ezelőtt az <strong>olajcsere</strong>, több más karbantartási feladattal együtt, rutinmunkának számított.</p><p>A benzines személygépkocsik esetében <strong>7500 - 10000</strong>, a dízelmotoros gépkocsiknál pedig általában <strong>5000</strong> kilométerenként kötelező jelleggel le kellett cserélni a motorolajat. A mai korszerű személygépkocsiknál azonban az olajcserék esedékességére és az alkalmazható motorolajok minőségére vonatkozó előírások már jóval szerteágazóbbak és emiatt kevés autós ismeri őket pontosan.</p>' +
                            '<p>Talán sokaknak meglepő az a tény, hogy az olajcsere szempontjából manapság már <strong>nem a megtett kilométerek száma</strong> a döntő adat. A kenőanyag minőségét nagyban befolyásolja a gépjárművel végzett <strong>hidegindítások száma, a környezeti hőmérséklet, a motor elhasználtsága, az üzemanyag minősége, illetve a vezetési stílus</strong>.</p>' +
                            '<p>Az <strong>olajcsereciklus hosszúságát</strong> a felsoroltakon túl a motor konstrukciója, az olajteknőben levő olaj mennyisége, az olaj hőmérséklete, az olajfogyasztás mértéke és a szűrés hatásfoka is befolyásolja.' +
                            ' Ajánlatos minden megtett <strong>10 000 km után, vagy évenként lecserélni</strong>, a kenőanyagot és az olajszűrőt. Ez a legegyszerűbb módja a motor védelmének és élettartam növelésének.</p>' +  
                            '<p>Műhelyünk vállalja a <strong>gépjárműve motorolajának cseréjét</strong>, amelyhez hozzátartozik az <strong>olaj-és levegőszűrők</strong> cseréje is.' +
                            'A szolgáltatása <strong>rendkívül kedvezményes díja ' + data.oil + ' Ft-tól</strong> számítható, amely magában foglalja a munkadíjat, valamint a szűrők és a motorolaj anyagárát is.</p></div>';
                        break;

                    case "Futómű beállítás":
                        text = '<div class="w3-row"><img src="images/futomu_modal.png" class="w3-image" alt="Futómű beállítás" /></div><div class="w3-row"><p>A <strong>futómű geometriájának</strong> helyes beállítása a biztonságos közlekedés egyik legfontosabb feltétele. Ehhez járulnak még hozzá a <strong>jó állapotú lengéscsillapítók</strong>, a megfelelően <strong>kiegyensúlyozott kerekek</strong>, valamint a jó minőségű és állapotú <strong>gumiabroncsok</strong>. Ugyanis hiába rendelkezünk precízen beállított a futóművekkel, ha az előbbiek állapota nem megfelelő.</p>' +
                            '<p>A futómű helyes beállítása a gépjármű <strong>menet, egyenes- és ívmeneti tulajdonságainak</strong> javításán túl csökkenti a <strong>gumiköpenyek igénybevételét és a gumiabroncsok kopását</strong>, ami jótékonyan hat a gépjárművek üzemeltetésének költségeire.</p>' +
                            '<p>A <strong>futómű ellenőrzése</strong> azonban nem tekinthető egyszerű feladatnak, mivel az előírt beállítási adatok mérését <strong>nagyméretű vagy nehezen megközelíthető alkatrészeken</strong> kell rendkívül pontosan elvégezni. Ebből adódóan a korszerű szervizek ma már modern futómű ellenőrző készülékeket alkalmaznak.</p>' +
                            '<p>Műhelyünk a gépjárművek futóműveinek beállítását <strong>4 fejes, 8 szenzoros, BLUETOOTH kapcsolattal rendelkező berendezéssel</strong> végzi, amelynek pontosságát a <strong>számítógépes vezérlés</strong> garantálja.</p>' +
                            '<p>A szolgáltatási rendkívül kedvezményes ára <strong>' + data.wheel + ' Ft.</strong></p></div>';
                        break;

                    case "Fényszóró beállítás":
                        text = '<div class="w3-row"><img src="images/fenyszoro_modal.png" class="w3-image" alt="Gumiszerelés és javítás" /></div><div class="w3-row"><p>A <strong>járművilágítás</strong> közlekedésbiztonsági jelentősége vitathatatlan. Minden gépjárművezető ismeri a <strong>"látni és látszani"</strong> alapelvet, amelyet már az első KRESZ órától kezdve sulykolnak a tanulókba. Az út szélén közlekedő gyalogost vagy kerékpárost észre kell venniük a gépjárművezetőknek, míg arra is ügyelni kell, hogy az egymással szemben haladó járművek vezetői <strong>ne vakítsák</strong> el egymást, ugyanakkor a fényszórók biztosítsák a megfelelő <strong>megvilágítottságot</strong>. Ezek a problémák <strong>kritikus helyzetekben</strong>, például vasúti átjáróban, hídakon vagy bukkanókon áthajtva még élesebben jelentkeznek.</p>' +
                            '<p>A <strong>fényszórók beállítása</strong> típusonként változó művelet, mivel a karosszéria vonalát követő berendezések típusonként különböznek egymástól.' +
                            'A fényszórók pontos beállítása tehát elengedhetetlen a biztonságos közlekedés szempontjából, amelyet kiemelten ajánlunk <strong>műszaki vizsgát megelőzően</strong>, illetve a <strong>világító berendezések cseréjét követően</strong>, és évenkénti ellenőrzését ajánljuk.</p>' +
                            '<p>Műhelyünk a gépjárművek <strong>fényszóróinak beállítását</strong> az elismerten megbízható és pontos <strong>HELLA típusú műszerrel végzi</strong>, rendkívül kedvező <strong>' + data.headlight + ' Ft-os</strong> szolgáltatási áron.</p></div>';
                        break;
                }
                $("article#modal-text").html(text);
            });

            $("section#modal").css("display", "block");
        });
    }, 1000);

    // Modal bezárása  
    $(window).click(function(e) {

        if (e.target.getAttribute("id") === "modal" || e.target.getAttribute("id") === "modal-close") {
            $("section#modal").css("display", "none");
        }
    });
});