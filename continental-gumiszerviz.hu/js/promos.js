$(document).ready(function() {

    /** Akciók kezelése */

    var currentDate = new Date();
    var currentMonth = currentDate.getMonth() + 1;

    // Téligumi-nyárigumi akció
    currentMonth >= 9 || currentMonth < 4 ? $("img#tyre-promo").attr({ "src": "images/teligumi_akcio.png", "alt": "Téligumi akció" }) : $("img#tyre-promo").attr({ "src": "images/nyarigumi_akcio.png", "alt": "Nyárigumi akció" });
});