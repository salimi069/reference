$(document).ready(function() {

    $(".tyrePrice").click(function() {

        setTimeout(function() {

            $.getJSON('js/servicePrices.json', function(data) {

                var counter = 1;
                var tyreSizesNumber = data.size.length; // Méretek darabszáma

                /** Táblázat megnyitása */
                var table = '<table id="tyre-table" class="w3-table-all"><thead><th style="background-color: #bdbdbd"></th>';

                /** Címsor */
                for (tyreType in data.tyre) {
                    table += '<th colspan="' + tyreSizesNumber + '" class="w3-center">' + data.tyre[tyreType]["name"] + '</th>';
                }
                table += '</thead>';

                /** Első sor */
                table += '<tr><td><strong>Szolgáltatások</strong></td>';

                var tyreTypes = Object.keys(data.tyre);
                var tyreTypeNumber = tyreTypes.length;

                do { // Méretek beírása a felni típusok alá
                    for (var s = 0; s < data.size.length; s++) {
                        table += '<td>' + data.size[s] + '"</td>';
                    }
                    counter++;
                }
                while (counter <= tyreTypeNumber);
                table += '</tr>';

                /** Szolgáltatások sorai árakkal */
                for (serviceName in data.services) {
                    table += '<tr>';
                    // Szolgáltatás neve
                    table += '<td>' + data.services[serviceName] + '</td>';

                    for (type in data.tyre) {

                        for (var si = 0; si < data.size.length; si++) {
                            // Árak
                            table += '<td>' + data.tyre[type][serviceName][data.size[si]] + ' Ft</td>';
                        }
                    }
                    table += '</tr>';
                }

                table += '</table>';

                $("div.tyrePrices").html("").append('<div class="w3-container"><div class="w3-responsive">' + table + '</div></div>');
            });
        }, 500);
    });
});