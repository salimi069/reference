$(document).ready(function() {

    /** Áraink cikk szövegének elkészítése */

    $.getJSON("js/servicePrices.json", function(data) {
        $("article#prices-content").html("<p class=\"w3-panel w3-grey w3-padding w3-large w3-round w3-card-4\"><strong>GUMIJAVÍTÁS ÉS SZERELÉS</strong></p>" +
            "<div class=\"tyrePrices\"></div>" +
            "<p class=\"w3-panel w3-grey w3-padding w3-large w3-round w3-card-4\"><strong>OLAJCSERE</strong></p> - munkadíj, olaj-és levegőszűrők, motorolaj: " + data.oil + " Ft-tól<br /><br />" +
            "<p class=\"w3-panel w3-grey w3-padding w3-large w3-round w3-card-4\"><strong>MŰSZERES FUTÓMŰ BEÁLLÍTÁS</strong></p>" + data.wheel + " Ft<br /><br />" +
            "<p class=\"w3-panel w3-grey w3-padding w3-large w3-round w3-card-4\"><strong>MŰSZERES FÉNYSZÓRÓ BEÁLLÍTÁS:</strong></p>" + data.headlight + " Ft");
    });
});