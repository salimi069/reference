<?php

error_reporting(0);

/** Felhasználói e-mail küldése (Ajánlat) */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../vendor/autoload.php';

if(!empty($_POST['email']) && !empty($_POST['msg'])) {
    $email = $_POST['email'];
    $msg = 'Üzenet szövege';

    if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

        try {
            $mail = new PHPMailer(true);
            $mail->CharSet = 'utf-8';
            $mail->Encoding = 'base64';
            $mail->setFrom('client@freemail.hu', 'Continental Gyorsszervíz');
            $mail->addAddress($email);
            $mail->isHTML(true);        
            $mail->Subject = 'Continental Gyorsszervíz - Ajánlat';
            $mail->Body = $msg;
            $mail->send();
            echo 'Sikeres e-mail küldés! Időpont egyezetetés érdekében elérhetőségeinken várjuk szíves megkeresését.';

            // Keresési statisztika
            include_once('log.php');
            searchLog('küldés', $_POST['service']);                                                       
        }
        catch(\Exception $e) {
            echo 'Hiba: ' . $mail->ErrorInfo;
        }
    }
    else {
        echo 'Érvénytelen e-mail cím. Kérem, próbálja meg újra.';
    }
}