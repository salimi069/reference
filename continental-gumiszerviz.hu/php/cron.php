<?php

error_reporting(0);

/** Keresési statisztika küldése */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../vendor/autoload.php';

if(file_exists('../docs/log.txt')) {
    $mail = new PHPMailer(true);
    $mail->CharSet = 'utf-8';
    $mail->Encoding = 'base64';
    $mail->setFrom('client@freemail.hu', 'Continental Gyorsszervíz');
    $mail->addAddress('client@gmail.com');    
    $mail->addAttachment('../docs/log.txt');
    $mail->isHTML(true);        
    $mail->Subject = 'Continental Gyorsszervíz - Keresések';
    $mail->Body = 'Keresési eredmények';
    $sent = $mail->send();
    
    if($sent) {
        unlink('../docs/log.txt');
    }
}