<?php

error_reporting(0);

/** Keresési statisztika mentése */

if(isset($_POST['action'])) {
    call_user_func('searchLog', $_POST['action'], $_POST['service']);
}

function searchLog($action = false, $service = false) {

    if($service) {

        switch($service) {
    
            case 'tyre':
            $service = 'Gumiszerelés';
            break;
    
            case 'oil':
            $service = 'Olajcsere';
            break;
    
            case 'wheel':
            $service = 'Futómű beállítás';
            break;
    
            case 'headlight':
            $service = 'Fényszóró beállítás';
            break;
        }
    }

    $logFile = fopen('../docs/log.txt', 'a+');
    fwrite($logFile, date('Y-m-d H:i:s') . ' - ' . $service . ' - ' . $action . "\r\n");
    fclose($logFile);    
}