<?php

error_reporting(0);

/** Napi dátum és hőmérséklet (Budapest) */

// Dátum
$dailyData['date'] = date('Y-m-d'); 

// Hőmérséklet
function curlGet($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $results = curl_exec($ch);
    return $results;    
}

function returnXpathObject($item) {
    $xmlPageDom = new DomDocument();
    @$xmlPageDom->loadHTML($item);
    $xmlPageXPath = new DOMXPath($xmlPageDom);
    return $xmlPageXPath;
}

$sourcePage = curlGet('https://example.com/Budapest');
$sourcePageXPath = returnXpathObject($sourcePage);

$tempData = $sourcePageXPath->query('//div[@class="homerseklet"]');
$dailyData['temp'] = !empty($tempData->item(0)->nodeValue) ? $tempData->item(0)->nodeValue : '';

echo(count($dailyData) == 2) ? json_encode($dailyData) : 'error';