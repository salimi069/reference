<?php

class BaseFunctions {

    /** Űrlap mezők értékeinek ellenőrzése */
    public static function validateUserInput($input) {
        $result = htmlspecialchars(trim($input));
        return $result;
    }
    
    /** Alias készítése */
    public static function createAlias($title) {
        $source = ['Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ö', 'ö', 'Ő', 'ő', 'Ú', 'ú', 'Ü', 'ü', 'Ű', 'ű'];
	    $target = ['a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u'];

        if(strpos($title, ' ') != false) {
            $titleParts = explode(' ', trim($title));
            $titleNoSpace = implode('-', $titleParts);
            $title = $titleNoSpace;         
        }
        $replace = str_replace($source, $target, $title);
        $alias = strtolower($replace);
        return $alias; 
    }
}