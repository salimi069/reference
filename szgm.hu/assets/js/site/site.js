$(document).ready(function() {

    var baseUrl = $("span#baseUrl").text();

    if ($(window).width() >= 1080) {
        $("div#nav").addClass("w3-top");
    } else {
        $("div#nav").removeClass("w3-top");
        $("body").attr("style", "padding: 0 !important");
    }


    /** Modal */

    // Form
    // Nyitás
    $(".offer-btn").click(function() {
        $("div#modal").css("display", "block");
    });

    // Zárás
    $(".modal-close").click(function() {

        if (confirm("Biztosan be akarja zárni az ablakot?")) {
            $("div#modal").css("display", "none");
        }
    });

    // Üzenet küldése (Ajánlatkérés)
    $("input#form_send").click(function() {

        if ($("span#validationStatus").text() === '') {
            var formData = {
                name: $("input#form_name").val(),
                email: $("input#form_email").val(),
                phone: $("input#form_phone").val(),
                type: $("input#form_auto").val(),
                msg: $("textarea#form_msg").val(),
                sec: $("input#form_sec").val(),
                hp: $("input#form_hp").val(),
                copy: $("input#form_copy").prop("checked") ? 1 : 0
            }
            var data = JSON.stringify(formData);

            $.ajax({
                url: baseUrl + 'index.php/admin/message/sendmessage',
                type: "POST",
                data: { data: data },
                dataType: "html",
                success: function(response) {

                    if (response === 'success') {
                        alert('Sikeres üzenetküldés!');
                        $("div#modal").css("display", "none");
                    } else {
                        alert(response);
                        return false;
                    }
                }
            });
        }
    });

    // Referencia képek modal 
    $("div.tdDiv").each(function() {

        $(this).click(function() {
            var img = $(this).children("img").attr("src");
            $("div#refImg-placeholder").html('<img class="w3-image w3-margin" src="' + img + '" alt="' + img + '" width="80%" />');
            $("div#modal-img").css("display", "block");
        });
    });
    $("span#ref-close").click(function() { // Zárás
        $("div#modal-img").css("display", "none");
    });

    /** Képek magassága */
    $("div#service1-placeholder").height($("article#service1-text").height() + +16 + "px");
    $("div#service1-img, img#serviceImg-1").css("height", $("div#service1-placeholder").height() + "px");

    $("div#service2-placeholder").height($("article#service2-text").height() + "px");
    $("div#service2-img, img#serviceImg-2").css("height", $("div#service2-placeholder").height() + "px");

    /** Menü kezelése (sidebar) */
    $("button#mobileMenuOpen").click(function() {
        !$("div#mobil-menu").hasClass("w3-show") ? $("div#mobil-menu").addClass("w3-show") : $("div#mobil-menu").removeClass("w3-show");
    });

    $(window).resize(function() {

        if ($(this).width() > 1163) {
            $("div#mobil-menu").removeClass("w3-show").addClass("w3-hide");
        }

        // Szolgáltatási képek magassága 
        var servPH1Height = $("div#service1-placeholder").height();
        $("div#service1-placeholder").height($("article#service1-text").height() + "px");
        $("div#service1-img, img#serviceImg-1").height(servPH1Height + "px");

        var servPH2Height = $("div#service2-placeholder").height();
        $("div#service2-placeholder").height($("article#service2-text").height() + "px");
        $("div#service2-img, img#serviceImg-2").height(servPH2Height + "px");
    });
});