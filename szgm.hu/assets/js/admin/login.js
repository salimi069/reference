$(document).ready(function() {

    var baseUrl = $("span#baseUrl").text();
    var formData, sendData;

    $(".send").click(function() {

        if ($("span#validationStatus").text() === '') {

            formData = {
                "userEmail": $("input#loginName").val(),
                "userPass": $("input#loginPass").val()
            }

            sendData = JSON.stringify(formData);

            $.ajax({
                url: baseUrl + 'index.php/admin/home/userlogin',
                type: "POST",
                data: { data: sendData },
                dataType: "html",
                success: function(response) {


                    if (response === "success") {
                        window.location.href = baseUrl + 'index.php/admin/home';
                    } else {

                        switch (response) {

                            case "userFail":
                                $("div#loginError").text("Sajnos, nincs ilyen felhasználónév az adatbázisban.").css("display", "block");
                                break;

                            case "passFail":
                                $("div#loginError").text("Sajnos, nincs ilyen jelszó az adatbázisban.").css("display", "block");
                                break;

                            case "accessFail":
                                $("div#loginError").text("Önnek nincs jogosultsága belépni az oldalra.").css("display", "block");
                                break;

                            default:
                                $("div#loginError").text("Hiba! Kérem, próbálja meg újra.").css("display", "block");
                                break;
                        }
                    }
                }
            });
        }
    });
});