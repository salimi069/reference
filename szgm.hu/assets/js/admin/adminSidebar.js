$(document).ready(function() {

    /** Admin oldalakon az oldamenü kinyitása és bezárása */
    $("button.sidebar-btn").click(function() {
        $("div#adminSidebar").toggle();
    });

    /** Accordion kezelése */
    $("button.accBtn").click(function() {
        var action = $(this).data("action");

        if ($("div#" + action).hasClass("w3-show")) {
            $("div#" + action).toggle();
            $("div#" + action).removeClass("w3-show");
            $(this).removeClass("w3-grey");
        } else {
            $(this).addClass("w3-grey");
            $("div#" + action).addClass("w3-show");
            $("div#" + action).toggle();
        }
    });
});