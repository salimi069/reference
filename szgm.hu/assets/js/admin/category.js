$(document).ready(function() {

    var baseUrl = $("span#baseUrl").text();
    var catNameFail = "Ezen a néven már van kategória menüpont elmentve. Kérem, válasszon egy másikat.";
    var artNameFail = "Ezen a néven már van CIKK menüpont elmentve. Kérem, válasszon egy másikat.";
    var articleFail = "A kategória cikke(ke)t tartalmaz. A törléshez kérem, először a cikk(ek)et törölje.";
    var catTitle, menuItems, title, action, formData, JSONData, targetUrl, confMsg, failMsg;

    /** Kategória menüpozíciók lekérdezése és a select lista felépítése (kategóriák és önálló cikkek a választott menün belül) */
    if ($("select#catParentMenu").val() !== null) {
        catTitle = $("input#catTitle").val();
        getMenuItems($("select#catParentMenu").val(), catTitle);
    }

    $("select#catParentMenu").change(function() {
        getMenuItems($(this).val());
    });

    function getMenuItems(parentMenu, catTitle = false) {
        $("option.menuPos").remove();

        $.ajax({
            url: baseUrl + 'index.php/admin/content/getmenuitems/' + parentMenu + '/ajax',
            success: function(response) {
                menuItems = JSON.parse(response);
                var menuPos = Object.keys(menuItems);

                if (menuItems !== '') {

                    for (var sn = 0; sn < menuPos.length; sn++) {
                        $("select#catMenuPos").append('<option value="' + menuPos[sn] + '" class="menuPos" data-title="' + menuItems[menuPos[sn]] + '">' + menuPos[sn] + ' - ' + menuItems[menuPos[sn]] + '</option>');
                    }

                    if (catTitle) {

                        $("option.menuPos").each(function() {

                            $(this).data("title") === catTitle ? $(this).attr("selected", true) : $(this).attr("selected", false);
                        });
                    } else {
                        $("select#catMenuPos").append('<option value="' + (++menuPos.length) + '" class="menuPos">' + (menuPos.length++) + ' -</option>');
                    }
                } else {
                    $("select#catMenuPos").append('<option value="1" class="menuPos">1 - </option>');
                }
            }
        });
    }

    /** Kategória címének másolása a menüpont neveként */
    $("input#copyCatTitle").change(function() {

        if ($(this).prop("checked")) {

            if ($("input#catTitle").val() !== '') {
                title = $("input#catTitle").val();
                $("input#catMenuTitle").val(title);
            } else {
                alert("Kérem, először adjon meg egy kategória címet!");
                $(this).prop("checked", false);
            }
        } else {
            $("input#catMenuTitle").val("");
        }
    });

    /** Kategória kezelése */
    $(".send").click(function() {
        action = $(this).data("action");

        if ($("span#validationStatus").text() === '') {

            formData = {
                "catTitle": $("input#catTitle").val(),
                "catParentMenu": $("select#catParentMenu").val(),
                "catMenuTitle": $("input#catMenuTitle").val(),
                "catMenuPos": $("select#catMenuPos").val(),
                "catStat": $("input#catStat").prop("checked") ? 1 : 0
            }

            JSONData = JSON.stringify(formData);

            switch (action) {

                case "save": // Mentés
                    targetUrl = baseUrl + 'index.php/admin/content/savecategory';
                    confMsg = "Sikeres mentés.";
                    failMsg = "Sikertelen mentés. Kérem, próbálja meg újra.";
                    break;

                case "update": // Módosítás
                    catId = $(this).data("id");
                    targetUrl = baseUrl + 'index.php/admin/content/savecatupdate/' + catId;
                    confMsg = "Sikeres módosítás.";
                    failMsg = "Sikertelen módosítás. Kérem, próbálja meg újra.";
                    break;

                case "delete": // Törlés

                    if (confirm("Biztosan törölni akarja a kategóriát?")) {
                        catId = $(this).data("id");
                        targetUrl = baseUrl + 'index.php/admin/content/deletecategory/' + catId;
                        confMsg = "Sikeres törlés.";
                        failMsg = "Sikertelen törlés. Kérem, próbálja meg újra.";
                    }
                    break;
            }

            $.ajax({
                url: targetUrl,
                type: "POST",
                data: { data: JSONData },
                dataType: "html",
                success: function(response) {

                    if (response === "success") {
                        alert(confMsg);
                        window.location.href = baseUrl + 'index.php/admin/content/managecategory';
                    } else {

                        switch (response) {

                            case 'catNameFail':
                                alert(catNameFail);
                                return false;
                                break;

                            case 'artNameFail':
                                alert(artNameFail);
                                return false;
                                break;

                            case "articleFail":
                                alert(articleFail);
                                return false;
                                break;

                            default:
                                alert(failMsg);
                                return false;
                                break;
                        }
                    }
                }
            });
        }
    });
});