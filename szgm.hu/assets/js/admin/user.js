$(document).ready(function() {

    var baseUrl = $("span#baseUrl").text();
    var editor = $("span#editor").text();
    var formData, sendData, action, targetUrl, confMsg, failMsg, userId;
    var adminFail = "Jelenleg már szerepel egy adminisztrátor az adatbázisban.";
    var emailExists = "Ez az e-mail cím már szerepel az adatbázisban.";
    var emailFail = "Kérem, adjon meg egy érvényes e-mail címet.";
    var accessFail = "Önnek nincs jogosultsága az adatok módosítására.";
    var minUserFail = "Legalább egy aktív adminisztrátornak maradnia kell az adatbázisban.";
    var delAdminFail = "Adminisztrátor nem törölhető az adatbázisból!";

    // Jogosultság mező kezelése
    $("select#userAccess").change(function() {

        if ($(this).val() == 1) {
            $("input#userStat").prop("checked", true).attr("disabled", true);
        } else {

            if ($("input#userStat").prop("checked")) {
                $("input#userStat").prop("checked", true).attr("disabled", false);
            } else {
                $("input#userStat").prop("checked", false).attr("disabled", false);
            }
        }
    });

    // Jelszó mezők kezelése
    if ($("input#userNewPass").length > 0) {

        $("input#userNewPass, input#userNewPassConf").blur(function() {

            if ($(this).val() !== '') {
                $(this).addClass("valid");
                $(this).siblings("label").children("span.asterisk").length == 0 ? $(this).siblings("label").append('<span class="asterisk">*</span>') : '';
                $("input#userNewPassConf").addClass("valid");
                $("input#userNewPassConf").siblings("label").children("span.asterisk").length == 0 ? $("input#userNewPassConf").siblings("label").append('<span class="asterisk">*</span>') : '';
            } else {
                $(this).removeClass("valid");
                $(this).siblings("label").children("span.asterisk").remove();
                $(this).siblings("span.alert, span.pass-check").text("");
                $("input#userNewPassConf").removeClass("valid")
                $("input#userNewPassConf").siblings("label").children("span.asterisk").remove();
                $("input#userNewPassConf").siblings("span.alert").text("");
            }
        });
    }

    $(".send").click(function() {

        if ($("span#validationStatus").text() === '') {

            action = $(this).data("action");
            userId = $(this).data("id");

            formData = {
                "userName": $("input#userName").val(),
                "userEmail": $("input#userEmail").val(),
                "userAccess": $("select#userAccess").val(),
                "userPass": $("input#userPass").val(),
                "userNewPass": $("input#userNewPass").val() !== '' ? $("input#userNewPass").val() : '',
                "userStat": $("input#userStat").prop("checked") ? 1 : 0
            }

            switch (action) {

                case "save":
                    targetUrl = baseUrl + 'index.php/admin/user/saveuser';
                    confMsg = "Sikeres mentés.";
                    failMsg = "Sikertelen mentés. Kérem, próbálja meg újra.";
                    break;

                case "update":
                    targetUrl = baseUrl + 'index.php/admin/user/saveUserUpdate/' + userId + '/' + editor;
                    confMsg = "Sikeres módosítás.";
                    failMsg = "Sikertelen módosítás. Kérem, próbálja meg újra.";
                    break;

                case "delete":
                    if (confirm("Biztosan törölni akarja a felhasználót?")) {
                        targetUrl = baseUrl + 'index.php/admin/user/deleteuser/' + userId;
                        confMsg = "Sikeres törlés.";
                        failMsg = "Sikertelen törlés. Kérem, próbálja meg újra.";
                    }
                    break;

            }

            sendData = JSON.stringify(formData);

            $.ajax({
                url: targetUrl,
                type: "POST",
                data: { data: sendData },
                dataType: "html",
                success: function(response) {
                    console.log(response);

                    if (response === "success") {
                        alert(confMsg);
                        window.location.href = baseUrl + 'index.php/admin/user';
                    } else {

                        switch (response) {

                            case "adminFail":
                                alert(adminFail);
                                return false;
                                break;

                            case "emailExists":
                                alert(emailExists);
                                return false;
                                break;

                            case "emailFail":
                                alert(emailFail);
                                return false;
                                break;

                            case "accessFail":
                                alert(accessFail);
                                return false;
                                break;

                            case "minUserFail":
                                alert(minUserFail);
                                return false;

                            case "delAdminFail":
                                alert(delAdminFail);
                                return false;

                            default:
                                alert(failMsg);
                                return false;
                                break;
                        }
                    }
                }
            });
        }
    });
});