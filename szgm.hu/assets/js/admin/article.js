$(document).ready(function() {

    var baseUrl = $("span#baseUrl").text();
    var catNameFail = "Ezen a néven már szerepel egy KATEGÓRIA menüpont az adatbázisban. Kérem, válasszon egy másik nevet!";
    var artNameFail = "Ezen a néven már szerepel egy CIKK menüpont az adatbázisban. Kérem, válasszon egy másik nevet!";
    var imgFileTypeFail = "Az egyik fájl formátuma nem engedélyezett! Csak .jpg, .jpeg, .gif, .png kiterjesztésű fájlok tölthetők fel!";
    var imgFileNameFail = "Az egyik kép fájlneve már szerepel a mentett képek között. Kérem, változtassa meg a fájl nevét!";
    var imgNumberFail = "Egyszerre csak max. 10 db kép tölthető fel.";
    var catId, artId, imgFolder, catItems, menuId, menuItems, action, targetUrl, confMsg, failMsg, data, text;

    /** VÉDETT CIKKEK BEÁLLÍTÁSAI */
    var protArtIds = [12, 13, 14, 15];
    var id = $("input#artDel").data("id");

    if (protArtIds.length > 0) {

        if (protArtIds.indexOf(id) > -1) {

            if (id == 12) {
                $("#artTitle, #artText").attr("readonly", true);
            } else {
                $("#artTitle").attr("readonly", true);
            }
            $("div.protField").hide();
            $("input#artDel").attr("disabled", true);
        }
    }

    /** 
     * MENÜ BEÁLLÍTÁSOK
     */
    if ($("select#artParentCat").val() !== "select") {
        $("select#artParentMenu").attr("disabled", true).removeClass("valid").siblings("label").children("span.asterisk").toggle();
    } else if ($("select#artParentMenu").val() !== "select") {
        $("select#artParentCat").attr("disabled", true);
    }

    $("select#artParentCat").change(function() { // Szülő kategória
        $("option.item").remove();
        $("select#artMenuPos").children("option[value='select']").prop("selected", true);

        if ($(this).val() !== "select") {
            catId = $(this).val();
            $("select#artParentMenu").removeClass("valid").attr("disabled", true).siblings("label").children("span.asterisk").toggle();
            $("select#artParentMenu").siblings("span.alert").text("");
            $("select#artParentMenu").children("option[value='select']").prop("selected", true);

            $.ajax({ // A kategória cikkeinek és pozícióiknak a lekérdezése 
                url: baseUrl + 'index.php/admin/content/getCategoryItems/' + catId + '/ajax',
                success: function(response) {
                    catItems = JSON.parse(response);
                    catItemsNo = catItems.length;
                    var cat;

                    if (catItems.length > 0) {

                        for (cat in catItems) {
                            $("select#artMenuPos").append('<option class="item" value="' + catItems[cat].artMenuPos + '">' + catItems[cat].artMenuPos + ' - ' + catItems[cat].artMenuTitle + '</option>');
                        }
                        $("select#artMenuPos").append('<option class="item" value="' + (++catItemsNo) + '">' + (catItemsNo++) + ' - ' + '</option>');
                    } else {
                        $("select#artMenuPos").append('<option value="1" class="item">1 - </option>');
                    }
                }
            });
        } else {
            $("select#artParentMenu").addClass("valid").attr("disabled", false).siblings("label").children("span.asterisk").toggle();
            $("select#artParentMenu").siblings("span.alert").text("");
        }
    });

    $("select#artParentMenu").change(function() { // Szülő menü
        $("option.item").remove();
        $("select#artMenuPos").children("option[value='select']").prop("selected", true);

        if ($(this).val() !== "select") {
            menuId = $(this).val();
            $("select#artParentCat").attr("disabled", true).children("option[value='select']").prop("selected", true);

            $.ajax({ // A szülőmenü elemeinek és pozícióiknak a lekérdezése
                url: baseUrl + 'index.php/admin/content/getMenuItems/' + menuId + '/ajax',
                success: function(response) {
                    menuItems = JSON.parse(response);
                    menuItemsNo = menuItems.length;
                    var menuPos = Object.keys(menuItems);

                    if (menuItems !== '') {

                        for (var mi = 0; mi < menuPos.length; mi++) {
                            $("select#artMenuPos").append('<option class="item" value="' + menuPos[mi] + '">' + menuPos[mi] + ' - ' + menuItems[menuPos[mi]] + '</option>');
                        }
                        $("select#artMenuPos").append('<option class="item" value="' + (++menuPos.length) + '">' + (menuPos.length++) + ' - ' + '</option>');
                    } else {
                        $("select#artMenuPos").append('<option value="1" class="item">1 - </option>');
                    }
                }
            });
        } else {
            $("select#artParentCat").attr("disabled", false);
        }
    });

    /** 
     * FELTÖLTENDŐ KÉPEK MEGJELENÍTÉSE
     */
    $(function() {
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).addClass('w3-image thumbImg').appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('input#artImg').on('change', function() {
            imagesPreview(this, 'div.galery');
        });
    });

    /** 
     * KÉPEK HIVATKOZÁSÁNAK MÁSOLÁSA 
     */
    $("button.copyLink").each(function() {

        $(this).click(function() {
            $(this).siblings("input[type='text']").select();
            document.execCommand("copy");
        });
    });

    /**
     * KÉPEK TÖRLÉSE
     */
    $("button.delImg").click(function() {
        action = $(this).data("action");
        imgFolder = $(this).data("folder");
        var img = [];

        switch (action) {

            case "selected": // Kiválasztott képek törlése               

                $("input.selImg").each(function() {

                    if ($(this).prop("checked")) {
                        img.push($(this).data("img"));
                    }
                });

                if (img.length > 0) {

                    if (confirm("FIGYELEM: Ha a képek szerepelnek a cikkben javasolt először onnan eltávolítani őket. Biztosan törölni akarja a képeket?")) {
                        var delImg = JSON.stringify(img);

                        $.ajax({
                            url: baseUrl + 'index.php/admin/content/deleteimage/selected',
                            type: "POST",
                            data: { img: delImg, folder: imgFolder },
                            dataType: "html",
                            success: function(response) {

                                if (response === "success") {
                                    alert("Sikeres törlés.");

                                    $("div.img-placeholder").each(function() {
                                        var imgName = $(this).data("imgplaceholder");

                                        if (img.indexOf(imgName) > -1) {
                                            $(this).remove();
                                        }
                                    });
                                }
                            }
                        });
                    }
                } else {
                    alert("Kérem, először válassza ki a törölni kívánt képeket.");
                    return false;
                }

                break;

            case "all": // Minden kép törlése

                if (confirm("FIGYELEM: Ha a képek szerepelnek a cikkben javasolt először onnan eltávolítani őket. Biztosan törölni akarja a képeket?")) {

                    $.ajax({
                        url: baseUrl + 'index.php/admin/content/deleteimage/all',
                        type: "POST",
                        data: { folder: imgFolder },
                        dataType: "html",
                        success: function(response) {

                            if (response === "success") {
                                alert("Sikeres törlés.");
                                $("div.img-placeholder, button#delSelImg, button#delAllImg").remove();
                            }
                        }
                    });
                }
                break;
        }
    });


    /**
     * CÍM MÁSOLÁSA MENÜPONT NÉVKÉNT
     */
    $("input#copyArtTitle").change(function() {

        if ($(this).prop("checked")) {

            if ($("input#artTitle").val() !== '') {
                title = $("input#artTitle").val();
                $("input#artMenuTitle").val(title);
            } else {
                alert("Kérem, először adjon meg egy cikk címet!");
                $(this).prop("checked", false);
            }
        } else {
            $("input#artMenuTitle").val("");
        }
    });

    /**
     * MENTÉS, MÓDOSÍTÁS, TÖRLÉS
     */

    $(".send").click(function() {
        action = $(this).data("action");

        if ($("span#validationStatus").text() === '') {

            switch (action) {

                case "save":
                    targetUrl = baseUrl + 'index.php/admin/content/savearticle';
                    confMsg = "Sikeres mentés.";
                    failMsg = "Sikertelen mentés. Kérem, próbálja meg újra.";
                    break;

                case "update":
                    artId = $(this).data("id");
                    targetUrl = baseUrl + 'index.php/admin/content/saveartupdate/' + artId;
                    confMsg = "Sikeres módosítás.";
                    failMsg = "Sikertelen módosítás. Kérem, próbálja meg újra.";
                    break;

                case "delete":
                    if (confirm("Biztosan törölni akarja a cikket?")) {
                        artId = $(this).data("id");
                        targetUrl = baseUrl + 'index.php/admin/content/deletearticle/' + artId;
                        confMsg = "Sikeres törlés.";
                        failMsg = "Sikertelen törlés. Kérem, próbálja meg újra.";
                    }
                    break;
            }

            data = new FormData($("form")[0]);
            text = CKEDITOR.instances.artText.getData();
            text !== '' ? data.append('artText', text) : data.append('artText', '');

            $.ajax({
                url: targetUrl,
                type: "POST",
                enctype: "multipart/formdata",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(response) {

                    if (response === "success") {
                        alert(confMsg);
                        window.location.href = baseUrl + "index.php/admin/content/managearticle";
                    } else {

                        switch (response) {

                            case "catNameFail":
                                alert(catNameFail);
                                return false;
                                break;

                            case "artNameFail":
                                alert(artNameFail);
                                return false;
                                break;

                            case "imgFileTypeFail":
                                alert(imgFileTypeFail);
                                return false;
                                break;

                            case "imgNumberFail":
                                alert(imgNumberFail);
                                return false;
                                break;

                            case "imgFileNameFail":
                                alert(imgFileNameFail);
                                return false;
                                break;

                            default:
                                alert(failMsg);
                                return false;
                                break;
                        }
                    }
                }
            });
        }
    });
});