$(document).ready(function() {

    var baseUrl = $("span#baseUrl").text();
    var formData, JSONData, action, targetUrl, target, confMsg, failMsg, menuTitleFail, menuStatFail, menuAmountFail, menuCatFail, menuArtFail;

    $(".send").click(function() {

        if ($("span#validationStatus").text() === '') {
            action = $(this).data("action");
            formData = {
                "menuTitle": $("input#menuTitle").val(),
                "menuStat": $("input#menuStat").prop("checked") ? 1 : 0
            }

            JSONData = JSON.stringify(formData);

            switch (action) {

                case "saveMenu":
                    targetUrl = baseUrl + 'index.php/admin/menu/savemenu';
                    confMsg = 'Sikeres mentés!';
                    failMsg = 'Sikertelen mentés. Kérem, próbálja meg újra.';
                    menuTitleFail = 'Ezen a néven már szerepel egy menü az adatbázisban. Kérem, válasszon másikat!';
                    break;

                case "updateMenu":
                    target = $(this).data("target");
                    targetUrl = baseUrl + 'index.php/admin/menu/saveMenuUpdate/' + target;
                    confMsg = 'Sikeres módosítás!';
                    failMsg = 'Sikertelen módosítás. Kérem, próbálja meg újra.';
                    menuTitleFail = 'Ezen a néven már szerepel egy menü az adatbázisban. Kérem, válasszon másikat!';
                    menuStatFail = 'Minimum egy aktív menünek szerepelnie kell a weboldalon. Kérem, aktiválja a menüt!';
                    break;

                case "deleteMenu":
                    if (confirm("Biztosan törölni akarja a menüt?")) {
                        target = $(this).data("target");
                        targetUrl = targetUrl = baseUrl + 'index.php/admin/menu/deletemenu/' + target;
                        confMsg = 'Sikeres törlés!';
                        failMsg = 'Sikertelen törlés. Kérem, próbálja meg újra.';
                        menuAmountFail = 'Legalább egy aktív menünek lennie kell a weboldalon.';
                        menuStatFail = 'Minimum egy aktív menünek szerepelnie kell a weboldalon.';
                        menuCatFail = "Legalább egy kategória még tartozik a menühöz. Kérem, először azt törölje.";
                        menuArtFail = "Legalább egy cikk még tartozik a menühöz. Kérem, először azt törölje.";
                    }
                    break;
            }

            $.ajax({
                url: targetUrl,
                type: "POST",
                data: { data: JSONData },
                dataType: "html",
                success: function(response) {

                    if (response === "success") {
                        alert(confMsg);
                        window.location.href = baseUrl + 'index.php/admin/menu/managemenu';
                    } else {

                        switch (response) {

                            case 'menuTitle':
                                alert(menuTitleFail);
                                return false;
                                break;

                            case 'menuStat':
                                alert(menuStatFail);
                                return false;
                                break;

                            case 'menuAmount':
                                alert(menuAmountFail);
                                return false;
                                break;

                            case "menuCatFail":
                                alert(menuCatFail);
                                return false;
                                break;

                            case "menuArtFail":
                                alert(menuArtFail);
                                return false;
                                break;

                            default:
                                alert(failMsg);
                                return false;
                                break;
                        }
                    }
                }
            });
        }
    });
});