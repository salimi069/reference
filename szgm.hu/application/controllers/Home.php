<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public $menuContent = null;
    public $reference = null;
    public $refImgs = [];
    public $service1 = null;
    public $service2 = null;
    public $artText = null;
    public $metaKeywords = null;
    public $metaDesc = null;
    public $page = null;
    private $imgFileType = null;
    private $baseUrl = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data');        
        $this->imgFileType = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'];
        $this->baseUrl = base_url();
    }

    public function index($page = false)
    {
        $this->page = $page;
        
        /** Főmenü és honlaptérkép */
        $art = $this->Data->getData('article', 'artParentCat, artMenuTitle, artMenuAlias, artMenuPos', ['artStat' => 1], 'artMenuPos ASC');

        if (count($art) > 0) {
            foreach ($art as $artItem) {
                if ($artItem->artParentCat == 0) {
                    $this->menuContent[$artItem->artMenuPos] = [
                        'artParentCat' => $artItem->artParentCat,
                        'artMenuTitle' => $artItem->artMenuTitle,
                        'artMenuAlias' => $artItem->artMenuAlias
                    ];
                } else {
                    $cat = $this->Data->getData('category', 'catMenuTitle, catMenuAlias, catMenuPos, catStat', ['catId' => $artItem->artParentCat, 'catStat' => 1], 'catMenuPos ASC', 1);
                    
                    if (count($cat) >0) {
                        $artData = [
                            'artParentCat' => $artItem->artParentCat,
                            'catMenuTitle' => $cat[0]->catMenuTitle,
                            'artMenuTitle' => $artItem->artMenuTitle,
                            'artMenuAlias' => $artItem->artMenuAlias
                        ];
                        $this->menuContent[$cat[0]->catMenuPos]['catMenuTitle'] = $cat[0]->catMenuTitle;
                        $this->menuContent[$cat[0]->catMenuPos][$artItem->artMenuPos] = $artData;
                    }
                }
            }
            ksort($this->menuContent, SORT_NUMERIC);
        }
        
        /** Referencia képek megjelenítése */
        if (is_dir('assets/uploads/images/articles/referencia')) {
            $refFiles = array_diff(scandir('assets/uploads/images/articles/referencia'), ['.', '..', 'index.html']);
            
            if (count($refFiles) > 0) {
                $this->load->library('table');
                
                foreach ($refFiles as $file) {
                    if (in_array(pathinfo($file, PATHINFO_EXTENSION), $this->imgFileType)) {
                        if (!in_array($file, $this->refImgs)) {
                            // Képek
                            $ext = pathinfo($file, PATHINFO_EXTENSION);
                            $name = explode('_', $file);
                            $this->refImgs[$file] = "{$name[0]}_02.{$ext}";
                        }
                    }
                }

                // Táblázat
                if (count($this->refImgs) > 0) {
                    $template = ['table_open' => '<table class="w3-table">'];
                    $this->table->set_template($template);
                    $this->table->set_heading('<span class="w3-card-4 w3-padding w3-margin header w3-round">Ilyen volt</span>', '<span class="w3-card-4 w3-padding w3-margin header w3-round">Ilyen lett</span>');

                    foreach ($this->refImgs as $thatWas => $thatIs) {
                        $this->table->add_row("<div class=\"tdDiv\"><img src=\"{$this->baseUrl}assets/uploads/images/articles/referencia/{$thatWas}\" /></div>", "<div class=\"tdDiv\"><img src=\"{$this->baseUrl}assets/uploads/images/articles/referencia/{$thatIs}\" /></div>");
                    }
                }
            }
        }

        /** CIKKEK TARTALMA */
        // Motor és hengerfej felújítás
        $this->service1 = $this->Data->getData('article', 'artText', ['artId' => 13], false, 1);
        
        // Motor és hengerfej felújítás menete
        $this->service2 = $this->Data->getData('article', 'artText', ['artId' => 14], false, 1);

        /** KAPCSOLAT */
        $this->contact = $this->Data->getData('article', 'artText', ['artId' => 15], false, 1);                   
        
        if (!$page) {
            $this->artText = $this->Data->getData('article', 'artKeyWords, artDesc', ['artId' => 19], false, 1);
            $this->metaKeywords = $this->artText[0]->artKeyWords;
            $this->metaDesc = $this->artText[0]->artDesc;
            $this->load->view('site/templates/head');
            $this->load->view('site/pages/home');
        }
        else {
            $this->artText = $this->Data->getData('article', 'artTitle, artText, artKeywords, artDesc', ['artMenuAlias' => $page], false, 1);
            $this->metaKeywords = $this->artText[0]->artKeywords;
            $this->metaDesc = $this->artText[0]->artDesc;

            $this->load->view('site/templates/head');
            $this->load->view('site/pages/page');
        }                 
        $this->load->view('site/templates/footer');
    }       
}
