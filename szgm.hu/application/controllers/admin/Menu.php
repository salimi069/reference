<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public $activeMenuExists = null;
    public $menus = null;
    public $allMenus = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data');
        require_once('assets/php/admin/BaseFunctions.php');
        $this->load->library('session');
    }
    
    /** Főoldal */
    public function index()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/menusidebar');
            $this->load->view('admin/pages/menu');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Menü készítése */
    public function createMenu()
    {
        // Aktív menü ellenőrzése
        $activeMenu = $this->Data->getData('menu', false, ['menuStat' => 1], false, 1);
        $this->activeMenuExists = count($activeMenu) > 0 ? true : false;

        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/menusidebar');
            $this->load->view('admin/pages/createmenu');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Menü mentése
    public function saveMenu()
    {
        $formData = json_decode($_POST['data']);
        $menuTitleCheck = $this->Data->getData('menu', 'menuTitle');
        $menuTitleCheckResult = [];

        foreach ($menuTitleCheck as $title) { // Menü nevének ellenőrzése
            
            if (strtolower($title->menuTitle) === strtolower($formData->menuTitle)) {
                $menuTitleCheckResult[] = $title->menuTitle;
            }
        }

        if (count($menuTitleCheckResult) == 0) {
            $data = [
                'menuTitle' => BaseFunctions::validateUserInput($formData->menuTitle),
                'menuStat' => BaseFunctions::validateUserInput($formData->menuStat)
            ];
            
            $this->Data->saveData('menu', $data);
            echo $this->Data->dbResponse;
        } else {
            echo 'menuTitle';
        }
    }

    /** Menü kezelése */
    public function manageMenu()
    {
        if ($this->session->has_userdata('userId')) {
            $this->menus = $this->Data->getData('menu', false, false, 'menuTitle ASC');
        
            if (count($this->menus) > 0) {
                $counter = 1;
                $this->load->library('table');
                $template = ['table_open' => '<table class="w3-table-all">'];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Név', 'Státusz', 'Kezelés');

                foreach ($this->menus as $menu) {
                    $stat = $menu->menuStat == 0 ? 'Inaktív' : 'Aktív';
                    $this->table->add_row($counter++, $menu->menuTitle, $stat, '<a href="' . base_url() . 'index.php/admin/menu/updatemenu/' . $menu->menuId . '" class="w3-btn w3-orange w3-round">Kezelés</a>');
                }
            }

            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/menusidebar');
            $this->load->view('admin/pages/managemenu');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Menü frissítése */
    public function updateMenu($menuId)
    {
        if ($this->session->has_userdata('userId')) {
            $this->allMenus = $this->Data->getData('menu');
            $this->menus = $this->Data->getData('menu', 'menuId, menuTitle, menuStat', ['menuId' => $menuId], false, 1);

            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/menusidebar');
            $this->load->view('admin/pages/updatemenu');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Menü módosításának mentése
    public function saveMenuUpdate($menuId)
    {
        $formData = json_decode($_POST['data']);
        $menu = $this->Data->getData('menu', false, ['menuId' => $menuId], false, 1);
        $menuTitleCheck = $this->Data->getData('menu', 'menuTitle', 'menuId !=' . $menuId);
        $menuTitleCheckResult = [];
        
        $data = [
            'menuTitle' => BaseFunctions::validateUserInput($formData->menuTitle),
            'menuStat' => BaseFunctions::validateUserInput($formData->menuStat)
        ];

        foreach ($menuTitleCheck as $title) { // Menü nevének ellenőrzése
            
            if (strtolower($title->menuTitle) === strtolower($formData->menuTitle)) {
                $menuTitleCheckResult[] = $title->menuTitle;
            }
        }

        if (count($menuTitleCheckResult) == 0) {
            if ($formData->menuStat != 0) {
                $this->Data->updateData('menu', $data, ['menuId' => $menuId], 1);
                echo $this->Data->dbResponse;
            } else { // Menüstátusz ellenőrzés értékváltozás esetén
                $menuIds = [];
                $menuStatCheck = $this->Data->getData('menu', 'menuId', ['menuStat' => 1]);

                foreach ($menuStatCheck as $stat) {
                    $menuIds[] = $stat->menuId;
                }

                if (in_array($menuId, $menuIds) && count($menuIds) == 1) {
                    echo 'menuStat';
                } else {
                    $this->Data->updateData('menu', $data, ['menuId' => $menuId], 1);
                    echo $this->Data->dbResponse;
                }
            }
        } else {
            echo 'menuTitle';
        }
    }

    /** Menü törlése */
    public function deleteMenu($menuId)
    {
        $menu = $this->Data->getData('menu', false, ['menuId' => $menuId], false, 1);
        $menus = $this->Data->getData('menu');
        $menuStatCheck = $this->Data->getData('menu', false, ['menuStat' => 1]);
        $menuIds = [];

        $menuCatItem = $this->Data->getData('category', false, ['catParentMenu' => $menuId], false, 1);
        $menuArtItem = $this->Data->getData('article', false, ['artParentMenu' => $menuId], false, 1);

        foreach ($menuStatCheck as $stat) {
            $menuIds[] = $stat->menuId;
        }

        if (count($menus) == 1) {
            echo 'menuAmount';
        } elseif (in_array($menuId, $menuIds) && count($menuIds) == 1) {
            echo 'menuAmount';
        } else {
            if (count($menuCatItem) == 0 && count($menuArtItem) == 0) {
                $this->Data->deleteData('menu', ['menuId' => $menuId], 1);
                echo(empty($this->Data->dbResponse)) ? 'success' : 'error';
            } else {
                switch (true) {

                    case count($menuCatItem) > 0:
                    echo 'menuCatFail';
                    break;

                    case count($menuArtItem) > 0:
                    echo 'menuArtFail';
                    break;
                }
            }
        }
    }
}
