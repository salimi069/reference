<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public $adminCheck = null;
    public $users = null;
    public $adminId = null;
    public $savedAdminId = null;
    public $editor = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data');
        require_once('assets/php/admin/BaseFunctions.php');
        $this->load->library('session');
    }
    
    /** Főoldal */
    public function index()
    {
        if ($this->session->has_userdata('userId')) {                        
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/usersidebar');
            $this->load->view('admin/pages/user');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Felhasználó felvétele */
    public function createUser()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/usersidebar');

            $this->adminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);

            $this->load->view('admin/pages/createuser');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Felhasználó mentése
    public function saveUser()
    {
        $formData = json_decode($_POST['data']);
        $emailCheck = $this->Data->getData('users', false, ['userEmail' => $formData->userEmail], false, 1);
        $adminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);
        
        if (filter_var($formData->userEmail, FILTER_VALIDATE_EMAIL)) {
            if (count($emailCheck) == 0) {
                if ((count($adminCheck) == 0 && $formData->userAccess == 1) || (count($adminCheck) > 0 && $formData->userAccess == 2)) {
                    $sendData = [
                        'userName' => BaseFunctions::validateUserInput($formData->userName),
                        'userEmail' => BaseFunctions::validateUserInput($formData->userEmail),
                        'userAccess' => BaseFunctions::validateUserInput($formData->userAccess),
                        'userPass' => password_hash(BaseFunctions::validateUserInput($formData->userPass), PASSWORD_DEFAULT),
                        'userStat' => BaseFunctions::validateUserInput($formData->userStat)
                    ];

                    $this->Data->saveData('users', $sendData);
                    echo $this->Data->dbResponse;
                } else {
                    echo 'adminFail';
                }
            } else {
                echo 'emailExists';
            }
        } else {
            echo 'emailFail';
        }
    }

    /** Felhasználó kezelése */
    public function manageUser()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/usersidebar');

            $this->users = $this->Data->getData('users', false, false, 'userName ASC');
        
            if (count($this->users) > 0) {
                $counter = 1;
                $this->load->library('table');
                $template = ['table_open' => '<table class="w3-table-all">'];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Név', 'E-mail', 'Jogosultság', 'Státusz', 'Kezelés');

                foreach ($this->users as $user) {
                    $access = $user->userAccess == 1 ? 'Adminisztrátor' : 'Szerkesztő';
                    $stat = $user->userStat == 1 ? 'Aktív' : 'Inaktív';
                    $this->table->add_row($counter++, $user->userName, $user->userEmail, $access, $stat, '<a href="' . base_url() . 'index.php/admin/user/updateuser/' . $user->userId . '" class="w3-btn w3-orange">Kezelés</a>');
                }
            }

            $this->load->view('admin/pages/manageuser');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Felhasználói adatok frissítése  */
    public function updateUser($userId, $editor = false)
    {
        if ($this->session->has_userdata('userId')) {
            $this->users = $this->Data->getData('users', false, ['userId' => $userId], false, 1);
            $this->adminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);
            $this->adminId = $userId;
            $this->savedAdminId = !empty($this->adminCheck[0]->userId) ? $this->adminCheck[0]->userId : 0;
            $this->editor = $editor ? 'editor' : 'admin';
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/usersidebar');
            $this->load->view('admin/pages/updateuser');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Módosított adatok mentése (admin)
    public function saveUserUpdate($userId, $editor)
    {
        $admin = $this->Data->getData('users', 'userPass', ['userAccess' => 1], false, 1);
        $user = $this->Data->getData('users', false, ['userId' => $userId], false, 1);
        $users = $this->Data->getData('users');
        $formData = json_decode($_POST['data']);
        $emailCheck = $this->Data->getData('users', false, ['userEmail' => $formData->userEmail], false, 1);
        $adminPass = count($admin) > 0 ? $admin[0]->userPass : null;

        switch ($editor) {
             
            case 'admin':
            $passCheck = password_verify($formData->userPass, $adminPass) || count($admin) == 0 ? true : false;
            break;
            
            case 'editor':
            $passCheck = password_verify($formData->userPass, $user[0]->userPass) ? true : false;
            break;
        }

        switch (true) {

            case $formData->userAccess == 1 || count($users) == 1:
                $stat = 1;
                break;

                case $editor === 'editor':
                $stat = $user[0]->userStat;
                break;

                default:
                $stat = BaseFunctions::validateUserInput($formData->userStat);
                break;
        }

        if ($passCheck) {
            $sendData = [
                'userName' => BaseFunctions::validateUserInput($formData->userName),
                'userEmail' => $formData->userEmail,
                'userAccess' => count($users) == 1 ? 1 : BaseFunctions::validateUserInput($formData->userAccess),
                'userPass' => !empty($formData->userNewPass) ? password_hash(BaseFunctions::validateUserInput($formData->userNewPass), PASSWORD_DEFAULT) : $user[0]->userPass,
                'userStat' => $stat
            ];

            if (count($users) == 1 && ($formData->userAccess != 1 || $formData->userStat != 1 || ($formData->userAccess != 1 && $formData->userStat != 1))) {
                echo 'minUserFail';
            }
            else {
                
                if($this->session->has_userdata('adminFail')) {
                    $this->session->unset_userdata('adminFail');
                }
                $this->Data->updateData('users', $sendData, ['userId' => $userId], 1);
                echo $this->Data->dbResponse;
            }            
        } else {
            echo 'accessFail';
        }
    }

    // Felhasználó törlése
    public function deleteUser($userId)
    {
        $admin = $this->Data->getData('users', 'userPass', ['userAccess' => 1], false, 1);
        $adminPass = count($admin) > 0 ? $admin[0]->userPass : null;
        $user = $this->Data->getData('users', false, ['userId' => $userId], false, 1);
        $users = $this->Data->getData('users');

        if (count($users) > 1 && $user[0]->userAccess != 1 && $adminPass != null) {
            $this->Data->deleteData('users', ['userId' => $userId], 1);
            echo(empty($this->Data->dbResponse)) ? 'success' : 'error';
        } else {
            switch (true) {

                case count($users) == 1:
                echo 'minUserFail';
                break;

                case $user[0]->userAccess == 1:
                echo 'delAdminFail';
                break;

                case $adminPass == null:
                echo 'accessFail';
                break;
            }
        }
    }
}
