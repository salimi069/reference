<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{    

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data');
        $this->load->library('session');        
    }

    /** Főoldal */
    public function index()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/pages/home');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location: ' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** Bejelentkezés */
    public function login()
    {
        $this->load->view('admin/templates/head');
        $this->load->view('admin/pages/login');
        $this->load->view('admin/templates/footer');
    }

    public function userLogin()
    {
        $data = json_decode($_POST['data']);
        $user = $this->Data->getData('users', 'userId, userAccess, userPass, userStat', ['userEmail' => $data->userEmail], false, 1);        
        
        if (count($user) > 0) {
            if (password_verify($data->userPass, $user[0]->userPass)) {
                if ($user[0]->userStat == 1) {
                    $sessionData = [
                        'userId' => $user[0]->userId,
                        'userAccess' => $user[0]->userAccess
                    ];                    
                    $this->session->set_userdata($sessionData);                    
                    echo 'success';
                } else {
                    echo 'accessFail';
                }
            } else {
                echo 'passFail';
            }
        } else {
            echo 'userFail';
        }
    }

    /** Kijelentkezés */
    public function userLogout()
    {
        if ($this->session->has_userdata('userId')) {

            $adminCheck = $this->Data->getData('users', false, ['userAccess' => 1], false, 1);

            if(count($adminCheck) == 0) {
                $this->session->set_userdata('adminFail', 'Yes');
                header('Location: ' . base_url() . 'index.php/admin/user');
            }
            else {
                if ($this->session->has_userdata('adminFail')) {
                    $this->session->unset_userdata('adminFail');
                }
                $this->session->unset_userdata('userId');
                $this->session->unset_userdata('userAccess');
                $this->session->sess_destroy();
                header('Location: ' . base_url() . 'index.php/admin/home/login');
            }
        }
    }
}
