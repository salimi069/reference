<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Content extends CI_Controller
{
    public $menus = null;
    public $categories = null;
    public $articles = null;
    public $menuPos = null;
    public $imgRootFolder = null;
    public $images = null;
    private $imgFileType = ['jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'png', 'PNG'];

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data');
        $this->load->library('table');
        $this->load->library('session');
        require_once('assets/php/admin/BaseFunctions.php');
        $this->imgRootFolder = 'assets/uploads/images/articles/';
    }
    
    /** FŐOLDAL */

    public function index()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');
            $this->load->view('admin/pages/content');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    /** KATEGÓRIÁK */

    // Kategória készítése
    public function createCategory()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');

            // Menüpontok lekérdezése
            $this->menus = $this->Data->getData('menu', false, false, 'menuTitle ASC');

            $this->load->view('admin/pages/createcategory');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Kategória mentése
    public function saveCategory()
    {
        $formData = json_decode($_POST['data']);
        $catMenuAlias = BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData->catMenuTitle));
        $nameCheckError = true;
                
        // Kategória menünév ellenőrzése (kategóriák és cikkek)
        $catMenuAliasCheck = $this->Data->getData('category', 'catId', ['catMenuAlias' => $catMenuAlias], false, 1);
        $artMenuAliasCheck = $this->Data->getData('article', 'artId', ['artMenuAlias' => $catMenuAlias], false, 1);
        $nameCheckError = count($catMenuAliasCheck) == 0 && count($artMenuAliasCheck) == 0 ? false : true;

        if (!$nameCheckError) {

            // Menüpozíció ellenőrzése és frissítése, ha szükséges
            $catMenuPos = BaseFunctions::validateUserInput($formData->catMenuPos);
            $catParentMenu = BaseFunctions::validateUserInput($formData->catParentMenu);
            $menuItems = [];
            $savedCats = $this->Data->getData('category', 'catMenuAlias', ['catParentMenu' => $catParentMenu]);
            $savedArts = $this->Data->getData('article', 'artMenuAlias', ['artParentMenu' => $catParentMenu]);

            if (count($savedCats) > 0) {
                foreach ($savedCats as $cat) {
                    $menuItems[] = $cat;
                }
            }

            if (count($savedArts) > 0) {
                foreach ($savedArts as $art) {
                    $menuItems[] = $art;
                }
            }
            $menuItemsAmount = count($menuItems);
            
            if ($catMenuPos <= $menuItemsAmount) {
                // Kategóriák frissítése
                $savedCatMenuPos = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos >= '$catMenuPos' AND catParentMenu = '$catParentMenu'", 'catMenuPos ASC');
                
                if (!empty($savedCatMenuPos)) {
                    foreach ($savedCatMenuPos as $savedCat) {
                        $this->Data->updateData('category', ['catMenuPos' => ++$savedCat->catMenuPos], ['catId' => $savedCat->catId], 1);
                    }
                }
                // Cikkek frissítése
                $savedArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$catMenuPos' AND artParentMenu = '$catParentMenu'", 'artMenuPos ASC');

                if (!empty($savedArtMenuPos)) {
                    foreach ($savedArtMenuPos as $savedArt) {
                        $this->Data->updateData('article', ['artMenuPos' => ++$savedArt->artMenuPos], ['artId' => $savedArt->artId], 1);
                    }
                }
            }

            $data = [
            'catTitle' => BaseFunctions::validateUserInput($formData->catTitle),
            'catTitleAlias' => BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData->catTitle)),
            'catParentMenu' => $catParentMenu,
            'catMenuTitle' => BaseFunctions::validateUserInput($formData->catMenuTitle),
            'catMenuAlias' => $catMenuAlias,
            'catMenuPos' => $catMenuPos,
            'catStat' => BaseFunctions::validateUserInput($formData->catStat)
        ];

            $this->Data->saveData('category', $data);
            echo $this->Data->dbResponse;
        } else {
            switch (true) {

                case count($catMenuAliasCheck) > 0:
                echo 'catNameFail';
                break;

                case count($artMenuAliasCheck) > 0:
                echo 'artNameFail';
                break;
            }
        }
    }
    
    // Kategória kezelése
    public function manageCategory()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');

            $this->categories = $this->Data->getData('category', false, false, 'catMenuPos ASC');

            if (count($this->categories) > 0) {
                $counter = 1;
                $template = ['table_open' => '<table class="w3-table-all">'];
                $this->table->set_template($template);
                $this->table->set_heading('No.', 'Cím', 'Menü', 'Menüpont', 'Pozíció', 'Státusz', 'Kezelés');

                foreach ($this->categories as $cat) {
                    $stat = $cat->catStat == 1 ? 'Aktív' : 'Inaktív';
                    $catMenu = $this->Data->getData('menu', 'menuTitle', ['menuId' => $cat->catParentMenu], false, 1);
                    $this->table->add_row($counter++, $cat->catTitle, $catMenu[0]->menuTitle, $cat->catMenuTitle, $cat->catMenuPos, $stat, '<a href="' . base_url() . 'index.php/admin/content/updatecategory/' . $cat->catId . '" class="w3-btn w3-orange w3-round">Kezelés</a>');
                }
            }

            $this->load->view('admin/pages/managecategory');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }
    
    // Kategória frissítése
    public function updateCategory($catId)
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');

            $this->categories = $this->Data->getData('category', false, ['catId' => $catId], false, 1);
            $this->menus = $this->Data->getData('menu', 'menuId, menuTitle', false, 'menuTitle ASC');

            $this->load->view('admin/pages/updatecategory');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Kategória frissítésének mentése
    public function saveCatUpdate($catId)
    {
        $formData = json_decode($_POST['data']);
        $category = $this->Data->getData('category', false, ['catId' => $catId], false, 1);
        $oldCatMenuPos = $category[0]->catMenuPos;
        $oldCatParentMenu = $category[0]->catParentMenu;
        $catMenuAlias = BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData->catMenuTitle));
        $nameCheckError = true;

        // Kategória menünév ellenőrzés (kategóriák és cikkek)
        $catMenuAliasCheck = $this->Data->getData('category', false, "catMenuAlias = '$catMenuAlias' AND catId != '$catId'", false, 1) ;
        $artMenuAliasCheck = $this->Data->getData('article', false, ['artMenuAlias' => $catMenuAlias], false, 1);
        $nameCheckError = count($catMenuAliasCheck) == 0 && count($artMenuAliasCheck) == 0 ? false : true;

        if (!$nameCheckError) {

            // Menüpozíció ellenőrzése és szükség szerint frissítése
            if ($formData->catParentMenu !== $category[0]->catParentMenu || $formData->catMenuPos !== $category[0]->catMenuPos) {
                $menuItems = [];
                $savedCats = $this->Data->getData('category', 'catMenuAlias', ['catParentMenu' => $formData->catParentMenu]);
                $savedArts = $this->Data->getData('article', 'artMenuAlias', ['artParentMenu' => $formData->catParentMenu]);

                if (count($savedCats) > 0) {
                    foreach ($savedCats as $cat) {
                        $menuItems[] = $cat;
                    }
                }
    
                if (count($savedArts) > 0) {
                    foreach ($savedArts as $art) {
                        $menuItems[] = $art;
                    }
                }
                $menuItemsAmount = count($menuItems);

                if ($formData->catMenuPos <= $menuItemsAmount || $formData->catParentMenu !== $category[0]->catParentMenu) {
                    if ($formData->catParentMenu === $category[0]->catParentMenu) { // Azonos menün belül
                        $targetCatMenuPos = $this->Data->getData('category', 'catId', "catMenuPos = '$formData->catMenuPos' AND catParentMenu = '$formData->catParentMenu'", false, 1);
                        $targetArtMenuPos = $this->Data->getData('article', 'artId', "artMenuPos = '$formData->catMenuPos' AND artParentMenu = '$formData->catParentMenu'", false, 1);

                        switch (true) {

                            case !empty($targetCatMenuPos):
                            $targetCatId = $targetCatMenuPos[0]->catId;
                            $this->Data->updateData('category', ['catMenuPos' => $oldCatMenuPos], ['catId' => $targetCatId], 1);
                            break;

                            case !empty($targetArtMenuPos):
                            $targetArtId = $targetArtMenuPos[0]->artId;
                            $this->Data->updateData('article', ['artMenuPos' => $oldCatMenuPos], ['artId' => $targetArtId], 1);
                        }
                    } else { // Menüváltás esetén
                        // Új menü kategóriának frissítése
                        $newMenuCats = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos >= '$formData->catMenuPos' AND catParentMenu = '$formData->catParentMenu'");

                        if (!empty($newMenuCats)) {
                            foreach ($newMenuCats as $newCats) {
                                $this->Data->updateData('category', ['catMenuPos' => ++$newCats->catMenuPos], ['catId' => $newCats->catId], 1);
                            }
                        }

                        // Új menü cikkeinek frissítése
                        $newMenuArts = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$formData->catMenuPos' AND artParentMenu = '$formData->catParentMenu'");

                        if (!empty($newMenuArts)) {
                            foreach ($newMenuArts as $newArts) {
                                $this->Data->updateData('article', ['artMenuPos' => ++$newArts->artMenuPos], ['artId' => $newArts->artId], 1);
                            }
                        }
                        
                        // Régi menü kategóriáinak frissítése
                        $oldMenuCats = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos > '$oldCatMenuPos' AND catParentMenu = '$oldCatParentMenu'");

                        if (!empty($oldMenuCats)) {
                            foreach ($oldMenuCats as $oldCat) {
                                $this->Data->updateData('category', ['catMenuPos' => --$oldCat->catMenuPos], ['catId' => $oldCat->catId], 1);
                            }
                        }

                        // Régi menü cikkeinek frissítése
                        $oldMenuArts = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$oldCatMenuPos' AND artParentMenu = '$oldCatParentMenu'");

                        if (!empty($oldMenuArts)) {
                            foreach ($oldMenuArts as $oldArts) {
                                $this->Data->updateData('article', ['artMenuPos' => --$oldArts->artMenuPos], ['artId' => $oldArts->artId], 1);
                            }
                        }
                    }
                }
            }

            $data = [
            'catTitle' => BaseFunctions::validateUserInput($formData->catTitle),
            'catTitleAlias' => BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData->catTitle)),
            'catParentMenu' => BaseFunctions::validateUserInput($formData->catParentMenu),
            'catMenuTitle' => BaseFunctions::validateUserInput($formData->catMenuTitle),
            'catMenuAlias' => $catMenuAlias,
            'catMenuPos' => BaseFunctions::validateUserInput($formData->catMenuPos),
            'catStat' => BaseFunctions::validateUserInput($formData->catStat)
        ];
            $this->Data->updateData('category', $data, ['catId' => $catId], 1);
            echo $this->Data->dbResponse;
        } else {
            switch (true) {

                case count($catMenuAliasCheck) > 0:
                echo 'catNameFail';
                break;

                case count($artMenuAliasCheck) > 0:
                echo 'artNameFail';
                break;
            }
        }
    }

    // Kategória törlése
    public function deleteCategory($catId)
    {
        // Menüpontok átszámozása
        $category = $this->Data->getData('category', 'catParentMenu, catMenuPos', ['catId' => $catId], false, 1);
        $catParentMenu = $category[0]->catParentMenu;
        $catMenuPos = $category[0]->catMenuPos;
        $catMenuItems = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos > '$catMenuPos' AND catParentMenu = '$catParentMenu'");
        $artMenuItems = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$catMenuPos' AND artParentMenu = '$catParentMenu'");
        $catArtItems = $this->Data->getData('article', false, ['artParentCat' => $catId], false, 1);

        if (!empty($catMenuItems)) {
            foreach ($catMenuItems as $catItems) {
                $this->Data->updateData('category', ['catMenuPos' => --$catItems->catMenuPos], ['catId' => $catItems->catId], 1);
            }
        }

        if (!empty($artMenuItems)) {
            foreach ($artMenuItems as $artItems) {
                $this->Data->updateData('article', ['artMenuPos' => --$artItems->artMenuPos], ['artId' => $artItems->artId], 1);
            }
        }

        if (count($catArtItems) == 0) {
            $this->Data->deleteData('category', ['catId' => $catId], 1);
            echo(empty($this->Data->dbResponse)) ? 'success' : 'error';
        } else {
            echo 'articleFail';
        }
    }
    
    /** CIKKEK */

    // Cikk készítése
    public function createArticle()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');
        
            // Kategóriák lekérdezése
            $this->categories = $this->Data->getData('category', 'catId, catTitle', false, 'catTitle ASC');
            // Menük lekérdezése
            $this->menus = $this->Data->getData('menu', 'menuId, menuTitle', false, 'menuTitle ASC');

            $this->load->view('admin/pages/createarticle');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Cikk mentése
    public function saveArticle()
    {
        $formData = $_POST;
        $img = !empty($_FILES['artImg']) ? $_FILES['artImg'] : '';
        $artMenuAlias = BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData['artMenuTitle']));
        $artMenuTitleCheck = false;

        // Menüpont nevének ellenőrzése
        $catMenuTitle = $this->Data->getData('category', 'catId', ['catMenuAlias' => $artMenuAlias], false, 1);
        $artMenuTitle = $this->Data->getData('article', 'artId', ['artMenuAlias' => $artMenuAlias], false, 1);
        $artMenuTitleCheck = count($catMenuTitle) == 0 && count($artMenuTitle) == 0 ? true : false;

        if ($artMenuTitleCheck) {

            // Menüpozíciók ellenőrzése és szükség szerinti frissítése
            $newArtMenuPos = $formData['artMenuPos'];

            if (!empty($formData['artParentCat'])) { // Kategória esetén
                $catMenuItems = $this->Data->getData('article', 'artId, artMenuPos', ['artParentCat' => $formData['artParentCat']]);

                if (count($catMenuItems) > 0) {
                    $catMenuItemsNum = count($catMenuItems);

                    if ($formData['artMenuPos'] <= $catMenuItemsNum) {
                        $newArtParentCat = $formData['artParentCat'];
                        $increaseMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$newArtMenuPos' AND artParentCat = '$newArtParentCat'");

                        if (count($increaseMenuPos) > 0) {
                            foreach ($increaseMenuPos as $incMenuPos) {
                                $this->Data->updateData('article', ['artMenuPos' => ++$incMenuPos->artMenuPos], ['artId' => $incMenuPos->artId], 1);
                            }
                        }
                    }
                }
            }

            if (!empty($formData['artParentMenu'])) { // Menü esetén
                $newArtParentMenu = $formData['artParentMenu'];
                $parentMenuItemsCat = $this->Data->getData('category', 'catId, catParentMenu', ['catParentMenu' => $newArtParentMenu]);
                $parentMenuItemsArt = $this->Data->getData('article', 'artId, artParentMenu', ['artParentMenu' => $newArtParentMenu]);
                $parentMenuItemsNum = count($parentMenuItemsCat) + count($parentMenuItemsArt);
                
                if ($newArtMenuPos <= $parentMenuItemsNum) {
                    if (count($parentMenuItemsCat) > 0) { // Kategóriák
                        $increaseMenuItemsCat = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos >= '$newArtMenuPos' AND catParentMenu = '$newArtParentMenu'");

                        if (count($increaseMenuItemsCat) > 0) {
                            foreach ($increaseMenuItemsCat as $incMenuItemCat) {
                                $this->Data->updateData('category', ['catMenuPos' => ++$incMenuItemCat->catMenuPos], ['catId' => $incMenuItemCat->catId], 1);
                            }
                        }
                    }

                    if (count($parentMenuItemsArt) > 0) { // Cikkek
                        $increaseMenuItemsArt = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$newArtMenuPos' AND artParentMenu = '$newArtParentMenu'");

                        if (count($increaseMenuItemsArt) > 0) {
                            foreach ($increaseMenuItemsArt as $incMenuItemArt) {
                                $this->Data->updateData('article', ['artMenuPos' => ++$incMenuItemArt->artMenuPos], ['artId' => $incMenuItemArt->artId], 1);
                            }
                        }
                    }
                }
            }

            $imgUploadError = false;

            // Képek mentése
            if (!empty($img['name'][0])) {
                if (count($img['name']) > 0 && count($img['name']) <= 10) {
                    mkdir($this->imgRootFolder . $artMenuAlias);
                    copy($this->imgRootFolder . 'index.html', $this->imgRootFolder . $artMenuAlias . '/index.html');
                    $imgNum = count($img['name']);

                    for ($ci = 0; $ci < $imgNum; $ci++) {
                        if (in_array(pathinfo($img['name'][$ci], PATHINFO_EXTENSION), $this->imgFileType)) {
                            move_uploaded_file($img['tmp_name'][$ci], $this->imgRootFolder . $artMenuAlias . '/' . $img['name'][$ci]);
                        } else {
                            $imgUploadError = true;
                            echo 'imgFileTypeFail';
                        }
                    }
                } else {
                    $imgUploadError = true;
                    echo 'imgNumberFail';
                }
            }

            if (!$imgUploadError) {
                $data = [
                'artTitle' => BaseFunctions::validateUserInput($formData['artTitle']),
                'artTitleAlias' => BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData['artTitle'])),
                'artText' => $formData['artText'],
                'artParentCat' => !empty($formData['artParentCat']) ? BaseFunctions::validateUserInput($formData['artParentCat']) : '',
                'artParentMenu' => !empty($formData['artParentMenu']) ? BaseFunctions::validateUserInput($formData['artParentMenu']) : '',
                'artMenuTitle' => BaseFunctions::validateUserInput($formData['artMenuTitle']),
                'artMenuAlias' => $artMenuAlias,
                'artMenuPos' => BaseFunctions::validateUserInput($formData['artMenuPos']),
                'artKeywords' => !empty($formData['artKeywords']) ? BaseFunctions::validateUserInput($formData['artKeywords']) : '',
                'artDesc' => !empty($formData['artDesc']) ? BaseFunctions::validateUserInput($formData['artDesc']) : '',
                'artStat' => !empty($formData['artStat']) ? BaseFunctions::validateUserInput($formData['artStat']) : ''
            ];
                $this->Data->saveData('article', $data);
                echo $this->Data->dbResponse;
            }
        } else {
            switch (true) {

                case count($catMenuTitle) > 0:
                echo 'catNameFail';
                break;

                case count($artMenuTitle) > 0:
                echo 'artNameFail';
                break;
            }
        }
    }

    // Cikk kezelése
    public function manageArticle()
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');

            $this->articles = $this->Data->getData('article', false, false, 'artTitle ASC');

            if (count($this->articles) > 0) {
                $template = ['table_open' => '<table class="w3-table-all">'];
                $this->table->set_template($template);
                $counter = 1;
                $this->table->set_heading('No.', 'Cím', 'Kategória', 'Szülő menü', 'Menüpont neve', 'Pozíció', 'Státusz', 'Kezelés');

                foreach ($this->articles as $art) {
                    $cat = $this->Data->getData('category', 'catTitle', ['catId' => $art->artParentCat], false, 1);
                    $catTitle = count($cat) > 0 ? $cat[0]->catTitle : 'N/A';
                    $menu = $this->Data->getData('menu', 'menuTitle', ['menuId' => $art->artParentMenu], false, 1);
                    $menuTitle = count($menu) > 0 ? $menu[0]->menuTitle : 'N/A';
                    $stat = $art->artStat == 1 ? 'Aktív' : 'Inaktív';
                    $this->table->add_row($counter++, $art->artTitle, $catTitle, $menuTitle, $art->artMenuTitle, $art->artMenuPos, $stat, '<a href="' . base_url() . 'index.php/admin/content/updatearticle/' . $art->artId . '" class="w3-btn w3-orange">Kezelés</a>');
                }
            }

            $this->load->view('admin/pages/managearticle');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Cikk frissítése
    public function updateArticle($artId)
    {
        if ($this->session->has_userdata('userId')) {
            $this->load->view('admin/templates/head');
            $this->load->view('admin/templates/contentsidebar');

            // Cikkek listája
            $this->articles = $this->Data->getData('article', false, ['artId' => $artId], false, 1);
            // Kategóriák listája
            $this->categories = $this->Data->getData('category', 'catId, catTitle', false, 'catTitle ASC');
            // Menük listája
            $this->menus = $this->Data->getData('menu', 'menuId, menuTitle', false, 'menuTitle ASC');

            // Menüpozíciók listája
            if ($this->articles[0]->artParentCat != 0) {
                $this->menuPos = self::getCategoryItems($this->articles[0]->artParentCat);
            } elseif ($this->articles[0]->artParentMenu != 0) {
                $this->menuPos = self::getMenuItems($this->articles[0]->artParentMenu);
                ksort($this->menuPos, SORT_NUMERIC);
            }
        
            // Képek
            $this->images = [];
            $imgFolders = scandir($this->imgRootFolder);
            $folders = array_diff($imgFolders, ['.', '..', 'index.html']);

            if (in_array($this->articles[0]->artMenuAlias, $folders)) {
                $this->images = array_diff(scandir($this->imgRootFolder . $this->articles[0]->artMenuAlias), ['.', '..', 'index.html']);
            }
                
            $this->load->view('admin/pages/updatearticle');
            $this->load->view('admin/templates/footer');
        } else {
            header('Location:' . base_url() . 'index.php/admin/home/login');
        }
    }

    // Cikk módosításának mentése
    public function saveArtUpdate($artId)
    {
        $art = $this->Data->getData('article', false, ['artId' => $artId], false, 1);
        $formData = $_POST;
        $img = !empty($_FILES['artImg']) ? $_FILES['artImg'] : '';
        $artMenuTitleAlias = BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData['artMenuTitle']));

        // MENÜPONT NEVÉNEK ELLENŐRZÉSE
        $artMenuTitleCheck = true;
        $savedCatMenuTitle = $this->Data->getData('category', false, ['catMenuAlias' => $artMenuTitleAlias], false, 1);
        $savedArtMenuTitle = $this->Data->getData('article', false, "artMenuAlias = '$artMenuTitleAlias' AND artId != '$artId'", false, 1);
        $artMenuTitleCheck = count($savedCatMenuTitle) == 0 && count($savedArtMenuTitle) == 0 ? false : true;

        if (!$artMenuTitleCheck) {

            // MENÜPONT POZÍCIÓ ELLENŐRZÉSE ÉS FRISSÍTÉSE
            $newMenuPos = $formData['artMenuPos'];
            $newCat = !empty($formData['artParentCat']) ? $formData['artParentCat'] : 0;
            $newMenu = !empty($formData['artParentMenu']) ? $formData['artParentMenu'] : 0;
            $oldMenuPos = $art[0]->artMenuPos;
            $oldCat = !empty($art[0]->artParentCat) ? $art[0]->artParentCat : 0;
            $oldMenu = !empty($art[0]->artParentMenu) ? $art[0]->artParentMenu : 0;

            if ($newMenuPos !== $oldMenuPos || $newCat !== $oldCat || $newMenu !== $oldMenu) {
                if ($newCat != 0) {
                    if ($newCat === $oldCat) { // Ugyanazon a kategórián belül
                        $swapMenuPos = $this->Data->getData('article', 'artId', "artMenuPos = '$newMenuPos' AND artParentCat = '$oldCat'", false, 1);
                    
                        if (count($swapMenuPos) > 0) {
                            $this->Data->updateData('article', ['artMenuPos' => $oldMenuPos], ['artId' => $swapMenuPos[0]->artId], 1);
                        }
                    } else {
                        if ($oldCat != 0) { // Különböző kategóriák között
                            $incMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$newMenuPos' AND artParentCat = '$newCat'");

                            if (count($incMenuPos) > 0) { // Új kategória frissítés

                                foreach ($incMenuPos as $pos) {
                                    $this->Data->updateData('article', ['artMenuPos' => ++$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                }
                            }

                            $decMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$oldMenuPos' AND artParentCat = '$oldCat'");

                            if (count($decMenuPos) > 0) { // Régi kategória frissítés

                                foreach ($decMenuPos as $pos) {
                                    $this->Data->updateData('article', ['artMenuPos' => --$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                }
                            }
                        }

                        if ($oldMenu != 0 && $oldCat == 0) { // Menüből kategóriába váltás
                            $decCatMenuPos = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos > '$oldMenuPos' AND catParentMenu = '$oldMenu'");
                            $decArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$oldMenuPos' AND artParentMenu = '$oldMenu'");
    
                            if (count($decCatMenuPos) > 0) {
                                foreach ($decCatMenuPos as $pos) {
                                    $this->Data->updateData('category', ['catMenuPos' => --$pos->catMenuPos], ['catId' => $pos->catId], 1);
                                }
                            }
    
                            if (count($decArtMenuPos) > 0) {
                                foreach ($decArtMenuPos as $pos) {
                                    $this->Data->updateData('article', ['artMenuPos' => --$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                }
                            }
    
                            $incCatMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$newMenuPos' AND artParentCat = '$newCat'");
    
                            if (count($incCatMenuPos) > 0) {
                                foreach ($incCatMenuPos as $pos) {
                                    $this->Data->updateData('article', ['artMenuPos' => ++$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                }
                            }
                        }
                    }
                } else {
                    if ($newMenu != 0) {
                        if ($newMenu === $oldMenu) { // Ugyanazon a menün belül
                            $cat = $this->Data->getData('category', 'catId', "catMenuPos = '$newMenuPos' AND catParentMenu = '$oldMenu'", false, 1);
                            $artic = $this->Data->getData('article', 'artId', "artMenuPos = '$newMenuPos' AND artParentMenu = '$oldMenu'", false, 1);

                            switch (true) {

                                case count($cat) > 0:
                                $this->Data->updateData('category', ['catMenuPos' => $oldMenuPos], ['catId' => $cat[0]->catId], 1);
                                break;

                                case count($artic) > 0:
                                $this->Data->updateData('article', ['artMenuPos' => $oldMenuPos], ['artId' => $artic[0]->artId], 1);
                                break;
                            }
                        } else {
                            if ($oldMenu != 0 && $newMenu != 0) { // Menük között
                                $incArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$newMenuPos' AND artParentMenu = '$newMenu'");
                                $incCatMenuPos = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos >= '$newMenuPos' AND catParentMenu = '$newMenu'");

                                if (count($incArtMenuPos) > 0) { // Új menü frissítése (cikkek)

                                    foreach ($incArtMenuPos as $pos) {
                                        $this->Data->updateData('article', ['artMenuPos' => ++$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                    }
                                }

                                if (count($incCatMenuPos) > 0) { // Új menü frissítése (kategóriák)

                                    foreach ($incCatMenuPos as $pos) {
                                        $this->Data->updateData('category', ['catMenuPos' => ++$pos->catMenuPos], ['catId' => $pos->catId], 1);
                                    }
                                }

                                $decCatMenuPos = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos > '$oldMenuPos' AND catParentMenu = '$oldMenu'");
                                $decArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$oldMenuPos' AND artParentMenu = '$oldMenu'");

                                if (count($decCatMenuPos) > 0) { // Régi menü frissítés (kategóriák)

                                    foreach ($decCatMenuPos as $pos) {
                                        $this->Data->updateData('category', ['catMenuPos' => --$pos->catMenuPos], ['catId' => $pos->catId]);
                                    }
                                }

                                if (count($decArtMenuPos) > 0) { // Régi menü frissítés (cikkek)

                                    foreach ($decArtMenuPos as $pos) {
                                        $this->Data->updateData('article', ['artMenuPos' => --$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                    }
                                }
                            }
                            // Kategóriából menübe
                            if ($oldCat != 0 && $newMenu != 0) {
                                $incArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos >= '$newMenuPos' AND artParentMenu = '$newMenu'");
                                $incCatMenuPos = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos >= '$newMenuPos' AND catParentMenu = '$newMenu'");

                                if (count($incArtMenuPos) > 0) { // Új menü frissítése (cikkek)

                                    foreach ($incArtMenuPos as $pos) {
                                        $this->Data->updateData('article', ['artMenuPos' => ++$pos->artMenuPos], ['artId' => $pos->artId], 1);
                                    }
                                }

                                if (count($incCatMenuPos) > 0) { // Új menü frissítése (kategóriák)

                                    foreach ($incCatMenuPos as $pos) {
                                        $this->Data->updateData('category', ['catMenuPos' => ++$pos->catMenuPos], ['catId' => $pos->catId], 1);
                                    }
                                }

                                $decArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$oldMenuPos' AND artParentCat = '$oldCat'");

                                if (count($decArtMenuPos) > 0) { // Régi kategória frissítése

                                    foreach ($decArtMenuPos as $pos) {
                                        $this->Data->updateData('article', ['artMenuPos' => --$pos->artMenuPos], ['artId' => $pos->artId], 2);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // KÉPEK KEZELÉSE
            $imgUploadError = false;
            $imgFolders = scandir($this->imgRootFolder);

            if ($artMenuTitleAlias !== $art[0]->artMenuAlias) { // Galéria mappanév változás ellenőrzése és szükség esetén átnevezése

                if (in_array($art[0]->artMenuAlias, $imgFolders)) {
                    rename($this->imgRootFolder . $art[0]->artMenuAlias, $this->imgRootFolder . $artMenuTitleAlias);
                }
            }

            if (!empty($img['name'][0])) { // Új képek mentése

                if (count($img['name']) > 0 && count($img['name']) <= 10) {
                    if (!in_array($artMenuTitleAlias, $imgFolders)) {
                        mkdir($this->imgRootFolder . $artMenuTitleAlias);
                        copy($this->imgRootFolder . 'index.html', $this->imgRootFolder . $artMenuTitleAlias . '/index.html');
                    }

                    $imgNum = count($img['name']);
                    $savedImgs = array_diff(scandir($this->imgRootFolder . $artMenuTitleAlias), ['.', '..', 'index.html']);

                    for ($ci = 0; $ci < $imgNum; $ci++) {
                        if (in_array(pathinfo($img['name'][$ci], PATHINFO_EXTENSION), $this->imgFileType)) {
                            if (!in_array($img['name'][$ci], $savedImgs)) {
                                move_uploaded_file($img['tmp_name'][$ci], $this->imgRootFolder . $artMenuTitleAlias . '/' . $img['name'][$ci]);
                            } else {
                                $imgUploadError = true;
                                echo 'imgFileNameFail';
                            }
                        } else {
                            $imgUploadError = true;
                            echo 'imgFileTypeFail';
                        }
                    }
                } else {
                    $imgUploadError = true;
                    echo 'imgNumberFail';
                }
            }
            
            if (!$imgUploadError) {
                $data = [
                    'artTitle' => BaseFunctions::validateUserInput($formData['artTitle']),
                    'artTitleAlias' => BaseFunctions::createAlias(BaseFunctions::validateUserInput($formData['artTitle'])),
                    'artText' => $formData['artText'],
                    'artParentCat' => !empty($formData['artParentCat']) ? BaseFunctions::validateUserInput($formData['artParentCat']) : '',
                    'artParentMenu' => !empty($formData['artParentMenu']) ? BaseFunctions::validateUserInput($formData['artParentMenu']) : '',
                    'artMenuTitle' => BaseFunctions::validateUserInput($formData['artMenuTitle']),
                    'artMenuAlias' => $artMenuTitleAlias,
                    'artMenuPos' => BaseFunctions::validateUserInput($formData['artMenuPos']),
                    'artKeywords' => !empty($formData['artKeywords']) ? BaseFunctions::validateUserInput($formData['artKeywords']) : '',
                    'artDesc' => !empty($formData['artDesc']) ? BaseFunctions::validateUserInput($formData['artDesc']) : '',
                    'artStat' => !empty($formData['artStat']) ? BaseFunctions::validateUserInput($formData['artStat']) : ''
                ];
        
                $this->Data->updateData('article', $data, ['artId' => $artId], 1);
                echo $this->Data->dbResponse;
            }
        } else {
            switch (true) {

                case count($savedCatMenuTitle) > 0:
                echo 'catNameFail';
                break;

                case count($savedArtMenuTitle) > 0:
                echo 'artNameFail';
                break;
            }
        }
    }

    /** Képek törlése */
    public function deleteImage($action, $imgFolder = false)
    {
        $images = $action === 'selected' ? json_decode($_POST['img']) : '';
        $folder = $imgFolder ? $imgFolder : $_POST['folder'];

        switch ($action) {

            case 'selected':

            foreach ($images as $img) {
                unlink($this->imgRootFolder . $folder . '/' . $img);
            }
            echo 'success';
            break;

            case 'all':
            array_map('unlink', array_filter((array) glob($this->imgRootFolder . $folder . '/*')));
            rmdir($this->imgRootFolder . $folder);
            
            if (!$imgFolder) {
                echo 'success';
            }
            break;

        }
    }

    /** Cikk törlése */
    public function deleteArticle($artId)
    {
        $art = $this->Data->getData('article', false, ['artId' => $artId], false, 1);
        $artMenuPos = $art[0]->artMenuPos;
        $artParentCat = $art[0]->artParentCat;
        $artParentMenu = $art[0]->artParentMenu;

        if ($art[0]->artParentCat != 0) { // Kategória frissítése
            $decCatMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$artMenuPos' AND artParentCat = '$artParentCat'");

            if (count($decCatMenuPos) > 0) {
                foreach ($decCatMenuPos as $pos) {
                    $this->Data->updateData('article', ['artMenuPos' => --$pos->artMenuPos], ['artId' => $pos->artId], 1);
                }
            }
        } elseif ($art[0]->artParentMenu != 0) { // Menü frissítése
            $decArtMenuPos = $this->Data->getData('article', 'artId, artMenuPos', "artMenuPos > '$artMenuPos' AND artParentMenu = '$artParentMenu'");
            $decCatMenuPos = $this->Data->getData('category', 'catId, catMenuPos', "catMenuPos > '$artMenuPos' AND catParentMenu = '$artParentMenu'");

            if (count($decArtMenuPos) > 0) {
                foreach ($decArtMenuPos as $pos) {
                    $this->Data->updateData('article', ['artMenuPos' => --$pos->artMenuPos], ['artId' => $pos->artId], 1);
                }
            }

            if (count($decCatMenuPos) > 0) {
                foreach ($decCatMenuPos as $pos) {
                    $this->Data->updateData('category', ['catMenuPos' => --$pos->catMenuPos], ['catId' => $pos->catId], 1);
                }
            }
        }

        // Galéria törlése
        $imgFolder = scandir($this->imgRootFolder);

        if (in_array($art[0]->artMenuAlias, array_diff($imgFolder, ['.', '..', 'index.html']))) {
            self::deleteImage('all', $art[0]->artMenuAlias);
        }

        $this->Data->deleteData('article', ['artId' => $artId], 1);
        echo(empty($this->Data->dbResponse)) ? 'success' : 'error';
    }

    /** Szülőmenü pontjainak lekérdezése (menüpont sorrend) */
    public function getMenuItems($menuId, $ajax = false)
    {
        $menuItems = [];
        
        $article = $this->Data->getData('article', 'artMenuTitle, artMenuPos', ['artParentMenu' => $menuId], 'artMenuPos ASC');

        foreach ($article as $art) {
            $menuItems[$art->artMenuPos] = $art->artMenuTitle;
        }

        $category = $this->Data->getData('category', 'catMenuTitle, catMenuPos', ['catParentMenu' => $menuId], 'catMenuPos ASC');

        foreach ($category as $cat) {
            $menuItems[$cat->catMenuPos] = $cat->catMenuTitle;
        }

        if ($ajax) {
            echo json_encode($menuItems);
        } else {
            return $menuItems;
        }
    }

    /** Kategóriák elemeinek a lekérdezése */
    public function getCategoryItems($catId, $ajax = false)
    {
        $catItemsRes = [];
        $catItems = $this->Data->getData('article', 'artMenuPos, artMenuTitle', ['artParentCat' => $catId], 'artMenuPos ASC');

        if (count($catItems) > 0) {
            foreach ($catItems as $catItem) {
                $catItemsRes[] = [
                    'artMenuPos' => $catItem->artMenuPos,
                    'artMenuTitle' => $catItem->artMenuTitle
                ];
            }
        }

        if ($ajax) {
            echo json_encode($catItemsRes);
        } else {
            return $catItemsRes;
        }
    }
}
