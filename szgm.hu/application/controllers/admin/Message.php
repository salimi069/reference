<?php
defined('BASEPATH') or exit('No direct script access allowed');

/** Ajánlatkérés */
class Message extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();        
        $this->load->library('email');
    }

    public function sendMessage()
    {
        $data = json_decode($_POST['data']);
        
        $this->cifence->check($data->sec, $data->hp);

        if ($this->cifence->passed) {
            $copySent = $data->copy == 1 ? 'Igen' : 'Nem';

            $msg = '<!DOCTYPE html><html><head><meta charset="utf-8" /></head><body>';
            $msg .= "Név: {$data->name}<br />";
            $msg .= "E-mail: <a href=\"mailto:{$data->email}\">{$data->email}</a><br />";
            $msg .= "Telefon: {$data->phone}<br />";
            $msg .= "Autó: {$data->type}";
            $msg .= "<p>Üzenet:</p><p>{$data->msg}</p>";
            $msg .= "Másolat elküldve: {$copySent}<br />";
            $msg .= 'Küldés dátuma: ' . date('Y-m-d H:i:s');
            $msg .= '</body></html>';

            $config = [
                'charset' => 'utf-8',
                'mailtype' => 'html'
            ];
            $this->email->initialize($config);

            $this->email->from('info@szgm.hu', 'Szendrei Gépműhely - Ajánlatkérés');
            $this->email->to('info@szgm.hu');
            $data->copy == 1 ? $this->email->cc($data->email) : '';
            $this->email->subject('Szendrei Gépműhely - Ajánlatkérés');
            $this->email->message($msg);

            echo($this->email->send()) ? 'success' : 'Hiba! Az üzenet nem került elküldésre!';
        } else {
            echo 'A biztonsági kérdésre adott válasz helytelen. Kérem, javítsa!';
        }
    }
}
