<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="content-placeholder" class="w3-row">
    <main id="content" class="w3-row w3-padding">
        <?php if(!empty($this->artText[0]->artText)) { ?>
        <header class="w3-card-4 w3-padding w3-margin">
            <h1><?php echo $this->artText[0]->artTitle; ?></h1>
        </header>
        <?php
        echo '<div class="w3-row w3-padding">' . $this->artText[0]->artText . '</div>';    
    }
    else {
        echo '<div class="w3-row w3-center"><img src="' . base_url() . 'assets/uploads/images/available_soon.png" alt="Feltöltés alatt" title="Feltöltés alatt" /><p>Feltöltés alatt</p></div>';
    } ?>
    </main>
</div>