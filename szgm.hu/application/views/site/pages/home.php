<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <div id="banner-placeholder" class="w3-row">
            <img id="banner-bg" class="w3-image" src="<?php echo base_url(); ?>assets/uploads/images/henger40.jpg" />
            <section id="banner-content" class="w3-container">
                <figure id="banner-logo" class="w3-quarter"><img
                        src="<?php echo base_url(); ?>assets/uploads/images/logo_trans.png" class="w3-image"
                        alt="Logo" /></figure>
                <div id="banner-text" class="w3-rest">
                    <article id="banner-motto" class="w3-container w3-center w3-margin w3-padding">Professzionális
                        motor- és hengerfej felújítás több évtizedes tapasztalattal</article>
                    <figure class="w3-container w3-center"><button
                            class="w3-btn w3-red offer-btn w3-xxlarge offerBtn w3-margin-top">Ajánlatot kérek</button>
                    </figure>
                </div>
            </section>
        </div>
        <div id="ref-placeholder" class="w3-row">
            <article id="ref" class="w3-responsive">
                <?php
            if(count($this->refImgs) > 0) {                
                echo $this->table->generate();
            }
            ?>
            </article>
        </div>
        <div id="service1-placeholder" class="w3-row service-div">
            <div id="service1-img" class="w3-half w3-mobile serv-img w3-hide-small"></div>
            <article id="service1-text" class="w3-half w3-mobile w3-padding">
                <?php echo(!empty($this->service1[0]->artText)) ? $this->service1[0]->artText : ''; ?>
            </article>
        </div>
        <div id="service2-placeholder" class="w3-row service-div">
            <article id="service2-text" class="w3-half w3-mobile w3-padding">
                <?php echo(!empty($this->service2[0]->artText)) ? $this->service2[0]->artText : ''; ?>
            </article>
            <div id="service2-img" class="w3-half w3-mobile serv-img w3-hide-small"></div>
        </div>