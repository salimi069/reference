<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <title>Szendrei Gépműhely</title>
    <meta name="author" content="Szendrei Gépműhely">
    <meta name="generator" content="CodeIgniter 3.1.11">
    <meta name="keywords" content="<?php echo $this->metaKeywords; ?>">
    <meta name="description"
        content="<?php echo $this->metaDesc; ?>">
    <meta name="robots" content="index, follow">
    <meta property="og:url" content="https://www.szgm.hu/" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Szendrei Gépműhely" />
    <meta property="og:description" content="Szendrei Gépműhely - Hengerfej és motorfelújítás 1988 óta" />
    <meta property="og:image" content="<?php echo base_url(); ?>assets/uploads/images/logo.jpg" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/uploads/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/uploads/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style/w3.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script><!--Telefon ikon-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style/siteStyle.css" />
    <script src="<?php echo base_url(); ?>assets/js/libs/jquery-3.4.1.min.js"></script>
</head>

<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v5.0">
    </script>
    <div id="wrapper" class="w3-row">
        <div id="nav" class="w3-row">
            <div id="motto-placeholder" class="w3-row w3-padding">
                <section id="motto-data" class="w3-row">
                    <article id="motto-text" class="w3-third w3-left w3-left motto">
                        Szendrei Gépműhely - Hengerfej és Motorfelújítás<br />
                        <strong>Budapest területén ingyenes házhoz szállítás!</strong>
                    </article>
                    <article id="motto-address" class="w3-third w3-center motto">
                        Szervíz: 1144 Budapest, Rátót utca 9.<br />
                        Iroda: 1161 Budapest, Szent Korona utca 99.</article>
                    <article id="motto-tel" class="w3-third w3-right w3-padding-16 motto"><i style='font-size:24px' class='fas fa-phone'></i>&nbsp;<a href="tel:+36309762696">+36 (30) 976 2696</a>
                    </article>
                </section>
            </div>           
            <div id="nav-placeholder" class="w3-row-padding w3-margin-top w3-margin-bottom">
                <nav id="hor-menu" class="w3-bar w3-border w3-green w3-large w3-col l10 m10 s6">
                    <a href="<?php echo base_url(); ?>" class="w3-bar-item w3-button w3-round">Főoldal</a>
                    <div id="menu-body">
                    <?php 
                    if(count($this->menuContent) > 0) {

                        foreach($this->menuContent as $menuItem) {                                                                                  
                            
                            if (isset($menuItem['artParentCat'])) {
                                echo '<a href="' . base_url() . 'index.php/home/' . $menuItem['artMenuAlias'] . '" class="w3-bar-item w3-button w3-hide-small w3-round">' . $menuItem['artMenuTitle'] . '</a>';
                            }
                            else { ?>
                    <div class="w3-dropdown-hover w3-hide-small">
                        <button class="w3-button w3-round"><?php echo $menuItem['catMenuTitle']; ?></button>
                        <div class="w3-dropdown-content w3-bar-block w3-card-4">
                            <?php                                
                                foreach($menuItem as $catItem) {
                                    
                                    if (is_array($catItem)) {
                                        echo '<a href="' . base_url() . 'index.php/home/' . $catItem['artMenuAlias'] . '" class="w3-bar-item w3-button w3-hide-small catMenuItem">' . $catItem['artMenuTitle'] . '</a>';
                                    } 
                                } ?>
                        </div>
                    </div>
                    <?php }                                 
                        }
                    }
                    ?>
                    <!--<a href="#" class="w3-bar-item w3-button w3-round w3-hide-small offer-btn">Ajánlatkérés</a>-->
                    <a href="#" class="w3-bar-item w3-button  w3-round w3-hide-small" onclick="$('#footer-data').animatescroll({padding: 160});">Kapcsolat</a>
                </div>
                    <button id="mobileMenuOpen" class="w3-bar-item w3-red w3-button w3-right w3-round w3-hide-large w3-hide-medium">Menü</button>
                </nav>
                <div id="mobil-menu" class="w3-bar-block w3-hide">                
                <?php 
                    if(count($this->menuContent) > 0) {

                        foreach($this->menuContent as $menuItem) {                                                                                  
                            
                            if (isset($menuItem['artParentCat'])) {
                                echo '<a href="' . base_url() . 'index.php/home/' . $menuItem['artMenuAlias'] . '" class="w3-bar-item w3-button w3-round">' . $menuItem['artMenuTitle'] . '</a>';
                            }
                            else { ?>
                    <div class="w3-dropdown-hover">
                        <button class="w3-button w3-round"><?php echo $menuItem['catMenuTitle']; ?></button>
                        <div class="w3-dropdown-content w3-bar-block w3-card-4">
                            <?php                                
                                foreach($menuItem as $catItem) {
                                    
                                    if (is_array($catItem)) {
                                        echo '<a href="' . base_url() . 'index.php/home/' . $catItem['artMenuAlias'] . '" class="w3-bar-item w3-button">' . $catItem['artMenuTitle'] . '</a>';
                                    } 
                                } ?>
                        </div>
                    </div>
                    <?php }                                 
                        }
                    }
                    ?>
                    <a href="#" class="w3-bar-item w3-button w3-round offer-btn">Ajánlatkérés</a>
                    <a href="#" class="w3-bar-item w3-button  w3-round" onclick="$('#footer-data').animatescroll({padding: 160});">Kapcsolat</a>
                </div>
                <?php echo(!empty($this->page)) ? '<article id="request-offer" class="w3-col l2 m2 s6 w3-mobile"><button id="offer-btn-req" class="w3-btn w3-red w3-large offer-btn">Ajánlatot kérek</button></article>' : ''; ?>
            </div>
        </div>        