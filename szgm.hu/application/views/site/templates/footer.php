<footer id="footer" class="w3-row">
    <span id="validationStatus" class="sys-msg"></span>
    <span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
    <section id="footer-data" class="w3-row">
        <article id="fb-news" class="w3-third w3-mobile w3-center footerData">
            <header class="w3-card-4 w3-padding w3-margin header w3-round">
                <h3>FACEBOOK</h3>
            </header>
            <div class="w3-row">
                <div class="fb-page" data-href="https://www.facebook.com/www.szgm.hu/" data-tabs="timeline"
                    data-width="" data-height="" data-small-header="false" data-adapt-container-width="true"
                    data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/www.szgm.hu/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/www.szgm.hu/">Szendrei hengerfej és motorfelújítás</a>
                    </blockquote>
                </div>
            </div>
            <div class="w3-row w3-padding-16">
                <div class="fb-share-button" data-href="https://www.facebook.com/www.szgm.hu/" data-layout="button"
                    data-size="large"><a target="_blank"
                        href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2Fwww.szgm.hu%2F&amp;src=sdkpreparse"
                        class="fb-xfbml-parse-ignore">Megosztás</a></div>
            </div>
        </article>
        <article id="links" class="w3-third w3-mobile footerData">
            <header class="w3-card-4 w3-padding w3-margin header w3-round w3-center">
                <h3>HONLAPTÉRKÉP</h3>
            </header>
            <ul>
                <li><a href="<?php echo base_url(); ?>">Főoldal</li>
                <?php 
                    if(count($this->menuContent) > 0) {

                        foreach($this->menuContent as $menuItem) {                                                                                  
                            
                            if (isset($menuItem['artParentCat'])) {
                                echo '<li><a href="' . base_url() . 'index.php/home/' . $menuItem['artMenuAlias'] . '">' . $menuItem['artMenuTitle'] . '</a></li>';
                            }
                            else { ?>
                <li>
                    <div class="w3-dropdown-hover"><button class="w3-button mapCat">
                            <?php echo $menuItem['catMenuTitle'];
                            echo '&nbsp;<i class="fa fa-caret-down w3-right"></i></button><div class="w3-dropdown-content w3-bar-block mapCatContent">';                                 
                                                                
                                foreach($menuItem as $catItem) {
                                    
                                    if (is_array($catItem)) {
                                        echo '<a class="w3-bar-item" href="' . base_url() . 'index.php/home/' . $catItem['artMenuAlias'] . '">' . $catItem['artMenuTitle'] . '</a>';
                                    } 
                                } 
                                echo '</div></div></li>';
                            }                                 
                        }
                    }
                    ?>
                <li><a href="#" class="offer-btn">Ajánlatkérés</a></li>
                <li><a href="#" onclick="$('#footer-data').animatescroll({padding: 126});">Kapcsolat</a></li>
            </ul>
        </article>
        <article id="contact" class="w3-third w3-mobile w3-center w3-margin-bottom footerData">
            <header class="w3-card-4 w3-padding w3-margin header w3-round">
                <h3>KAPCSOLAT</h3>
            </header>
            <div id="address" class="w3-col w3-mobile">
                <?php echo(!empty($this->contact[0]->artText)) ? $this->contact[0]->artText : ''; ?>
                <button id="offer-btn-footer" class="w3-btn w3-grey w3-large offer-btn offerBtn">Ajánlatot kérek</button>
            </div>
        </article>
        <article id="map" class="w3-row w3-mobile">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2694.904250013136!2d19.148721515818423!3d47.511255902552385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741c4ad6aeb22ef%3A0x3a88c94011333a44!2zQnVkYXBlc3QsIFLDoXTDs3QgdS4gOSwgMTE0NA!5e0!3m2!1shu!2shu!4v1570267721789!5m2!1shu!2shu"
                width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </article>
    </section>
    <article id="impressum" class="w3-row w3-center">
        <p>Szendrei Gépműhely - 2019.<br /><small>Honlap: <a href="https://www.swh-it.hu" target="_blank">swh-it.hu</a></small></p>
    </article>
</footer>
</div>
<!-- Modal Form -->
<div id="modal" class="w3-modal">
    <div class="w3-modal-content w3-animate-zoom w3-round">
        <div class="w3-container">
            <div id="modal-header" class="w3-row-padding">
                <span class="w3-button w3-display-topright modal-close">&times;</span>
                <header class="w3-half w3-card-4 w3-padding w3-margin header w3-round">
                    <h2>Ajánlatkérés</h2>
                </header>
            </div>
            <div id="modal-form" class="w3-row-padding w3-margin">
                <div class="w3-row">
                    <p class="w3-card-4 w3-padding header w3-round w3-large"><strong>Fontos tudnivaló!</strong></p>
                </div>
                <p>Ahhoz, hogy pontos árajánlatot tudjak adni, kérem írja le milyen jellegű felújításra gondolt.</p>
                <p>Hengerfejnél pl. egy általános felújítás a következőkből áll: szétszerelés, előmosás,
                    próbanyomás,hiba megállapítás, szelep-köszörülés, szelepfészek marás-csiszolás, síklapolás, mosás,
                    szelep-hézag állítás (hidrotőke tisztítás), szimmering
                    csere, vezérműtengely szerelés).</p>
                <p>Motorblokknál: szétszerelés, hiba megállapítás, henger fúrás, főtengely köszörülés, mosás,
                    derékszögelés.</p>
                <p>Kérjük adja meg a gépjármű pontos típusát az alábbiakat szerint pl. :</p>
                <ul>
                    <li>Opel Astra F Classic Kombi 1.6 i.</li>
                    <li>4hengeres alumínium hengerfej, 16szelepes, hidro tőkés</li>
                    <li>1998’ évjárat, 1598ccm, 74kw, 101 lóerős</li>
                    <li>X 16 XEL motorkód</li>
                </ul>
                </p>
                <p class="w3-card-4 w3-padding header w3-round w3-large">Ajánlatkéréshez kérem, töltse ki és küldje el
                    az alábbi űrlapot!</p>
                <p><small>A <span class="asterisk">*</span> jelölt adatok megadása kötelező!</small></p>
                <form method="POST" class="w3-container">
                    <div class="w3-section">
                        <div class="w3-margin-bottom">
                            <label for="form_name">Név:<span class="asterisk">*</span></label>
                            <input type="text" name="form_name" id="form_name" class="w3-input w3-border valid" />
                            <span class="alert"></span>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="form_email">E-mail cím:<span class="asterisk">*</span></label>
                            <input type="email" name="form_email" id="form_email" class="w3-input w3-border valid" />
                            <span class="alert"></span>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="form_phone">Telefonszám:<span class="asterisk">*</span></label>
                            <input type="text" name="form_phone" id="form_phone" class="w3-input w3-border valid"
                                placeholder="pl. 06/60/555-5555" />
                            <span class="alert"></span>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="form_auto">Autó pontos típusa (a forgalmi engedély alapján, évjárat, ccm,
                                motorkód,
                                kw vagy lóerő):<span style="color: red">*</span></label>
                            <input type="text" name="form_auto" id="form_auto" class="w3-input w3-border valid" />
                            <span class="alert"></span>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="form_msg">Üzenet:<span class="asterisk">*</span></label>
                            <textarea name="form_msg" id="form_msg" rows="5"
                                class="w3-input w3-border valid"></textarea>
                            <span class="alert"></span>
                        </div>
                        <div class="w3-margin-bottom">
                            <label for="form_sec">Biztonsági kérdés:<span class="asterisk">*</span></label>
                            <input type="text" name="form_sec" id="form_sec" class="w3-input w3-border valid"
                                placeholder="<?php echo $this->cifence->placeholder; ?>" />
                            <span class="alert"></span>
                            <input type="text" name="form_hp" id="form_hp" autocomplete="off" value="" />
                        </div>
                        <!--<div class="w3-margin-bottom">
                            <input type="checkbox" name="form_gdpr" id="form_gdpr"
                                class="w3-check valid" />&nbsp;Az <a href="#">adatvédelmi
                                nyilatkozatot</a> elolvastam és elfogadom!<span class="asterisk">*</span><br />
                            <span class="alert"></span>
                        </div>-->
                        <input type="checkbox" name="form_copy" id="form_copy"
                            class="w3-check w3-margin-bottom" />&nbsp;Kérek egy másolatot!<br />
                        <input type="submit" name="form_send" id="form_send" class="w3-btn w3-red w3-round send"
                            value="Küldés" />&nbsp;<button type="button" name="form_close" id="form_close"
                            class="w3-btn w3-blue w3-round modal-close w3-margin">Az ablak bezárása</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- The Modal Image -->
<div id="modal-img" class="w3-modal w3-padding">
    <div class="w3-modal-content w3-animate-zoom">
        <div class="w3-container">
            <span id="ref-close" class="w3-button w3-display-topright">&times;</span>
            <div id="refImg-placeholder" class="w3-padding w3-center"></div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/site/site.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/scroll/animatescroll.js"></script>
</body>

</html>