<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <header>
        <h1>Tartalomkezelő</h1>
    </header>
    <article class="docs">
        A tartalomkezelő funkció segítségével hozhatók létre az odalon megjelenő szöveges, illetve képi tartalmak,
        amelyek kategóriákban (cikkek csoportja) és cikkekben (önálló tartalmak) kerülnek létrehozásra.
        A különböző funkciókhoz a jobb oldalon található menü almenüpontjain keresztül juthatunk el.
        <p>A tartalmak létrehozásával kapcsolatos tudnivalók az alábbiak:</p>
        <ol>
            <li><strong>Kategóriák</strong></li>
            A kategóriákba az egy tematikához tartozó cikkek oszthatók be, amelyek az odalon (a Főoldal kategória
            kivételével) legördülő menü formájában lesznek elérhetők.<br />
            <ul>
                <li>
                    <p>Kategória <strong><u>létrehozásához</u></strong> a jobb oldali menü segítségével navigáljunk a
                        "Kategória készítése" aloldalra, ahol értelemszerűen töltsük ki az űrlapot, majd mentsük a
                        létrehozott kategóriát.</p>
                    A <strong>menü beállításakor</strong> adjuk meg azt a menüt, amelyben meg szeretnénk jeleníteni a
                    kategóriát, majd adjuk meg a menüpont nevét (ami eltérhet a kategória nevétől) és adjuk meg a
                    menüpont pozícióját, ami a menüpont
                    menün belül elfoglalt helyét jelenti, fentről lefelé haladva.
                    <p>A kategória csak akkor jelenik meg a kiválasztott menün belül, ha a <strong>státusza</strong>
                        aktívként kerül beállításra és mentésre.</p>
                </li>
                <li>
                    <p>Kategória <strong><u>kezeléséhez</u></strong> a jobb oldali menü segítségével navigáljunk a
                        "Kategória kezelése" aloldalra, ahol a megjelenő táblázatban kattintsunk a megfelelő kategória
                        sorában található "Kezelés" gombra.
                        A megnyíló aloldalon belül értelemszerűen módosítsuk a megfelelő adatokat, majd mentsük a
                        módosításokat ("Módosítás").</p>
                    Amennyiben a <strong>menüpont pozícióját</strong> szeretnénk módosítani, a pozíciókat megjelenítő
                    legördülő menüben kattintsunk az új pozícióra és mentsük a módosítást.<br />
                    <p>A kategória <strong>deaktiválása</strong> esetén a kategóriához tartozó összes tartalom (cikk)
                        elérhetetlenné válik az odalon, a kategória újra aktiválásáig.</p>
                </li>
                <li>
                    <p>Kategória csak abban az esetben <strong>törölhető</strong>, ha nem található benne cikk.
                        Törlésekor a kategória adatai véglegesen eltávolításra kerülnek, azok nem visszaállíthatók.</p>
                </li>
            </ul>
            <li><strong>Cikkek</strong></li>
            A cikkek azok a szöveges és/vagy képi tartalmak, amelyek saját menüpontokon keresztül jelennek meg oldalon.
            A cikkek lehetnek önállóak, vagy beoszthatók egy számukra létrehozott kategóriába.
            <ul>
                <li>
                    <p>Cikkek <strong><u>létrehozásához</u></strong> a jobb oldali menü segítségével navigáljunk a "Cikk
                        készítése" aloldalra, ahol értelemszerűen töltsük ki a megjelenő űrlapot.<br />
                        <p>A <strong>"Menü beállítás"</strong> panelen belül a <strong>kategóriába sorolt
                                cikkek</strong> esetében a "Szülő kategória" című listából válasszuk ki a megfelelő
                            kategóriát. Ebben az esetben "Szülő menü" választására nincs szükség, mivel a cikk
                            menüpontja automatikusan
                            az adott kategória legördülő menüjében fog megjelenni.<br />
                            <strong>Önálló cikk esetén</strong> a "Szülő menü" listából válasszuk ki azt a menüt, amiben
                            a cikk menüpontját meg szeretnénk jeleníteni. Ebben az esetben "Szülő kategória"
                            kiválasztására nincs szükség és a cikk a saját menüpontja alatt önállóan lesz elérhető.</p>
                        <p>A cikkhez tartozó <strong>képek feltöltésére</strong> a "Képek" panelen belül van
                            lehetőség.<br />
                            <strong>FIGYELEM:</strong> a cikkekhez korlátlan számú (a tárhely méretének függvényében),
                            de egyszerre csak maximum 10 db kép tölthető fel. Amennyiben a feltöltendő képek száma
                            meghaladja a 10 darabot az ezen a számon felüli képek - szintén 10 db-os csomagokban - a
                            cikkek módosítása során tölthetők fel.
                            A feltöltött képek kizárólag a cikk mentése után illeszthetők be a cikk szövegében a cikkek
                            módosításánál leírtak szerint (lsd. lent).</p>
                        <p>Az adott cikk csak "Aktív" státusz esetén jelenik meg a honlapon.</p>
                </li>
                <li>
                    <p>Cikkek <strong><u>módosításához</u></strong> a jobb oldali menü segítségével navigáljunk a "Cikk
                        kezelése" aloldalra és az ott megjelenő táblázatban kattintsunk az adott cikk sorában található
                        "Kezelés" gombra. A megnyíló űrlapban értelemszerűen módosítsuk a cikk adatait, majd mentsük a
                        módosítást.</p>
                    <p><strong>Képek a cikk szövegébe történő beillesztéséhez</strong> a kiválasztott képnél kattintsunk
                        az "URL másolása" gombra, majd a cikk szövegében válasszuk ki azt a helyet, ahova a képet be
                        kívánjuk szúrni és kattintsunk oda. Ezt követően a szövegszerkesztő panelen keressük meg a
                        a "Kép" gombot (2. sor jobbról a 8. gomb), majd a "Hivatkozás" mezőbe illesszük be (CTRL+V) a
                        kép URL-jét. Töltsük ki az "Alternatív szöveg" című mezőt és szükség szerint állítsuk be a kép
                        méreteit (Szélesség-Magasság), illetve a szövegtől mért távolságát (Vízsz. táv - Függ. táv).
                        Az "Igazítás" mezőben megadhatjuk a kép helyzetét, az azt körülvevő szöveghez képest (jobb
                        oldal, bal oldal). Amennyiben a képet középre szeretnénk helyezni, zárjuk be a képszerkesztő
                        ablakot és a szövegszerkesztőben kattintsunk a kép mellé. Ezt követően a szövegszerkesztő panel
                        második sorában keressük meg a
                        megfelelő pozíció ikonját, majd kattintsunk rá.<br />
                        <strong>FIGYELEM:</strong> a kép kizárólag a cikk módosításnak mentésekor kerül beillesztésre a
                        cikk szövegébe, ezért a beillesztést követően mentsük a cikk módosítását.<br />
                        Egy adott kép törléséhez válasszuk ki az adott képet, majd kattintsunk a "Kiválasztott képek
                        törlése" gombra. Az összes kép törléséhez kattintsunk az "Összes kép törlése" gombra. A
                        kiválasztott képek véglegesen eltávolításra kerülnek a szerverről, ezért ezek nem
                        visszaállíthatók.</p>
                    <p>A cikkre vonatkozó <strong>menü beállítások</strong> a "Menü beállítás" panelen belül
                        módosíthatók. Amennyiben egy kategóriába sorolt cikket szeretnénk kiemelni a kategóriából és a
                        továbbiakban önállóan megjeleníteni, a "Szülő kategória" listában válasszuk ki a "Kérem,
                        válasszon" lehetőséget.
                        Ezt követően a panel listái alaphelyzetbe kerülnek, ami lehetővé teszi a megfelelő menü
                        kiválasztását. Önálló cikk kategóriába helyezése esetén ugyanezt végezzük el a "Szülő menü"
                        listával, amelyet követően lehetővé válik a megfelelő kategória kiválasztása.<br />
                        A menüpont pozíciójának megváltoztatása esetén a "Menüpont pozíciója" listában kattintunk a
                        megfelelő új pozícióra, majd mentse a cikk módosítását.</p>
                    <p>A cikk csak <strong>"Aktív"</strong> státusz esetén jelenik meg az oldalon.</p>
                    <p><strong>FIGYELEM:</strong> A módosítások csak a mentés ("Módosítás" gomb) után kerülnek
                        véglegesítésre.</strong></p>
                    <p><strong>Törlés</strong> esetén a cikk és a hozzá tartozó képek véglesen eltávolításra kerülnek a
                        szerverről, ezért ezek nem visszaállíthatók.</p>
                </li>
            </ul>
        </ol>
    </article>
</main>