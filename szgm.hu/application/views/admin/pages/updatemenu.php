<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<span id="validationStatus" class="sys-msg"></span>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Menü módosítása</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form method="POST" action="">
        <div class="w3-row w3-padding-large">
            <label for="menuTitle">Menü neve:<span class="asterisk">*</span></label>
            <input type="text" id="menuTitle" name="menuTitle" class="w3-input w3-border valid" value="<?php echo $this->menus[0]->menuTitle; ?>" />
            <span class="alert"></span>
        </div>
        <div class="w3-row w3-padding-large">
            <input type="checkbox" class="w3-check" name="menuStat" id="menuStat" value="<?php echo(count($this->allMenus)) == 1 ? 1 : ''; ?>" <?php echo($this->menus[0]->menuStat == 1) ? 'checked' : ''; ?> <?php echo(count($this->allMenus) == 1) ? 'disabled' : ''; ?> />&nbsp;Aktív
        </div>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="updateMenu" id="updateMenu" class="w3-btn w3-green w3-round send" data-action="updateMenu" data-target="<?php echo $this->menus[0]->menuId; ?>" value="Módosítás" />&nbsp;
            <input type="submit" name="menuDel" id="menuDel" class="w3-btn w3-red w3-round send" data-action="deleteMenu" data-target="<?php echo $this->menus[0]->menuId; ?>" value="Törlés" <?php echo(count($this->allMenus)) == 1 ? 'disabled' : ''; ?> />&nbsp;
            <a id="menuList" href="<?php echo base_url(); ?>index.php/admin/menu/managemenu" class="w3-btn w3-blue w3-round">Vissza a menükhöz</a>
        </div>
    </form>
</main>
<script src="<?php echo base_url(); ?>assets/js/admin/menu.js"></script>