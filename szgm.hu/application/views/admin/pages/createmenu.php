<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<span id="validationStatus" class="sys-msg"></span>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Menü készítése</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form id="menuForm" method="POST" action="">
        <div class="w3-row w3-padding-large">
            <label for="menuTitle">Menü neve:<span class="asterisk">*</span></label>
            <input type="text" id="menuTitle" name="menuTitle" class="w3-input w3-border valid" />
            <span class="alert"></span>
        </div>
        <div class="w3-row w3-padding-large">
            <input type="checkbox" class="w3-check" name="menuStat" id="menuStat" value="<?php echo(!$this->activeMenuExists) ? 1 : ''; ?>" <?php echo(!$this->activeMenuExists) ? 'checked disabled' : ''; ?> />&nbsp;Aktív
        </div>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="saveMenu" id="saveMenu" class="w3-btn w3-green w3-round send" data-action="saveMenu" value="Mentés" />
        </div>
    </form>
</main>
<script src="<?php echo base_url(); ?>assets/js/admin/menu.js"></script>