<span id="validationStatus" class="sys-msg"></span>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<div class="w3-row">
    <div class="w3-content w3-center">
        <div class="w3-panel w3-grey w3-card-4 w3-xlarge w3-round w3-padding-32">Kérem, adja meg a belépési adatait!</div>
        <div id="loginError" class="w3-panel w3-red w3-card-4 w3-large w3-round w3-margin w3-padding-16 sys-msg"></div>
        <div class="w3-row">
            <div class="w3-content w3-padding-large">
                <label for="loginName"><strong>Felhasználó név:</strong></label>
                <input type="email" name="loginName" id="loginName" class="w3-input w3-border valid" />
                <span class="alert"></span>
            </div>
            <div class="w3-content w3-padding-large">
                <label for="loginPass"><strong>Jelszó:</strong></label>
                <input type="password" name="loginPass" id="loginPass" class="w3-input w3-border valid" />
                <span class="alert"></span>
            </div>
            <div class="w3-content w3-padding-large">
                <button class="w3-btn w3-red w3-round send" name="login" id="login">Belépés</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/admin/login.js"></script>