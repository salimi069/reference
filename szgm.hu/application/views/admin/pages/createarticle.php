<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="<?php echo base_url(); ?>assets/js/libs/ckeditor/ckeditor.js"></script>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<span id="validationStatus" class="sys-msg"></span>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Cikk készítése</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form method="POST" action="" enctype="form/multipart">
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Alapadatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artTitle">Cikk címe:<span class="asterisk">*</span></label>
                <input type="text" name="artTitle" id="artTitle" class="w3-input w3-border valid" />
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artText">Cikk szövege:</label>
                <textarea name="artText" id="artText" class="w3-input w3-border"></textarea>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Menü beállítás:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artParentCat">Szülő kategória:</label>
                <select name="artParentCat" id="artParentCat" class="w3-input w3-border">
                    <option value="select" selected>Kérem, válasszon</option>
                    <?php if(count($this->categories) > 0) {
                        
                        foreach($this->categories as $cat) {
                            echo "<option value=\"{$cat->catId}\">{$cat->catTitle}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artParentMenu">Szülő menü:<span class="asterisk">*</span></label>
                <select name="artParentMenu" id="artParentMenu" class="w3-input w3-border valid">
                    <option value="select" selected>Kérem, válasszon</option>
                    <?php 
                    if(count($this->menus) > 0) {

                        foreach($this->menus as $menu) {
                            echo "<option value=\"{$menu->menuId}\">{$menu->menuTitle}</option>";
                        }
                    }                   
                    ?>
                </select>
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artMenuTitle">Menüpont neve:<span class="asterisk">*</span></label>
                <input type="text" name="artMenuTitle" id="artMenuTitle" class="w3-input w3-border valid" />
                <span class="alert"></span><br />
                <input type="checkbox" class="w3-check" name="copyArtTitle" id="copyArtTitle" />&nbsp;Cím másolása
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artMenuPos">Menüpont pozíciója:<span class="asterisk">*</span></label>
                <select name="artMenuPos" id="artMenuPos" class="w3-input w3-border valid">
                    <option value="select" disabled selected>Kérem, válasszon</option>
                </select>
                <span class="alert"></span>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Képek:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artImg">Feltöltés:</label>
                <input type="file" class="w3-input w3-border" name="artImg[]" id="artImg" multiple />
                <div class="w3-row">
                    <div class="galery w3-container"></div>
                </div>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Meta adatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artKeyWords">Kulcsszavak:</label>
                <input type="text" name="artKeywords" id="artKeywords" class="w3-input w3-border"
                    placeholder="Max. 20 db egymástól vesszővel elválasztott kulcsszó." />
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artDesc">Leírás:</label>
                <input type="text" name="artDesc" id="artDesc" class="w3-input w3-border"
                    placeholder="Összefoglaló egy mondatban." />
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Státusz:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <input type="checkbox" class="w3-check" name="artStat" id="artStat" value="1" />&nbsp;Aktív
            </div>
        </div>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="artSave" id="artSave" class="w3-btn w3-green w3-round send" data-action="save"
                value="Mentés" />
        </div>
    </form>
</main>
<script>
CKEDITOR.replace('artText');
</script>
<script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/admin/article.js"></script>