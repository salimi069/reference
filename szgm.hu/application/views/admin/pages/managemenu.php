<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="content" class="w3-row w3-padding w3-mobile">
    <header><h1>Menü kezelése</h1></header>
    <article class="w3-row w3-center">
    <?php echo(count($this->menus) > 0) ? $this->table->generate() : '<div class="w3-panel w3-padding-16 w3-large w3-red w3-card-4">Jelenleg nincs mentett menü az adatbázisban.</div>'; ?>
    </article>
</main>
<script src="<?php echo base_url(); ?>assets/js/admin/menu.js"></script>