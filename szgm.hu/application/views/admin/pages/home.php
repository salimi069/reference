<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <header>
        <h1>Üdvözlet az adminisztratív kezelőfelület főoldalán!</h1>
    </header>
    <article class="docs">
        <p>Az admimnisztratív felületen a következő funkciók kezelésére van lehetőség:</p>
        <ul>
            <li>Menük</li>
            <li>Tartalom
                <ul>
                    <li>Kategóriák</li>
                    <li>Cikkek</li>
                </ul>
            </li>
            <li>Felhasználók</li>
        </ul>
        <p>Az említett funkciókhoz a főmenüsávon keresztül lehet eljutni.<br />
        A funkciók kezelésére vonatkozó leglényegesebb információk a funkciók főoldalain olvashatók.</p>
        <p>A honlapra vonatkozó <strong>főbb technikai információk</strong> az alábbiak:</p>
        <ul>
        <li>Backend keretrendszer: CodeIgniter 3.1.11 (PHP)</li>
        <li>CSS könyvtár: w3.css</li>
        <li>JavaScript könyvtár: jQuery 3.4.1</li>
        <li>Adatbázis: MySQL</li> 
        </ul>
    </article>
</main>