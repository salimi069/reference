<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <header>
        <h1>Menükezelő</h1>
    </header>
    <article class="docs">
        <p>A menükezelő funkción belül az oldalhoz tartozó menük hozhatók létre, illetve ezek beállításai kezelhetők. A
            bizonyos alfunkciókhoz (létrehozás, kezelés)
            a jobb oldalon található kinyíló menü pontjain keresztül lehet eljutni.</p>
        <p><strong>Mivel jelenleg a létrehozott új menük nem jelennek meg automatikusan az odalon, ezért új menü
                létrehozása csak akkor javasolt, ha annak elhelyezéséhez tudjuk szerkeszteni
                az oldal struktúrájának forráskódját!</strong></p>
        <ol>
            <li><strong>Menü készítése</strong><br /></li>
            Menü készítéséhez lépjünk be a megfelelő aloldalra a jobb oldali menü "Menü készítése" című almenüpontján
            keresztül.<br />
            A megnyíló aloldalon adjuk meg a menü nevét és státuszát, majd mentsük a menüt.
            <p>FIGYELEM! Az aktív menü nem jelenik automatikusan az oldalon, azt a forráskód szerkesztésével kell az
                oldal struktúrájában elhelyezni.</p>
            <li><strong>Menü kezelése</strong></p>
                Menü kezeléséhez lépjünk be a megfelelő aloldalra a jobb oldali menü "Menü kezelése" című almenüpontján
                keresztül, majd a táblázatban kattintsunk a menü sorában található "Kezelés" gombra.<br />
                A megnyíló aloldalon szerkesszük a menü nevét és/vagy státuszát, majd mentsük el a módosításokat.<br />
                Amennyiben az oldalon csak egy menü került beállításra, a menü törlésére, vagy inaktívra állítására (az
                oldal elérhetőségének biztosítása érdekében) nincs lehetőség.
                <p>FIGYELEM! Menü törlésére csak akkor van lehetőség, ha ahhoz nincs más tartalom (kategória, cikk)
                    rendelve.</p>
        </ol>
    </article>
</main>