<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library('session');
echo $this->session->userdata('access');
?>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <header>
        <h1>Felhasználókezelő</h1>
    </header>
    <?php 
    if($this->session->has_userdata('adminFail') && $this->session->adminFail === 'Yes') { ?>
    <div class="w3-panel w3-red w3-padding-32 w3-large">FIGYELEM: Jelenleg nincs <strong>aktív adminisztrátor</strong>
        jogosultságú személy elmentve az adatbázisban, aminek a
        hiányában a honlap <strong>adminisztrációs felülete elérhetetlenné válik</strong>.<br />
        Kérem, hogy kilépés előtt jelöljön egy aktív adminisztrátort a felhasználók közül!
        <?php } ?>
        <article class="docs">
            <p>A felhasználókezelő funkción belül az oldal adminisztrátori, illetve szerkesztői jogosultsággal
                rendelkező felhasználóinak adatai kezelhetők.</p>
            <p><strong>FIGYELEM:</strong> A adminisztratív felületre csak a felhasználó adatainál regisztrált
                <strong>e-mail címmel, jelszóval és aktív státusz beállítással</strong> lehet belépni.</p>
            A felhasználókezelő főbb funkciói kizárólag az adminisztrátori joggal rendelkező felhasználó számára érhetők
            el. A honlap kezeléséhez tartozó jogosultságok és a hozzájuk tartozó funkciók az alábbiak:
            <ol>
                <li><strong>Adminisztrátor</strong><br />
                    Adminisztrátori joggal a felhasználók közül kizárólag egy fő ruházható fel. Mivel az oldal
                    adminisztratív felülete felhasználók nélkül elérhetetlenné válik, ezért minimum egy fő felhasználó
                    mentésére mindenképpen szükség van.
                    Amennyiben az adatbázisban kizárólag egy fő szerep, akkor annak a felhasználónak adminisztrátori
                    jogokkal kell rendelkeznie. Ha ez a feltétel nem teljesül az éppen bejelentkezett felhasználó nem
                    tud kilépni az adminisztrációs felületről, amíg a megfelelő jogosultságokat be nem állítja.
                    <p>Az adminisztrátor jogosultságai az alábbiak:
                        <ul>
                            <li>menük teljes jogú kezelése</li>
                            <li>tartalom teljes jogú kezelése</li>
                            <li>felhasználók teljes jogú kezelése
                                <ul>
                                    <li>új felhasználó (szerkesztő) felvétele</li>
                                    <li>felhasználók adatainak bizonyos fokú szerkesztése (e-mail cím, jelszó
                                        kivételével)</li>
                                    <li>felhasználó aktiválása, deaktiválása</li>
                                    <li>felhasználó törlése</li>
                                    <li>új adminisztrátor kijelölése az adminisztrátor jogok átadásával</li>
                                </ul>
                        </ul>
                        <ul>
                            <li>
                                <p><strong>Új felhasználó felvételéhez</strong> a jobb oldalon található menü
                                    segítségével navigáljunk a "Felhasználó fevétele" aloldalra és értelemszerűen
                                    töltsük ki a megjelenő űrlapot.<br />
                                    Az itt megadott jelszót a felvett felhasználó a későbbiekben módosíthatja.</p>
                                <p>Amennyiben a rendszerben szerepel adminisztrátori jogosultságú személy, új
                                    felhasználó csak szerkesztői jogokkal vehető fel.</p>
                                <p>A felvett felhasználó csak "Aktív" státusszal lesz képes belépni az adminisztratív
                                    felületre.</p>
                            </li>
                            <li>
                                <p>A <strong>felhasználó adatainak módosításához</strong> a jobb oldalon található menü
                                    segítségével navigáljunk a "Felhasználók kezelése" aloldalra és a megfelelő
                                    felhasználó sorában kattinstunk a "Kezelés" gombra.
                                    Ezt követően értelemszerűen végezzük el az adatok módosítását.</p>
                                <p>Az <strong>adatok módosításának hitelesítéséhez</strong> a "Jelszó" panel első
                                    mezőjébe ("Jelszó") írjuk be az éppen érvényes jelszavunkat (a módosítást végző
                                    személy jelszavát!). Ennek hiányában a módosítás nem kerül hitelesítésre és a
                                    módosítás nem kerül mentésre.<br />
                                    Amennyiben a felhasználó deaktiválásra kerül az újabb aktiválásig nem fog tudni
                                    belépni az adminisztratív felületre.</p>
                                <p>Amennyiben az <strong>adminisztrátori jog átadásra kerül</strong>, az átadó
                                    adminisztrátor a saját adatlapján a jogosultságot állítsa át szerkesztőre, majd
                                    mentse a módosítást. Ezt követően lépjen be az átvevő adminisztrátor adatlapjára és
                                    a jogosultságot állítsa át adminisztrátorra, majd mentse a módosítást.<br />
                                    Aktív adminisztrátor kijelölése nélkül az éppen bejelentkezett felhasználó nem tud
                                    kilépni az adminisztrációs felületből. Amennyiben ezt mégis megtenné a felhasználói
                                    adatok szerkesztésével kapcsolatos főbb funkciók elérhetetlenné válnak.</p>
                                <p>Felhasználó <strong>törlése</strong> csak akkor lehetséges, ha a törölni kívánt
                                    felhasználón kívül még legalább egy fő (adminisztrátor) szerepel az
                                    adatbázisban.<br />
                                    A törölt felhasználó adatai véglegesen eltávolításra kerülnek, azok nem
                                    visszaállíthatók.</p>
                            </li>
                        </ul>
                </li>
                <li><strong>Szerkesztő</strong><br />
                    <p>A rendszerbe szerkesztői joggal regsiztrálható személyek száma nem korlátozott.</p>
                    <p>A szerkesztők jogosultságai az alábbiak:</p>
                    <ul>
                        <li>menük teljes jogú kezelése</li>
                        <li>tartalom teljes jogú kezelése</li>
                        <li>saját felhasználói adatok teljes jogú kezelése</li>
                    </ul>
                    <p>A <strong>saját adatok kezeléséhez</strong> a jobb oldali menü segítségével navigáljunk a "Saját
                        adatok kezelése" aloldalra és értelemszerűen végezzük el a szükséges módosításokat, majd mentsük
                        el azokat ("Módosítás" gomb).</p>
                    <p><strong>FIGYELEM:</strong> A módosítás hitelesítése kizárólag a módosítás pillanatában érvényes
                        jelszó megadásával lehetséges, amelyet a "Jelszó" panel "Jelszó" című mezőjének kitöltésével
                        lehet elvégezni. Hitelesítés nélkül a módosítás nem kerül mentésre!</p>
                </li>
            </ol>
            <p><strong>FIGYELEM:</strong> Mentett felhasználók nélkül a honlap adminisztrációs felülete elérhetetlenné
                válik, ezért legalább egy adminisztrátori jogokkal rendelkező felhasználónak szerepelnie kell az
                adatbázisban.<br />
                Alapértelmezett beállítások szerint, amennyiben a rendszerben nem szerepel mentett felhasználó az éppen
                bejelentkezett felhasználó nem tud kilépni az adminisztrációs felületről a minimálisan szükséges
                felhasználói adatok és jogosultságok beállítása nélkül.</p>
        </article>
</main>