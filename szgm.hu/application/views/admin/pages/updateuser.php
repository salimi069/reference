<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<span id="validationStatus" class="sys-msg"></span>
<span id="editor" class="sys-msg"><?php echo $this->editor; ?></span>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Felhasználói adatok módosítása</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form method="POST" action="">
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Alapadatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userName">Név:<span class="asterisk">*</span></label>
                <input type="text" name="userName" id="userName" class="w3-input w3-border valid" value="<?php echo $this->users[0]->userName; ?>" />
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userEmail">E-mail:<span class="asterisk">*</span></label>
                <input type="email" name="userEmail" id="userEmail" class="w3-input w3-border valid" value="<?php echo $this->users[0]->userEmail; ?>" <?php echo($this->adminId && $this->savedAdminId || $this->editor === 'editor') ? '' : 'readonly'; ?> />
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userAccess">Jogosultság:<span class="asterisk">*</span></label>
                <select name="userAccess" id="userAccess" class="w3-input w3-border valid">
                <option value="select" disabled selected>Kérem, válasszon</option>
                <option value="1" <?php echo($this->users[0]->userAccess == 1) ? 'selected': ''; ?> <?php echo(count($this->adminCheck) > 0 && $this->users[0]->userAccess != 1) ? 'disabled' : ''; ?>>Adminisztrátor</option>
                <option value="2" <?php echo($this->users[0]->userAccess == 1) ? '': 'selected'; ?>>Szerkesztő</option>
                </select>
                <span class="alert"></span>
            </div>            
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Jelszó:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userPass">Jelszó:<span class="asterisk">*</span></label>
                <input type="password" name="userPass" id="userPass" class="w3-input w3-border <?php echo(count($this->adminCheck) == 0) ? '' : 'valid'; ?>" <?php echo(count($this->adminCheck) == 0) ? 'disabled' : ''; ?> />
                <span class="alert"></span>
            </div>
            <?php if($this->adminId == $this->savedAdminId || $this->editor === 'editor') { ?>
            <div class="w3-row w3-padding-large pass">
                <label for="userNewPass">Új jelszó:</label>
                <input type="password" name="userNewPass" id="userNewPass" class="w3-input w3-border" />
                <span class="alert"></span>
                <span class="pass-check"></span>
            </div>
            <div class="w3-row w3-padding-large pass">
                <label for="userPassConf">Új jelszó megerősítés:</label>
                <input type="password" name="userNewPassConf" id="userNewPassConf" class="w3-input w3-border" />
                <span class="alert"></span>
            </div>
            <?php } ?>            
        </div>
        <?php if($this->editor === 'admin') { ?>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Státusz:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <input type="checkbox" class="w3-check" name="userStat" id="userStat" <?php echo($this->users[0]->userStat == 1) ? 'checked' : ''; ?> <?php echo($this->users[0]->userAccess == 1) ? 'disabled' : ''; ?> />&nbsp;Aktív
            </div>
        </div>
        <?php } ?>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="userUpdate" id="userUpdate" data-action="update" data-id="<?php echo $this->users[0]->userId; ?>" class="w3-btn w3-green w3-round send"
                value="Módosítás" />&nbsp;
            <?php if($this->editor === 'admin') { ?>    
            <input type="submit" name="userDel" id="userDel" data-action="delete" data-id="<?php echo $this->users[0]->userId; ?>" class="w3-btn w3-red w3-round send" value="Törlés" <?php echo($this->users[0]->userAccess == 1) ? 'disabled' : ''; ?> />&nbsp;
            <a href="<?php echo base_url(); ?>index.php/admin/user/manageuser" class="w3-btn w3-blue w3-round">Vissza a
                felhasználókhoz</a>
            <?php } ?>
        </div>
    </form>
</main>
<script src="<?php echo base_url(); ?>assets/js/admin/user.js"></script>