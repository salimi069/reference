<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<span id="validationStatus" class="sys-msg"></span>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Felhasználó felvétele</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form method="POST" action="">
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Alapadatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userName">Név:<span class="asterisk">*</span></label>
                <input type="text" name="userName" id="userName" class="w3-input w3-border valid" />
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userEmail">E-mail:<span class="asterisk">*</span></label>
                <input type="email" name="userEmail" id="userEmail" class="w3-input w3-border valid" />
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="userAccess">Jogosultság:<span class="asterisk">*</span></label>
                <select name="userAccess" id="userAccess" class="w3-input w3-border valid">
                <option value="select" disabled selected>Kérem, válasszon</option>
                <option value="1" <?php echo(count($this->adminCheck) > 0) ? 'disabled' : ''; ?>>Adminisztrátor</option>
                <option value="2" <?php echo(count($this->adminCheck) > 0) ? '' : 'disabled'; ?>>Szerkesztő</option>
                </select>
                <span class="alert"></span>
            </div>            
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Jelszó beállítás:</h3>
            </div>
            <div class="w3-row w3-padding-large pass">
                <label for="userPass">Jelszó:<span class="asterisk">*</span></label>
                <input type="password" name="userPass" id="userPass" class="w3-input w3-border valid" placeholder="A jelszó minimális hossza: 6 karakter" />
                <span class="alert"></span>
                <span class="pass-check"></span>
            </div>
            <div class="w3-row w3-padding-large pass">
                <label for="userPassConf">Jelszó megerősítés:<span class="asterisk">*</span></label>
                <input type="password" name="userPassConf" id="userPassConf" class="w3-input w3-border valid" />
                <span class="alert"></span>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Státusz:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <input type="checkbox" class="w3-check" name="userStat" id="userStat" />&nbsp;Aktív
            </div>
        </div>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="userSave" id="userSave" class="w3-btn w3-green w3-round send" data-action="save" value="Mentés" />
        </div>
    </form>
</main>
<script src="<?php echo base_url(); ?>assets/js/admin/user.js"></script>