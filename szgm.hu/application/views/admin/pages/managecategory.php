<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="content" class="w3-row w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Kategóriák kezelése</h1>
        </header>
        <article class="w3-row w3-center">
        <?php 
        echo(count($this->categories) > 0) ? $this->table->generate() : '<div class="w3-panel w3-padding-16 w3-large w3-red w3-card-4">Jelenleg nincs mentett kategória az adatbázisban.</div>';        
        ?>
        </article>
    </div>
</main>