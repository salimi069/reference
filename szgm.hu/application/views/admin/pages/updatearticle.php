<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span id="validationStatus" class="sys-msg"></span>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<script src="<?php echo base_url(); ?>assets/js/libs/ckeditor/ckeditor.js"></script>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Cikk módosítása</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form method="POST" action="" enctype="multipart/form-data">
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Alapadatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artTitle">Cikk címe:<span class="asterisk">*</span></label>
                <input type="text" name="artTitle" id="artTitle" class="w3-input w3-border valid"
                    value="<?php echo $this->articles[0]->artTitle; ?>" />
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artText">Cikk szövege:</label>
                <textarea name="artText" id="artText"
                    class="w3-input w3-border"><?php echo(!empty($this->articles[0]->artText)) ? $this->articles[0]->artText : ''; ?></textarea>
            </div>
        </div>
        <div class="form-part w3-padding protField">
            <div class="w3-container">
                <h3>Menü beállítás:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artParentCat">Szülő kategória:</label>
                <select name="artParentCat" id="artParentCat" class="w3-input w3-border">
                    <option value="select" selected>Kérem, válasszon</option>
                    <?php 
                    if(!empty($this->categories)) {

                        foreach($this->categories as $cat) { ?>
                    <option value="<?php echo $cat->catId; ?>"
                        <?php echo($this->articles[0]->artParentCat != 0 && ($cat->catId === $this->articles[0]->artParentCat)) ? "selected" : ''; ?>>
                        <?php echo $cat->catTitle; ?></option>
                    <?php }
                    }                    
                    ?>
                </select>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artParentMenu">Szülő menü:<span class="asterisk">*</span></label>
                <select name="artParentMenu" id="artParentMenu" class="w3-input w3-border valid">
                    <option value="select" selected>Kérem, válasszon</option>
                    <?php 
                    if(!empty($this->menus)) {

                        foreach($this->menus as $menu) { ?>
                    <option value="<?php echo $menu->menuId; ?>"
                        <?php echo($this->articles[0]->artParentMenu != 0 && ($menu->menuId === $this->articles[0]->artParentMenu)) ? "selected" : ''; ?>>
                        <?php echo $menu->menuTitle; ?></option>
                    <?php }
                    }                    
                    ?>
                </select>
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artMenuTitle">Menüpont neve:<span class="asterisk">*</span></label>
                <input type="text" name="artMenuTitle" id="artMenuTitle" class="w3-input w3-border valid"
                    value="<?php echo $this->articles[0]->artMenuTitle; ?>" />
                <span class="alert"></span><br />
                <input type="checkbox" class="w3-check" name="copyArtTitle" id="copyArtTitle" />&nbsp;Cím másolása
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artMenuPos">Menüpont pozíciója:<span class="asterisk">*</span></label>
                <select name="artMenuPos" id="artMenuPos" class="w3-input w3-border valid">
                    <option value="select" disabled selected>Kérem, válasszon</option>
                    <?php 
                    if (count($this->menuPos) > 0) {                        

                        if ($this->articles[0]->artParentCat != 0) {

                            foreach ($this->menuPos as $menuPos) { ?>
                    <option class="item" value="<?php echo $menuPos['artMenuPos']; ?>"
                        <?php echo($menuPos['artMenuPos'] == $this->articles[0]->artMenuPos) ? "selected" : ''; ?>>
                        <?php echo $menuPos['artMenuPos'] . ' - ' . $menuPos['artMenuTitle']; ?></option>
                    <?php   }
                        } elseif ($this->articles[0]->artParentMenu != 0) {
                            
                            foreach ($this->menuPos as $pos => $title) {                            
                            ?>
                    <option class="item" value="<?php echo $pos; ?>"
                        <?php echo($pos == $this->articles[0]->artMenuPos) ? 'selected' : ''; ?>>
                        <?php echo $pos . ' - ' . $title; ?></option>
                    <?php   }
                        }
                    }
                    ?>
                </select>
                <span class="alert"></span>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Képek:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artImg">Feltöltés:</label>
                <input type="file" class="w3-input w3-border" name="artImg[]" id="artImg" multiple />
                <div class="w3-row">
                    <div class="w3-container">
                        <?php 
                    if(count($this->images) > 0) {
                        
                        foreach($this->images as $img) { ?>
                        <div class="w3-third w3-mobile w3-padding img-placeholder" data-imgplaceholder="<?php echo $img; ?>">
                            <div class="w3-row thumbImg">
                                <img src="<?php echo base_url() . 'assets/uploads/images/articles/' . $this->articles[0]->artMenuAlias . '/' . $img; ?>"
                                    class="w3-image" /><br />
                            </div>
                            <div class="w3-row">
                                <div class="w3-half w3-mobile">
                                    <button type="button"
                                        class="w3-btn w3-blue w3-round w3-margin-top copyLink">URL másolása</button>
                                    <input type="text" class="w3-input w3-border w3-margin-top"
                                        value="<?php echo base_url() . 'assets/uploads/images/articles/' . $this->articles[0]->artMenuAlias . '/' . $img; ?>" />
                                </div>
                                <div class="w3-half w3-mobile"><input type="checkbox" class="w3-check w3-margin-top selImg"
                                        data-img="<?php echo $img; ?>" />&nbsp;Törlés</div>
                            </div>                            
                        </div>
                        <?php }
                    }
                    ?>
                    <div class="galery w3-container"></div>
                    </div>
                    <?php if(count($this->images) > 0) { ?>
                    <button type="button" id="delSelImg" class="w3-btn w3-red w3-round w3-margin-top delImg" data-action="selected" data-folder="<?php echo $this->articles[0]->artMenuAlias; ?>">Kiválasztott képek
                        törlése</button>&nbsp;
                    <button type="button" id="delAllImg" class="w3-btn w3-orange w3-round w3-margin-top delImg" data-action="all" data-folder="<?php echo $this->articles[0]->artMenuAlias; ?>">Összes kép
                        törlése</button>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="form-part w3-padding protField">
            <div class="w3-container">
                <h3>Meta adatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artKeyWords">Kulcsszavak:</label>
                <input type="text" name="artKeywords" id="artKeywords" class="w3-input w3-border"
                    placeholder="Max. 20 db egymástól vesszővel elválasztott kulcsszó."
                    value="<?php echo $this->articles[0]->artKeywords; ?>" />
            </div>
            <div class="w3-row w3-padding-large">
                <label for="artKeyWords">Leírás:</label>
                <input type="text" name="artDesc" id="artDesc" class="w3-input w3-border"
                    placeholder="Összefoglaló egy mondatban." value="<?php echo $this->articles[0]->artDesc; ?>" />
            </div>
        </div>
        <div class="form-part w3-padding protField">
            <div class="w3-container">
                <h3>Státusz:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <input type="checkbox" class="w3-check" name="artStat" value="1" id="artStat"
                    <?php echo($this->articles[0]->artStat == 1) ? 'checked' : ''; ?> />&nbsp;Aktív
            </div>
        </div>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="artUpdate" id="artUpdate" data-action="update" data-id="<?php echo $this->articles[0]->artId; ?>" class="w3-btn w3-green w3-round send"
                value="Módosítás" />&nbsp;
            <input type="submit" name="artDel" id="artDel" class="w3-btn w3-red w3-round send" data-action="delete" data-id="<?php echo $this->articles[0]->artId; ?>" value="Törlés" />&nbsp;
            <a href="<?php echo base_url(); ?>index.php/admin/content/managearticle"
                class="w3-btn w3-blue w3-round">Vissza a cikkekhez</a>
        </div>
    </form>
</main>
<script>
CKEDITOR.replace('artText');
</script>
<script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
<script src="<?php echo base_url(); ?>assets/js/admin/article.js"></script>