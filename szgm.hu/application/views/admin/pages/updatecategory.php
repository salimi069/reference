<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<span id="validationStatus" class="sys-msg"></span>
<span id="baseUrl" class="sys-msg"><?php echo base_url(); ?></span>
<main id="content" class="w3-twothird w3-padding w3-mobile">
    <div class="w3-row w3-padding-large">
        <header>
            <h1>Kategória módosítása</h1>
        </header>
        <small>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező!</small>
    </div>
    <form method="POST" action="">
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Alapadatok:</h3>
            </div>
            <div class="w3-row w3-padding-large">            
                <label for="catTitle">Kategória címe:<span class="asterisk">*</span></label>
                <input type="text" name="catTitle" id="catTitle" class="w3-input w3-border valid" value="<?php echo $this->categories[0]->catTitle; ?>" />
                <span class="alert"></span>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Menü beállítás:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="catParentMenu">Szülő menü:<span class="asterisk">*</span></label>
                <select name="catParentMenu" id="catParentMenu" class="w3-input w3-border valid">
                    <option value="select" disabled selected>Kérem, válasszon</option>
                    <?php
                    if (count($this->menus) > 0) {
                        foreach ($this->menus as $menu) { ?>
                            <option value="<?php echo $menu->menuId; ?>" <?php echo($menu->menuId === $this->categories[0]->catParentMenu) ? "selected" : ''; ?>><?php echo $menu->menuTitle; ?></option>                                                              
                <?php }
                    }?>
                </select>
                <span class="alert"></span>
            </div>
            <div class="w3-row w3-padding-large">
                <label for="catMenuTitle">Menüpont neve:<span class="asterisk">*</span></label>
                <input type="text" name="catMenuTitle" id="catMenuTitle" class="w3-input w3-border valid" value="<?php echo $this->categories[0]->catMenuTitle; ?>" />
                <span class="alert"></span><br />
                <input type="checkbox" class="w3-check" name="copyCatTitle" id="copyCatTitle" />&nbsp;Cím másolása
            </div>
            <div class="w3-row w3-padding-large">
                <label for="catMenuPos">Menü pozíciója:<span class="asterisk">*</span></label>
                <select name="catMenuPos" id="catMenuPos" class="w3-input w3-border valid">
                    <option value="select" disabled selected>Kérem, válasszon</option>                                       
                </select>
                <span class="alert"></span>
            </div>
        </div>
        <div class="form-part w3-padding">
            <div class="w3-container">
                <h3>Státusz:</h3>
            </div>
            <div class="w3-row w3-padding-large">
                <input type="checkbox" class="w3-check" name="catStat" id="catStat" <?php echo($this->categories[0]->catStat == 1) ? "checked" : ''; ?> />&nbsp;Aktív
            </div>
        </div>
        <div class="w3-row w3-padding-large">
            <input type="submit" name="catUpdate" id="catUpdate" class="w3-btn w3-green w3-round send" data-action="update" data-id="<?php echo $this->categories[0]->catId; ?>" value="Módosítás" />&nbsp;
            <input type="submit" name="catDel" id="catDel" class="w3-btn w3-red w3-round send" data-action="delete" data-id="<?php echo $this->categories[0]->catId; ?>" value="Törlés" />&nbsp;
            <a href="<?php echo base_url(); ?>index.php/admin/content/managecategory" class="w3-btn w3-blue w3-round">Vissza a kategóriákhoz</a>
        </div>
    </form>    
</main>
<script src="<?php echo base_url(); ?>assets/js/admin/category.js"></script>