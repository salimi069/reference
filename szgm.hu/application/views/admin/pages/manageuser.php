<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<main id="content" class="w3-row w3-padding w3-mobile">
    <header><h1>Felhasználók kezelése</h1></header>
    <article class="w3-row w3-center">
    <?php echo(count($this->users) > 0) ? $this->table->generate() : '<div class="w3-panel w3-padding-16 w3-large w3-red w3-card-4">Jelenleg nincs mentett felhasználó az adatbázisban.</div>'; ?>
    </article>
</main>