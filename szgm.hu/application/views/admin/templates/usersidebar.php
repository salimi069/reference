<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<button class="w3-button w3-teal w3-xlarge w3-right sidebar-btn">Menü</button>
<div class="w3-sidebar w3-bar-block w3-border-right w3-animate-right" id="adminSidebar">
  <button class="w3-bar-item w3-large w3-teal sidebar-btn">Bezárás &times;</button>
  <?php if($this->session->userAccess == 1) { ?>  
  <a href="<?php echo base_url(); ?>index.php/admin/user/createuser" class="w3-bar-item w3-button">Felhasználó felvétele</a>
  <a href="<?php echo base_url(); ?>index.php/admin/user/manageuser" class="w3-bar-item w3-button">Felhasználók kezelése</a>
  <?php } else { ?>  
  <a href="<?php echo base_url(); ?>index.php/admin/user/updateuser/<?php echo $this->session->userId; ?>/editor" class="w3-bar-item w3-button">Saját adatok kezelése</a>
  <?php } ?>    
</div>