<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<button class="w3-button w3-teal w3-xlarge w3-right sidebar-btn">Menü</button>
<div class="w3-sidebar w3-bar-block w3-border-right w3-animate-right" id="adminSidebar">
  <button class="w3-bar-item w3-large w3-teal sidebar-btn">Bezárás &times;</button>
  <a href="<?php echo base_url(); ?>index.php/admin/menu/createmenu" class="w3-bar-item w3-button">Menü készítése</a>
  <a href="<?php echo base_url(); ?>index.php/admin/menu/managemenu" class="w3-bar-item w3-button">Menük kezelése</a>  
</div>