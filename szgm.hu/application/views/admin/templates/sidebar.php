<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<button class="w3-button w3-teal w3-xlarge w3-right sidebar-btn">Menü</button>
<div class="w3-sidebar w3-bar-block w3-border-right w3-animate-right" id="adminSidebar">
  <button class="w3-bar-item w3-large w3-teal sidebar-btn">Bezárás &times;</button>
  <a href="#" class="w3-bar-item w3-button">Link 1</a>
  <a href="#" class="w3-bar-item w3-button">Link 2</a>
  <a href="#" class="w3-bar-item w3-button">Link 3</a>
</div>