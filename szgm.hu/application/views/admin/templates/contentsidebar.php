<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<button class="w3-button w3-teal w3-xlarge w3-right sidebar-btn">Menü</button>
<div class="w3-sidebar w3-bar-block w3-border-right w3-animate-right" id="adminSidebar">
  <button class="w3-bar-item w3-large w3-teal sidebar-btn">Bezárás &times;</button>
  <button class="w3-button w3-block w3-left-align accBtn" data-action="cat">Kategóriák <i class="fa fa-caret-down"></i></button>
  <div id="cat" class="w3-bar-block w3-hide w3-white w3-card-4">
    <a href="<?php echo base_url(); ?>index.php/admin/content/createcategory" class="w3-bar-item w3-button">Kategória készítése</a>
    <a href="<?php echo base_url(); ?>index.php/admin/content/managecategory" class="w3-bar-item w3-button">Kategória kezelése</a>
  </div>
  <button class="w3-button w3-block w3-left-align accBtn" data-action="art">Cikkek <i class="fa fa-caret-down"></i></button>
  <div id="art" class="w3-bar-block w3-hide w3-white w3-card-4">
    <a href="<?php echo base_url(); ?>index.php/admin/content/createarticle" class="w3-bar-item w3-button">Cikk készítése</a>
    <a href="<?php echo base_url(); ?>index.php/admin/content/managearticle" class="w3-bar-item w3-button">Cikk kezelése</a>
  </div>
</div>