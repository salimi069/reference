<?php defined('BASEPATH') OR exit('No direct script access allowed');
$uri = $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html lang="hu">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <title>Szendrei Gépműhely - Admin</title>
    <link href="<?php echo base_url(); ?>assets/style/w3.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/style/adminStyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="<?php echo base_url(); ?>assets/js/libs/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/admin/validation.js"></script>
</head>

<body>
    <div id="wrapper" class="w3-row">
        <nav id="mainNav" class="w3-row">
            <div class="w3-bar w3-border">
                <div id="logo-placeholder" class="w3-col s1 m1 l1">
                    <figure><img src="<?php echo base_url(); ?>assets/uploads/images/logo_trans.png" width="80px" alt="Szendrei Gépműhely" /></figure>
                </div>
                <div id="mainMenu-items" class="w3-rest w3-xlarge w3-left w3-padding-32">
                <?php if(strpos($uri, 'login') > -1) { ?>
                    <a href="<?php echo base_url(); ?>" class="w3-bar-item w3-button w3-mobile w3-round">Főoldal</a>
                <?php } else { ?>
                    <a href="<?php echo base_url(); ?>index.php/admin/home" class="w3-bar-item w3-button w3-mobile w3-round">Főoldal</a>
                    <a href="<?php echo base_url(); ?>index.php/admin/menu" class="w3-bar-item w3-button w3-mobile w3-round">Menük</a>
                    <a href="<?php echo base_url(); ?>index.php/admin/content" class="w3-bar-item w3-button w3-mobile w3-round">Tartalomkezelés</a>                    
                    <a href="<?php echo base_url(); ?>index.php/admin/user" class="w3-bar-item w3-button w3-mobile w3-round">Felhasználók</a>
                    <a href="<?php echo base_url(); ?>index.php/admin/home/userLogout/<?php echo $this->session->userdata('userId'); ?>" class="w3-bar-item w3-button w3-mobile w3-round">Kilépés</a>
                <?php } ?>
                </div>
            </div>
        </nav>
        <div id="content-placeholder" class="w3-row">