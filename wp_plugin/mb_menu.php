<?php
/**
 * Plugin Name: Málna Bisztró - Étlap Mester
 * Description: Étlap készítő kisalkalmazás
 * Version: 1.0.0
 * Author: Sallér Imre
 */

 /**
  * ----------------------------
  * Prevention of direct access
  * ----------------------------
  */
 if (!function_exists('add_action')) {
     _e('403 - Access Denied');
     exit();
 }

 /**
  * -----------------------------
  * Activation && Deactivation
  * -----------------------------
  */
 register_activation_hook(__FILE__, 'mb_menu_activate');
 register_deactivation_hook(__FILE__, 'mb_menu_deactivate');

 /**
  * -----------------------------
  * Activation
  * -----------------------------
  */
 function mb_menu_activate()
 {
     // WP version check
     global $wp_version;

     if (version_compare($wp_version, '5.4', '<')) {
         wp_die('Ez az alkalmazás minimum 5.4-es WordPress verziót igényel!');
     }
    
     /**
      * Creation of db tables
      */
     global $wpdb;
     $charset_collate = $wpdb->get_charset_collate();

     // Table Names
     $table_cat = $wpdb->prefix . 'menu_categories';     
     $table_food = $wpdb->prefix . 'menu_foods';
     $table_menu = $wpdb->prefix . 'menu_menu';
    
     // Queries
     $sql_cat = 'CREATE TABLE ' . $table_cat . ' (
        cat_id int(2) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        cat_rank int(2) NOT NULL,
        cat_name varchar(50) NOT NULL,
        cat_name_alias varchar(100) NOT NULL,
        cat_stat int(1) NOT NULL DEFAULT 1
    );';        

     $sql_food = 'CREATE TABLE ' . $table_food . ' (
        f_id int(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        f_cat int(2) NOT NULL,
        f_name varchar(256) NOT NULL,
        f_alias varchar(256) NOT NULL
    );';     

    $sql_menu = 'CREATE TABLE ' . $table_menu . ' (
        m_id int(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        m_date date,
        m_stat int(1) NOT NULL DEFAULT 1,               
        m_daily_offer int(1) NOT NULL DEFAULT 2,
        m_template_name VARCHAR(256),
        m_template_alias VARCHAR(256),
        m_menu text NOT NULL
    );';

    $sql_daily_menu_cost = "INSERT INTO mb_options(option_name, option_value, autoload) VALUES('mb_daily_menu_cost', '0', 'no');";
    $sql_breakfast = "INSERT INTO mb_options(option_name, option_value, autoload) VALUES('mb_menu_breakfast', '', 'no');";
    $sql_alacart = "INSERT INTO mb_options(option_name, option_value, autoload) VALUES('mb_menu_alacart', '', 'no');";
    $sql_daily_offer = "INSERT INTO mb_options(option_name, option_value, autoload) VALUES('mb_menu_daily_offer', '', 'no');"; 

     $sqls = [$sql_cat, $sql_food, $sql_menu, $sql_daily_menu_cost, $sql_daily_offer];

     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

     foreach ($sqls as $query) {
         dbDelta($query);
     }
 }

 require_once(plugin_dir_path(__FILE__) . 'include.php');
 
 /**
  * -------------------------------
  * Creation of Dashboard menu
  * -------------------------------
   */
 function create_admin_menu()
 {
     add_menu_page(__('Étlap Mester'), __('Étlap Mester'), 'manage_options', 'mb_admin_menu', 'mb_menu_main_page', plugins_url('images/logo.png', __FILE__), 3);
     add_submenu_page('mb_admin_menu', __('Kategóriák'), __('Kategóriák'), 'manage_options', 'mb_menu_categories_page', 'mb_menu_categories_page');
     add_submenu_page('mb_admin_menu', __('Ételek-Italok'), __('Ételek&Italok'), 'manage_options', 'mb_menu_foods_page', 'mb_menu_foods_page');
     add_submenu_page('mb_admin_menu', __('Napi Menük'), __('Napi Menük'), 'manage_options', 'mb_menu_menu_page', 'mb_menu_menu_page');
     add_submenu_page('mb_admin_menu', __('Reggeli Menü'), __('Reggeli Menü'), 'manage_options', 'mb_menu_breakfast_page', 'mb_menu_breakfast_page');
     add_submenu_page('mb_admin_menu', __('A lá Cart Menü'), __('A lá Cart Menü'), 'manage_options', 'mb_menu_alacart_page', 'mb_menu_alacart_page');
     add_submenu_page('mb_admin_menu', __('Napi Ajánlat'), __('Napi Ajánlat'), 'manage_options', 'mb_menu_daily_offer_page', 'mb_menu_daily_offer_page');
     add_submenu_page('mb_admin_menu', __('Karbantartás'), __('Karbantartás'), 'manage_options', 'mb_menu_debug_page', 'mb_menu_debug_page');
     add_submenu_page('mb_admin_menu', __('Hiba üzenetek'), __('Hiba üzenetek'), 'manage_options', 'mb_menu_error_page', 'mb_menu_error_page');
 }

 add_action('admin_menu', 'create_admin_menu');

 /**
  * -----------------------------
  * Adding custom files
  * -----------------------------
  */
 function custom_files()
 {
     /** Styles */

     // W3css
     wp_register_style('w3css_style', APP_URL . 'style/w3.css');
     wp_enqueue_style('w3css_style');

     // Custom
     wp_register_style('custom_style', APP_URL . 'style/style.css');
     wp_enqueue_style('custom_style');

     // DataTables lib
     wp_register_style('datatables_style', APP_URL . 'libs/datatables/datatables.min.css');
     wp_enqueue_style('datatables_style');
     
     /** JavaScript */

     // jQuery 3.5.1
     wp_register_script('jquery_update', APP_URL . 'libs/jquery-3.5.1.min.js', [], '3.5.1');
     wp_enqueue_script('jquery_update');

     // DataTables lib
     wp_register_script('datatables_js', APP_URL . 'libs/datatables/datatables.min.js');
     wp_enqueue_script('datatables_js');

     // Validation
     wp_register_script('validation_js', APP_URL . 'js/validation.js');
     wp_enqueue_script('validation_js');
     
     // Debug
     wp_register_script('debug_js', APP_URL . 'js/mb_debug.js');
     wp_enqueue_script('debug_js');
     
     // Categories
     wp_register_script('categories_js', APP_URL . 'js/mb_categories.js');
     wp_enqueue_script('categories_js');

     // Foods & Drinks
     wp_register_script('foods_js', APP_URL . 'js/mb_foods.js');
     wp_enqueue_script('foods_js');

     // Menus
     wp_register_script('menu_js', APP_URL . 'js/mb_menu.js');
     wp_enqueue_script('menu_js');

     // Breakfast
     wp_register_script('breakfast_js', APP_URL . 'js/mb_breakfast.js');
     wp_enqueue_script('breakfast_js');

     // A lá Cart
     wp_register_script('alacart_js', APP_URL . 'js/mb_alacart.js');
     wp_enqueue_script('alacart_js');

     // Daily offer
     wp_register_script('daily_offer_js', APP_URL . 'js/mb_daily_offer.js');
     wp_enqueue_script('daily_offer_js');

     // Print menu
     wp_register_script('print_menu_js', APP_URL . 'js/mb_print.js');
     wp_enqueue_script('print_menu_js');     

     // Miscellaneous
     wp_register_script('misc_js', APP_URL . 'js/mb_misc.js');
     wp_enqueue_script('misc_js');
 }

 add_action('admin_enqueue_scripts', 'custom_files');
 
 /**
  * -------------------------------
  * Pages
  * -------------------------------
  */
  require_once(APP_ROOT . 'functions/main_page_fn.php');            // Homepage
  require_once(APP_ROOT . 'functions/categories_page_fn.php');      // Categories
  require_once(APP_ROOT . 'functions/foods_page_fn.php');           // Foods & Drinks
  require_once(APP_ROOT . 'functions/menu_page_fn.php');            // Menu
  require_once(APP_ROOT . 'functions/breakfast_page_fn.php');       // Breakfast
  require_once(APP_ROOT . 'functions/alacart_page_fn.php');         // A la Cart
  require_once(APP_ROOT . 'functions/daily_offer_fn.php');          // Daily offer
  require_once(APP_ROOT . 'functions/debug_page_fn.php');           // Debug
  require_once(APP_ROOT . 'functions/error_page_fn.php');           // Errors
  
  // Deletion of Debug & Error logs
  function mb_menu_delete_log()
  {
      $type = $_POST['type'];
      $action = $type === 'debug' ? 'mb_debug_delete' : 'mb_error_delete';
    
      if (wp_verify_nonce($_POST['nonce'], $action) != false) {
          $del = Debug::delete($type);
      
          if ($del == 200) {
              wp_send_json_success();
          } else {
              wp_send_json_error();
          }
      }
  }

  add_action('wp_ajax_mb_menu_delete_log', 'mb_menu_delete_log');

  /**
   * ------------------------------
   * Filters
   * ------------------------------
   */

   // Creation of name alias
  function create_alias_filter($name)
  {
      $from = ['Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ö', 'ö', 'Ő', 'ő', 'Ú', 'ú', 'Ü', 'ü', 'Ű', 'ű'];
      $to = ['a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u'];
      $changeLetters = str_replace($from, $to, $name);
      $lowerCase = strtolower($changeLetters);
      $removeSpace = explode(' ', $lowerCase);
      $alias = rtrim(implode('', $removeSpace), ' ');
      return $alias;
  }

add_filter('default_content', 'create_alias_filter');

 /**
  * -------------------------------
  * Deactivation
  * -------------------------------
  */
 function mb_menu_deactivate()
 {
     remove_menu_page('mb_admin_menu');
 }
