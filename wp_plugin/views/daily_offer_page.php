<div class="w3-container">
    <span id="validationStatus" data-url="<?php echo admin_url('admin-ajax.php'); ?>"></span>
    <?php _e('<h1>Étlap Mester - Napi Ajánlat</h1>'); ?>
    <div class="loader"></div>
    <div class="w3-row mb_content_placeholder">
    <?php
    wp_nonce_field('mb_menu_daily_offer_nonce', 'mb_menu_daily_offer_nonce_field');
    if (!$saved_daily_offer) { ?>
        <div id="daily_offer_alert" class="w3-panel w3-padding w3-blue w3-round w3-card-4 w3-large">Jelenleg nincs mentett Napi Ajánlat.</div>
        <?php } ?>
        <div id="daily_offer_editor_container" class="w3-row">
            <?php wp_editor($daily_offer_text, 'daily_offer_editor'); ?>
        </div>
        <div id="daily_offer_manage" class="w3-row w3-margin-top">
            <button id="daily_offer_save_btn" class="w3-btn w3-round w3-green daily_offer_action">Mentés</button>&nbsp;
            <button id="daily_offer_delete_btn" class="w3-btn w3-round w3-red daily_offer_action">Törlés</button>
        </div>
    </div>
</div>