<div class="w3-container">
    <span id="validationStatus" data-url="<?php echo admin_url('admin-ajax.php'); ?>"></span>
    <?php _e('<h1>A lá Cart Menü</h1>'); ?>
    <div class="loader"></div>
    <div class="w3-row mb_content_placeholder">
    <?php
    wp_nonce_field('mb_menu_alacart_nonce', 'mb_menu_alacart_nonce_field');
    if (!$saved_alacart_menu) { ?>
        <div id="alacart_alert" class="w3-panel w3-padding w3-blue w3-round w3-card-4 w3-large">Jelenleg nincs mentett A lá Cart menü.</div>
        <?php } ?>
        <div id="alacart_editor" class="w3-row">
            <?php wp_editor($alacart_menu_text, 'alacart_menu_editor'); ?>
        </div>
        <div id="alacart_manage" class="w3-row w3-margin-top">
            <button id="alacart_save_btn" class="w3-btn w3-round w3-green alacart_action">Mentés</button>&nbsp;
            <button id="alacart_delete_btn" class="w3-btn w3-round w3-red alacart_action">Törlés</button>
        </div>
    </div>
</div>