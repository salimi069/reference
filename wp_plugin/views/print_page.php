<?php
require_once(dirname(__FILE__, 5) . '/wp-config.php');

try {
    $db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASSWORD);
} catch (PDOException $e) {
    echo 'Db error: ' . $e->getMessage();
}

if (isset($_GET)) {
    $request_params = $_GET;
    $weekly_menu = [];
    
    if ($request_params['id'] === 'weekly') {
        $q = $db->query("SELECT * FROM mb_menu_menu WHERE m_stat = 1 ORDER BY m_date ASC");
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $weekly_menu = $q->fetchAll();        
    }
    else {
        foreach($request_params as $key => $menu_id) {
            $q = $db->query("SELECT * FROM mb_menu_menu WHERE m_id = '$menu_id' LIMIT 1");
            $q->setFetchMode(PDO::FETCH_ASSOC);
            $sel_menu = $q->fetchAll();
            array_push($weekly_menu, $sel_menu[0]);            
        }
    }
}
?>

<DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8" />
        <title>Menü nyomtatása</title>
        <link rel="stylesheet" href="../style/w3.css" type="text/css" />
        <link rel="stylesheet" href="../style/style.css" type="text/css" />
    </head>

    <body>
        <div id="print_wrapper" class="w3-row-padding">
            <header id="print_header" class="w3-row w3-mobile">
                <div id="print_logo" class="w3-col s1 m1 l1 w3-margin-top"><img width="150px" 
                        src="../images/logo_ceg.png" alt="Málna Bisztró" /></div>
                <div id="print_header_title" class="w3-col s11 m11 l11 w3-margin-top w3-center">
                    <h1>Menü</h1>
                </div>
            </header>
            <section id="print_menu_container" class="w3-container w3-margin-top w3-responsive">
                <?php
                if (!empty($weekly_menu)) {

                    if ($request_params['id'] === 'weekly') {
                        echo '<div class="w3-panel w3-blue w3-padding w3-round w3-card-4">Reggeli és A lá Cart menüinkért kérem, látogassa meg <a href="http://www.malnabisztro.hu" target="_blank">honlapunkat</a></div>';
                    }
                    
                    foreach ($weekly_menu as $menu_item) {                        
                        
                        // Daily offer (if exists)
                        if ($menu_item['m_daily_offer'] == 1) {
                            $q = $db->query("SELECT * FROM mb_options WHERE option_name = 'mb_menu_daily_offer' LIMIT 1");
                            $q->setFetchMode(PDO::FETCH_ASSOC);
                            $daily_offer = $q->fetchAll();
                        }
                        
                        // Menu tables
                        $menu = json_decode($menu_item['m_menu'], true);                        
                        echo "<div class=\"w3-row\">
                        <table class=\"w3-table w3-bordered w3-margin-bottom\">
                        <tr><td class=\"w3-center\" colspan=\"4\">{$menu_item['m_date']}.</td></tr>
                        <tr><th class=\"w3-center\">Levesek</th><th class=\"w3-center\">Főételek</th><th class=\"w3-center\">Desszert</th><th class=\"w3-center\">Napi menü</th></tr>
                        <tr>";

                        foreach ($menu as $food_cat => $food_list) {
                            if ($food_cat !== 'ital' && $food_cat !== 'daily_menu') {
                                echo '<td class="w3-small">';
                                foreach ($food_list as $food => $cost) {
                                    if ($food !== 'cat_name') {
                                        echo($cost !== 'N/A') ? "{$food} - {$cost} Ft<br />" : "{$food}<br />";
                                    }
                                }
                                echo '</td>';
                            } elseif ($food_cat === 'daily_menu') {
                                echo '<td class="w3-small">';
                                foreach ($food_list as $food_cat => $food_name) {
                                    echo($food_cat !== 'cost') ? "{$food_name}<br />" : "{$food_name} Ft";
                                }
                                echo '</td>';
                            }
                        }
                        echo '</tr>';

                        // Daily offer
                        if ($menu_item['m_daily_offer'] === "1" && !empty($daily_offer[0]['option_value'])) {                            
                            echo '<tr><td colspan="4"><p>Napi ajánlat:</p><span class="w3-small">' . preg_replace("/<img[^>]+\>/i", '', htmlspecialchars_decode($daily_offer[0]['option_value'])) . '</span></td></tr>';                                                                                                              
                        }
                        
                        echo '</table></div>';
                    }
                    echo '<div class="w3-row w3-left"><button class="w3-btn w3-round w3-red" onclick="window.print()">Menü nyomtatása</button></div>';
                }
                else {
                    echo '<div class="w3-panel w3-padding w3-red w3-round w3-card-4 w3-large">Jelenleg nincs megjeleníthető menünk. Kérem, nézzen vissza később!</div>';
                }
            ?>
            </section>
        </div>
    </body>

    </html>