<div class="w3-container">
    <?php 
    _e('<h1>Étlap Mester - Karbantartás</h1>');
    
    if (!empty($response)) {
        wp_nonce_field('mb_debug_delete', 'mb_debug_delete_field');
        echo '<div class="w3-row-padding w3-margin-bottom"><button data-url="' . admin_url('admin-ajax.php') . '" data-type="debug" class="w3-btn w3-red delete_log" id="del_debug">Jelentések törlése</button></div>';
        echo '<div id="debug_table_container" class="w3-row-padding"><table class="w3-table-all"><thead><th>Dátum</th><th>Oldal</th><th>Adatok</th></thead>';

        foreach ($response as $res) {
            echo "<tr>
            <td>{$res['date']}</td>
            <td>{$res['page']}</td>
            <td>";

            if (is_array($res['data'])) {
                print("<pre>" . print_r($res['data'], true) . "</pre>");
            } else {
                echo $res['data'];
            }
            echo '</td></tr>';
        }
        echo '</table></div>';
    } else {
        _e('<div class="w3-panel w3-padding w3-round w3-card-4 w3-blue w3-large">Jelenleg nincs mentett üzenet!</div>');
    }
    ?>
</div>