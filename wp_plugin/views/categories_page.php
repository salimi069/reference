<div class="w3-container">
    <span id="validationStatus" data-url="<?php echo admin_url('admin-ajax.php'); ?>"></span>
    <?php _e('<h1>Étlap Mester - Kategóriák</h1>'); ?>
    <div class="loader"></div>
    <div class="w3-row mb_content_placeholder">
        <div class="w3-row">
            <button class="w3-btn w3-round w3-orange cat_btn"
                id="add_category"><?php _e('Új kategória létrehozása'); ?></button>&nbsp;
            <?php if(count($cat_list) > 0) { ?>
            <button class="w3-btn w3-round w3-red send"
                id="del_category"><?php _e('Kijelölt kategória törlése'); ?></button>
            <?php } ?>
        </div>
        <div class="w3-row w3-margin-top w3-responsive">
            <?php if(count($cat_list) > 0) { ?>
            <table id="cat_table" data-url="<?php echo APP_URL; ?>">
                <thead>
                    <th><input type="checkbox" name="cat_del_all" />&nbsp;<?php _e('Törlés'); ?></th>
                    <th><?php _e('Menü sorrend'); ?></th>
                    <th><?php _e('Név'); ?></th>
                    <th><?php _e('Státusz'); ?></th>
                    <th><?php _e('Módosítás'); ?></th>
                </thead>
                <tbody>
                    <?php 
                    foreach($cat_list as $cat) {
                        $stat = $cat['cat_stat'] == 1 ? __('Aktív') : __('Inaktív');
                        
                        $table_content = '<tr>';
                        $table_content .= "<td><input type=\"checkbox\" name=\"cat_del\" id=\"{$cat['cat_id']}\" /></td>";
                        $table_content .= '<td>' . esc_html($cat['cat_rank']) . '</td>';
                        $table_content .= '<td>' . esc_html($cat['cat_name']) . '</td>';
                        $table_content .= "<td>{$stat}</td>";
                        $table_content .= "<td><button class=\"w3-btn w3-round w3-orange cat_btn\" id=\"{$cat['cat_id']}\">" . __('Módosítás') . "</button></td>";
                        $table_content .= '</tr>';

                        echo $table_content;                                              
                    }
                    ?>
                </tbody>
            </table>
            <?php }
        else { ?>
            <div class="w3-panel w3-round w3-card-4 w3-padding w3-blue w3-large">
                <?php _e('Jelenleg nincs mentett kategória!'); ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="categories_modal" class="w3-modal">
    <div class="w3-modal-content">
        <div class="w3-container">
            <span class="w3-button w3-display-topright" id="categories_modal_close">&times;</span>
            <div class="w3-row" id="modal_title"></div>
            <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező.</p>
            <form id="save_category_form">
                <div class="form_group">
                    <?php wp_nonce_field('mb_menu_category_save_nonce', 'mb_menu_category_save_nonce_field'); ?>
                    <label for="cat_name">Kategória neve:<span class="asterisk">*</span></label>
                    <input type="text" class="w3-input w3-border valid" name="cat_name" />
                    <span class="alert"></span>
                </div>
                <div class="form_group">
                    <label for="cat_rank">Menü sorrend:<span class="asterisk">*</span></label>
                    <select class="w3-input w3-border valid" name="cat_rank">
                        <option value="select" selected disabled>Kérlek, válassz</option>
                        <?php 
                        foreach($cat_list as $cat) {
                            echo "<option class=\"cat\" value=\"{$cat['cat_rank']}\">{$cat['cat_rank']} - {$cat['cat_name']}</option>"; 
                        }
                        ?>
                    </select>
                    <span class="alert"></span>
                </div>
                <div class="form_group">
                    <input type="checkbox" name="cat_stat" checked />&nbsp;Aktív
                </div>
            </form>
            <div class="form_group">
                <button class="w3-btn w3-round w3-green send" id="save_category">Mentés</button>
            </div>
        </div>
    </div>
</div>
</div>