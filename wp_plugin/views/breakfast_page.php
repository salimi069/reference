<div class="w3-container">
    <span id="validationStatus" data-url="<?php echo admin_url('admin-ajax.php'); ?>"></span>
    <?php _e('<h1>Reggeli Menü</h1>'); ?>
    <div class="loader"></div>
    <div class="w3-row mb_content_placeholder">
    <?php
    wp_nonce_field('mb_menu_breakfast_nonce', 'mb_menu_breakfast_nonce_field');
    if (!$saved_breakfast_menu) { ?>
        <div id="breakfast_alert" class="w3-panel w3-padding w3-blue w3-round w3-card-4 w3-large">Jelenleg nincs mentett
            reggeli menü.</div>
        <?php } ?>       
        <div id="breakfast_editor" class="w3-row">
            <?php wp_editor($breakfast_menu_text, 'breakfast_menu_editor'); ?>
        </div>
        <div id="breakfast_manage" class="w3-row w3-margin-top">
            <button id="breakfast_save_btn" class="w3-btn w3-round w3-green breakfast_action">Mentés</button>&nbsp;
            <button id="breakfast_delete_btn" class="w3-btn w3-round w3-red breakfast_action">Törlés</button>
        </div>
    </div>
</div>