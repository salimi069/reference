<div class="w3-container">
    <?php 
    _e('<h1>Étlap Mester - Hiba üzenetek</h1>');

    if (!empty($response)) {
        wp_nonce_field('mb_error_delete', 'mb_error_delete_field');
        $logs = explode('[', $response);
        echo '<div class="w3-row-padding w3-margin-bottom"><button data-url="' . admin_url('admin-ajax.php') . '" data-type="error" class="w3-btn w3-red delete_log" id="del_error">Jelentések törlése</button></div>';                
        
        foreach($logs as $log) {

            if (!empty($log)) {
                echo "<div class=\"w3-panel w3-padding w3-grey\">[{$log}</div>";
            }
        }               
    } else {
        _e('<div class="w3-panel w3-padding w3-round w3-card-4 w3-blue w3-large">Jelenleg nincs mentett hiba üzenet!</div>');
    }
    ?>
</div>