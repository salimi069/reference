<div class="w3-container">
    <span id="validationStatus" data-url="<?php echo admin_url('admin-ajax.php'); ?>"></span>
    <?php _e('<h1>Étlap Mester - Ételek & Italok</h1>'); ?>
    <div class="loader"></div>
    <div class="w3-row mb_content_placeholder">
        <div class="w3-row">
            <button class="w3-btn w3-round w3-orange food_btn"
                id="add_food"><?php _e('Új Étel vagy Ital mentése'); ?></button>&nbsp;
            <?php if (count($food_list) > 0) { ?>
            <button class="w3-btn w3-round w3-red send"
                id="del_food"><?php _e('Kijelölt Étel vagy Ital törlése'); ?></button>
            <?php } ?>
        </div>
        <div class="w3-row w3-margin-top w3-responsive">
            <?php if (count($food_list) > 0) { ?>
            <table id="food_table" data-url="<?php echo APP_URL; ?>">
                <thead>
                    <th><input type="checkbox" name="food_del_all" />&nbsp;<?php _e('Törlés'); ?></th>
                    <th><?php _e('Név'); ?></th>
                    <th><?php _e('Kategória'); ?></th>
                    <th><?php _e('Ár'); ?></th>
                    <th><?php _e('Módosítás'); ?></th>
                </thead>
                <tbody>
                    <?php
                    foreach ($food_list as $food) {                       
                        $f_saved_price = !empty($food['f_price']) && $food['f_price'] !== 'N/A' ? $food['f_price'] . ' Ft' : 'N/A';
                        
                        $table_content = '<tr>';
                        $table_content .= "<td><input type=\"checkbox\" name=\"food_del\" id=\"{$food['f_id']}\" /></td>";
                        $table_content .= '<td>' . esc_html($food['f_name']) . '</td>';
                        $table_content .= '<td>' . esc_html($food['cat_name']) . '</td>';
                        $table_content .= '<td>' . $f_saved_price . '</td>';
                        $table_content .= "<td><button class=\"w3-btn w3-round w3-orange food_btn\" id=\"{$food['f_id']}\">" . __('Módosítás') . "</button></td>";
                        $table_content .= '</tr>';

                        echo $table_content;
                    }
                    ?>
                </tbody>
            </table>
            <?php } else { ?>
            <div class="w3-panel w3-round w3-card-4 w3-padding w3-blue w3-large">
                <?php _e('Jelenleg nincs mentett étel vagy ital!'); ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="foods_modal" class="w3-modal">
    <div class="w3-modal-content">
        <div class="w3-container">
            <span class="w3-button w3-display-topright" id="foods_modal_close">&times;</span>
            <div class="w3-row" id="modal_title"></div>
            <p>A <span class="asterisk">*</span>-al jelölt mezők kitöltése kötelező!</p>
            <form id="save_food_form">
                <div class="form_group">
                    <?php wp_nonce_field('mb_menu_food_save_nonce', 'mb_menu_food_save_nonce_field'); ?>
                    <label for="food_name">Étel vagy Ital neve:<span class="asterisk">*</span></label>
                    <input type="text" class="w3-input w3-border valid" name="f_name" />
                    <span class="alert"></span>
                </div>
                <div class="form_group">
                    <label for="food_cat_name">Kategória neve:<span class="asterisk">*</span></label>
                    <select class="w3-input w3-border valid" name="f_cat">
                        <option value="select" disabled selected>Kérlek, válassz</option>
                        <?php
                        foreach ($cat_list as $cat) {
                            echo "<option class=\"f_cat_opt\" value=\"{$cat['cat_id']}\">{$cat['cat_name']}</option>";
                        }
                        ?>
                    </select>
                    <span class="alert"></span>
                </div>
            </form>
            <div class="form_group">
                <button class="w3-btn w3-round w3-green send" id="save_food">Mentés</button>
            </div>
        </div>
    </div>
</div>