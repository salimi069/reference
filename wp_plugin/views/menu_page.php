<div class="w3-container">
    <span id="validationStatus" data-url="<?php echo admin_url('admin-ajax.php'); ?>" data-app_url="<?php echo APP_URL; ?>"></span>
    <?php _e('<h1>Étlap Mester - Menük</h1>'); ?>
    <div class="loader"></div>
    <div class="w3-row mb_content_placeholder">
        <div class="w3-row">
            <button class="w3-btn w3-round w3-orange w3-margin-bottom menu_btn"
                id="menu_add"><?php _e('Új menü létrehozása'); ?></button>&nbsp;
            <?php if(count($menu_list) > 0) {
                wp_nonce_field('mb_menu_manage_menus_nonce', 'mb_menu_manage_menus_nonce_field');  
            ?>
            <button class="w3-btn w3-round w3-green w3-margin-bottom send"
                id="activate_menu"><?php _e('Kijelölt menü akitiválása'); ?></button>&nbsp;
            <button class="w3-btn w3-round w3-blue w3-margin-bottom send"
                id="deactivate_menu"><?php _e('Kijelölt menü deaktiválása'); ?></button>&nbsp;
            <button class="w3-btn w3-round w3-red w3-margin-bottom send"
                id="delete_menu"><?php _e('Kijelölt menü törlése'); ?></button></div>
        <div class="w3-row">
            <button class="w3-btn w3-round w3-grey w3-margin-bottom print"
                id="print_weekly_menu"><?php _e('Heti menü nyomtatása'); ?></button>&nbsp;
                <button class="w3-btn w3-round w3-purple w3-margin-bottom print"
                id="print_selected_menu"><?php _e('Kiválasztott menük nyomtatása'); ?></button>
            <?php } ?>
        </div>
        <div class="w3-row w3-margin-top w3-responsive">
            <?php if(count($menu_list) > 0) { ?>
            <table id="menu_table" data-url="<?php echo APP_URL; ?>">
                <thead>
                    <th><?php _e('Kijelölés'); ?></th>
                    <th><?php _e('Dátum'); ?></th>
                    <th><?php _e('Státusz'); ?></th>
                    <th><?php _e('Sablon'); ?></th>
                    <th><?php _e('Módosítás'); ?></th>
                </thead>
                <tbody>
                    <?php 
                    foreach($menu_list as $menu) {
                        $stat = $menu['m_stat'] == 1 ? __('Aktív') : __('Inaktív');
                        $template = !empty($menu['m_template_name']) ? $menu['m_template_name'] : 'N/A';
                        $date = !empty($menu['m_date']) ? $menu['m_date'] : 'N/A'; 
                        
                        $table_content = '<tr>';
                        $table_content .= "<td><input type=\"checkbox\" name=\"menu_check\" id=\"{$menu['m_id']}\" data-stat=\"{$stat}\" data-template=\"{$template}\" /></td>";
                        $table_content .= "<td>{$date}</td>";
                        $table_content .= "<td>{$stat}</td>";
                        $table_content .= "<td>{$template}</td>";                                                                       
                        $table_content .= "<td><button class=\"w3-btn w3-round w3-blue menu_btn\" id=\"{$menu['m_id']}_update\">" . __('Módosítás') . "</button></td>";
                        $table_content .= '</tr>';

                        echo $table_content;                                              
                    }
                    ?>
                </tbody>
            </table>
            <?php }
        else { ?>
            <div class="w3-panel w3-round w3-card-4 w3-padding w3-blue w3-large">
                <?php _e('Jelenleg nincs mentett menü!'); ?>
            </div>
            <?php } ?>
        </div>
    </div>

    <!-- Modal -->
    <div id="menus_modal" class="w3-modal">
        <div class="w3-modal-content">
            <div class="w3-container">
                <span class="w3-button w3-display-topright" id="menus_modal_close">&times;</span>
                <div class="w3-row" id="modal_title"></div>
                <p>A <span class="asterisk">*</span> jelölt mezők kitöltése kötelező.</p>
                <form id="save_menu_form">
                    <?php wp_nonce_field('mb_menu_menus_save_nonce', 'mb_menu_menus_save_nonce_field'); ?>
                    <div class="form_group menu_block" id="menu_template_placeholder">
                        <label>Sablon: </label><span id="menu_template"></span>
                    </div>

                    <div class="form_group menu_block">
                        <label for="m_date">Dátum</label>
                        <input type="date" name="m_date" class="w3-input w3-border" />
                    </div>

                    <div id="m_categories">
                        <label>Menü kategóriák:<span class="asterisk">*</span></label><br />
                        <small class="asterisk">(Legalább egy étel kiválasztása kötelező.)</small>
                    </div>

                    <div class="form_group menu_block" id="daily_menu">
                        <p class="meal_title">Napi Menü</strong></p>
                        <div id="daily_menu_container">
                            <div id="daily_menu_alert" class="w3-panel w3-padding w3-red w3-round">
                                <?php _e('Jelenleg nincsenek ételek kiválasztva.'); ?></div>
                        </div>
                        <div class="daily_menu_item" id="daily_cost">
                            <label for="daily_menu_cost">Ára</label>
                            <input type="text" class="w3-input w3-border" name="daily_menu_cost"
                                value="<?php echo _e($daily_menu_cost); ?> Ft" />
                        </div>
                    </div>

                    <div class="form_group menu_block">
                        <label for="m_stat">Státusz</label><br />
                        <input type="checkbox" name="m_stat" />&nbsp;Aktív
                    </div>

                    <div class="form_group menu_block">
                        <label for="m_daily_offer">Napi ajánlat</label><br />
                        <input type="checkbox" name="m_daily_offer" />&nbsp;Aktív
                    </div>

                    <div class="form_group menu_block">
                        <label for="m_template_name">Sablon</label>
                        <input type="text" name="m_template_name" class="w3-input w3-border" />
                    </div>

                </form>
                <div class="form_group">
                    <button class="w3-btn w3-round w3-green send" id="save_menus" data-action="save">Mentés</button>
                </div>
            </div>
        </div>
    </div>
</div>