<?php
/**
 * Autoloader class
 */
class Autoloader {
    public static function load($class) {

        if(!empty($class)) {                        
            
            foreach($class as $class_name) {                
                require_once(APP_ROOT . '/classes/' . $class_name . '.php');
                new $class_name;
            }            
        }
    }
}