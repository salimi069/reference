<?php
/**
 * ----------------------
 * Breakfast Menu Page
 * ----------------------
 */

 /** Main Page */
function mb_menu_breakfast_page()
{
    $breakfast_menu = get_option('mb_menu_breakfast');

    if (!empty($breakfast_menu)) {
        $saved_breakfast_menu = true;
        $breakfast_menu_text = __($breakfast_menu);
    } else {
        $saved_breakfast_menu = false;
        $breakfast_menu_text = '';
    }
        
    require_once(APP_ROOT . 'views/breakfast_page.php');
}

/** Management of breakfast menu  */
function mb_menu_breakfast_manage()
{
    global $wpdb;
    
    if (wp_verify_nonce($_POST['nonce'], 'mb_menu_breakfast_nonce')) {
        $data = $_POST['btn_action'] === 'save' ? $_POST['data'] : '';

        $sql = update_option('mb_menu_breakfast', wp_unslash($data), 'no');
        $sql ? wp_send_json_success() : wp_send_json_error();
    }
}

add_action('wp_ajax_mb_menu_breakfast_manage', 'mb_menu_breakfast_manage');
