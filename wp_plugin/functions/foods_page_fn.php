<?php
/**
 * Foods page functions
 */

/** Main Page */
 function mb_menu_foods_page()
 {
     global $wpdb;
     $food_list = [];

     // List of foods&drinks (table)
     $food_list_q = "SELECT f.f_id, f.f_name, f.f_price, c.cat_name FROM mb_menu_foods AS f INNER JOIN mb_menu_categories AS c ON(c.cat_id = f.f_cat)";
     $food_list = $wpdb->get_results($food_list_q, ARRAY_A);

     // List of categories (Modal)
     $cat_list_q = "SELECT cat_id, cat_name FROM mb_menu_categories WHERE cat_stat = 1 ORDER BY cat_id ASC";
     $cat_list = $wpdb->get_results($cat_list_q, ARRAY_A);
    
     require_once(APP_ROOT . 'views/foods_page.php');
 }

 /** Pulling Food/Drink data from db (update modal) */
 function mb_menu_foods_get()
 {
     global $wpdb;
     $food_id = $_POST['id'];

     if (!empty($food_id)) {
         $selected_food = $wpdb->get_results("SELECT f.f_id, f.f_name, c.cat_id FROM mb_menu_foods AS f INNER JOIN mb_menu_categories AS c ON(f.f_cat = c.cat_id) WHERE f.f_id = '$food_id' LIMIT 1", ARRAY_A);

         if (!empty($selected_food[0])) {
             wp_send_json_success($selected_food[0]);
         } else {
             wp_send_json_error(500);
         }
     }
 }

 add_action('wp_ajax_mb_menu_foods_get', 'mb_menu_foods_get');

/** Creation and update of foods & drinks */
function mb_menu_food_save_update()
{
    global $wpdb;

    if (wp_verify_nonce($_POST['mb_menu_food_save_nonce_field'], 'mb_menu_food_save_nonce')) {
        $btn_action = $_POST['btn_action'] === 'save_food' ? 'save' : 'update';       
                 
        // Name Alias        
        $f_name_alias = create_alias_filter(esc_html($_POST['f_name']));
        
        $data = [
            'f_cat' => esc_html($_POST['f_cat']),
            'f_name' => esc_html($_POST['f_name']),
            'f_alias' => $f_name_alias,
         ];

        switch ($btn_action) {

             case 'save':
                 $name_check = $wpdb->get_row("SELECT * FROM mb_menu_foods WHERE f_alias = '$f_name_alias'", ARRAY_A);

                 if (empty($name_check)) {
                     $sql = $wpdb->prepare("INSERT INTO mb_menu_foods(f_cat, f_name, f_alias) VALUES(%s, %s, %s)", $data);
                     $q = $wpdb->query($sql);
                 } else {
                     wp_send_json_error(400);
                 }
             break;

             case 'update':
                $name_check = $wpdb->get_row("SELECT * FROM mb_menu_foods WHERE f_alias = '$f_name_alias'", ARRAY_A);
                $update_parts = explode('_', $_POST['btn_action']);
                 $id = $update_parts[2];

                 if (!empty($name_check && $name_check['f_id'] === $id) || empty($name_check)) {
                     $sql = $wpdb->prepare("UPDATE mb_menu_foods SET f_cat = %d, f_name = %s, f_alias = %s WHERE f_id = '$id' LIMIT 1", $data);
                     $q = $wpdb->query($sql);
                 } else {
                     wp_send_json_error(400);
                 }
             break;
             }

        if ($q != false) {
            wp_send_json_success(201);
        } else {
            wp_send_json_error(500);
        }
    } else {
        wp_send_json_error(403);
    }
}

add_action('wp_ajax_mb_menu_food_save_update', 'mb_menu_food_save_update');

/** Deletion of foods/drinks */
function mb_menu_foods_delete()
{
    global $wpdb;

    $id_string = '';

    foreach ($_POST['ids'] as $id) {
        $id_string .= $id . ', ';
    }

    $q_string = rtrim($id_string, ', ');

    $q = $wpdb->query("DELETE FROM mb_menu_foods WHERE f_id IN($q_string)");
    
    if ($q != false) {
        wp_send_json_success(200);
    } else {
        wp_send_json_error(500);
    }
}

add_action('wp_ajax_mb_menu_foods_delete', 'mb_menu_foods_delete');
