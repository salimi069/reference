<?php
/**
 * ----------------------
 * Daily Offer Page
 * ----------------------
 */

 /** Main Page */
 function mb_menu_daily_offer_page()
 {
     $daily_offer = get_option('mb_menu_daily_offer');
 
     if (!empty($daily_offer)) {
         $saved_daily_offer = true;
         $daily_offer_text = __($daily_offer);
     } else {
         $saved_daily_offer = false;
         $daily_offer_text = '';
     }     
         
     require_once(APP_ROOT . 'views/daily_offer_page.php');
 }
 
 /** Management of breakfast menu  */
 function mb_menu_daily_offer_manage()
 {
     global $wpdb;
     
     if (wp_verify_nonce($_POST['nonce'], 'mb_menu_daily_offer_nonce')) {
         $data = $_POST['btn_action'] === 'save' ? $_POST['data'] : '';
 
         $sql = update_option('mb_menu_daily_offer', wp_unslash($data), 'no');
         $sql ? wp_send_json_success() : wp_send_json_error();
     }
 }
 
 add_action('wp_ajax_mb_menu_daily_offer_manage', 'mb_menu_daily_offer_manage');