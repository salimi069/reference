<?php
/**
 * Menu page functions
 */

/** Main Page */
 function mb_menu_menu_page()
 {
     global $wpdb;
    
     $menu_list = [];
     $menu_list = $wpdb->get_results("SELECT * FROM mb_menu_menu ORDER BY m_date ASC", ARRAY_A);
     $daily_menu_cost = get_option('mb_daily_menu_cost');
    
     require_once(APP_ROOT . 'views/menu_page.php');
 }

 /** Initial data of the modal (templates, active categories) */
 function mb_menu_get_modal_data()
 {
     global $wpdb;
     $modal_data = [];

     // List of foods
     $foods_list = $wpdb->get_results("SELECT f_id, f_cat, f_name, f_price FROM mb_menu_foods ORDER BY f_name ASC", ARRAY_A);

     if (!empty($foods_list)) {
         $modal_data['foods'] = $foods_list;
     }

     // Templates
     $template_list = $wpdb->get_results("SELECT m_id, m_template_name FROM mb_menu_menu WHERE m_template_name IS NOT NULL", ARRAY_A);

     if (!empty($template_list)) {
         $modal_data['templates'] = $template_list;
     }

     // Active categories
     $category_list = $wpdb->get_results("SELECT * FROM mb_menu_categories WHERE cat_stat = 1 ORDER BY cat_rank ASC", ARRAY_A);

     if (!empty($category_list)) {
         $modal_data['categories'] = $category_list;
     }

     // Daily Offer
     $daily_offer = get_option('mb_menu_daily_offer');

     if (!empty($daily_offer)) {
         $modal_data['daily_offer'] = $daily_offer;
     }

     if ($_POST['btn_role'] === 'update') {
         $menu_id = esc_html($_POST['btn_id']);
         $saved_menu = $wpdb->get_row("SELECT * FROM mb_menu_menu WHERE m_id = '$menu_id' LIMIT 1", ARRAY_A);

         if (!empty($saved_menu)) {
             $modal_data['saved_menu'] = $saved_menu;
         }
     }

     if (!empty($modal_data)) {
         wp_send_json_success($modal_data);
     } else {
         wp_send_json_error(404);
     }
 }

 add_action('wp_ajax_mb_menu_get_modal_data', 'mb_menu_get_modal_data');

/** Save & Update menus */
function mb_menu_save_update_menu()
{
    global $wpdb;
    
    if (isset($_POST['data']) && !empty($_POST['data'])) {
        $menu_data = (json_decode(wp_unslash($_POST['data']), true));
        
        // Saving new menu
        if (!empty($menu_data)) { // Parameters of the menu
            if (wp_verify_nonce($menu_data['nonce'], 'mb_menu_menus_save_nonce')) {
                $save_menu = [];
                $selected_foods = [];
                $categories = $wpdb->get_results("SELECT cat_name, cat_name_alias FROM mb_menu_categories WHERE cat_stat = 1 ORDER BY cat_rank ASC", ARRAY_A);
                
                foreach ($categories as $cat) {
                    $selected_foods[$cat['cat_name_alias']] = [
                        'cat_name' => $cat['cat_name']
                    ];
                }

                foreach ($menu_data as $key => $data) {
                    if (!is_array($data) && $key !== 'nonce') {
                        if (!empty($menu_data[$key])) {
                            $save_menu[$key] = $data;

                            if ($key === 'm_template_name') {
                                $save_menu['m_template_alias'] = create_alias_filter($menu_data[$key]);
                            }
                        }
                    } elseif (is_array($data)) { // List of selected foods and prices
                        if ($key !== 'daily_menu') {
                            foreach ($data as $selected_cat_name => $selected_food_list) {
                                foreach ($selected_foods as $saved_cat_name => $saved_food_list) {
                                    if ($selected_cat_name === $saved_cat_name) {
                                        foreach ($data[$selected_cat_name] as $food_id => $food_price) {
                                            $food_data = $wpdb->get_row("SELECT f_name, f_price FROM mb_menu_foods WHERE f_id = '$food_id'", ARRAY_A);
                                            $selected_foods[$saved_cat_name][$food_data['f_name']] = $food_price;

                                            if ($food_price !== $food_data['f_price']) {
                                                $food_update_price = ['f_price' => esc_html($food_price)];
                                                $sql = $wpdb->prepare("UPDATE mb_menu_foods SET f_price = %s WHERE f_id = '$food_id' LIMIT 1", $food_update_price);
                                                $wpdb->query($sql);
                                            }
                                        }
                                    }
                                }
                            }
                        } else { // Daily menu
                            foreach ($menu_data[$key] as $daily_cat_alias => $daily_food_id) {
                                if ($daily_cat_alias !== 'cost' && !empty($menu_data[$key][$daily_cat_alias])) {
                                    $daily_food_name = $wpdb->get_var("SELECT f_name FROM mb_menu_foods WHERE f_id = '$daily_food_id' LIMIT 1");
                                    $menu_data[$key][$daily_cat_alias] = $daily_food_name;
                                } elseif ($daily_cat_alias === 'cost') {
                                    $menu_data[$key][$daily_cat_alias] = str_replace('Ft', '', $menu_data[$key][$daily_cat_alias]);
                                    $saved_daily_menu_cost = get_option('mb_daily_menu_cost');

                                    if ($saved_daily_menu_cost !== $menu_data[$key][$daily_cat_alias]) {
                                        update_option('mb_daily_menu_cost', $menu_data[$key][$daily_cat_alias]);
                                    }
                                }
                            }
                            $selected_foods['daily_menu'] = $menu_data[$key];
                        }
                    }
                }
                
                $selected_foods_json = json_encode($selected_foods);
                $save_menu['m_menu'] = $selected_foods_json;

                // Date && Template name check
                $menu_date = isset($save_menu['m_date']) && !empty($save_menu['m_date']) ? $save_menu['m_date'] : '';
                $template_alias = isset($save_menu['m_template_alias']) && !empty($save_menu['m_template_alias']) ? $save_menu['m_template_alias'] : '';
                $menu_date_check_result = true;
                $menu_template_alias_check_result = true;                

                // Date check
                if (isset($save_menu['m_date']) && !empty($save_menu['m_date'])) {
                    $menu_date_check = $wpdb->get_var("SELECT m_id FROM mb_menu_menu WHERE m_date = '$menu_date' LIMIT 1");                    

                    if (isset($menu_data['update_id']) && !empty($menu_data['update_id']) && !empty($menu_date_check)) {                        
                        $menu_date_check_result = $menu_data['update_id'] === $menu_date_check ? true : false;
                    } else {
                        $menu_date_check_result = $menu_date_check != null && !empty($menu_date_check) ? false : true;
                    }
                }               

                // Template name check
                if (isset($save_menu['m_template_alias']) && !empty($save_menu['m_template_alias'])) {
                    $menu_template_alias_check = $wpdb->get_var("SELECT m_id FROM mb_menu_menu WHERE m_template_alias = '$template_alias' LIMIT 1");
                    
                    if (isset($menu_data['update_id']) && !empty($menu_data['update_id']) && !empty($menu_template_alias_check)) {
                        $menu_template_alias_check_result = $menu_data['update_id'] === $menu_template_alias_check ? true : false;
                    } else {
                        $menu_template_alias_check_result = $menu_template_alias_check != null ? false : true;
                    }                                       
                }
                
                // Saving or Updating of menu
                if ($menu_date_check_result && $menu_template_alias_check_result) {
                    $tokens = [
                        'm_date' => '%s',
                        'm_stat' => '%d',
                        'm_daily_offer' => '%d',
                        'm_template_name' => '%s',
                        'm_template_alias' => '%s',
                        'm_menu' => '%s'
                    ];

                    unset($save_menu['update_id']);

                    foreach ($save_menu as $db_column => $db_value) {
                        if (isset($save_menu[$db_column]) && !empty($save_menu[$db_column])) {
                            $sql_params[$db_column] = $tokens[$db_column];
                        }
                    }

                    $sql_columns = rtrim(implode(', ', array_keys($sql_params)), ', ');
                    $sql_tokens = rtrim(implode(', ', array_values($sql_params)), ', ');
                    
                    if (!isset($menu_data['update_id']) || empty($menu_data['update_id'])) {
                        $sql = $wpdb->prepare("INSERT INTO mb_menu_menu($sql_columns) VALUES($sql_tokens)", $save_menu);
                    } else {
                        $update_id = $menu_data['update_id'];
                        unset($save_menu['update_id']);
                        unset($sql_params['update_id']);
                        
                        $sql_params['m_date'] = !isset($sql_params['m_date']) || empty($sql_params['m_date']) ? 'NULL' : $sql_params['m_date'];
                        $sql_params['m_template_name'] = !isset($sql_params['m_template_name']) || empty($sql_params['m_template_name']) ? 'NULL' : $sql_params['m_template_name'];
                        $sql_params['m_template_alias'] = !isset($sql_params['m_template_name']) || empty($sql_params['m_template_name']) ? 'NULL' : create_alias_filter($sql_params['m_template_name']);
                                                                         
                        $query = "UPDATE mb_menu_menu SET ";
                        $value_set_string = '';

                        foreach ($sql_params as $db_column => $db_value) {
                            $value_set_string .= $db_column . '=' . $db_value . ', ';
                        }

                        $query .= rtrim($value_set_string, ', ');
                        
                        $query .= " WHERE m_id = '" . $update_id . "' LIMIT 1";
                                              
                        $sql = $wpdb->prepare($query, $save_menu);
                    }
                    
                    $q = $wpdb->query($sql);

                    $q ? wp_send_json_success(201) : wp_send_json_error(500);
                } else {
                    switch (true) {
                         
                        case !$menu_date_check_result:
                            wp_send_json_error('date');
                        break;

                        case !$menu_template_alias_check_result:
                            wp_send_json_error('template');
                        break;
                    }
                }
            }
        } else {
            wp_send_json_error(403);
        }
    } else {
        wp_send_json_error(400);
    }
}

add_action('wp_ajax_mb_menu_save_update_menu', 'mb_menu_save_update_menu');

/** Manage menus */
function mb_menu_manage_menu()
{
    global $wpdb;

    if (wp_verify_nonce($_POST['nonce'], 'mb_menu_manage_menus_nonce')) {
        $action = $_POST['btn_action'];
        $ids = $_POST['id'];
        $stat_value = $action === 'activate' ? 1 : ($action === 'deactivate' ? 2 : '');
        $json_response = $action === 'activate' ? 'Sikeres aktiválás' : ($action === 'deactivate' ? 'Sikeres deaktiválás' : '');
        
        if (!empty($ids)) {
            switch ($action) {

                case 'activate':
                case 'deactivate':
                    foreach ($ids as $id) {
                        $wpdb->update('mb_menu_menu', ['m_stat' => $stat_value], ['m_id' => $id]);
                    }
                    wp_send_json_success($json_response);
                break;

                case 'deactivate_delete':
                    $not_deleted_menus = [];
                    foreach ($ids as $id) {
                        $template = $wpdb->get_var("SELECT m_template_name FROM mb_menu_menu WHERE m_id = '$id' LIMIT 1");

                        if (empty($template)) {
                            $wpdb->delete('mb_menu_menu', ['m_id' => $id]);
                        } else {
                            array_push($not_deleted_menus, $id);
                        }
                    }

                    if (empty($not_deleted_menus)) {
                        wp_send_json_success(200);
                    } elseif (count($not_deleted_menus) == 1) {
                        $wpdb->update('mb_menu_menu', ['m_stat' => 2], ['m_id' => $id]);
                        wp_send_json_success('A kijelölt menü sablon, ezért nem került törlésre.');
                    } elseif (count($not_deleted_menus) > 1) {
                        foreach ($not_deleted_menus as $ndm) {
                            $wpdb->update('mb_menu_menu', ['m_stat' => 2], ['m_id' => $ndm]);
                        }
                        wp_send_json_success('Sikeres deaktiválás.');
                    }
                break;

                case 'delete':
                    foreach ($ids as $id) {
                        $wpdb->delete('mb_menu_menu', ['m_id' => $id]);
                    }
                    wp_send_json_success(200);
                break;
            }
        } else {
            wp_send_json_error(500);
        }
    } else {
        wp_send_json_error(403);
    }
}

add_action('wp_ajax_mb_menu_manage_menu', 'mb_menu_manage_menu');
