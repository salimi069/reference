<?php
/**
 * Error page functions
 */

function mb_menu_error_page()
{
    $response = Debug::report('error'); // Displaying error logs
    require_once(APP_ROOT . 'views/error_page.php');
}
