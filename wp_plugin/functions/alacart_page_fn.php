<?php
/**
 * ----------------------
 * A la Cart Menu Page
 * ----------------------
 */

 /** Main Page */
 function mb_menu_alacart_page()
 {
     $alacart_menu = get_option('mb_menu_alacart');
 
     if (!empty($alacart_menu)) {
         $saved_alacart_menu = true;
         $alacart_menu_text = __($alacart_menu);
     } else {
         $saved_alacart_menu = false;
         $alacart_menu_text = '';
     }     
         
     require_once(APP_ROOT . 'views/alacart_page.php');
 }
 
 /** Management of breakfast menu  */
 function mb_menu_alacart_manage()
 {
     global $wpdb;
     
     if (wp_verify_nonce($_POST['nonce'], 'mb_menu_alacart_nonce')) {
         $data = $_POST['btn_action'] === 'save' ? $_POST['data'] : '';
 
         $sql = update_option('mb_menu_alacart', wp_unslash($data), 'no');
         $sql ? wp_send_json_success() : wp_send_json_error();
     }
 }
 
 add_action('wp_ajax_mb_menu_alacart_manage', 'mb_menu_alacart_manage');
