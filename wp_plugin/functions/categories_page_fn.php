<?php
/**
 * Categories page functions
 */

/** Main page */
 function mb_menu_categories_page()
 {
     global $wpdb;
     $cat_list = [];

     // List of categories (table)
     $cat_list_q = "SELECT * FROM mb_menu_categories ORDER BY cat_rank ASC";
     $cat_list = $wpdb->get_results($cat_list_q, ARRAY_A);
    
     require_once(APP_ROOT . 'views/categories_page.php');
 }

/** Creation and update of categories */
function mb_menu_categories_save_update()
{
    global $wpdb;
    
    if (wp_verify_nonce($_POST['mb_menu_category_save_nonce_field'], 'mb_menu_category_save_nonce')) {
        $stat = isset($_POST['cat_stat']) && $_POST['cat_stat'] === 'on' ? 1 : 2;
        $btn_action = $_POST['btn_action'] === 'save_category' ? 'save' : 'update';

        $data = [
            'cat_rank' => esc_html($_POST['cat_rank']),
            'cat_name' => esc_html($_POST['cat_name']),
            'cat_name_alias' => create_alias_filter(esc_html($_POST['cat_name'])),
            'cat_stat' => $stat
        ];

        switch ($btn_action) {
            
            case 'save':
                $saved_cats = $wpdb->get_results("SELECT * FROM mb_menu_categories", ARRAY_A);
                $saved_cats_number = !empty($saved_cats) ? count($saved_cats) : 0;

                if ($saved_cats_number > 0) {
                    foreach ($saved_cats as $cat) {
                        if ($cat['cat_rank'] >= $_POST['cat_rank']) {
                            $incr_cat_rank = ++$cat['cat_rank'];
                            $id = $cat['cat_id'];
                            $update_cat_rank_data = ['cat_rank' => $incr_cat_rank];
                        
                            $update_cats_rank = $wpdb->prepare("UPDATE mb_menu_categories SET cat_rank = %d WHERE cat_id = '$id' LIMIT 1", $update_cat_rank_data);
                            $q = $wpdb->query($update_cats_rank);
                        }
                    }
                }

                $sql = $wpdb->prepare("INSERT INTO mb_menu_categories (cat_rank, cat_name, cat_name_alias, cat_stat) VALUES(%d, %s, %s, %d)", $data);
                $q = $wpdb->query($sql);
            break;

            case 'update':
                $update_parts = explode('_', $_POST['btn_action']);
                $id = $update_parts[2];
                $query_success = true;

                $selected_cat = $wpdb->get_results("SELECT * FROM mb_menu_categories WHERE cat_id = '$id' LIMIT 1", ARRAY_A);
                $selected_cat_rank = $selected_cat[0]['cat_rank'];

                if ($selected_cat_rank !== $_POST['cat_rank']) {
                    $saved_cats = $wpdb->get_results("SELECT * FROM mb_menu_categories WHERE cat_id != '$id'", ARRAY_A);

                    if (!empty($saved_cats)) {
                        foreach ($saved_cats as $cat) {
                            $cat_id = $cat['cat_id'];
                            $update_cat_rank_data = ['cat_id' => $selected_cat_rank];

                            if ($cat['cat_rank'] === $_POST['cat_rank']) {
                                $update_query = $wpdb->prepare("UPDATE mb_menu_categories SET cat_rank = %d WHERE cat_id = '$cat_id' LIMIT 1", $update_cat_rank_data);
                                $update_cat_rank = $wpdb->query($update_query);
                                $query_success = !$update_cat_rank ? false : true;
                            }
                        }
                    }
                }

                if ($query_success) {
                    $sql = $wpdb->prepare("UPDATE mb_menu_categories SET cat_rank = %d, cat_name = %s, cat_name_alias = %s, cat_stat = %d WHERE cat_id = '$id' LIMIT 1", $data);
                    $q = $wpdb->query($sql);
                } else {
                    $q = false;
                }
            break;
        }
        
        if ($q != false) {
            wp_send_json_success(201);
        } else {
            wp_send_json_error(500);
        }
    } else {
        wp_send_json_error(403);
    }
}

add_action('wp_ajax_mb_menu_categories_save_update', 'mb_menu_categories_save_update');

/** Selecting one category (Update) */
function mb_menu_categories_get()
{
    global $wpdb;
    $id = $_POST['id'];

    $q = $wpdb->get_row("SELECT * FROM mb_menu_categories WHERE cat_id = '$id' LIMIT 1", ARRAY_A);
    
    if ($q != false) {
        wp_send_json_success($q);
    } else {
        wp_send_json_error(500);
    }
}

add_action('wp_ajax_mb_menu_categories_get', 'mb_menu_categories_get');

/** Deletion of category(ies) */
function mb_menu_categories_delete()
{
    global $wpdb;

    $id_string = '';

    foreach ($_POST['ids'] as $id) {
        $id_string .= $id . ', ';
    }

    $q_string = rtrim($id_string, ', ');

    $q = $wpdb->query("DELETE FROM mb_menu_categories WHERE cat_id IN($q_string)");
    $saved_cats = $wpdb->get_results("SELECT * FROM mb_menu_categories ORDER BY cat_rank ASC", ARRAY_A);

    if (!empty($saved_cats)) {
        $cats_rank_counter = 0;

        foreach ($saved_cats as $cat) {
            $id = $cat['cat_id'];
            $update_data = ['cat_rank' => ++$cats_rank_counter];
            
            $cat_rank_update_query = $wpdb->prepare("UPDATE mb_menu_categories SET cat_rank = %d WHERE cat_id = '$id' LIMIT 1", $update_data);
            $cat_rank_update = $wpdb->query($cat_rank_update_query);
        }
    }
    
    if ($q != false) {
        wp_send_json_success(200);
    } else {
        wp_send_json_error(500);
    }
}

add_action('wp_ajax_mb_menu_categories_delete', 'mb_menu_categories_delete');
