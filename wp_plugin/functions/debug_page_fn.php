<?php
/**
 * Debug page functions
 */

function mb_menu_debug_page()
{
    $response = Debug::report('debug'); // Displaying debug logs
    require_once(APP_ROOT . 'views/debug_page.php');
}
