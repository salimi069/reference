<?php
/**
 * Including base files and classes
 */
define('APP_ROOT', plugin_dir_path(__FILE__));
define('APP_URL', plugin_dir_url(__FILE__));

/**
 * Autoloader
 */
require_once(APP_ROOT . 'classes/Autoloader.php');

$classes = [
    'Debug'            
];

Autoloader::load($classes);
