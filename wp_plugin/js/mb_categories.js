jQuery.noConflict();
jQuery(document).ready(function($) {
    var admin_ajax = $("span#validationStatus").data("url");

    /** 
     * --------------------------
     * Categories Modal
     * -------------------------- 
     */

    // Show
    $("button.cat_btn").on("click", function() {
        var btn_id = $(this).prop("id");

        if (btn_id === "add_category") {
            $("div#modal_title").html("<h3>Új kategória létrehozása</h3>");
            var cat_num = $("option.cat").length;
            var added_cat_num = parseInt(cat_num) + 1;
            $("select[name='cat_rank']").append('<option class="added_cat" value="' + added_cat_num + '">' + added_cat_num + ' -</option>');
            $("div#categories_modal").css("display", "block");
        } else { // Selecting one category for update
            $("div#modal_title").html("<h3>Kategória módosítása</h3>");

            $.ajax({
                url: admin_ajax,
                type: "POST",
                data: { action: 'mb_menu_categories_get', id: btn_id },
                dataType: "html",
                success: function(response) {
                    var resp = JSON.parse(response);

                    if (resp.data !== 500) {
                        $("input[name='cat_name']").val(resp.data.cat_name);

                        if ($("option:last").hasClass("added_cat")) {
                            $("option:last").remove();
                        }

                        resp.data.cat_stat == 1 ? $("input[name='cat_stat']").prop("checked", true) : $("input[name='cat_stat']").prop("checked", false);

                        $("option.cat").each(function() {

                            if ($(this).val() === resp.data.cat_rank) {
                                $(this).prop("selected", true);
                            }
                        });

                        $("button.send").prop("id", "update_category_" + btn_id);
                        $("div#categories_modal").css("display", "block");
                    } else {
                        alert("A kategória adatainak lekérdezése nem sikerült.");
                        return false;
                    }
                }
            });
        }
    });

    // Hide
    $("span#categories_modal_close").on("click", function() {
        $("input[name='cat_name']").val("");
        $("span.alert").text("");
        $("button.send").prop("id", "save_category");
        $("div#categories_modal").css("display", "none");
    });

    /**
     * -------------------------
     * CRUD management of categories
     * -------------------------
     */
    /** Selecting all categories for deletion */
    $("input[name='cat_del_all']").on("change", function() {

        if ($(this).prop("checked")) {
            $("input[name='cat_del']").prop("checked", true);
        } else {
            $("input[name='cat_del']").prop("checked", false);
        }
    });

    $("button.send").on("click", function() {
        var button_id = $(this).prop("id");
        var update_button = button_id.indexOf("update_category") > -1 ? true : false;

        if (button_id === "save_category" || button_id === "del_category" || update_button) {

            /** Deletion of selected category(ies) */
            if (button_id === "del_category") {
                var del_cats = [];

                $("input[name='cat_del']").each(function() {

                    if ($(this).prop("checked")) {
                        del_cats.push($(this).prop("id"));
                    }
                });

                if (del_cats.length > 0) {
                    if (confirm("Biztosan törölni akarod a kijelölt kategóriá(ka)t?")) {

                        $.ajax({
                            url: admin_ajax,
                            type: "POST",
                            data: { action: "mb_menu_categories_delete", ids: del_cats },
                            dataType: "html",
                            success: function(response) {
                                var resp = JSON.parse(response);

                                if (resp.data === 200) {
                                    alert("Sikeres törlés.");
                                    location.reload();
                                } else {
                                    alert("Sikertelen törlés.");
                                    return false;
                                }
                            }
                        });
                    }
                } else {
                    alert("Törléshez kérlek, jelölj ki legalább egy kategóriát.");
                    return false;
                }
            } else {

                /** Create or update categories */
                setTimeout(function() {
                    if ($("span#validationStatus").text() === "") {
                        var formData = new FormData($("form")[0]);
                        formData.append("action", "mb_menu_categories_save_update");
                        formData.append("btn_action", button_id);

                        $.ajax({
                            url: admin_ajax,
                            type: "POST",
                            enctype: "multipart/formdata",
                            data: formData,
                            processData: false,
                            contentType: false,
                            cache: false,
                            success: function(response) {

                                switch (response.data) {

                                    case 500:
                                        alert("Sikertelen mentés. Próbáld meg újra!");
                                        return false;
                                        break;

                                    case 403:
                                        alert("Hozzáférés megtagadva.");
                                        return false;
                                        break;

                                    default:
                                        alert("Sikeres mentés.");
                                        location.reload();
                                        break;
                                }
                            }
                        });
                    }
                }, 1000);
            }
        }
    });

    /**
     * ---------------------------
     * DataTable
     * ---------------------------
     */
    $("table#cat_table").DataTable({
        language: {
            url: $("table#cat_table").data("url") + 'libs/datatables/localisation/HU_hu.json'
        },
        dom: 'Bfrtip',
        buttons: ['excel', 'csvHtml5']
    });
});