jQuery.noConflict();
jQuery(document).ready(function($) {
    var admin_ajax = $("span#validationStatus").data("url");

    /**
     * -----------------------
     * Management of Daily Offer
     * -----------------------
     */
    $("button.daily_offer_action").click(function() {
        var btn_id = $(this).prop("id");
        var btn_action, nonce, breakfast_menu_text, success_msg, fail_msg, text_msg, do_action;

        btn_action = btn_id.indexOf('save') > -1 ? 'save' : 'delete';
        nonce = $("input#mb_menu_daily_offer_nonce_field").val();
        daily_offer_text = tinymce.get('daily_offer_editor').getContent();
        do_action = true;

        switch (btn_action) {

            case "save":
                success_msg = "Sikeres mentés";
                fail_msg = "Sikertelen mentés";
                text_msg = "Az ajánlat mentéséhez kérlek, add meg annak tartalmát.";
                break;

            case "delete":
                success_msg = "Sikeres törlés";
                fail_msg = "Sikertelen törlés";
                text_msg = "Jelenleg nincs mentett Ajánlat az adatbázisban.";

                if (daily_offer_text !== "") {
                    if (confirm("Biztosan törölni akarod az Ajánlatot?")) {
                        do_action = true;
                    } else {
                        do_action = false;
                    }
                } else {
                    alert(text_msg);
                    return false;
                }
                break;
        }

        if (daily_offer_text !== "" && do_action) {

            $.ajax({
                url: admin_ajax,
                type: "POST",
                data: { action: "mb_menu_daily_offer_manage", data: daily_offer_text, nonce: nonce, btn_action: btn_action },
                dataType: "html",
                success: function(response) {
                    var resp = JSON.parse(response);

                    if (resp.success === true) {
                        alert(success_msg);
                        location.reload();
                    } else {
                        alert(fail_msg);
                        return false;
                    }
                }
            });
        } else {

            switch (true) {

                case daily_offer_text === "":
                    alert(text_msg);
                    return false;
                    break;

                default:
                    return false;
                    break;
            }
        }
    });
});