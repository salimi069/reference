jQuery.noConflict();
jQuery(document).ready(function($) {

    /** Deletion of debug & error logs */
    $("button.delete_log").on("click", function() {

        if (confirm("Biztosan törölni akarod a jelentéseket?")) {
            var admin_ajax = $(this).data("url");
            var type = $(this).data("type");
            var nonce = type === 'debug' ? $("input#mb_debug_delete_field").val() : $("input#mb_error_delete_field").val();

            var data = {
                action: 'mb_menu_delete_log',
                type: type,
                nonce: nonce
            };

            $.ajax({
                url: admin_ajax,
                type: "POST",
                data: data,
                dataType: "html",
                success: function(response) {
                    var resp = JSON.parse(response);

                    if (resp.success == true) {
                        alert("A jelentések sikeresen törlésre kerültek.");
                        location.reload();
                    } else {
                        alert("A jelentések törlése nem sikerült!");
                        return false;
                    }
                }
            });
        }
    });
});