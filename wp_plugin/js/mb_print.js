jQuery.noConflict();
jQuery(document).ready(function($) {
    var app_url = $("span#validationStatus").data("app_url");

    $("button.print").on("click", function() {
        var btn_id = $(this).prop("id");
        var selected_menus, request_string;

        if (btn_id === "print_selected_menu") {
            selected_menus = [];

            $("input[name='menu_check']").each(function() {

                if ($(this).prop("checked")) {
                    selected_menus.push($(this).prop("id"));
                }
            });

            if (Array.isArray(selected_menus) && selected_menus.length > 0) {
                request_string = 'id=' + selected_menus[0];

                if (selected_menus.length > 1) {

                    for (var sm = 1; sm < selected_menus.length; sm++) {
                        request_string += '&id' + sm + '=' + selected_menus[sm];
                    }
                }
            } else {
                alert("Nyomtatáshoz kérlek, legalább egy menüt válassz ki.");
                return false;
            }
        } else {
            request_string = "id=weekly";
        }

        window.open(app_url + "views/print_page.php?" + request_string, '_blank');
    });
});