jQuery.noConflict();
jQuery(document).ready(function($) {
    var admin_ajax = $("span#validationStatus").data("url");

    /** 
     * --------------------------
     * Menus Modal
     * -------------------------- 
     */

    // Show Modal
    $("button.menu_btn").on("click", function() {
        var btn_id_role = $(this).prop("id");
        var btn_id_role_array = btn_id_role.split("_");
        var btn_id = btn_id_role_array[0];
        var btn_role = btn_id_role_array[1];

        if (btn_role === "update") {
            $("button#save_menus").data("action", btn_id + "_update");
        } else {
            $("div#menu_template_placeholder").remove();
        }

        var modal_title, meal_type, meal_counter;

        switch (btn_role) {
            case "add":
                modal_title = '<div class="w3-col l10 m10 s10 w3-panel w3-blue w3-padding w3-xlarge w3-card-4 w3-round">Új menü létrehozása</div>';
                break;

            case "update":
                modal_title = '<div class="w3-col l10 m10 s10 w3-panel w3-blue w3-padding w3-xlarge w3-card-4 w3-round">Menü módosítása</div>';
                break;
        }

        // Get modal data
        $.ajax({
            url: admin_ajax,
            type: "POST",
            data: { action: "mb_menu_get_modal_data", btn_id: btn_id, btn_role: btn_role },
            dataType: "html",
            success: function(response) {
                var modal_data = JSON.parse(response);

                if (modal_data.success == true) {

                    var menu_categories;
                    var menu_foods = modal_data.data.foods;

                    // Role: Update menu
                    var saved_menu_id, saved_menu_date, saved_menu_stat, saved_menu_daily_offer, saved_menu_template_name, saved_menu_template_alias, saved_menu_foods;

                    if (modal_data.data.hasOwnProperty("saved_menu")) {
                        saved_menu_id = modal_data.data.saved_menu.m_id;
                        saved_menu_date = modal_data.data.saved_menu.m_date;
                        saved_menu_stat = parseInt(modal_data.data.saved_menu.m_stat);
                        saved_menu_daily_offer = parseInt(modal_data.data.saved_menu.m_daily_offer);
                        saved_menu_template_name = modal_data.data.saved_menu.m_template_name;
                        saved_menu_template_alias = modal_data.data.saved_menu.m_template_alias;
                        saved_menu_foods = JSON.parse(modal_data.data.saved_menu.m_menu);

                        if (saved_menu_template_name !== "" && saved_menu_template_name != null) {
                            $("span#menu_template").text(saved_menu_template_name);
                        } else {
                            $("div#menu_template_placeholder").remove();
                        }
                    }

                    // Setting minimum date in datepicker
                    if (saved_menu_date === undefined) {
                        // Role: Save new menu
                        var current_date = new Date();
                        var min_date_month = String((current_date.getMonth() + 1)).length < 2 ? "0" + (current_date.getMonth() + 1) : (current_date.getMonth() + 1);
                        var min_date_day = String(current_date.getDate()).length < 2 ? "0" + current_date.getDate() : current_date.getDate();
                        var date_string = current_date.getFullYear() + "-" + min_date_month + "-" + min_date_day;
                        $("input[name='m_date']").prop("min", date_string);
                    } else {
                        // Role: Update menu
                        $("input[name='m_date']").val(saved_menu_date);
                    }

                    // Categories
                    if (modal_data.data.hasOwnProperty("categories")) {
                        menu_categories = modal_data.data.categories;

                        for (var c = 0; c < menu_categories.length; c++) {
                            var cat_container = '<div class="form_group menu_block" id="' + menu_categories[c].cat_name_alias + '" data-id="' + menu_categories[c].cat_id + '">';
                            cat_container += '<p class="meal_title">' + menu_categories[c].cat_name + '</p>';
                            cat_container += '<div id="' + menu_categories[c].cat_name_alias + '_container"></div>';
                            cat_container += '<button type="button" class="w3-btn w3-round w3-orange add_meal" name="' + menu_categories[c].cat_name_alias + '"><small>' + menu_categories[c].cat_name + ' kiválasztása</small></button>';
                            cat_container += '</div>';

                            $("div#m_categories").append(cat_container);

                            //Daily menu
                            $("div#daily_menu_container").append('<div class="daily_menu_item" id="daily_' + menu_categories[c].cat_name_alias + '">' +
                                '<p><strong>' + menu_categories[c].cat_name + '</strong></p>' +
                                '<select class="w3-input w3-border" name="daily_' + menu_categories[c].cat_name_alias + '"><option value="select">Kérlek, válassz</option></select>' +
                                '</div>');

                            $("div#daily_" + menu_categories[c].cat_name_alias).hide();
                        }
                    } else {
                        $("div#m_categories").append('<div class="w3-panel w3-padding w3-red w3-large">Jelenleg nincs mentett kategória.</div>');
                    }

                    // Adding meals to the menu categories
                    if (saved_menu_foods !== undefined) { // Role: Update menu (loading saved foods & prices and daily menu)
                        var menu_cats = Object.keys(saved_menu_foods);

                        for (var mc = 0; mc < menu_cats.length; mc++) {

                            if (menu_cats[mc] !== "daily_menu") {
                                meal_type = menu_cats[mc];
                                meal_counter = 1;
                                var saved_foods_names = Object.keys(saved_menu_foods[menu_cats[mc]]);

                                for (var sfn = 0; sfn < saved_foods_names.length; sfn++) {

                                    if (saved_foods_names[sfn] !== "cat_name") {
                                        var menu_ctg_id;

                                        for (var ctg = 0; ctg < menu_categories.length; ctg++) {

                                            if (meal_type === menu_categories[ctg].cat_name_alias) {
                                                menu_ctg_id = menu_categories[ctg].cat_id;
                                            }
                                        }

                                        var food_select = '<div class="w3-row form_group ' + meal_type + ' food_select_placeholder">';
                                        food_select += '<div class="w3-third w3-padding" id="' + meal_type + '_' + meal_counter + '_select"><select class="w3-input w3-border ' + meal_type + '" name="' + meal_type + '_' + meal_counter + '_select"><option value="select">Kérlek, válassz</option>';

                                        for (var fl = 0; fl < menu_foods.length; fl++) {
                                            if (menu_foods[fl].f_cat === menu_ctg_id) {
                                                var selected = menu_foods[fl].f_name === saved_foods_names[sfn] ? " selected" : '';
                                                food_select += '<option value="' + menu_foods[fl].f_id + '"' + selected + '>' + menu_foods[fl].f_name + '</option>';
                                            }
                                        }

                                        food_select += '</select></div>';
                                        food_select += '<div class="w3-third w3-padding food_price_placeholder"><input type="text" class="w3-input w3-border ' + meal_type + '" name="' + meal_type + '_' + meal_counter + '_price" placeholder="Ára" /></div>';
                                        food_select += '<div class="w3-third w3-padding"><button type="button" class="w3-btn w3-round w3-red delete_meal" name="' + meal_type + '_' + meal_counter + '_delete"><small>Törlés</small></button></div>';
                                        food_select += '</div>';

                                        $("div#" + meal_type + "_container").append(food_select);

                                        var sel_food_id = $("input[name='" + meal_type + "_" + meal_counter + "_price']").closest(".w3-row").find("select." + meal_type).children("option:selected").val();

                                        for (var fp = 0; fp < menu_foods.length; fp++) {
                                            if (menu_foods[fp].f_id === sel_food_id) {
                                                $("input[name='" + meal_type + "_" + meal_counter + "_price']").val(menu_foods[fp].f_price);
                                            }
                                        }

                                        meal_counter++;
                                    }
                                }
                            } else { // Adding Daily Menu (Role: Update menu)
                                var menu_cat_alias = [];
                                var daily_menu_pool = {};

                                $("div#m_categories").children("div.menu_block").each(function() {
                                    menu_cat_alias.push($(this).prop("id"));
                                });

                                for (var mca = 0; mca < menu_cat_alias.length; mca++) {
                                    daily_menu_pool[menu_cat_alias[mca]] = {};

                                    $("select." + menu_cat_alias[mca]).each(function() {

                                        if ($(this).children("option:selected").val() !== 'select') {
                                            var sel_food_id = $(this).children("option:selected").val();
                                            var sel_food_name = $(this).children("option:selected").text();
                                            daily_menu_pool[menu_cat_alias[mca]][sel_food_id] = sel_food_name;
                                        }
                                    });
                                }

                                var daily_menu_keys = Object.keys(daily_menu_pool);

                                if (daily_menu_keys.length > 0) {
                                    $("div#daily_menu_alert").css("display", "none");
                                    $("div#daily_cost").css("display", "block");
                                    $("button#save_menus").prop("disabled", false);

                                    for (var dmk = 0; dmk < daily_menu_keys.length; dmk++) {
                                        var daily_menu_food_id = Object.keys(daily_menu_pool[daily_menu_keys[dmk]]);

                                        if (daily_menu_food_id.length > 0) {

                                            for (var dmfi = 0; dmfi < daily_menu_food_id.length; dmfi++) {
                                                var daily_menu_selected = saved_menu_foods[menu_cats[mc]][daily_menu_keys[dmk]] === daily_menu_pool[daily_menu_keys[dmk]][daily_menu_food_id[dmfi]] ? " selected" : "";

                                                if (!$("select[name='daily_" + daily_menu_keys[dmk] + "']").children("option[value='" + daily_menu_food_id[dmfi] + "']").length) {
                                                    $("select[name='daily_" + daily_menu_keys[dmk] + "']").append('<option value="' + daily_menu_food_id[dmfi] + '"' + daily_menu_selected + '>' + daily_menu_pool[daily_menu_keys[dmk]][daily_menu_food_id[dmfi]] + '</option>');
                                                }
                                            }

                                            $("div#daily_" + daily_menu_keys[dmk]).show();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Adding/removing food input fields to menu categories and daily menu
                    $(document).on("click", "button.add_meal", function(e) {
                        e.preventDefault();
                        var meal_counter, meal_type, added_meals_number, cat_id;

                        meal_type = $(this).parent("div").prop("id");
                        cat_id = $(this).parent("div").data("id");
                        added_meals_number = $("div." + meal_type).length;
                        meal_counter = added_meals_number == 0 ? 1 : added_meals_number + +1;

                        if (cat_id !== "") {
                            var food_select = '<div class="w3-row form_group ' + meal_type + ' food_select_placeholder">';
                            food_select += '<div class="w3-third w3-padding" id="' + meal_type + '_' + meal_counter + '_select"><select class="w3-input w3-border ' + meal_type + '" name="' + meal_type + '_' + meal_counter + '_select"><option value="select">Kérlek, válassz</option></select></div>';
                            food_select += '<div class="w3-third w3-padding food_price_placeholder"><input type="text" class="w3-input w3-border ' + meal_type + '" name="' + meal_type + '_' + meal_counter + '_price" placeholder="Ára" /></div>';
                            food_select += '<div class="w3-third w3-padding"><button type="button" class="w3-btn w3-round w3-red delete_meal" name="' + meal_type + '_' + meal_counter + '_delete"><small>Törlés</small></button></div>';
                            food_select += '</div>';

                            $("div#" + meal_type + "_container").append(food_select);

                            for (var i = 0; i < menu_foods.length; i++) {

                                if (cat_id === parseInt(menu_foods[i].f_cat)) {
                                    $("select[name='" + meal_type + '_' + meal_counter + "_select']").append('<option value="' + menu_foods[i].f_id + '">' + menu_foods[i].f_name + '</option>');
                                }
                            }

                            $("select[name='" + meal_type + '_' + meal_counter + "_select']").on("change", function() {
                                var food_id = $(this).val();

                                for (var p = 0; p < menu_foods.length; p++) {

                                    if (menu_foods[p].f_id === food_id) {
                                        var food_price = menu_foods[p].f_price !== "" && menu_foods[p].f_price != null ? menu_foods[p].f_price : 'N/A';
                                        $("input[name='" + meal_type + '_' + meal_counter + "_price']").val(food_price);
                                    }
                                }
                            });
                        }

                        // Composing daily menu                               
                        $(document).on("change", "select." + meal_type, function() {
                            var selected_food_id = $(this).val();
                            var selected_food_name = $(this).children("option:selected").text();

                            $("div#daily_menu_alert").css("display", "none");
                            $("div#daily_" + meal_type).show();
                            $("div#daily_cost").css("display", "block");
                            $("button#save_menus").prop("disabled", false);

                            if (!$("select[name='daily_" + meal_type + "']").children("option[value='" + selected_food_id + "']").length) {
                                $("select[name='daily_" + meal_type + "']").append('<option value="' + selected_food_id + '">' + selected_food_name + '</option>');
                            }
                        });
                    });

                    // Removing meal from the menu
                    $(document).on("click", "button.delete_meal", function(e) {
                        e.preventDefault();

                        var ext_counter = $(this).prop("name").split("_");
                        meal_type = ext_counter[0];
                        meal_counter = ext_counter[1];
                        var meal_id = $(this).closest(".w3-row").find("select").val();

                        $("div." + meal_type).each(function() {
                            $(this).find("select, input, button").each(function() {
                                var split_name = $(this).prop("name").split("_");
                                var count = split_name[1];

                                if (count > meal_counter) {
                                    var new_count = count - 1;
                                    $(this).prop({ "name": meal_type + '_' + new_count + '_delete' });
                                }
                            });
                        });

                        // Removing meal from daily menu select field
                        if ($(this).closest(".w3-row").find("select." + meal_type).val() !== "select") {
                            $("select[name='daily_" + meal_type + "']").children("option[value='" + meal_id + "']").remove();
                        }

                        if ($("select[name='daily_" + meal_type + "']").children("option").length == 1) {
                            $("div#daily_" + meal_type).hide();
                        }

                        // Removing meal from the menu
                        $(this).closest(".w3-row").remove();

                        // Displaying daily menu alert
                        if (!$("div.food_select_placeholder").length) {
                            $("div#daily_cost").css("display", "none");
                            $("div#daily_menu_alert").css("display", "block");
                            $("button#save_menus").prop("disabled", true);
                        }
                    });

                    // Status (Role: Update menu)
                    saved_menu_stat === 1 ? $("input[name='m_stat']").prop("checked", true) : $("input[name='m_stat']").prop("checked", false);

                    // Daily Offer                    
                    !modal_data.data.hasOwnProperty("daily_offer") ? $("input[name='m_daily_offer']").prop("disabled", true) : $("input[name='m_daily_offer']").prop("disabled", false);

                    // Role: Update menu
                    saved_menu_daily_offer === 1 ? $("input[name='m_daily_offer']").prop("checked", true) : $("input[name='m_daily_offer']").prop("checked", false);

                    // Template (Role: Update menu)
                    saved_menu_template_name !== "" ? $("input[name='m_template_name']").val(saved_menu_template_name) : $("input[name='m_template_name']").val("");
                }
            }
        });

        $("div#modal_title").html(modal_title);
        $("button#save_menus").prop("disabled", true);
        $("div#menus_modal").css("display", "block");
    });

    // Hide Modal
    $("span#menus_modal_close").on("click", function() {
        location.reload();
    });

    /**
     * --------------------------------
     * Save new menu
     * --------------------------------
     */
    $("button#save_menus").on("click", function() {
        var action = $(this).data("action");
        var menu_form_data = {};

        menu_form_data.nonce = $("input#mb_menu_menus_save_nonce_field").val(); // Nonce
        menu_form_data.m_date = $("input[name='m_date']") !== "" ? $("input[name='m_date']").val() : ''; // Menu date

        // Getting menu id (Role: update)
        if (action !== "save") {
            var update_data = action.split("_");
            menu_form_data.update_id = update_data[0];
        }

        // Selected foods and prices
        menu_form_data.selected_foods = {};
        var menu_cat_name;

        $("div#m_categories").children("div.menu_block").each(function() {
            menu_cat_name = $(this).prop("id");
            menu_form_data.selected_foods[menu_cat_name] = {};

            $("select." + menu_cat_name).each(function() {
                var selected_food = $(this).val();

                if (selected_food !== "select") {
                    menu_form_data.selected_foods[menu_cat_name][selected_food] = $(this).closest("div.food_select_placeholder").find("div.food_price_placeholder").children("input." + menu_cat_name).val();
                }
            });
        });

        // Daily menu
        menu_form_data.daily_menu = {};

        $("div.daily_menu_item").each(function() {
            var id = $(this).prop("id");
            var id_split = id.split("_");
            var daily_cat_name = id_split[1];

            if (daily_cat_name !== "cost") {

                if ($("select[name='daily_" + daily_cat_name).val() !== "select") {
                    menu_form_data.daily_menu[daily_cat_name] = $("select[name='daily_" + daily_cat_name).val();
                }
            } else {
                menu_form_data.daily_menu["cost"] = $("input[name='daily_menu_cost'").val();
            }
        });

        // Status
        menu_form_data.m_stat = $("input[name='m_stat']").prop("checked") ? 1 : 2;

        // Daily Offer
        menu_form_data.m_daily_offer = $("input[name='m_daily_offer']").prop("checked") ? 1 : 2;

        // Template
        menu_form_data.m_template_name = $("input[name='m_template_name']").val();

        var menu_form_data_json = JSON.stringify(menu_form_data);

        if ($("input[name='m_date']").val() === "" && $("input[name='m_template_name']").val() === "") {
            alert("Dátum nélküli menüt csak sablonként lehet menteni. Kérlek, adj meg egy sablon nevet vagy egy dátumot.");
            return false;
        }

        if (menu_form_data !== "") {

            $.ajax({
                url: admin_ajax,
                type: "POST",
                data: { action: 'mb_menu_save_update_menu', data: menu_form_data_json },
                dataType: "html",
                success: function(response) {
                    var resp = JSON.parse(response);

                    if (resp.success == true) {
                        alert("Sikeres mentés.");
                        location.reload();
                    } else {
                        switch (resp.data) {

                            case 500:
                                alert("Sikertelen mentés.");
                                return false;
                                break;

                            case "date":
                                alert("Ezen a napon már van mentett menü.");
                                return false;
                                break;

                            case "template":
                                alert("Ezen a néven már van sablon elmentve.");
                                return false;
                                break;

                            case 403:
                                alert("Hozzáférés megtagadva.");
                                return false;
                                break;

                            case 400:
                                alert("A keresett oldal nem található.")
                                return false;
                                break;
                        }
                    }
                }
            });
        } else {
            alert("A menü mentéséhez legalább egy étel kiválasztása szükséges.");
            return false;
        }
    });

    /**
     * ---------------------------
     * Management of menus (table)
     * ---------------------------
     */
    $("button.send").click(function() {
        var btn_id = $(this).prop("id");

        if (btn_id === "activate_menu" || btn_id === "deactivate_menu" || btn_id === "delete_menu") {
            var menu_id = [];
            var active_menus = [];
            var template_menus = [];
            var send_ajax = false;
            var menu_action;

            $("input[name='menu_check']").each(function() {

                if ($(this).prop("checked")) {
                    menu_id.push($(this).prop("id"));

                    if ($(this).data("stat") === "Aktív") {
                        active_menus.push($(this).prop("id"));
                    }

                    if ($(this).data("template") !== "N/A") {
                        template_menus.push($(this).prop("id"));
                    }
                }
            });

            if (menu_id.length > 0) {

                switch (btn_id) {

                    case "activate_menu":
                        menu_action = "activate";
                        send_ajax = true;
                        break;

                    case "deactivate_menu":
                        if (menu_id.length > template_menus.length) {
                            if (confirm("Törlöd a deaktivált NEM SABLON menüket?")) {
                                menu_action = "deactivate_delete";
                            } else {
                                menu_action = "deactivate";
                            }
                        } else {
                            menu_action = "deactivate";
                        }
                        send_ajax = true;
                        break;

                    case "delete_menu":
                        if (active_menus.length == 0 && template_menus.length == 0) {

                            if (confirm("Biztosan törlöd a kijelölt menüket?")) {
                                menu_action = "delete";
                                send_ajax = true;
                            }
                        } else if (active_menus.length > 0) {
                            alert("Aktív menük nem törölhetők. Törlés előtt kérlek, deaktiváld őket.")
                            send_ajax = false;
                            return false;
                        } else if (template_menus.length > 0) {
                            if (menu_id.length > template_menus.length) {
                                if (confirm("A kijelölt menük között SABLON(OK) is van(nak). Biztosan folytatod?")) {
                                    menu_action = "delete";
                                    send_ajax = true;
                                }
                            } else if (menu_id.length === template_menus.length) {
                                if (confirm("A kijelölt menü(k) sablon(ok). Biztosan folytatod?")) {
                                    menu_action = "delete";
                                    send_ajax = true;
                                }
                            }
                        }
                        break;
                }

                if (send_ajax) {
                    var nonce = $("input#mb_menu_manage_menus_nonce_field").val();

                    $.ajax({
                        url: admin_ajax,
                        type: "POST",
                        data: { action: "mb_menu_manage_menu", btn_action: menu_action, nonce: nonce, id: menu_id },
                        dataType: "html",
                        success: function(response) {
                            var resp = JSON.parse(response);

                            if (resp.success) {

                                if (resp.data !== 200) {
                                    alert(resp.data)
                                    location.reload();
                                } else {
                                    alert("Sikeres törlés.");
                                    location.reload();
                                }
                            } else {
                                switch (resp.data) {

                                    case 500:
                                        alert("Sikertelen művelet.");
                                        return false;
                                        break;

                                    case 403:
                                        alert("Hozzáférés megtagadva.");
                                        return false;
                                        break;

                                    default:
                                        alert(resp.data);
                                        return false;
                                        break;
                                }
                            }
                        }
                    });
                }
            } else {
                alert("Legalább egy menüt jelölj ki a művelet elvégzéshez.");
                return false;
            }
        }
    });

    /**
     * ---------------------------
     * DataTable
     * ---------------------------
     */
    $("table#menu_table").DataTable({
        language: {
            url: $("table#menu_table").data("url") + 'libs/datatables/localisation/HU_hu.json'
        },
        dom: 'Bfrtip',
        buttons: ['excel', 'csvHtml5']
    });
});