jQuery.noConflict();
jQuery(document).ready(function($) {
    var admin_ajax = $("span#validationStatus").data("url");

    /** 
     * --------------------------
     * Foods & Drinks Modal
     * -------------------------- 
     */

    // Show
    $("button.food_btn").on("click", function() {
        var btn_id = $(this).prop("id");

        if (btn_id === "add_food") {
            $("div#modal_title").html("<h3>Új Étel vagy Ital mentése</h3>");
            $("div#foods_modal").css("display", "block");
        } else { // Selecting one food or drink for update
            $("div#modal_title").html("<h3>Étel vagy Ital módosítása</h3>");

            $.ajax({
                url: admin_ajax,
                type: "POST",
                data: { action: 'mb_menu_foods_get', id: btn_id },
                dataType: "html",
                success: function(response) {
                    var resp = JSON.parse(response);

                    if (resp.data !== 500) {
                        $("input[name='f_name']").val(resp.data.f_name);

                        $("option.f_cat_opt").each(function() {

                            if ($(this).prop("value") === resp.data.cat_id) {
                                $(this).prop("selected", true);
                            }
                        });

                        $("button.send").prop("id", "update_food_" + btn_id);
                        $("div#foods_modal").css("display", "block");
                    } else {
                        alert("A választott étel vagy ital adatainak lekérdezése nem sikerült.");
                        return false;
                    }
                }
            });
        }
    });

    // Hide
    $("span#foods_modal_close").on("click", function() {
        $("input[name='f_name']").val("");
        $("select[name='f_cat']").val("select");
        $("div#foods_modal").css("display", "none");
    });

    /**
     * --------------------------------------
     * CRUD management of foods and drinks
     * --------------------------------------
     */

    /** Selecting all foods for deletion */
    $("input[name='food_del_all']").on("change", function() {

        if ($(this).prop("checked")) {
            $("input[name='food_del']").prop("checked", true);
        } else {
            $("input[name='food_del']").prop("checked", false);
        }
    });

    $("button.send").on("click", function() {
        var button_id = $(this).prop("id");
        var update_button = button_id.indexOf("update_food") > -1 ? true : false;

        if (button_id === "save_food" || button_id === "del_food" || update_button) {

            /** Deletion of foods/drinks */
            if (button_id === "del_food") {
                var del_foods = [];

                $("input[name='food_del']").each(function() {

                    if ($(this).prop("checked")) {
                        del_foods.push($(this).prop("id"));
                    }
                });

                if (del_foods.length > 0) {
                    if (confirm("Biztosan törölni akarod a kijelölt ételeket/italokat?")) {

                        $.ajax({
                            url: admin_ajax,
                            type: "POST",
                            data: { action: "mb_menu_foods_delete", ids: del_foods },
                            dataType: "html",
                            success: function(response) {
                                var resp = JSON.parse(response);

                                if (resp.data === 200) {
                                    alert("Sikeres törlés.");
                                    location.reload();
                                } else {
                                    alert("Sikertelen törlés.");
                                    return false;
                                }
                            }
                        });
                    }
                } else {
                    alert("Törléshez kérlek, jelölj ki legalább egy kategóriát.");
                    return false;
                }
            } else {

                setTimeout(function() {
                    if ($("span#validationStatus").text() === "") {
                        var formData = new FormData($("form")[0]);
                        formData.append("btn_action", button_id);
                        formData.append("action", "mb_menu_food_save_update");

                        $.ajax({
                            url: admin_ajax,
                            type: "POST",
                            enctype: "multipart/formdata",
                            data: formData,
                            processData: false,
                            contentType: false,
                            cache: false,
                            success: function(response) {

                                switch (response.data) {

                                    case 500:
                                        alert("Sikertelen mentés. Próbáld meg újra!");
                                        return false;
                                        break;

                                    case 403:
                                        alert("Hozzáférés megtagadva.");
                                        return false;
                                        break;

                                    case 400:
                                        alert("Ilyen néven már szerepel étel/ital az adatbázisban.");
                                        return false;
                                        break;

                                    default:
                                        alert("Sikeres mentés.");
                                        location.reload();
                                        break;
                                }
                            }
                        });
                    }
                }, 1000);
            }
        }
    });

    /**
     * ---------------------------
     * DataTable
     * ---------------------------
     */
    $("table#food_table").DataTable({
        language: {
            url: $("table#food_table").data("url") + 'libs/datatables/localisation/HU_hu.json'
        },
        dom: 'Bfrtip',
        buttons: ['excel', 'csvHtml5']
    });
});