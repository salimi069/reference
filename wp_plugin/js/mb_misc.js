jQuery.noConflict();
jQuery(document).ready(function($) {

    /** 
     * -----------------------------
     * Loader
     * -----------------------------
     */
    setTimeout(function() {
        $("div.loader").css("display", "none");
        $("div.mb_content_placeholder").css("display", "block");
    }, 1500);
});